package vtijuq_szakdoli.util.cdiconfig.options;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValueProducer;
import vtijuq_szakdoli.util.cdiconfig.PropertyResolver;

public class OptionProducer extends ConfigurationValueProducer {

    private final OptionResolver resolver;

    @Inject
    public OptionProducer(OptionResolver resolver) {
        this.resolver = resolver;
    }

    protected PropertyResolver getResolver(){
        return resolver;
    }

    @Produces
    @Option @ConfigurationValue
    public String getStringConfigValue(InjectionPoint ip) {
        return super.getStringConfigValue(ip);
    }

    @Produces
    @Option @ConfigurationValue
    public Double getDoubleConfigValue(InjectionPoint ip) {
        return super.getDoubleConfigValue(ip);
    }

    @Produces
    @Option @ConfigurationValue
    public Integer getIntegerConfigValue(InjectionPoint ip) {
        return super.getIntegerConfigValue(ip);
    }

    @Produces
    @Option @ConfigurationValue
    public Boolean getBooleanConfigValue(InjectionPoint ip) {
        return super.getBooleanConfigValue(ip);
    }
}
