package vtijuq_szakdoli.util.cdiconfig.options;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;

import vtijuq_szakdoli.util.cdiconfig.PropertyResolver;

@Startup
@ApplicationScoped
public class OptionResolver extends PropertyResolver {

	@PostConstruct
	public void init(){
		super.init("vtijuq_szakdoli_options.properties");
	}

}
