package vtijuq_szakdoli.util.cdiconfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Reads all valid property files within the classpath and prepare them to be fetched.
 * </p>
 * 
 * <p>
 * This class <strong>can</strong> be accessed concurrently by multiple clients. The inner representation of
 * properties <strong>should not</strong> be leaked out; if this is absolutely required, use unmodifiable
 * collection.
 * </p>
 * 
 * <p>
 * This resolver <strong>doesn't pay attention</strong> to multiple properties defined with the same name in
 * different files. It's impossible to determine which one will take precedence, so the responsibility for
 * name-clash is a deployer concern.
 * </p>
 * 
 * @author Piotr Nowicki
 * 
 */
public abstract class PropertyResolver {

    private static final Logger LOG = LoggerFactory.getLogger(PropertyResolver.class);
    private static final String JBOSS_CONFIG_DIR = System.getProperty("jboss.server.config.dir");

    private Properties properties = new Properties();

    /**
     * Initializes the properties by reading and uniforming them.
     * 
     * This method is called by the container only. It's not supposed to be invoked by the client directly.
     * 
     * @throws IOException
     *             in case of any property file access problem
     * @throws URISyntaxException
     */
    protected void init(String fileName) {
        final String path = JBOSS_CONFIG_DIR + File.separator + fileName;
        try(FileInputStream fis = new FileInputStream(path)) {
            properties.load(fis);
        } catch (IOException e) {
            LOG.error("Nem sikerült betölteni az options file-t.");
        }
    }

    /**
     * Returns property held under specified <code>key</code>. If the value is supposed to be of any other
     * type than {@link String}, it's up to the client to do appropriate casting.
     * 
     * @param key
     * @return value for specified <code>key</code> or null if not defined.
     */
    public String getValue(String key) {
        //üres kérdésre üres válasz
        if (StringUtils.isBlank(key)) return StringUtils.EMPTY;

        Object value = properties.get(key);

        return (value != null) ? String.valueOf(value) : null;
    }
}
