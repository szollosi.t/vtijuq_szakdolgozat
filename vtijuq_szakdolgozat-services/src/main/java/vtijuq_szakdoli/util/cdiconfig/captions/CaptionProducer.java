package vtijuq_szakdoli.util.cdiconfig.captions;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValueProducer;
import vtijuq_szakdoli.util.cdiconfig.PropertyResolver;

public class CaptionProducer extends ConfigurationValueProducer {

    private final CaptionResolver resolver;

    @Inject
    public CaptionProducer(CaptionResolver resolver) {
        this.resolver = resolver;
    }

    protected PropertyResolver getResolver(){
        return resolver;
    }

    @Produces
    @Caption @ConfigurationValue
    public String getStringConfigValue(InjectionPoint ip) {
        return super.getStringConfigValue(ip);
    }

    @Produces
    @Caption @ConfigurationValue
    public Double getDoubleConfigValue(InjectionPoint ip) {
        return super.getDoubleConfigValue(ip);
    }

    @Produces
    @Caption @ConfigurationValue
    public Integer getIntegerConfigValue(InjectionPoint ip) {
        return super.getIntegerConfigValue(ip);
    }

    @Produces
    @Caption @ConfigurationValue
    public Boolean getBooleanConfigValue(InjectionPoint ip) {
        return super.getBooleanConfigValue(ip);
    }
}
