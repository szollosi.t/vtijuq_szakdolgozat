package vtijuq_szakdoli.util.cdiconfig.messages;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;

import vtijuq_szakdoli.util.cdiconfig.PropertyResolver;

@Startup
@ApplicationScoped
public class MessageResolver extends PropertyResolver {

	@PostConstruct
	public void init(){
		super.init("vtijuq_szakdoli_messages.properties");
	}

	@Override
	public String getValue(String key) {
		String value = super.getValue(key);
		return value == null ? key : value;
	}
}
