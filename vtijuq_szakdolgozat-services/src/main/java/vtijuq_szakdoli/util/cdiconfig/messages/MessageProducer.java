package vtijuq_szakdoli.util.cdiconfig.messages;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValueProducer;
import vtijuq_szakdoli.util.cdiconfig.PropertyResolver;

public class MessageProducer extends ConfigurationValueProducer {

    private final MessageResolver resolver;

    @Inject
    public MessageProducer(MessageResolver resolver) {
        this.resolver = resolver;
    }

    protected PropertyResolver getResolver(){
        return resolver;
    }

    @Produces
    @Message @ConfigurationValue
    public String getStringConfigValue(InjectionPoint ip) {
        return super.getStringConfigValue(ip);
    }

    @Produces
    @Message @ConfigurationValue
    public Double getDoubleConfigValue(InjectionPoint ip) {
        return super.getDoubleConfigValue(ip);
    }

    @Produces
    @Message @ConfigurationValue
    public Integer getIntegerConfigValue(InjectionPoint ip) {
        return super.getIntegerConfigValue(ip);
    }

    @Produces
    @Message @ConfigurationValue
    public Boolean getBooleanConfigValue(InjectionPoint ip) {
        return super.getBooleanConfigValue(ip);
    }
}
