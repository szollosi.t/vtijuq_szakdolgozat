package vtijuq_szakdoli.login;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Munkatars;

import java.util.UUID;

public class LoggedInUser {
	@Getter @Setter
	private String username;
	@Getter @Setter
	private UUID token;

	public LoggedInUser(Munkatars munkatars) {
		username = munkatars.getFelhasznalonev();
		token = UUID.randomUUID();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(username).toHashCode();
	}

	@Override
	public boolean equals(Object that) {
		return (that instanceof LoggedInUser) && StringUtils.equals(username, ((LoggedInUser) that).getUsername());
	}
}
