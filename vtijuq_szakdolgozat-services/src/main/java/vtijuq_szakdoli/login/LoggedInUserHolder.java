package vtijuq_szakdoli.login;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.BooleanUtils.isTrue;

public class LoggedInUserHolder {

	@Getter
	private LoggedInUser user;
	private Map<String, Boolean> viewInvalidMap = new HashMap<>();

	public LoggedInUserHolder(LoggedInUser user) {
		this.user = user;
	}

	public boolean isViewInvalid(String viewType) {
		return isTrue(viewInvalidMap.get(viewType));
	}

	public void setViewInvalid(String viewType, boolean modelValid) {
		viewInvalidMap.put(viewType, modelValid);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (!(o instanceof LoggedInUserHolder)) {
			return false;
		}

		LoggedInUserHolder that = (LoggedInUserHolder) o;

		return new EqualsBuilder()
				.append(user, that.user)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(user)
				.toHashCode();
	}
}
