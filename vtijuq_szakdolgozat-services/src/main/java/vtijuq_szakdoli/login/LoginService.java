package vtijuq_szakdoli.login;

import static java.lang.Byte.parseByte;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.common.util.login.PasswordEncryptionService;
import vtijuq_szakdoli.dao.entitas.adminisztracio.MunkatarsDao;
import vtijuq_szakdoli.dao.entitas.adminisztracio.SzerepkorDao;
import vtijuq_szakdoli.dao.entitas.adminisztracio.SzervezetDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Munkatars;

@Startup
@ApplicationScoped
public class LoginService {

	private static final Logger LOG = LoggerFactory.getLogger(LoginService.class);

	@Inject
	private MunkatarsDao munkatarsDao;

	@Inject
	private SzerepkorDao szerepkorDao;

	@Inject
	private SzervezetDao szervezetDao;

	private final Map<LoggedInUser, LoggedInUserHolder> loggedInUserHolders = new HashMap<>();

	@PostConstruct
	public void init(){
		munkatarsDao.saltUnsalted();
	}

	public LoggedInUser login(String username, String password) throws LoginException {
		Munkatars munkatars = checkPassword(username, password);
		if(munkatars != null){
			LoggedInUser loggedInUser = new LoggedInUser(munkatars);
			LoggedInUserHolder loggedInUserHolder = new LoggedInUserHolder(loggedInUser);
			if(!loggedInUserHolders.containsKey(loggedInUser)){
				loggedInUserHolders.put(loggedInUser, loggedInUserHolder);
				return loggedInUserHolder.getUser();
			}
		}
		return null;
	}

	public void logout(LoggedInUser loggedInUser) {
		if(loggedIn(loggedInUser)){
			loggedInUserHolders.remove(loggedInUser);
		}
	}

	public boolean isEditable(LoggedInUser loggedInUser, String funkcio) {
		return loggedIn(loggedInUser) && szerepkorDao.isEditable(loggedInUser.getUsername(), funkcio);
	}

	public boolean isAccessible(LoggedInUser loggedInUser, String funkcio) {
		return loggedIn(loggedInUser) && szerepkorDao.isAccessible(loggedInUser.getUsername(), funkcio);
	}

	public boolean illetekes(LoggedInUser loggedInUser, Szervezet szervezet){
		return loggedIn(loggedInUser) && szervezetDao.isIlletekes(loggedInUser.getUsername(), szervezet.getId());
	}

	private boolean loggedIn(LoggedInUser loggedInUser) {
		return loggedInUserHolders.keySet().stream()
				.anyMatch( u ->
						u.getUsername().equals(loggedInUser.getUsername())
								&& u.getToken().equals(loggedInUser.getToken())
				);
	}

	private Munkatars checkPassword(String username, String password) throws LoginException {
		Munkatars munkatars = munkatarsDao.findAktivByFelhasznaloNev(username);
		if(munkatars == null){
			throw new FailedLoginException("Hibás felhasználónév.");
		}
		try {
			if (PasswordEncryptionService.authenticate(password,
					stringToByteArray(munkatars.getJelszo()),
					stringToByteArray(munkatars.getSalt()))) {
				return munkatars;
			} else {
				throw new FailedLoginException("Hibás jelszó.");
			}
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new LoginException("Hiba történt a login közben: "+e.getMessage());
		}
	}

	private byte[] stringToByteArray(String string){
		String[] byteValues = string.substring(1, string.length() - 1).split(",");
		byte[] bytes = new byte[byteValues.length];

		for(int i=0, len=bytes.length; i<len; i++){
			bytes[i] = parseByte(byteValues[i].trim());
		}
		return bytes;
	}

	public Boolean isViewInvalid(LoggedInUser loggedInUser, String viewType) {
		return loggedIn(loggedInUser) && loggedInUserHolders.get(loggedInUser).isViewInvalid(viewType);
	}
}
