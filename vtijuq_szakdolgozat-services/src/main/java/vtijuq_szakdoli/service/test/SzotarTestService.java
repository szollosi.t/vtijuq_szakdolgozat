package vtijuq_szakdoli.service.test;

import java.util.HashSet;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.dto.filterDTO.test.SzotarTestFilterDTO;
import vtijuq_szakdoli.common.util.graph.Dag;
import vtijuq_szakdoli.common.util.graph.Hierarchy;
import vtijuq_szakdoli.common.util.graph.Tree;
import vtijuq_szakdoli.dao.entitas.test.SzotarTestDao;
import vtijuq_szakdoli.filter.test.SzotarTestFilter;
import vtijuq_szakdoli.mapper.test.SzotarTestMapper;
import vtijuq_szakdoli.service.BusinessService;

@Stateless
public class SzotarTestService extends BusinessService {

	@Inject
	private SzotarTestDao szotarTestDao;

	@Inject
	private SzotarTestMapper szotarTestMapper;

	public vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO getSzotarTestById(Long id) {
		return szotarTestMapper.toEntitasDTO(szotarTestDao.findById(id));
    }

	public vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO getSzotarTestEditableById(Long id) {
		return szotarTestMapper.toEditableDTO(szotarTestDao.findById(id));
    }

	public vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO save(vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO dto){
		return szotarTestMapper.toEditableDTO(szotarTestDao.save(szotarTestMapper.toEntitas(dto)));
    }

	public vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO save(
			vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO principalDTO,
			vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO dto){
		return szotarTestMapper.toEditableDTO(szotarTestDao.save(principalDTO.getId(), szotarTestMapper.toEntitas(dto)));
    }

	public void delete(SzotarTestDTO dto) {
		szotarTestDao.deleteSubHierarchy(dto.getId());
	}

	public List<vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO> listAllEditable(){
		return szotarTestMapper.toEditableDTOList(szotarTestDao.findAll());
	}

	public List<SzotarTestDTO> listAllByFilter(SzotarTestFilterDTO filterDTO){
		return szotarTestMapper.toEntitasDTOList(szotarTestDao.findAllByFilter(filterDTO.merge(new SzotarTestFilter())));
	}

	public boolean existsByFilter(SzotarTestFilterDTO filterDTO){
		return szotarTestDao.existsByFilter(filterDTO.merge(new SzotarTestFilter()));
	}

	public Hierarchy<SzotarTestDTO> getEntitasHierarchyBelowFiltered(final SzotarTestFilterDTO filterDTO) {
		if (!filterDTO.hasConditions()) {
			return getWholeTree();
		}
		return new Dag<>(listAllByFilter(filterDTO),
				sz -> szotarTestMapper.toEntitasDTOSet(new HashSet<>(szotarTestDao.findSubordinates(szotarTestDao.findById(sz.getId()))))
		);
	}

	public List<SzotarTestDTO> listAllTipus() {
		return szotarTestMapper.toEntitasDTOList(szotarTestDao.findAllSzotarTipus());
	}

	public List<vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO> listAllTipusEditable(){
		return szotarTestMapper.toEditableDTOList(szotarTestDao.findAllSzotarTipus());
	}

	public List<SzotarTestDTO> listAllByTipusKod(String kod){
		return szotarTestMapper.toEntitasDTOList(szotarTestDao.findAllByTipusKod(kod));
	}

	public Tree<vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO> getWholeTree() {
        return new Tree<>(
				szotarTestMapper.toEntitasDTO(szotarTestDao.findSzotarTestRoot()),
				sz -> szotarTestMapper.toEntitasDTOSet(new HashSet<>(szotarTestDao.findSubordinates(szotarTestDao.findById(sz.getId()))))
		);
	}

	public Tree<vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO> getWholeTreeEditable() {
        return new Tree<>(
				szotarTestMapper.toEditableDTO(szotarTestDao.findSzotarTestRoot()),
				sz -> szotarTestMapper.toEditableDTOSet(new HashSet<>(szotarTestDao.findSubordinates(szotarTestDao.findById(sz.getId()))))
		);
	}

	public Tree<vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO> getSubTreeEditable(SzotarTestDTO principal) {
        return new Tree<>(
				szotarTestMapper.toEditableDTO(szotarTestDao.findById(principal.getId())),
				sz -> szotarTestMapper.toEditableDTOSet(new HashSet<>(szotarTestDao.findSubordinates(szotarTestDao.findById(sz.getId()))))
		);
	}
}
