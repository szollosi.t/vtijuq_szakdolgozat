package vtijuq_szakdoli.service.kovetelmeny;

import java.util.HashSet;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.IsmeretFilterDTO;
import vtijuq_szakdoli.common.dto.subordinationDTO.SubordinationDTO;
import vtijuq_szakdoli.common.util.graph.Dag;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.IsmeretDao;
import vtijuq_szakdoli.filter.kovetelmeny.IsmeretFilter;
import vtijuq_szakdoli.mapper.kovetelmeny.IsmeretMapper;
import vtijuq_szakdoli.service.BusinessService;

@Stateless
public class IsmeretService extends BusinessService {

	private static final Logger LOG = LoggerFactory.getLogger(IsmeretService.class);

	@Inject
	private IsmeretDao ismeretDao;

	@Inject
	private IsmeretMapper ismeretMapper;

	public List<IsmeretDTO> listAllIsmeret() {
		return ismeretMapper.toEntitasDTOList(ismeretDao.findAll());
	}

	public Dag<vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO> getWholeTreeEditable() {
		return new Dag<>(
				ismeretMapper.toEditableDTOSet(new HashSet<>(ismeretDao.findSovereigns())),
                ism -> ismeretMapper.toEditableDTOSet(new HashSet<>(ismeretDao.findSubordinates(ism.getId())))
        );
	}

	public Dag<vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO> getHierarchyBelowFilteredEditable(IsmeretFilterDTO filterDTO) {
		if (!filterDTO.hasConditions()) {
			return getWholeTreeEditable();
		}
		return new Dag<>(listAllByFilter(filterDTO),
				ism -> ismeretMapper.toEditableDTOSet(new HashSet<>(ismeretDao.findSubordinates(ism.getId())))
		);
	}

	private List<vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO> listAllByFilter(IsmeretFilterDTO filterDTO) {
		return ismeretMapper.toEditableDTOList(ismeretDao.findAllByFilter(filterDTO.merge(new IsmeretFilter())));
	}

	public vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO save(vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO dto){
		return ismeretMapper.toEditableDTO(ismeretDao.save(ismeretMapper.toEntitas(dto)));
	}

	public vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO save(vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO dto, Long idPrincipal){
		return ismeretMapper.toEditableDTO(ismeretDao.save(idPrincipal, ismeretMapper.toEntitas(dto)));
	}

	public void delete(vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO dto) {
		ismeretDao.delete(dto.getId());
	}

	public boolean hasAlismeretek(Long ismeretId){
		return ismeretDao.hasSubordinates(ismeretId);
	}

	public void createSubordination(SubordinationDTO<IsmeretDTO> dto) {
		ismeretDao.saveSubordination(dto.getPrincipalId(), dto.getSubordinateId());
	}

	public void removeSubordination(SubordinationDTO<IsmeretDTO> dto) {
		ismeretDao.removeSubordination(dto.getPrincipalId(), dto.getSubordinateId());
	}
}
