package vtijuq_szakdoli.service.kovetelmeny;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyWithSzakiranyokDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyWithSzakmaiJellemzokDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.KepzesiTeruletDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.SzakFilterDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.SzakKovetelmenyFilterDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.SzakiranyFilterDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.SzakiranyKovetelmenyFilterDTO;
import vtijuq_szakdoli.common.exception.ValidationException;
import vtijuq_szakdoli.common.util.graph.Dag;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.KepzesiTeruletDao;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakDao;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakKovetelmenyDao;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakiranyDao;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakiranyKovetelmenyDao;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakmaiJellemzoDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakKovetelmeny;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakiranyKovetelmeny;
import vtijuq_szakdoli.filter.kovetelmeny.SzakFilter;
import vtijuq_szakdoli.filter.kovetelmeny.SzakKovetelmenyFilter;
import vtijuq_szakdoli.filter.kovetelmeny.SzakiranyFilter;
import vtijuq_szakdoli.filter.kovetelmeny.SzakiranyKovetelmenyFilter;
import vtijuq_szakdoli.mapper.kovetelmeny.KepzesiTeruletMapper;
import vtijuq_szakdoli.mapper.kovetelmeny.SzakKovetelmenyMapper;
import vtijuq_szakdoli.mapper.kovetelmeny.SzakMapper;
import vtijuq_szakdoli.mapper.kovetelmeny.SzakiranyKovetelmenyMapper;
import vtijuq_szakdoli.mapper.kovetelmeny.SzakiranyMapper;
import vtijuq_szakdoli.mapper.kovetelmeny.SzakmaiJellemzoMapper;
import vtijuq_szakdoli.service.BusinessService;

@Stateless
public class KovetelmenyService extends BusinessService {

	private static final Logger LOG = LoggerFactory.getLogger(KovetelmenyService.class);

	@Inject
	private KepzesiTeruletDao kepzesiTeruletDao;
	@Inject
	private KepzesiTeruletMapper kepzesiTeruletMapper;

	@Inject
	private SzakDao szakDao;
	@Inject
	private SzakMapper szakMapper;

	@Inject
	private SzakKovetelmenyDao szakKovetelmenyDao;
	@Inject
	private SzakKovetelmenyMapper szakKovetelmenyMapper;

	@Inject
	private SzakiranyDao szakiranyDao;
	@Inject
	private SzakiranyMapper szakiranyMapper;

	@Inject
	private SzakiranyKovetelmenyDao szakiranyKovetelmenyDao;
	@Inject
	private SzakiranyKovetelmenyMapper szakiranyKovetelmenyMapper;

	@Inject
	private SzakmaiJellemzoDao szakmaiJellemzoDao;
	@Inject
	private SzakmaiJellemzoMapper szakmaiJellemzoMapper;

	//szakKovetelmeny
	public List<SzakKovetelmenyDTO> listAllSzakKovetelmeny() {
		return szakKovetelmenyMapper.toEntitasDTOList(szakKovetelmenyDao.findAll());
	}

	public List<SzakKovetelmenyDTO> listAllSzakKovetelmenyByFilter(SzakKovetelmenyFilterDTO dto) {
		return szakKovetelmenyMapper.toEntitasDTOList(szakKovetelmenyDao.findAllByFilter(dto.merge(new SzakKovetelmenyFilter())));
	}

	public vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO validate(
			vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO szakKovetelmeny)
			throws ValidationException {
		Set<String> hibaUzenetek = szakKovetelmeny.getErrors();
		if(!hibaUzenetek.isEmpty()){
			throw new ValidationException(hibaUzenetek);
		}
		return szakKovetelmeny;
	}

	public vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO getSzakKovetelmenyById(Long id) {
		return szakKovetelmenyMapper.toEditableDTO(szakKovetelmenyDao.findById(id));
	}

	public SzakKovetelmenyWithSzakiranyokDTO getSzakKovetelmenyWithSzakiranyokById(Long id) {
		//szakKovetelmeny alapadatai
		vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO szakKovetelmenyDTO = getSzakKovetelmenyById(id);
		//alapszakirany
		final Long alapSzakiranyId = szakiranyKovetelmenyDao.findAlapSzakiranyIdkBySzakKovetelmenyId(id);
		final SzakiranyKovetelmenyWithSzakmaiJellemzokDTO alapSzakiranyKovetelmeny = getSzakiranyKovetelmenyWithSzakmaiJellemzokById(alapSzakiranyId);
		//megmutatásra a nem alap szakirányok
		final List<Long> szakiranyKovetelmenyIdk = szakiranyKovetelmenyDao.findSzakiranyKovetelmenyIdkBySzakKovetelmenyId(id);
		final List<SzakiranyKovetelmenyWithSzakmaiJellemzokDTO> szakiranyKovetelmenyek = szakiranyKovetelmenyIdk.stream()
				.map(this::getSzakiranyKovetelmenyWithSzakmaiJellemzokById)
				.collect(Collectors.toList());
		//compose
		return new SzakKovetelmenyWithSzakiranyokDTO(szakKovetelmenyDTO, alapSzakiranyKovetelmeny, szakiranyKovetelmenyek);
	}

	public vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO save(vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO dto) {
		final boolean newSzakKovetelmeny = dto.getId() == null;
		final SzakKovetelmeny szakKovetelmeny = szakKovetelmenyDao.save(szakKovetelmenyMapper.toEntitas(dto));
		if (newSzakKovetelmeny) {
			final SzakiranyKovetelmeny alapSzakiranyKovetelmeny = new SzakiranyKovetelmeny();
			alapSzakiranyKovetelmeny.setSzakKovetelmeny(szakKovetelmeny);
			szakiranyKovetelmenyDao.save(alapSzakiranyKovetelmeny);
		}
		return szakKovetelmenyMapper.toEditableDTO(szakKovetelmenyDao.findById(szakKovetelmeny.getId()));
	}

	public SzakKovetelmenyWithSzakiranyokDTO saveSzakKovetelmenyWithSzakiranyok(SzakKovetelmenyWithSzakiranyokDTO dto) {
		//csak a szakKovetelmeny alapadatai és az alapSzakiranyKovetelmeny adatai váltzohatnak, így csk őket mentjük
		final vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO savedSzakKovetelmenyDTO = save(dto);
		final SzakiranyKovetelmenyWithSzakmaiJellemzokDTO savedAlapSzakiranyKovetelmeny = saveWithSzakmaiJellemzok(dto.getAlapSzakiranyKovetelmeny());
		//a szakiranyKovetelmenyek adatai nem változhatnak, ezért nem is futtatjuk meg őket
		//TODO érdemes-e frisset felkeresni? (concurrent modification)
		return new SzakKovetelmenyWithSzakiranyokDTO(savedSzakKovetelmenyDTO, savedAlapSzakiranyKovetelmeny, dto.getSzakiranyKovetelmenyek());
	}

	public void delete(vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO dto) {
		//FIXME alapSzakiranyKovetelmeny törlése
		szakKovetelmenyDao.delete(dto.getId());
	}

	//szakiranyKovetelmeny
	public vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO getSzakiranyKovetelmenyById(Long id) {
		return szakiranyKovetelmenyMapper.toEditableDTO(szakiranyKovetelmenyDao.findById(id));
	}

	public SzakiranyKovetelmenyWithSzakmaiJellemzokDTO getSzakiranyKovetelmenyWithSzakmaiJellemzokById(Long id) {
		final vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO szakiranyKovetelmenyDTO =
				szakiranyKovetelmenyMapper.toEditableDTO(szakiranyKovetelmenyDao.findById(id));
		final Dag<SzakmaiJellemzoDTO> szakmaiJellemzoDTODag = szakmaiJellemzoDao
				.findBySzakiranyKovetelmenyId(id).map(szakmaiJellemzoMapper::toEditableDTO);
		return new SzakiranyKovetelmenyWithSzakmaiJellemzokDTO(szakiranyKovetelmenyDTO, szakmaiJellemzoDTODag);
	}

	public List<SzakiranyKovetelmenyDTO> listAllSzakiranyKovetelmeny() {
		return szakiranyKovetelmenyMapper.toEntitasDTOList(szakiranyKovetelmenyDao.findAll());
	}

	public List<SzakiranyKovetelmenyDTO> listAllSzakiranyKovetelmenyByFilter(SzakiranyKovetelmenyFilterDTO dto) {
		return szakiranyKovetelmenyMapper.toEntitasDTOList(szakiranyKovetelmenyDao
				.findAllByFilter(dto.merge(new SzakiranyKovetelmenyFilter())));
	}

	public List<SzakiranyKovetelmenyDTO> listAllSzakiranyKovetelmenyBySzakKovetelmeny(SzakKovetelmenyDTO dto) {
		final SzakiranyKovetelmenyFilter filter = new SzakiranyKovetelmenyFilter();
		filter.setIdSzakKovetelmeny(dto.getId());
		return szakiranyKovetelmenyMapper.toEntitasDTOList(szakiranyKovetelmenyDao.findAllByFilter(filter));
	}

	public vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO save(vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO dto) {
		return szakiranyKovetelmenyMapper.toEditableDTO(szakiranyKovetelmenyDao.save(szakiranyKovetelmenyMapper.toEntitas(dto)));
	}

	public vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyWithSzakmaiJellemzokDTO saveWithSzakmaiJellemzok(vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyWithSzakmaiJellemzokDTO dto) {
		final vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO szakiranyKovetelmenyDTO =
				szakiranyKovetelmenyMapper.toEditableDTO(szakiranyKovetelmenyDao.save(szakiranyKovetelmenyMapper.toEntitas(dto)));
		final Dag<SzakmaiJellemzoDTO> szakmaiJellemzoDTODag = szakmaiJellemzoDao
				.save(szakiranyKovetelmenyDTO.getId(),
						dto.getSzakmaiJellemzok().map(szakmaiJellemzoMapper::toEntitas))
				.map(szakmaiJellemzoMapper::toEditableDTO);
		return new SzakiranyKovetelmenyWithSzakmaiJellemzokDTO(szakiranyKovetelmenyDTO, szakmaiJellemzoDTODag);
	}

	public void delete(vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyWithSzakmaiJellemzokDTO dto) {
		szakmaiJellemzoDao.deleteBySzakiranyKovetelmenyId(dto.getId());
		szakiranyKovetelmenyDao.delete(dto.getId());
	}

	//szak
	public vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakDTO save(vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakDTO dto) {
		return szakMapper.toEditableDTO(szakDao.save(szakMapper.toEntitas(dto)));
	}

	public List<SzakDTO> listAllSzak() {
		return szakMapper.toEntitasDTOList(szakDao.findAll());
	}

	public List<SzakDTO> listAllSzakByFilter(SzakFilterDTO dto) {
		return szakMapper.toEntitasDTOList(szakDao.findAllByFilter(dto.merge(new SzakFilter())));
	}

	//szakirany
	public vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO save(vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO dto) {
		return szakiranyMapper.toEditableDTO(szakiranyDao.save(szakiranyMapper.toEntitas(dto)));
	}

	public List<SzakiranyDTO> listAllSzakirany() {
		return szakiranyMapper.toEntitasDTOList(szakiranyDao.findAll());
	}

	public List<SzakiranyDTO> listAllSzakiranyByFilter(SzakiranyFilterDTO dto) {
		return szakiranyMapper.toEntitasDTOList(szakiranyDao.findAllByFilter(dto.merge(new SzakiranyFilter())));
	}

	//kepzesi terulet
	public List<KepzesiTeruletDTO> listAllKepzesiTerulet() {
		return kepzesiTeruletMapper.toEntitasDTOList(kepzesiTeruletDao.findAll());
	}
}
