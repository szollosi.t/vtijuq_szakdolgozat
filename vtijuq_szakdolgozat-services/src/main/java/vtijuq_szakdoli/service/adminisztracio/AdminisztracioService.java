package vtijuq_szakdoli.service.adminisztracio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.common.dto.filterDTO.adminisztracio.MunkatarsFilterDTO;
import vtijuq_szakdoli.common.dto.filterDTO.adminisztracio.SzervezetFilterDTO;
import vtijuq_szakdoli.common.util.graph.Hierarchy;
import vtijuq_szakdoli.common.util.graph.Tree;
import vtijuq_szakdoli.dao.entitas.adminisztracio.MunkatarsDao;
import vtijuq_szakdoli.dao.entitas.adminisztracio.SzerepkorDao;
import vtijuq_szakdoli.dao.entitas.adminisztracio.SzervezetDao;
import vtijuq_szakdoli.dao.entitas.adminisztracio.SzervezetTipusDao;
import vtijuq_szakdoli.filter.adminisztracio.MunkatarsFilter;
import vtijuq_szakdoli.filter.adminisztracio.SzervezetFilter;
import vtijuq_szakdoli.mapper.adminisztracio.MunkatarsMapper;
import vtijuq_szakdoli.mapper.adminisztracio.SzerepkorMapper;
import vtijuq_szakdoli.mapper.adminisztracio.SzervezetMapper;
import vtijuq_szakdoli.mapper.adminisztracio.SzervezetTipusMapper;
import vtijuq_szakdoli.service.BusinessService;

@Stateless
public class AdminisztracioService extends BusinessService {

	private static final Logger LOG = LoggerFactory.getLogger(AdminisztracioService.class);

	@Inject
	private MunkatarsDao munkatarsDao;

	@Inject
	private MunkatarsMapper munkatarsMapper;

	@Inject
	private SzervezetDao szervezetDao;

	@Inject
	private SzervezetMapper szervezetMapper;

	@Inject
	private SzerepkorDao szerepkorDao;

	@Inject
	private SzerepkorMapper szerepkorMapper;

	@Inject
	private SzervezetTipusDao szervezetTipusDao;

	@Inject
	private SzervezetTipusMapper szervezetTipusMapper;

	//szervezetTipus
	public List<SzervezetTipusDTO> listAllSzervezetTipus() {
		return szervezetTipusMapper.toEntitasDTOList(szervezetTipusDao.findAll());
	}

	//szerepkor
	public List<SzerepkorDTO> listAllSzerepkor() {
		return szerepkorMapper.toEntitasDTOList(szerepkorDao.findAll());
	}

	//szervezet
	public vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO getSzervezetById(Long id) {
        return szervezetMapper.toEditableDTO(szervezetDao.findById(id));
    }

	public List<SzervezetDTO> listAllSzervezet() {
		return szervezetMapper.toEntitasDTOList(szervezetDao.findAll());
	}

	public Tree<vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO> getWholeSzervezetTreeEditable() {
		return szervezetDao.findWholeSzervezetTree().map(szervezetMapper::toEditableDTO);
	}

	public Tree<SzervezetDTO> getWholeSzervezetTreeEntitas() {
		return szervezetDao.findWholeSzervezetTree().map(szervezetMapper::toEntitasDTO);
	}

	public Hierarchy<vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO> getEditableHierarchyBelowFiltered(SzervezetFilterDTO filterDTO) {
		if (!filterDTO.hasConditions()) {
			return getWholeSzervezetTreeEditable();
		}
		return szervezetDao.findSzervezetHierarchyByFilter(filterDTO.merge(new SzervezetFilter())).map(szervezetMapper::toEditableDTO);
	}

	public vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO save(vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO dto) {
		return szervezetMapper.toEditableDTO(szervezetDao.save(szervezetMapper.toEntitas(dto)));
    }

	public void delete(vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO dto) {
		szervezetDao.delete(dto.getId());
	}

	//munkatars
	public vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO getMunkatarsById(Long id) {
		return munkatarsMapper.toEditableDTO(munkatarsDao.findById(id));
	}

	public List<vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO> listAllMunkatarsEditable() {
		return  munkatarsMapper.toEditableDTOList(munkatarsDao.findAll());
	}

	public List<vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO> listAllMunkatarsByFilterEditable(MunkatarsFilterDTO dto) {
		return munkatarsMapper.toEditableDTOList(munkatarsDao.findAllByFilter(dto.merge(new MunkatarsFilter())));
	}

	public vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO save(vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO dto) {
		return munkatarsMapper.toEditableDTO(munkatarsDao.save(munkatarsMapper.toEntitas(dto)));
	}

	public void inactivate(vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO dto) {
		dto.setAktiv(false);
		save(dto);
	}
}
