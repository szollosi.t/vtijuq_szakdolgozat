package vtijuq_szakdoli.service.akkreditacio;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.entitasDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.dto.filterDTO.akkreditacio.AkkreditacioFilterDTO;
import vtijuq_szakdoli.common.exception.ValidationException;
import vtijuq_szakdoli.dao.entitas.akkreditacio.AkkreditacioDao;
import vtijuq_szakdoli.filter.akkreditacio.AkkreditacioFilter;
import vtijuq_szakdoli.mapper.akkreditacio.AkkreditacioMapper;
import vtijuq_szakdoli.service.BusinessService;

@Stateless
public class AkkreditacioService extends BusinessService {

	private static final Logger LOG = LoggerFactory.getLogger(AkkreditacioService.class);

	@Inject
	private AkkreditacioDao akkreditacioDao;

	@Inject
	private AkkreditacioMapper akkreditacioMapper;

	public List<AkkreditacioDTO> listAllAkkreditacio() {
		return akkreditacioMapper.toEntitasDTOList(akkreditacioDao.findAll());
	}

	public List<vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO> listAllAkkreditacioEditable() {
		return akkreditacioMapper.toEditableDTOList(akkreditacioDao.findAll());
	}

	public AkkreditacioDTO getAkkreditacioById(Long id) {
		return akkreditacioMapper.toEntitasDTO(akkreditacioDao.findById(id));
    }

	public vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO getAkkreditacioEditableById(Long id) {
		return akkreditacioMapper.toEditableDTO(akkreditacioDao.findById(id));
    }

	public vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO validate(
			vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO akkreditacio) {
		Set<String> hibaUzenetek = akkreditacio.getErrors();
		if(!hibaUzenetek.isEmpty()){
			throw new ValidationException(hibaUzenetek);
		}
		return akkreditacio;
	}

	public vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO save(vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO dto) {
		return akkreditacioMapper.toEditableDTO(akkreditacioDao.save(akkreditacioMapper.toEntitas(dto)));
    }

	public void delete(vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO dto) {
		akkreditacioDao.delete(dto.getId());
	}

	public List<vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO> listAkkreditacioEditableByFilter(AkkreditacioFilterDTO dto) {
		return akkreditacioMapper.toEditableDTOList(akkreditacioDao.findAllByFilter(dto.merge(new AkkreditacioFilter())));
	}
}
