package vtijuq_szakdoli.service;

import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;

import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.validator.AkkreditacioValidator;
import vtijuq_szakdoli.validator.KovetelmenyValidator;

@Stateless
public class ValidatorService extends BusinessService {
//TODO EditorView-k, ... átgondolandó, hogy tudna-e ide DTO-t adni?

	@Inject
	private AkkreditacioValidator akkreditacioValidator;

	@Inject
	private KovetelmenyValidator kovetelmenyValidator;

	//KovetelmenyValidator methods
	public Set<String> finaliseSzakKovetelmeny(SzakKovetelmenyDTO dto) {
		return kovetelmenyValidator.finalise(dto.getId());
	}

	public Set<String> validateSzakKovetelmeny(SzakKovetelmenyDTO dto) {
		return kovetelmenyValidator.validate(dto.getId());
	}

	public void invalidateSzakKovetelmeny(SzakKovetelmenyDTO dto){
		kovetelmenyValidator.invalidate(dto.getId());
	}

	public boolean isSzakKovetelmenyValid(SzakKovetelmenyDTO dto) {
		return kovetelmenyValidator.isSzakKovetelmenyValid(dto.getId());
	}

	public boolean isSzakKovetelmenyFinal(Long id) {
		return kovetelmenyValidator.isSzakKovetelmenyFinal(id);
	}

	public boolean isSzakiranyKovetelmenyFinal(Long id) {
		return kovetelmenyValidator.isSzakiranyKovetelmenyFinal(id);
	}

	public boolean isIsmeretFinal(Long id) {
		return kovetelmenyValidator.isIsmeretFinal(id);
	}

	//AkkreditacioValidator methods
	public Set<String> finaliseAkkreditacio(AkkreditacioDTO dto) {
		return akkreditacioValidator.finalise(dto.getId());
	}

	public Set<String> validateAkkreditacio(AkkreditacioDTO dto) {
		return akkreditacioValidator.validate(dto.getId());
	}

	public void invalidateAkkreditacio(AkkreditacioDTO dto){
		akkreditacioValidator.invalidate(dto.getId());
	}

	public boolean isAkkreditacioValid(AkkreditacioDTO dto) {
		return akkreditacioValidator.isAkkreditacioValid(dto.getId());
	}

	public boolean isAkkreditacioFinal(Long id) {
		return akkreditacioValidator.isAkkreditacioFinal(id);
	}

	public boolean isKonkretSzakiranyFinal(Long id) {
		return akkreditacioValidator.isKonkretSzakiranyFinal(id);
	}

	public boolean isKonkretSzakFinal(Long id) {
		return akkreditacioValidator.isKonkretSzakFinal(id);
	}

	public boolean isTantervFinal(Long id) {
		return akkreditacioValidator.isTantervFinal(id);
	}

	public boolean isTantargyFinal(Long id) {
		return akkreditacioValidator.isTantargyFinal(id);
	}

	public boolean isTematikaFinal(Long id) {
		return akkreditacioValidator.isTematikaFinal(id);
	}

	public boolean isTemaFinal(Long id) {
		return akkreditacioValidator.isTemaFinal(id);
	}
}
