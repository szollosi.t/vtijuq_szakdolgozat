package vtijuq_szakdoli.service.megvalositas;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyWithTantervekAndTematikakDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.IsmeretSzintDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.TantargyFilterDTO;
import vtijuq_szakdoli.dao.entitas.megvalositas.IsmeretSzintDao;
import vtijuq_szakdoli.dao.entitas.megvalositas.TantargyDao;
import vtijuq_szakdoli.dao.entitas.megvalositas.TantervDao;
import vtijuq_szakdoli.dao.entitas.megvalositas.TemaDao;
import vtijuq_szakdoli.dao.entitas.megvalositas.TematikaDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tanterv;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tematika;
import vtijuq_szakdoli.filter.megvalositas.TantargyFilter;
import vtijuq_szakdoli.mapper.megvalositas.IsmeretSzintMapper;
import vtijuq_szakdoli.mapper.megvalositas.TantargyMapper;
import vtijuq_szakdoli.mapper.megvalositas.TantervMapper;
import vtijuq_szakdoli.mapper.megvalositas.TemaMapper;
import vtijuq_szakdoli.mapper.megvalositas.TematikaMapper;
import vtijuq_szakdoli.service.BusinessService;

@Stateless
public class TantargyService extends BusinessService {

	private static final Logger LOG = LoggerFactory.getLogger(TantargyService.class);

	@Inject
	private TantargyDao tantargyDao;
	@Inject
	private TantargyMapper tantargyMapper;

	@Inject
	private TantervDao tantervDao;
	@Inject
	private TantervMapper tantervMapper;

	@Inject
	private TematikaDao tematikaDao;
	@Inject
	private TematikaMapper tematikaMapper;

	@Inject
	private TemaDao temaDao;
	@Inject
	private TemaMapper temaMapper;

	@Inject
	private IsmeretSzintDao ismeretSzintDao;
	@Inject
	private IsmeretSzintMapper ismeretSzintMapper;

	//tantargy
	public vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO getTantargyById(Long id) {
		return tantargyMapper.toEditableDTO(tantargyDao.findById(id));
	}

	public TantargyWithTantervekAndTematikakDTO getTantargyWithTantervekAndTematikakById(Long id) {
		final vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO tantargyDTO = getTantargyById(id);
		final List<vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO> tantervek = tantervMapper
				.toEditableDTOList(tantervDao.findTantervekByTantargyId(id));
		final List<vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO> tematikak = tematikaMapper
				.toEditableDTOList(tematikaDao.findTematikakByTantargyId(id));
		return new TantargyWithTantervekAndTematikakDTO(tantargyDTO, tantervek, tematikak);
	}

	public vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO save(vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO dto) {
		return tantargyMapper.toEditableDTO(tantargyDao.save(tantargyMapper.toEntitas(dto)));
	}

	public vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyWithTantervekAndTematikakDTO saveWithTantervekAndTematikak(vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyWithTantervekAndTematikakDTO dto) {
		final vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO savedTantargyDTO = save(dto);

		final List<Tanterv> oldTantervek = tantervDao.findTantervekByTantargyId(savedTantargyDTO.getId());
		final List<Tanterv> newTantervek = new ArrayList<>(oldTantervek);
		tantervMapper.mergeCollection(dto.getTantervek(), newTantervek);
		final List<vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO> savedTantervDTOList =
				tantervMapper.toEditableDTOList(tantervDao.saveCollection(oldTantervek, newTantervek));

		//az új vagy módosított témákat külön mentjük
		dto.getTematikak().forEach(t -> {
			if (t.getTema() instanceof vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO) {
				final vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO editedTema = (vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO)t.getTema();
				t.setTema(temaMapper.toEntitasDTO(temaDao.save(temaMapper.toEntitas(editedTema))));
			}
		});

		final List<Tematika> oldTematikak = tematikaDao.findTematikakByTantargyId(savedTantargyDTO.getId());
		final List<Tematika> newTematikak = new ArrayList<>(oldTematikak);
		tematikaMapper.mergeCollection(dto.getTematikak(), newTematikak);
		final List<vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO> savedTematikaDTOList =
				tematikaMapper.toEditableDTOList(tematikaDao.saveCollection(oldTematikak, newTematikak));

		return new TantargyWithTantervekAndTematikakDTO(savedTantargyDTO, savedTantervDTOList, savedTematikaDTOList);
	}

	public void delete(vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO dto) {
		tantargyDao.delete(dto.getId());
	}

	public void delete(vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyWithTantervekAndTematikakDTO dto) {
		tematikaDao.deleteByTantargyId(dto.getId());
		tantargyDao.delete(dto.getId());
	}

	public List<TantargyDTO> listAllTantargy(){
		return tantargyMapper.toEntitasDTOList(tantargyDao.findAll());
	}

	public List<TantargyDTO> listAllKonkretTantargy() {
		return tantargyMapper.toEntitasDTOList(tantargyDao.findAllKonkret());
	}

	public List<vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO> listAllTantargyEditable(){
		return tantargyMapper.toEditableDTOList(tantargyDao.findAll());
	}

	public List<vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO> listTantargyEditableByFilter(TantargyFilterDTO dto) {
		return tantargyMapper.toEditableDTOList(tantargyDao.findAllByFilter(dto.merge(new TantargyFilter())));
	}

	//tema
	public List<TemaDTO> listAllTema() {
		return temaMapper.toEntitasDTOList(temaDao.findAll());
	}

	public boolean hasSajatTema(TematikaDTO dto) {
		return dto.getTema() != null && (dto.getTema().getId() == null || temaDao.isSajatTema(dto.getTema().getId()));
	}

	//ismeretszint
	public List<IsmeretSzintDTO> listAllSzamonkertSzint() {
		return ismeretSzintMapper.toEntitasDTOList(ismeretSzintDao.findAll());
	}

	public List<IsmeretSzintDTO> listAllLeadottSzint() {
		return ismeretSzintMapper.toEntitasDTOList(ismeretSzintDao.findAll());
	}
}
