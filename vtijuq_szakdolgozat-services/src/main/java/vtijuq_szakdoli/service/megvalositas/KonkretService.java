package vtijuq_szakdoli.service.megvalositas;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakWithSzakiranyokDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyWithTantervekDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.ErtekelesTipusDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.KonkretSzakFilterDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.KonkretSzakiranyFilterDTO;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakiranyKovetelmenyDao;
import vtijuq_szakdoli.dao.entitas.megvalositas.ErtekelesTipusDao;
import vtijuq_szakdoli.dao.entitas.megvalositas.KonkretSzakDao;
import vtijuq_szakdoli.dao.entitas.megvalositas.KonkretSzakiranyDao;
import vtijuq_szakdoli.dao.entitas.megvalositas.TantervDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakiranyKovetelmeny;
import vtijuq_szakdoli.domain.entitas.megvalositas.KonkretSzak;
import vtijuq_szakdoli.domain.entitas.megvalositas.KonkretSzakirany;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tanterv;
import vtijuq_szakdoli.filter.megvalositas.KonkretSzakFilter;
import vtijuq_szakdoli.filter.megvalositas.KonkretSzakiranyFilter;
import vtijuq_szakdoli.mapper.megvalositas.ErtekelesTipusMapper;
import vtijuq_szakdoli.mapper.megvalositas.KonkretSzakMapper;
import vtijuq_szakdoli.mapper.megvalositas.KonkretSzakiranyMapper;
import vtijuq_szakdoli.mapper.megvalositas.TantervMapper;
import vtijuq_szakdoli.service.BusinessService;

@Stateless
public class KonkretService extends BusinessService {

	private static final Logger LOG = LoggerFactory.getLogger(KonkretService.class);

	@Inject
	private ErtekelesTipusDao ertekelesTipusDao;
	@Inject
	private ErtekelesTipusMapper ertekelesTipusMapper;

	@Inject
	private KonkretSzakDao konkretSzakDao;
	@Inject
	private KonkretSzakMapper konkretSzakMapper;

	@Inject
	private KonkretSzakiranyDao konkretSzakiranyDao;
	@Inject
	private KonkretSzakiranyMapper konkretSzakiranyMapper;

	@Inject
	private SzakiranyKovetelmenyDao szakiranyKovetelmenyDao;

	@Inject
	private TantervDao tantervDao;
	@Inject
	private TantervMapper tantervMapper;

	//konkretSzak
	public List<KonkretSzakDTO> listAllKonkretSzak() {
		return konkretSzakMapper.toEntitasDTOList(konkretSzakDao.findAll());
	}

	public List<KonkretSzakDTO> listKonkretSzakByFilter(KonkretSzakFilterDTO dto) {
		return konkretSzakMapper.toEntitasDTOList(konkretSzakDao.findAllByFilter(dto.merge(new KonkretSzakFilter())));
	}

	public vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO getKonkretSzakById(Long id) {
		return konkretSzakMapper.toEditableDTO(konkretSzakDao.findById(id));
	}

	public KonkretSzakWithSzakiranyokDTO getKonkretSzakWithSzakiranyokById(Long id) {
		//konkret szak alapadatai
		vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO konkretSzakDTO = getKonkretSzakById(id);
		//alapszakirany
		final Long alapSzakiranyId = konkretSzakiranyDao.findAlapSzakiranyIdkByKonkretSzakId(id);
		final KonkretSzakiranyWithTantervekDTO alapKonkretSzakirany = getKonkretSzakiranyWithTantervekById(alapSzakiranyId);
		//megmutatásra a nem alap szakirányok
		final List<Long> konkretSzakiranyIdk = konkretSzakiranyDao.findKonkretSzakiranyIdkByKonkretSzakId(id);
		final List<KonkretSzakiranyWithTantervekDTO> konkretSzakiranyok = konkretSzakiranyIdk.stream()
				.map(this::getKonkretSzakiranyWithTantervekById)
				.collect(Collectors.toList());
		//compose
		return new KonkretSzakWithSzakiranyokDTO(konkretSzakDTO, alapKonkretSzakirany, konkretSzakiranyok);
	}

	public vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO save(vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO dto) {
		final boolean newKonkretSzak = dto.getId() == null;
		final KonkretSzak konkretSzak = konkretSzakDao.save(konkretSzakMapper.toEntitas(dto));
		if (newKonkretSzak) {
			final KonkretSzakirany alapKonkretSzakirany = new KonkretSzakirany();
			alapKonkretSzakirany.setKonkretSzak(konkretSzak);
			final SzakiranyKovetelmeny alapSzakiranyKovetelmeny = szakiranyKovetelmenyDao
					.findAlapSzakiranyKovetelmeny(konkretSzak.getSzakKovetelmeny().getId());
			alapKonkretSzakirany.setSzakiranyKovetelmeny(alapSzakiranyKovetelmeny);
			konkretSzakiranyDao.save(alapKonkretSzakirany);
		}
		return konkretSzakMapper.toEditableDTO(konkretSzakDao.findById(konkretSzak.getId()));
	}

	public KonkretSzakWithSzakiranyokDTO saveKonkretSzakWithSzakiranyok(KonkretSzakWithSzakiranyokDTO dto) {
		//csak a konkretSzak alapadatai és az alapKonkretSzakirany adatai váltzohatnak, így csk őket mentjük
		final vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO savedKonkretSzakDTO = save(dto);
		final KonkretSzakiranyWithTantervekDTO savedAlapKonkretSzakirany = saveKonkretSzakiranyWithTantervek(dto.getAlapKonkretSzakirany());
		//a konkretSzakiranyok adatai nem változhatnak, ezért nem is futtatjuk meg őket
		//TODO érdemes-e frisset felkeresni? (concurrent modification)
		return new KonkretSzakWithSzakiranyokDTO(savedKonkretSzakDTO, savedAlapKonkretSzakirany, dto.getKonkretSzakiranyok());
	}

	public void delete(vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO dto) {
		//FIXME alapKonkretSzakirany törlése
		konkretSzakDao.delete(dto.getId());
	}

	//konkretSzakirany
	public vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO getKonkretSzakiranyById(Long id) {
		return konkretSzakiranyMapper.toEditableDTO(konkretSzakiranyDao.findById(id));
	}

	public vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyWithTantervekDTO getKonkretSzakiranyWithTantervekById(Long id) {
		final vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO konkretSzakiranyDTO = getKonkretSzakiranyById(id);
		final List<vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO> tantervek = tantervMapper
				.toEditableDTOList(tantervDao.findTantervekByKonkretSzakiranyId(id));
		return new KonkretSzakiranyWithTantervekDTO(konkretSzakiranyDTO, tantervek);
	}

	public List<KonkretSzakiranyDTO> listAllKonkretSzakirany() {
		return konkretSzakiranyMapper.toEntitasDTOList(konkretSzakiranyDao.findAll());
	}

	public List<KonkretSzakiranyDTO> listAllKonkretSzakiranyByFilter(KonkretSzakiranyFilterDTO dto) {
		return konkretSzakiranyMapper.toEntitasDTOList(
				konkretSzakiranyDao.findAllByFilter(dto.merge(new KonkretSzakiranyFilter())));
	}

	public vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO save(vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO dto) {
		return konkretSzakiranyMapper.toEditableDTO(konkretSzakiranyDao.save(konkretSzakiranyMapper.toEntitas(dto)));
	}

	public vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyWithTantervekDTO saveKonkretSzakiranyWithTantervek(
			vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyWithTantervekDTO dto) {
		final vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO savedKonkretSzakiranyDTO = save(dto);

		final List<Tanterv> oldTantervek = tantervDao.findTantervekByKonkretSzakiranyId(savedKonkretSzakiranyDTO.getId());
		final List<Tanterv> newTantervek = new ArrayList<>(oldTantervek);
		tantervMapper.mergeCollection(dto.getTantervek(), newTantervek);
		final List<vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO> savedTantervDTOList =
				tantervMapper.toEditableDTOList(tantervDao.saveCollection(oldTantervek, newTantervek));

		return new KonkretSzakiranyWithTantervekDTO(savedKonkretSzakiranyDTO, savedTantervDTOList);
	}

	public void delete(vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO dto) {
		konkretSzakiranyDao.delete(dto.getId());
	}

	//tanterv
	public List<TantervDTO> listAllTanterv() {
		return tantervMapper.toEntitasDTOList(tantervDao.findAll());
	}

	//ertekelesTipus
	public List<ErtekelesTipusDTO> listAllErtekelesTipus() {
		return ertekelesTipusMapper.toEntitasDTOList(ertekelesTipusDao.findAll());
	}
}
