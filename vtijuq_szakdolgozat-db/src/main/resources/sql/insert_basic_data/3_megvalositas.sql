--megvalósításhoz tartozó domain
-- basic ismeretszint
insert into ismeretszint(ID, KOD, NEV, LEIRAS)
  VALUES (seq_helper.NEXTID('s_ismeretszint'),
          'K', 'KIMONDAS', 'Ki tudja mondani az állíítást.')
;

insert into ismeretszint(ID, KOD, NEV, LEIRAS)
  VALUES (seq_helper.NEXTID('s_ismeretszint'),
          'B', 'BIZONYITAS', 'Tud legalább egy bizonyítást az állíításhoz.')
;
-- basic ertekelestipus
insert into ertekelestipus(ID, KOD, NEV, LEIRAS)
  VALUES (seq_helper.NEXTID('s_ertekelestipus'),
          'A', 'ALAIRAS', 'Aláírás.')
;
insert into ertekelestipus(ID, KOD, NEV, LEIRAS)
  VALUES (seq_helper.NEXTID('s_ertekelestipus'),
          'GY', 'GYAKORLAT', 'Gyakorlati jegy.')
;
insert into ertekelestipus(ID, KOD, NEV, LEIRAS)
  VALUES (seq_helper.NEXTID('s_ertekelestipus'),
          'B3', 'BEADANDO3', 'Beadandó, 3-szintű értékeléssel.')
;
