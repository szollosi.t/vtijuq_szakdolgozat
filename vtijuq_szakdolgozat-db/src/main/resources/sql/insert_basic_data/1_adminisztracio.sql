-- az adminisztratív, technikai főszervezet és a legfőbb adminisztrátor
INSERT INTO szervezettipus (id, nev, kod)
	VALUES (0, 'Technikai', 'TECHNIKAI')
;
INSERT INTO szervezet (id, idfoszervezet, nev, rovidites, idtipus, cim)
	VALUES (0, NULL, 'Adminisztratív főszervezet', 'ADMIN', 0, NULL)
;
INSERT INTO szerepkor (id, nev, kod, idszervezettipus)
	VALUES (0, 'Főadminisztrátor', 'FOADMIN', 0)
;
INSERT INTO munkatars (id, felhasznalonev, nev, idszervezet, idszerepkor, jelszo)
	VALUES (0, 'admin', 'Főadminisztrátor', 0, 0, 'admin')
;
-- funkciótörzs
BEGIN
	funkcio_helper.ADDFUNKCIO('KOVETELMENY_SZERKESZTO','KovetelmenySzerkeszto');
	funkcio_helper.ADDFUNKCIO('ISMERET_KARBANTARTO','IsmeretKarbantarto');
	funkcio_helper.ADDFUNKCIO('ISMERET_SZERKESZTO','IsmeretSzerkeszto');
	funkcio_helper.ADDFUNKCIO('TANTARGY_KARBANTARTO','TantargyKarbantarto');
	funkcio_helper.ADDFUNKCIO('TANTARGY_SZERKESZTO','TantargySzerkeszto');
	funkcio_helper.ADDFUNKCIO('KONKRET_SZERKESZTO','KonkretSzerkeszto');
	funkcio_helper.ADDFUNKCIO('AKKREDITACIO_KARBANTARTO','AkkreditacioKarbantarto');
	funkcio_helper.ADDFUNKCIO('AKKREDITACIO_SZERKESZTO','AkkreditacioSzerkeszto');
	funkcio_helper.ADDFUNKCIO('SZERVEZET_KARBANTARTO','SzervezetKarbantarto');
	funkcio_helper.ADDFUNKCIO('SZERVEZET_SZERKESZTO','SzervezetSzerkeszto');
	funkcio_helper.ADDFUNKCIO('MUNKATARS_KARBANTARTO','MunkatarsKarbantarto');
	funkcio_helper.ADDFUNKCIO('MUNKATARS_SZERKESZTO','MunkatarsSzerkeszto');
END;
/
--adminisztratív szerepkör (munkatárs- és szervezet-karbantartó, illetékesség szerint)
INSERT INTO szerepkor (id, nev, kod, idszervezettipus)
	SELECT seq_helper.nextId('s_szerepkor'), 'Adminisztrátor', 'ADMIN', NULL
	FROM dual
;

BEGIN
	FUNKCIO_HELPER.IRHASSA('ADMIN','SZERVEZET_KARBANTARTO');
	FUNKCIO_HELPER.IRHASSA('ADMIN','SZERVEZET_SZERKESZTO');
	FUNKCIO_HELPER.IRHASSA('ADMIN','MUNKATARS_KARBANTARTO');
	FUNKCIO_HELPER.IRHASSA('ADMIN','MUNKATARS_SZERKESZTO');
END;
/

--intézménytípusok
INSERT INTO szervezettipus (id, nev, kod)
	VALUES (seq_helper.nextId('s_szervezettipus'), 'Mab', 'MAB')
;
INSERT INTO szervezettipus (id, nev, kod)
	VALUES (seq_helper.nextId('s_szervezettipus'), 'Intezmeny', 'INTEZMENY')
;

--MAB-felelős (követelmények)
INSERT INTO szerepkor (id, nev, kod, idszervezettipus)
	SELECT seq_helper.nextId('s_szerepkor'), 'MAB-felelős', 'MAB', szt.id
	FROM SZERVEZETTIPUS szt
  WHERE szt.kod = 'MAB'
;
BEGIN
	funkcio_helper.IRHASSA('MAB','KOVETELMENY_SZERKESZTO');
	funkcio_helper.IRHASSA('MAB','ISMERET_KARBANTARTO');
	funkcio_helper.IRHASSA('MAB','ISMERET_SZERKESZTO');
END;
/

--Intézmény-felelős (megvalósítások)
INSERT INTO szerepkor (id, nev, kod, idszervezettipus)
	SELECT seq_helper.nextId('s_szerepkor'), 'Intézmény-felelős', 'INTEZMENY', szt.id
  FROM SZERVEZETTIPUS szt
  WHERE szt.kod = 'INTEZMENY'
;
BEGIN
	funkcio_helper.LATHASSA('INTEZMENY','ISMERET_KARBANTARTO');
	funkcio_helper.LATHASSA('INTEZMENY','ISMERET_SZERKESZTO');
	funkcio_helper.IRHASSA('INTEZMENY','KONKRET_SZERKESZTO');
	funkcio_helper.IRHASSA('INTEZMENY','TANTARGY_KARBANTARTO');
	funkcio_helper.IRHASSA('INTEZMENY','TANTARGY_SZERKESZTO');
END;
/

--Akkreditáció-karbantartó (akkreditációk)
INSERT INTO szerepkor (id, nev, kod, idszervezettipus)
	SELECT seq_helper.nextId('s_szerepkor'), 'Akkreditáció-karbantartó', 'AKKREDITACIO', szt.id
  FROM SZERVEZETTIPUS szt
  WHERE szt.kod = 'MAB'
;
BEGIN
	funkcio_helper.LATHASSA('AKKREDITACIO','ISMERET_KARBANTARTO');
	funkcio_helper.LATHASSA('AKKREDITACIO','ISMERET_SZERKESZTO');
	funkcio_helper.IRHASSA('AKKREDITACIO','AKKREDITACIO_KARBANTARTO');
	funkcio_helper.IRHASSA('AKKREDITACIO','AKKREDITACIO_SZERKESZTO');
END;
/
