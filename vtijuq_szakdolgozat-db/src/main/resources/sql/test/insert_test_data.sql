BEGIN
	szotar_helper.addSzotar('MERTEKEGYSEG_TIPUS', 'Mértékegység-típus');
		szotar_helper.addElem('MERTEKEGYSEG_TIPUS', 'TOMEG', 'Tömeg');
		szotar_helper.addElem('MERTEKEGYSEG_TIPUS', 'URTARTALOM', 'Űrtartalom');
		szotar_helper.addElem('MERTEKEGYSEG_TIPUS', 'EGYSEG', 'Egység');
		szotar_helper.addElem('MERTEKEGYSEG_TIPUS', 'CSOMAG', 'Csomag');
		szotar_helper.addElem('MERTEKEGYSEG_TIPUS', 'KESZITESI', 'Készítési');

	szotar_helper.addSzotar('HELY', 'Hely');
	UPDATE szotar
		SET ervkezdet = to_date('2016-01-01', 'yyyy-MM-dd'),
			ervveg = to_date('2017-01-01', 'yyyy-MM-dd')
		WHERE idtipus = 0
		  AND kod = 'HELY'
	;
		szotar_helper.addErvenyesElem('HELY', 'K_KONYHA', 'Konyha', to_date('2016-01-01', 'yyyy-MM-dd'), to_date('2017-01-01', 'yyyy-MM-dd'));
			szotar_helper.addErvenyesElem('K_KONYHA', 'K1', 'Konyha1', to_date('2016-01-01', 'yyyy-MM-dd'), to_date('2017-01-01', 'yyyy-MM-dd'));
			szotar_helper.addErvenyesElem('K_KONYHA', 'K2', 'Konyha2', to_date('2016-01-01', 'yyyy-MM-dd'), to_date('2017-01-01', 'yyyy-MM-dd'));
		szotar_helper.addErvenyesElem('HELY', 'R_RAKTAR', 'Raktár', to_date('2016-01-01', 'yyyy-MM-dd'), to_date('2017-01-01', 'yyyy-MM-dd'));
			szotar_helper.addErvenyesElem('R_RAKTAR', 'R1', 'Raktár1', to_date('2016-01-01', 'yyyy-MM-dd'), to_date('2017-01-01', 'yyyy-MM-dd'));
			szotar_helper.addErvenyesElem('R_RAKTAR', 'R2', 'Raktár2', to_date('2016-01-01', 'yyyy-MM-dd'), to_date('2017-01-01', 'yyyy-MM-dd'));
		szotar_helper.addErvenyesElem('HELY', 'M_KAMRA', 'Kamra', to_date('2016-01-01', 'yyyy-MM-dd'), to_date('2017-01-01', 'yyyy-MM-dd'));
			szotar_helper.addErvenyesElem('M_KAMRA', 'M2', 'Kamra2', to_date('2016-01-01', 'yyyy-MM-dd'), to_date('2017-01-01', 'yyyy-MM-dd'));
			szotar_helper.addErvenyesElem('M_KAMRA', 'M1', 'Kamra1', to_date('2016-01-01', 'yyyy-MM-dd'), to_date('2017-01-01', 'yyyy-MM-dd'));

	szotar_helper.addSzotar('RECEPT_TIPUS', 'Recept típus');
		szotar_helper.addElem('RECEPT_TIPUS', 'TESZTAFELE', 'Tésztaféle');
		szotar_helper.addElem('RECEPT_TIPUS', 'DESSZERT', 'Desszert');
		szotar_helper.addElem('RECEPT_TIPUS', 'EGYTALETEL', 'Egytálétel');
		szotar_helper.addElem('RECEPT_TIPUS', 'FOZELEK', 'Főzelék');

	szotar_helper.addSzotar('ALAPANYAG_TIPUS', 'Alapanyag típus');
		szotar_helper.addElem('ALAPANYAG_TIPUS', 'GYUMOLCS', 'Gyümölcs');
			szotar_helper.addElem('GYUMOLCS', 'GYUMOLCSLE', 'Gyümölcslé');
		szotar_helper.addElem('ALAPANYAG_TIPUS', 'ZOLDSEG', 'Zöldség');
			szotar_helper.addElem('ZOLDSEG', 'ZOLDSEGLE', 'Zöldséglé');
		szotar_helper.addElem('ALAPANYAG_TIPUS', 'FUSZER', 'Fűszer');
		szotar_helper.addElem('ALAPANYAG_TIPUS', 'TEJTERMEK', 'Tejtermék');
			szotar_helper.addElem('TEJTERMEK', 'SAJT', 'Sajt');
		szotar_helper.addElem('ALAPANYAG_TIPUS', 'OLAJ', 'Olaj');
		szotar_helper.addElem('ALAPANYAG_TIPUS', 'MARTAS', 'Mártás');
		szotar_helper.addElem('ALAPANYAG_TIPUS', 'EGYEB', 'Egyéb');
		szotar_helper.addElem('ALAPANYAG_TIPUS', 'HUS', 'Hús');
		szotar_helper.addElem('HUS', 'BAROMFI', 'Baromfi');
			szotar_helper.addElem('HUS', 'SERTES', 'Sertés');
			szotar_helper.addElem('HUS', 'MARHA', 'Marha');
			szotar_helper.addElem('HUS', 'EGYEB', 'Egyéb');
		szotar_helper.addElem('ALAPANYAG_TIPUS', 'HAL', 'Hal');
		szotar_helper.addElem('ALAPANYAG_TIPUS', 'ITAL', 'Ital');
		szotar_helper.addElem('ALAPANYAG_TIPUS', 'PEKARU', 'Pékáru');

END;
/

BEGIN
	funkcio_helper.ADDFUNKCIO('TEST', 'Test');
END;
/
