--nem szúrhatunk be még egy gyökeret
insert into SZERVEZET (id, nev, ROVIDITES, IDFOSZERVEZET, idtipus)
  values (1, 'test1', 'test1', null, 0)
;
--nem alkothatunk kört a szervezet-fába
insert into SZERVEZET (id, nev, ROVIDITES, IDFOSZERVEZET, idtipus)
  values (1, 'test1', 'test1', 0, 0)
;

insert into SZERVEZET (id, nev, ROVIDITES, IDFOSZERVEZET, idtipus)
  values (2, 'test2', 'test2', 1, 0)
;

insert into SZERVEZET (id, nev, ROVIDITES, IDFOSZERVEZET, idtipus)
  values (3, 'test3', 'test3', 2, 0)
;

update szervezet
  set IDFOSZERVEZET = 3
  where id = 1
;
--rossz mojo
insert into SZERVEZET (id, nev, ROVIDITES, IDFOSZERVEZET, idtipus)
  select id, nev, ROVIDITES, IDFOSZERVEZET, idtipus
  from (
    (select 1 id, 'test1' nev, 'test1' rovidites, 0 idfoszervezet, 0 idtipus from dual)
    union all
    (select 2, 'test2', 'test2', 1, 0 from dual)
    union all
    (select 3, 'test3', 'test3', 2, 0 from dual)
    )
;

update szervezet
  set IDFOSZERVEZET = 3
  where id = 1
;

--lehetséges
insert into SZERVEZET (id, nev, ROVIDITES, IDFOSZERVEZET, idtipus)
  select id, nev, ROVIDITES, IDFOSZERVEZET, idtipus
  from (
    (select 1 id, 'test1' nev, 'test1' rovidites, 3 idfoszervezet, 0 idtipus from dual)
    union all
    (select 2, 'test2', 'test2', 1, 0 from dual)
    union all
    (select 3, 'test3', 'test3', 2, 0 from dual)
    )
;

delete from szerv_toprend;
delete from szerv_kepzo where id <> 0;
delete from szervezet where id <> 0;



insert into kepzesiterulet(id, kod, nev) VALUES (0, 'test', 'test');

insert into szak(id, idterulet, ciklus, nev) VALUES (0,0, 'B', 'test');

insert into szakkovetelmeny(id, idszak) VALUES (0,0);

insert into konkretszak (id, idszakkovetelmeny, idszervezet) VALUES (0,0,0);

delete from konkretszak;
delete from szakkovetelmeny;
delete from szak;
DELETE FROM kepzesiterulet;
