--nem alkothatunk kört az ismeretgráfban
insert into ismeret (id, KOD, nev, leiras)
  values (1, 'test1', 'test1', 'test1')
;

insert into ismeret (id, KOD, nev, leiras)
  values (2, 'test2', 'test2', 'test2')
;

insert into ismeret (id, KOD, nev, leiras)
  values (3, 'test3', 'test3', 'test3')
;

select ism.id, tr.rendszam
  from ismeret ism
  join toprend tr on tr.id = ism.id
;
--első módszer
insert into ISMERETGRAF (idmi, idhova)
  values (1,2)
;

insert into ISMERETGRAF (idmi, idhova)
  values (2,3)
;

insert into ISMERETGRAF (idmi, idhova)
  values (3,1)
;

--rossz mojo
insert into ISMERETGRAF (idmi, idhova)
  select idmi, idhova
  from (
    (select 1 idmi, 2 idhova from dual)
    union all
    (select 2,3 from dual)
    union all
    (select 3,1 from dual)
  )
;

--toprend jó-e egyébként
insert into ismeret (id, KOD, nev, leiras)
  select id, KOD, nev, leiras
  from (
    (select 1 id, 'test1' kod, 'test1' nev, 'test1' leiras from dual) 
    union all
    (select 2, 'test2', 'test2', 'test2' from dual)
    union all
    (select 3, 'test3', 'test3', 'test3' from dual)
    union all
    (select 4, 'test4', 'test4', 'test4' from dual)
    union all
    (select 5, 'test5', 'test5', 'test5' from dual)
    union all
    (select 6, 'test6', 'test6', 'test6' from dual)
  )
;

insert into ISMERETGRAF (idmi, idhova)
  select idmi, idhova
  from (
    (select 2 idmi, 1 idhova from dual)
    union all
    (select 3,2 from dual)
    union all
    (select 5,4 from dual)
    union all
    (select 6,5 from dual)
    union all
    (select 6,3 from dual)
    union all
    (select 5,2 from dual)
  )
;

select mi.id mi, hova.id hova, mi.rendszam mi_rsz, hova.rendszam hova_rsz
from toprend mi
join ismeretgraf ig on ig.idmi = mi.id
join toprend hova on hova.id = ig.idhova
;

delete from ismeretgraf;
delete from toprend;
delete from ismeret;