CREATE TABLE szotar (
	id        NUMBER(19),
	kod       VARCHAR2(50 CHAR)  NOT NULL,
	nev       VARCHAR2(100 CHAR) NOT NULL,
	leiras    VARCHAR2(1000 CHAR),
	idtipus   NUMBER(19)         NOT NULL,
	ervkezdet DATE,
	ervveg    DATE,
	technikai NUMBER(1, 0),
	PRIMARY KEY (id),
	CONSTRAINT u_szotar_kod_tipus UNIQUE (kod, idtipus)
);
CREATE INDEX ix_szotar_idtipus ON szotar (idtipus ASC);
CREATE SEQUENCE s_szotar START WITH 1 INCREMENT BY 1;

INSERT INTO szotar (id, kod, nev, idtipus, technikai) VALUES (0, 'SZOTAR', 'Szótár gyökér', 0, 1);

CREATE OR REPLACE PACKAGE szotar_helper
IS

	PROCEDURE addSzotar(i_tipus_kod VARCHAR2, i_tipus_nev VARCHAR2);

	PROCEDURE addElem(i_tipus_kod VARCHAR2, i_kod VARCHAR2, i_nev VARCHAR2);

	PROCEDURE addErvenyesElem(i_tipus_kod VARCHAR2, i_kod VARCHAR2, i_nev VARCHAR2, i_ervkezdet DATE, i_ervveg DATE);

END;
/

CREATE OR REPLACE PACKAGE BODY szotar_helper
IS
	PROCEDURE addSzotar(i_tipus_kod VARCHAR2, i_tipus_nev VARCHAR2) IS
		BEGIN
			INSERT INTO szotar (id, kod, nev, idtipus)
				SELECT
					seq_helper.nextId('s_szotar'),
					i_tipus_kod,
					i_tipus_nev,
					0
				FROM dual;
		END;

	PROCEDURE addElem(i_tipus_kod VARCHAR2, i_kod VARCHAR2, i_nev VARCHAR2) IS
		BEGIN
			INSERT INTO szotar (id, kod, nev, idtipus)
				SELECT
					seq_helper.nextId('s_szotar'),
					i_kod,
					i_nev,
					id
				FROM szotar
				WHERE kod = i_tipus_kod;
		END;

	PROCEDURE addErvenyesElem(i_tipus_kod VARCHAR2, i_kod VARCHAR2, i_nev VARCHAR2, i_ervkezdet DATE, i_ervveg DATE) IS
		BEGIN
			INSERT INTO szotar (id, kod, nev, idtipus, ervkezdet, ervveg)
				SELECT
					seq_helper.nextId('s_szotar'),
					i_kod,
					i_nev,
					id,
					i_ervkezdet,
					i_ervveg
				FROM szotar
				WHERE kod = i_tipus_kod;
		END;
END;
/
