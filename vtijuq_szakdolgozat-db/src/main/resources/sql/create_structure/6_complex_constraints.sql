/*
Mentési feltételek
	Ezek a feltételek az adatbázisban közvetlenül is helyet érdemes kapjanak, hiszen a konzisztenciát őrzik.
*/

-- egyszerre csak egy szakkovetelmeny lehet érvényes adott szakhoz
-- DROP MATERIALIZED VIEW mv_atfedoszakkov;
CREATE MATERIALIZED VIEW mv_atfedoszakkov
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM szakkovetelmeny szk1, szakkovetelmeny szk2
	WHERE szk1.id <> szk2.id AND szk1.idszak = szk2.idszak
				AND (szk1.ervkezdet IS NOT NULL AND szk2.ervkezdet <= szk1.ervkezdet AND szk1.ervkezdet <= szk2.ervveg
						 OR szk1.ervveg IS NOT NULL AND szk2.ervkezdet <= szk1.ervveg AND szk1.ervveg <= szk2.ervveg)
;
ALTER TABLE mv_atfedoszakkov
	ADD CONSTRAINT ck_atfedoszakkov
CHECK ( 1 = 0) DEFERRABLE;

-- egyszerre csak egy szakkovetelmeny lehet érvényes adott oklevélbeli megjelöléssel
-- DROP MATERIALIZED VIEW mv_atfedoszakkovmegjel;
CREATE MATERIALIZED VIEW mv_atfedoszakkovmegjel
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM szakkovetelmeny szk1, szakkovetelmeny szk2
	WHERE szk1.id <> szk2.id AND szk1.oklevelmegjeloles = szk2.oklevelmegjeloles
				AND (szk1.ervkezdet IS NOT NULL AND szk2.ervkezdet <= szk1.ervkezdet AND szk1.ervkezdet <= szk2.ervveg
						 OR szk1.ervveg IS NOT NULL AND szk2.ervkezdet <= szk1.ervveg AND szk1.ervveg <= szk2.ervveg)
;
ALTER TABLE mv_atfedoszakkovmegjel
	ADD CONSTRAINT ck_atfedoszakkovmegjel
CHECK ( 1 = 0) DEFERRABLE;

-- egyszerre csak egy akkreditacio lehet érvényes adott konkretszakiranyhoz
-- DROP MATERIALIZED VIEW mv_atfedoakkreditaco;
CREATE MATERIALIZED VIEW mv_atfedoakkreditaco
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM akkreditacio akk1, akkreditacio akk2
	WHERE akk1.id <> akk2.id AND akk1.idkonkretszakirany = akk2.idkonkretszakirany
				AND (akk1.ervkezdet IS NOT NULL AND akk2.ervkezdet <= akk1.ervkezdet AND akk1.ervkezdet <= akk2.ervveg
						 OR akk1.ervveg IS NOT NULL AND akk2.ervkezdet <= akk1.ervveg AND akk1.ervveg <= akk2.ervveg)
;
ALTER TABLE mv_atfedoakkreditaco
	ADD CONSTRAINT ck_atfedoakkreditaco
CHECK ( 1 = 0) DEFERRABLE;

-- szakiranykovetelmeny-szak egyértelműség
-- DROP MATERIALIZED VIEW mv_szakiranykovszaknemegyert;
CREATE MATERIALIZED VIEW mv_szakiranykovszaknemegyert
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM szakiranykovetelmeny szik, szakirany szi, szakkovetelmeny szk
	WHERE szi.id = szik.idszakirany AND szk.id = szik.idszakkovetelmeny
				AND szk.idszak <> szi.idszak
;
ALTER TABLE mv_szakiranykovszaknemegyert
	ADD CONSTRAINT ck_szakiranykovszaknemegyert
CHECK ( 1 = 0) DEFERRABLE;

-- konkretszakirany-szakkovetelmeny egyértelműség
-- DROP MATERIALIZED VIEW mv_kszakiranyszakkovnemegyert;
CREATE MATERIALIZED VIEW mv_kszakiranyszakkovnemegyert
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM konkretszakirany kszi, szakiranykovetelmeny szik, konkretszak ksz
	WHERE szik.id = kszi.idszakiranykovetelmeny AND ksz.id = kszi.idkonkretszak
				AND szik.idszakkovetelmeny <> ksz.idszakkovetelmeny
;
ALTER TABLE mv_kszakiranyszakkovnemegyert
	ADD CONSTRAINT ck_kszakiranyszakkovnemegyert
CHECK ( 1 = 0) DEFERRABLE;

-- azonos (tema,tantargy)-hoz tartozó tematikák nem lehetnek átfedő érvényességűek
-- DROP MATERIALIZED VIEW mv_atfedotematika;
CREATE MATERIALIZED VIEW mv_atfedotematika
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM tematika t1, tematika t2
	WHERE t1.id <> t2.id AND t1.idtema = t2.idtema AND t1.idtantargy = t2.idtantargy
				AND (t1.ervkezdet IS NOT NULL AND t2.ervkezdet <= t1.ervkezdet AND t1.ervkezdet <= t2.ervveg
						 OR t1.ervveg IS NOT NULL AND t2.ervkezdet <= t1.ervveg AND t1.ervveg <= t2.ervveg)
;
ALTER TABLE mv_atfedotematika
	ADD CONSTRAINT ck_atfedotematika
CHECK ( 1 = 0) DEFERRABLE;

-- azonos (konkretszakirany,tantargy)-hoz tartozó tantervek nem lehetnek átfedő érvényességűek
-- DROP MATERIALIZED VIEW mv_atfedotanterv;
CREATE MATERIALIZED VIEW mv_atfedotanterv
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM tanterv t1, tanterv t2
	WHERE t1.id <> t2.id AND t1.idkonkretszakirany = t2.idkonkretszakirany AND t1.idtantargy = t2.idtantargy
				AND (t1.ervkezdet IS NOT NULL AND t2.ervkezdet <= t1.ervkezdet AND t1.ervkezdet <= t2.ervveg
						 OR t1.ervveg IS NOT NULL AND t2.ervkezdet <= t1.ervveg AND t1.ervveg <= t2.ervveg)
;
ALTER TABLE mv_atfedotanterv
	ADD CONSTRAINT ck_atfedotanterv
CHECK ( 1 = 0) DEFERRABLE;

--az egymás alá rendelt szakmaijellemzo-k azonos szakiranykovetelmeny-hez tartoznak
-- DROP MATERIALIZED VIEW mv_foszjmasszakiranykov;
CREATE MATERIALIZED VIEW mv_foszjmasszakiranykov
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM szakmaijellemzo szj, szakmaijellemzo fj
	WHERE szj.idfojellemzo = fj.id
		AND szj.idszakiranykovetelmeny <> fj.idszakiranykovetelmeny
;
ALTER TABLE mv_foszjmasszakiranykov
	ADD CONSTRAINT ck_foszjmasszakiranykov
CHECK ( 1 = 0) DEFERRABLE;

--az ismeretgráf irányított
-- DROP MATERIALIZED VIEW mv_ketiranyelismeretgrafban;
CREATE MATERIALIZED VIEW mv_ketiranyelismeretgrafban
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM ismeretgraf ig1, ismeretgraf ig2
	WHERE ig1.idmi = ig2.idhova
		AND ig2.idmi = ig1.idhova
;
ALTER TABLE mv_ketiranyelismeretgrafban
	ADD CONSTRAINT ck_ketiranyelismeretgrafban
CHECK ( 1 = 0) DEFERRABLE;

-- munkatárs csak szervezetének megfelelő szerepkört kaphat
-- DROP MATERIALIZED VIEW mv_userroletipusnemegyert;
CREATE MATERIALIZED VIEW mv_userroletipusnemegyert
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM munkatars m, szervezet sz, szerepkor szk
	WHERE m.idszerepkor = szk.id AND m.idszervezet = sz.id
				AND szk.idszervezettipus is not null and sz.idtipus <> szk.idszervezettipus
;
ALTER TABLE mv_userroletipusnemegyert
	ADD CONSTRAINT ck_userroletipusnemegyert
CHECK ( 1 = 0) DEFERRABLE;
-- téma nem maradhat gyerek nélkül
-- DROP MATERIALIZED VIEW mv_tematematikaszam;
CREATE MATERIALIZED VIEW mv_tematematikaszam
REFRESH FORCE ON COMMIT AS
	SELECT
		tema.id     idtema,
		sum(case when tematika.idtema = tema.id then 1 else 0 end) tematikaszam
	FROM tema, tematika
	GROUP BY tema.id
;
ALTER TABLE mv_tematematikaszam
	ADD CONSTRAINT ck_gyermektelentema
CHECK ( tematikaszam > 0) DEFERRABLE;
--nem valid követelményekhez nem tartozhat valid akkreditacio
-- DROP MATERIALIZED VIEW mv_invalid_kov_valid_akkred;
CREATE MATERIALIZED VIEW mv_invalid_kov_valid_akkred
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM akkreditacio akk, konkretszakirany kszi, szakiranykovetelmeny szik, szakkovetelmeny szk
	WHERE akk.idkonkretszakirany = kszi.id
		AND kszi.idszakiranykovetelmeny = szik.id
		AND szk.id = szik.idszakkovetelmeny
	  AND akk.valid = 1 AND szk.valid = 0
;
ALTER TABLE mv_invalid_kov_valid_akkred
	ADD CONSTRAINT ck_invalid_kov_valid_akkred
CHECK ( 1 = 0) DEFERRABLE;
--nem valid tanterv nem metszheti kapcsolódó valid akkreditáció érvényességének határait
-- DROP MATERIALIZED VIEW mv_inv_tanterv_valid_akkred;
CREATE MATERIALIZED VIEW mv_inv_tanterv_valid_akkred
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM akkreditacio akk, tanterv
	WHERE akk.idkonkretszakirany = tanterv.idkonkretszakirany
				AND (tanterv.ervkezdet <= akk.ervkezdet AND akk.ervkezdet <= tanterv.ervveg)
				AND (tanterv.ervkezdet <= akk.ervveg AND akk.ervveg <= tanterv.ervveg)
				AND akk.valid = 1 AND tanterv.valid = 0
;
ALTER TABLE mv_inv_tanterv_valid_akkred
	ADD CONSTRAINT ck_inv_tanterv_valid_akkred
CHECK ( 1 = 0) DEFERRABLE;
--nem valid tematika nem metszheti kapcsolódó valid tanterv érvényességének határait
-- DROP MATERIALIZED VIEW mv_inv_tematika_valid_tanterv;
CREATE MATERIALIZED VIEW mv_inv_tematika_valid_tanterv
REFRESH FORCE ON COMMIT AS
	SELECT 1
	FROM tanterv, tematika
	WHERE tanterv.idtantargy = tematika.idtantargy
				AND (tematika.ervkezdet <= tanterv.ervkezdet AND tanterv.ervkezdet <= tematika.ervveg)
				AND (tematika.ervkezdet <= tanterv.ervveg AND tanterv.ervveg <= tematika.ervveg)
				AND tanterv.valid = 1 AND tematika.valid = 0
;
ALTER TABLE mv_inv_tematika_valid_tanterv
	ADD CONSTRAINT ck_inv_tematika_valid_tanterv
CHECK ( 1 = 0) DEFERRABLE;

