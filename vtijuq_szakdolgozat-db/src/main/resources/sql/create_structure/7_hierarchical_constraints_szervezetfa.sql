/*
Mentési feltételek
	Ezek a feltételek az adatbázisban közvetlenül is helyet érdemes kapjanak, hiszen a konzisztenciát őrzik.
	A többinél bonyolultabbak és eltérő megoldással ellenőrzöttek, így külön script-ben kaptak helyet.

  általában kell:
	  egy temporary table a tranzakcióban érintett, ellenőrizendő adatoknak
	  egy after each row trigger, ami tölti a temporary table-t
	  egy materialized view, mely frissíttethető a megfelelő tranzakciók végén
	  egy before statement trigger a materialized view-n, mellyel elvégezzük a karbantartást

  a szervezethierarchia fa létének biztosításáshoz kicsit más kell, mint a körmentesség szavatolásához
	a szervezet táblán van check, hogy csak egy igazi gyökér-elem legyen benne,
	az is a 0 id-jú ezek után már csak azt kell vizsgálnunk,
	hogy minden szervezetből el lehet jutni a gyökérbe a főszervezet-kapcsolatokon keresztül

  az egyes szervezetek képző jellegét letárolhatjuk a szervezet táblában
    kevésszer frissülő számolt adat, később lehet, hogy nem számolttá válik és csak default értéke lesz

  kell még:
    két materialized view ami lekéri a nem intézményi szervezetekhez kapcsolódó tanterv-eket, konkretszak-okat
    soha nem teljesülő check-k a materialized view-kon

  ezzel a konstrukcióval számolást megspórolunk,
    hiszen az intézményiséget csak szervezet-beszúrásnál, -változásnál számoljuk
    tanterv, konkretszak beszúrásnál, változásnál csak ellenőrzünk

  a karbantartást végző materialized view-ra kötött trigger
  	nem változtathat a materialized view szülőtáblájában adatot,
  	így a képző státuszt is segédtáblába kell kiszervezni

*/
--segédtábla szervezetek intézményiségének frissíthető tárolására
--DROP TABLE szerv_kepzo;
CREATE TABLE szerv_kepzo (
	id            NUMBER(19, 0) PRIMARY KEY REFERENCES szervezet (id),
	kepzo NUMBER(1, 0)
);

--segédtábla a topologikus rendezéshez
--DROP TABLE szerv_toprend;
CREATE TABLE szerv_toprend (
	id            NUMBER(19, 0) PRIMARY KEY REFERENCES szervezet (id),
	idfoszervezet NUMBER(19, 0) REFERENCES szervezet (id),
	rendszam      NUMBER(19, 0) DEFAULT 0
);

--DROP INDEX ix_szerv_toprend;
CREATE INDEX ix_szerv_toprend ON szerv_toprend (rendszam);

--temporary table az ideiglenes tároláshoz
--DROP TABLE tmp_szerv;
CREATE GLOBAL TEMPORARY TABLE tmp_szerv (
	id            NUMBER(19, 0),
	idfoszervezet NUMBER(19, 0),
	idtipus       NUMBER(19, 0)
)
ON COMMIT DELETE ROWS
;

--DROP TRIGGER tr_temp_szerv_tolt;
CREATE OR REPLACE TRIGGER tr_temp_szerv_tolt
	AFTER INSERT
		OR UPDATE OF idfoszervezet
		OR UPDATE OF idtipus
	ON szervezet
	FOR EACH ROW
	BEGIN
		INSERT INTO tmp_szerv (id, idfoszervezet, idtipus)
			VALUES (:new.id, :new.idfoszervezet, :new.idtipus)
		;
		INSERT INTO szerv_toprend(id, idfoszervezet)
			VALUES (:new.id, :new.idfoszervezet)
		;
		INSERT INTO szerv_kepzo(id)
			VALUES (:new.id)
		;
	END
;

--DROP TRIGGER tr_temp_szerv_torol;
CREATE OR REPLACE TRIGGER tr_temp_szerv_torol
	BEFORE DELETE
	ON szervezet
	FOR EACH ROW
	BEGIN
		DELETE FROM szerv_toprend
			WHERE id = :old.id
		;
		DELETE FROM szerv_kepzo
			WHERE id = :old.id
		;
	END
;

--robosztusabbá kell tenni, hierarchikus lekérdezés kell hozzá:
-- elérhető-e egy INTEZMENY szervezet főszervezet kapcsolatokon keresztül
--a szervezet-hierarchia körmentes
--DROP PACKAGE karbantartas_szervezet;
CREATE OR REPLACE PACKAGE karbantartas_szervezet
AS
	PROCEDURE topologikus_rendezes;

	PROCEDURE kepzo_toltes;
END;

CREATE OR REPLACE PACKAGE BODY karbantartas_szervezet AS
	PROCEDURE topologikus_rendezes IS
		BEGIN
		DECLARE
			rossz_toprend NUMBER(1, 0) := 0;
		BEGIN
			MERGE INTO szerv_toprend
			USING (
					  WITH toprend_start (id, rendszam) AS (
						  --a rendezés frissítését, a változás helyén kezdi, ha el is rontotta a rendezést
						  -- friss kapcsolatokat is figyelembe véve
						  -- javaslatot is számol az új rendszam-hoz
							  SELECT
								  fszr.id, max(szr.rendszam + 1) rendszam
							  FROM szerv_toprend fszr
								  JOIN tmp_szerv tmp
									  ON tmp.idfoszervezet = fszr.id
								  JOIN szerv_toprend szr
									  ON szr.id = tmp.id
										 AND fszr.rendszam <= szr.rendszam
							  GROUP BY fszr.id
					  ), toprend_javasol (id, rendszam) AS (
						  --az új rendezés számolása
						  (SELECT
							   ts.id, ts.rendszam
						   FROM toprend_start ts
						  )
						  UNION ALL (
							  --új toprend javaslása
							  SELECT
								  fszr.id, tj.rendszam + 1 rendszam
							  FROM toprend_javasol tj
								  JOIN szerv_toprend sz
									  ON sz.id = tj.id
								  JOIN szerv_toprend fszr
									  ON fszr.id = sz.idfoszervezet
										 --kifordult rendezések mentén tovább megy
										 AND fszr.rendszam <= tj.rendszam
						  )
					  )
						  SEARCH DEPTH FIRST BY id SET seq
						  --ha kört talál jelzi, és nem hibára fut
						  CYCLE id SET cycle TO 1 DEFAULT 0
					  , toprend_kor (id, rendszam) AS (
						  --biztos hibás toprend a körökbe
							  SELECT DISTINCT
								  id, -1
							  FROM toprend_javasol
							  WHERE cycle = 1
					  ), toprend_max (id, rendszam) AS (
						  --veszi a javaslatok közül a legnagyobbat
							  SELECT
								  id, max(rendszam) rendszam
							  FROM toprend_javasol
							  GROUP BY id
					  )
					  --a körökbe biztos hibás toprend kerül,
					  -- a többiekbe a frissen számolt
					  SELECT
						  trm.id,
						  coalesce(trk.rendszam, trm.rendszam) rendszam
					  FROM toprend_max trm
						  LEFT JOIN toprend_kor trk
							  ON trk.id = trm.id
				  ) toprendvalt
			ON (toprendvalt.id = szerv_toprend.id)
			WHEN MATCHED THEN UPDATE
			SET szerv_toprend.rendszam = toprendvalt.rendszam
			;

			SELECT CASE WHEN exists(
					SELECT 1
					FROM szervezet sz, szerv_toprend szr, szerv_toprend fszr
					WHERE szr.id = sz.id AND fszr.id = sz.idfoszervezet
						  AND fszr.rendszam <= szr.rendszam)
				THEN 1
				   ELSE 0 END
			INTO rossz_toprend
			FROM dual
			;

			IF (rossz_toprend = 1)
			THEN
				raise_application_error(-20001, 'Az szervezethierarchia megszűnt fa lenni!')
				;
			END IF
			;
		END;
		END
	;
	PROCEDURE kepzo_toltes IS
		BEGIN
			MERGE INTO szerv_kepzo
			USING (
				WITH ik_start (id, tipus) AS (
				  --a frissítést, a változás helyén kezdi
				  -- potenciálisan változott az alattuk levő szervezetek státusza is
						SELECT tmp.id, t.kod tipus
						FROM tmp_szerv tmp
						JOIN szervezettipus t ON t.id = tmp.idtipus
				), intezmeny_keres (id, tipus, id_start) AS (
					--INTEZMENY típusú főszervezet keresése
					  (
						  SELECT ks.id, ks.tipus, ks.id id_start
						  FROM ik_start ks
					  )
					  UNION ALL
					  (
						  SELECT
							  fsz.id, fszt.kod tipus,
							  ik.id_start
						  FROM intezmeny_keres ik
							  JOIN szervezet sz ON sz.id = ik.id
								--INTEZMENY-kből nem megy tovább
								AND ik.tipus <> 'INTEZMENY'
							  JOIN szervezet fsz ON fsz.id = sz.idfoszervezet
							  JOIN szervezettipus fszt ON fszt.id = sz.idtipus

					)
				)
				  SEARCH DEPTH FIRST BY id SET seq
					--ha kört talál jelzi, és nem hibára fut
				  CYCLE id SET cycle TO 1 DEFAULT 0
				, frissitendo_keres (id, id_start) AS (
					--frissítendo alszervezetek keresése
					(
						SELECT ks.id, ks.id id_start
						FROM ik_start ks
						LEFT JOIN szerv_kepzo szk ON szk.id = ks.id
						--nincs felette másik, ami változott
						WHERE NOT exists(SELECT 1
							FROM intezmeny_keres ik
							JOIN ik_start iks ON iks.id = ik.id AND ik.id <> ik.id_start
							WHERE ik.id_start = ks.id)
						AND (szk.kepzo IS NULL--új szervezet
							--képző, de nincs felette INTEZMENY
							 OR szk.kepzo = 1 AND NOT exists(SELECT 1
															 FROM intezmeny_keres ik
															 WHERE ik.id_start = ks.id
															   AND ik.tipus = 'INTEZMENY')
							--nem képző, de van felette INTEZMENY
							 OR szk.kepzo = 0 AND exists(SELECT 1
												 		 FROM intezmeny_keres ik
														 WHERE ik.id_start = ks.id
														   AND ik.tipus = 'INTEZMENY')
							  )
					)
					UNION ALL
					(
						SELECT
							asz.id,
							fk.id_start
						FROM frissitendo_keres fk
							JOIN szervezet sz ON sz.id = fk.id
							JOIN szervezet asz ON asz.idfoszervezet = sz.id
							JOIN szervezettipus aszt ON aszt.id = sz.idtipus
							WHERE aszt.kod <> 'INTEZMENY'
					)
				)
					SEARCH BREADTH FIRST BY id SET seq
					--ha kört talál jelzi, és nem hibára fut
					CYCLE id SET cycle TO 1 DEFAULT 0
				--ha van felette INTEZMENY, kepzo lesz
				SELECT
				  fk.id,
				  CASE WHEN exists(SELECT 1
							FROM intezmeny_keres ik
				  			WHERE ik.id_start = fk.id_start
							  AND ik.tipus = 'INTEZMENY')
					  THEN 1 ELSE 0 END kepzo
				FROM frissitendo_keres fk
		  	) kepzovalt
			ON (kepzovalt.id = szerv_kepzo.id)
			WHEN MATCHED THEN UPDATE
			SET szerv_kepzo.kepzo = kepzovalt.kepzo
			;
		END
	;
END;

--DROP MATERIALIZED VIEW mv_szervezet_belso;
CREATE MATERIALIZED VIEW mv_szervezet_belso
	REFRESH FORCE ON COMMIT
	AS
		SELECT sz.id
		FROM szervezet sz, szervezet asz
		WHERE sz.id = asz.idfoszervezet AND sz.idfoszervezet IS NOT NULL
;

--DROP TRIGGER tr_szerv_karbantartas;
CREATE OR REPLACE TRIGGER tr_szerv_karbantartas
	BEFORE INSERT OR UPDATE
	ON mv_szervezet_belso
	BEGIN
		karbantartas_szervezet.topologikus_rendezes();
		karbantartas_szervezet.kepzo_toltes();
	END
;

--csak kepzo szervezethez tartozhat konkretszak
--DROP TRIGGER tr_konkretszak_nem_kepzo;
CREATE OR REPLACE TRIGGER tr_konkretszak_nem_kepzo
	BEFORE INSERT OR UPDATE
	ON konkretszak
	FOR EACH ROW
	DECLARE
		kepzo NUMBER(1,0) := 0;
	BEGIN
		SELECT kepzo
			INTO kepzo
			FROM szerv_kepzo sz
			WHERE sz.id = :new.idszervezet
		;

		IF (kepzo <> 1) THEN
				raise_application_error(-20001, 'Nem INTEZMENY alatti szervezethez nem rendelhető konkrét szak!');
		END IF;
	END
;

--csak kepzo szervezethez tartozhat tantargy
--DROP TRIGGER tr_tantargy_nem_kepzo;
CREATE OR REPLACE TRIGGER tr_tantargy_nem_kepzo
BEFORE INSERT OR UPDATE
	ON tantargy
FOR EACH ROW
	DECLARE
		kepzo_hiba NUMBER(1,0) := 0;
	BEGIN
		SELECT case
				when :new.idszervezet is not null
					and exists(select 1 from szerv_kepzo sz where sz.id = :new.idszervezet and sz.kepzo =1)
				then 0 else 1 end
			INTO kepzo_hiba from dual
		;

		IF (kepzo_hiba = 1) THEN
			raise_application_error(-20001, 'Nem INTEZMENY alatti szervezethez nem rendelhető tantárgy!');
		END IF;
	END
;

--csak kepzo szervezethez tartozó tantargy szerepelhet tantervben
--DROP TRIGGER tr_tanterv_nem_kepzo;
CREATE OR REPLACE TRIGGER tr_tanterv_nem_kepzo
BEFORE INSERT OR UPDATE
	ON tanterv
FOR EACH ROW
	DECLARE
		szervezet_id NUMBER(19,0) := 0;
		kepzo_hiba NUMBER(1,0) := 0;
	BEGIN
		select t.idszervezet into szervezet_id
			from tantargy t
			WHERE t.id = :new.idtantargy
		;
		SELECT case
				when szervezet_id is not null
					and exists (select 1 from szerv_kepzo sz where sz.id = szervezet_id and sz.kepzo = 1)
				then 0 else 1 end
			INTO kepzo_hiba from dual
		;

		IF (kepzo_hiba = 1) THEN
			raise_application_error(-20001, 'Nem INTEZMENY alatti szervezethez nem rendelhető tantervi bejegyzés!');
		END IF;
	END
;
