--követelményekhez tartozó domain
/*

	id          NUMBER(19,0)
	ervkezdet	DATE
	ervveg		DATE
	kod         VARCHAR2(50 CHAR)
	nev         VARCHAR2(100 CHAR)
	leiras      VARCHAR2(1000 CHAR)
	valid       NUMBER(1,0)
	vegleges    NUMBER(1,0)
	...kredit	NUMBER(3,0)

*/
--kepzesiterulet(uuline{id}, uline{nev}, uwave{kod})
CREATE TABLE kepzesiterulet (
	id     NUMBER(19, 0) PRIMARY KEY,
	kod    VARCHAR2(50 CHAR)  NOT NULL UNIQUE,
	nev    VARCHAR2(100 CHAR) NOT NULL UNIQUE
);
CREATE SEQUENCE s_kepzesiterulet START WITH 1 INCREMENT BY 1;

--szak(uuline{id}, uline{nev, idterulet, ciklus})
/*
szak
    ciklus  VARCHAR2(1 CHAR)
*/
CREATE TABLE szak (
	id        NUMBER(19, 0) PRIMARY KEY,
	nev       VARCHAR2(100 CHAR) NOT NULL,
	idterulet NUMBER(19, 0)      NOT NULL REFERENCES kepzesiterulet (id),
	ciklus    VARCHAR2(1 CHAR)   NOT NULL,
	CONSTRAINT u_szak UNIQUE (idterulet, nev, ciklus)
);
CREATE SEQUENCE s_szak START WITH 1 INCREMENT BY 1;

--szakirany(uuline{id}, uline{nev, idszak}, leiras)
CREATE TABLE szakirany (
	id     NUMBER(19, 0) PRIMARY KEY,
	idszak NUMBER(19, 0)      NOT NULL REFERENCES szak (id),
	nev    VARCHAR2(100 CHAR) NOT NULL,
	leiras VARCHAR2(1000 CHAR),
	CONSTRAINT u_szakirany UNIQUE (idszak, nev)
);
CREATE SEQUENCE s_szakirany START WITH 1 INCREMENT BY 1;

--szakkovetelmeny(uuline{id}, idszak, ervkezdet, ervveg,
--                oklevelmegjeloles, felevek,
--                osszkredit, szakdolgozatkredit, szabvalminkredit,
--                szakiranyminkredit, szakiranymaxkredit, szakmaigyakminkredit,
--                szakmaigyakeloirasok, valid, vegleges)
/*
szakkovetelmeny
    oklevelmegjeloles   	VARCHAR2(100 CHAR)
    felevek             	NUMBER(2,0)
    szakmaigyakeloirasok    VARCHAR2(2000 CHAR)
*/
CREATE TABLE szakkovetelmeny (
	id                   NUMBER(19, 0) PRIMARY KEY,
	idszak               NUMBER(19, 0)       NOT NULL REFERENCES szak (id),
	ervkezdet            DATE,
	ervveg               DATE,
	valid                NUMBER(1,0) DEFAULT 0 NOT NULL,
	vegleges             NUMBER(1,0) DEFAULT 0 NOT NULL,
	oklevelmegjeloles    VARCHAR2(100 CHAR),
	felevek              NUMBER(2, 0),
	osszkredit           NUMBER(3, 0),
	szakdolgozatkredit   NUMBER(3, 0),
	szakmaigyakminkredit NUMBER(3, 0),
	szabvalminkredit     NUMBER(3, 0),
	szakiranyminkredit   NUMBER(3, 0),
	szakiranymaxkredit   NUMBER(3, 0),
	szakmaigyakeloirasok VARCHAR2(2000 CHAR),
	CONSTRAINT ck_szakkov_szkirany_kredit CHECK (szakiranyminkredit is null or szakiranymaxkredit is null or szakiranyminkredit <= szakiranymaxkredit),
	CONSTRAINT ck_szakkov_erv CHECK (ervkezdet is NULL or ervveg is null or ervkezdet < ervveg),
	CONSTRAINT ck_szakkov_valid CHECK (vegleges <> 1 or valid = 1)
);
CREATE SEQUENCE s_szakkovetelmeny START WITH 1 INCREMENT BY 1;

CREATE INDEX ix_szakkovstart ON szakkovetelmeny(idszak, ervkezdet, valid, vegleges);
CREATE INDEX ix_szakkovinterval ON szakkovetelmeny(idszak, ervkezdet, ervveg, valid, vegleges);
CREATE INDEX ix_szakkovnevstart ON szakkovetelmeny(oklevelmegjeloles, ervkezdet, valid, vegleges);
CREATE INDEX ix_szakkovnevinterval ON szakkovetelmeny(oklevelmegjeloles, ervkezdet, ervveg, valid, vegleges);
CREATE INDEX ix_szakkovstarterv ON szakkovetelmeny(vegleges, ervkezdet, idszak);
CREATE INDEX ix_szakkovintervalerv ON szakkovetelmeny(vegleges, ervkezdet, ervveg, idszak);
CREATE INDEX ix_szakkovnevstarterv ON szakkovetelmeny(vegleges, ervkezdet, oklevelmegjeloles);
CREATE INDEX ix_szakkovnevintervalerv ON szakkovetelmeny(vegleges, ervkezdet, ervveg, oklevelmegjeloles);

--szakiranykovetelmeny(uuline{id}, uline{idszakkovetelmeny, idszakirany}, csakkozos)
/*
szakiranykovetelmeny
	csakkozos  				NUMBER(1,0)
*/
--szakiranykovetelmeny-nél csak null értékű idszakirany (ekkor alap), esetén lehet csak közös ez egy egyszerű check constraint beszúráskor, módosításkor
create TABLE szakiranykovetelmeny(
	id                NUMBER(19, 0) PRIMARY KEY,
	idszakkovetelmeny NUMBER(19, 0) NOT NULL REFERENCES szakkovetelmeny (id),
	idszakirany       NUMBER(19, 0) NULL REFERENCES szakirany (id),
	csakkozos         NUMBER(1) DEFAULT 0 NOT NULL,
	CONSTRAINT ck_szakiranykov_alap_kozos CHECK (csakkozos <> 1 OR idszakirany IS NULL),
	CONSTRAINT u_szakiranykov UNIQUE (idszakkovetelmeny, idszakirany)
);
CREATE SEQUENCE s_szakiranykovetelmeny START WITH 1 INCREMENT BY 1;
CREATE INDEX ix_szakiranykovkozos ON szakiranykovetelmeny (csakkozos, idszakkovetelmeny);

--ismeret(uuline{id}, uwave{nev, leiras}, uline{kod})
CREATE TABLE ismeret (
	id     NUMBER(19, 0) PRIMARY KEY,
	nev    VARCHAR2(100 CHAR) NOT NULL UNIQUE,
	leiras VARCHAR2(1000 CHAR),
	kod    VARCHAR2(50 CHAR)  NOT NULL UNIQUE
);
CREATE SEQUENCE s_ismeret START WITH 1 INCREMENT BY 1;

--ismeretgraf(uuline{idmi, idhova})
create TABLE ismeretgraf(
	idmi   NUMBER(19, 0) NOT NULL REFERENCES ismeret (id),
	idhova NUMBER(19, 0) NOT NULL REFERENCES ismeret (id),
	CONSTRAINT pk_ismeretgraf PRIMARY KEY (idmi, idhova),
	CONSTRAINT ck_ismeretgraf_nohurok CHECK (idmi <> idhova)
);

--szakmaijellemzo(uuline{id}, uline{idszakiranykovetelmeny, idismeret}, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
/*
szakmaijellemzo
	minvalaszt              NUMBER(2,0)
*/
CREATE TABLE szakmaijellemzo (
	id                     NUMBER(19, 0) PRIMARY KEY,
	idszakiranykovetelmeny NUMBER(19, 0) NOT NULL REFERENCES szakiranykovetelmeny (id),
	idismeret              NUMBER(19, 0) NOT NULL REFERENCES ismeret (id),
	idfojellemzo           NUMBER(19, 0) NULL REFERENCES szakmaijellemzo (id),
	nev                    VARCHAR2(100 CHAR),
	minkredit              NUMBER(3, 0),
	maxkredit              NUMBER(3, 0),
	minvalaszt             NUMBER(2, 0),
	CONSTRAINT u_szakmaijellemzo UNIQUE (idszakiranykovetelmeny, idismeret),
	CONSTRAINT ck_szj_nohurok CHECK (idfojellemzo IS NULL OR idfojellemzo <> id),
	CONSTRAINT ck_szj_kredit CHECK (minkredit IS NULL OR maxkredit IS NULL OR minkredit <= maxkredit)
);
CREATE SEQUENCE s_szakmaijellemzo START WITH 1 INCREMENT BY 1;
CREATE INDEX ix_szakmaijellemzohier ON szakmaijellemzo (idfojellemzo, id);
CREATE INDEX ix_szakmaijellemzoism ON szakmaijellemzo (idismeret, idfojellemzo, idszakiranykovetelmeny);
