--akkreditációhoz tartozó domain
/*
akkreditacio(uuline{id}, idkonkretszakirany, ervkezdet, ervveg, valid, vegleges)

	id          NUMBER(19,0)
	ervkezdet	DATE
	ervveg		DATE
	kod         VARCHAR2(50 CHAR)
	nev         VARCHAR2(100 CHAR)
	leiras      VARCHAR2(1000 CHAR)
	valid       NUMBER(1,0)
	vegleges    NUMBER(1,0)
	...kredit	NUMBER(3,0)
*/
CREATE TABLE akkreditacio (
	id                     NUMBER(19, 0) PRIMARY KEY,
	idkonkretszakirany     NUMBER(19, 0) NOT NULL REFERENCES konkretszakirany (id),
	ervkezdet              DATE,
	ervveg                 DATE,
	valid                  NUMBER(1) DEFAULT 0 NOT NULL,
	vegleges               NUMBER(1) DEFAULT 0 NOT NULL,
	CONSTRAINT ck_akkred_erv CHECK (ervkezdet is NULL or ervveg is null or ervkezdet < ervveg),
	CONSTRAINT ck_akkred_valid CHECK (vegleges <> 1 or valid = 1)
);
CREATE SEQUENCE s_akkreditacio START WITH 1 INCREMENT BY 1;

CREATE INDEX ix_akkredstart ON akkreditacio(idkonkretszakirany, ervkezdet, valid, vegleges);
CREATE INDEX ix_akkredinterval ON akkreditacio(idkonkretszakirany, ervkezdet, ervveg, valid, vegleges);
CREATE INDEX ix_akkredstarterv ON akkreditacio(vegleges, ervkezdet, idkonkretszakirany);
CREATE INDEX ix_akkredintervalerv ON akkreditacio(vegleges, ervkezdet, ervveg, idkonkretszakirany);
