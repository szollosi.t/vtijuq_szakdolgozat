--adminisztatív domain
/*
	id          NUMBER(19,0)
	ervkezdet	DATE
	ervveg		DATE
	kod         VARCHAR2(50 CHAR)
	nev         VARCHAR2(100 CHAR)
	leiras      VARCHAR2(1000 CHAR)
	valid       NUMBER(1,0)
	vegleges    NUMBER(1,0)
	...kredit	NUMBER(3,0)

*/
--szervezettipus((uuline{id}, uline{nev}, uwave{kod})
CREATE TABLE szervezettipus (
	id  NUMBER(19, 0) PRIMARY KEY,
	nev VARCHAR2(100 CHAR) NOT NULL UNIQUE,
	kod VARCHAR2(50 CHAR) NOT NULL UNIQUE
);
CREATE SEQUENCE s_szervezettipus START WITH 1 INCREMENT BY 1;
--szervezet(uuline{id}, uline{nev, uwave{idfoszervezet}}uwave{, rovidites}, idtipus, cim)
/*
szervezet
	tipus                   VARCHAR2(20 CHAR)
	rovidites               VARCHAR2(50 CHAR)
	cim                     VARCHAR2(400 CHAR)
*/
CREATE TABLE szervezet (
	id            NUMBER(19, 0) PRIMARY KEY,
	idfoszervezet NUMBER(19, 0) NULL REFERENCES szervezet (id),
	nev           VARCHAR2(100 CHAR) NOT NULL,
	rovidites     VARCHAR2(50 CHAR),
	idtipus       NUMBER(19, 0)      NOT NULL REFERENCES szervezettipus (id),
	cim           VARCHAR2(400 CHAR),
	CONSTRAINT u_szervezet_kod UNIQUE (idfoszervezet, rovidites),
	CONSTRAINT u_szervezet_nev UNIQUE (idfoszervezet, nev),
	CONSTRAINT ck_szervezet_fa CHECK (id = 0 AND idfoszervezet IS NULL AND idtipus = 0 OR id <> 0 AND idfoszervezet IS NOT NULL),
	CONSTRAINT ck_szervezet_nohurok CHECK (idfoszervezet <> id)
);
CREATE SEQUENCE s_szervezet START WITH 1 INCREMENT BY 1;
CREATE INDEX ix_szervezet_hier ON szervezet (idfoszervezet, id);

--szerepkor(uuline{id}, uline{nev}, uwave{kod})
CREATE TABLE szerepkor (
	id  NUMBER(19, 0) PRIMARY KEY,
	idszervezettipus NUMBER(19, 0) NULL REFERENCES szervezettipus(id),
	nev VARCHAR2(100 CHAR) NOT NULL UNIQUE,
	kod VARCHAR2(50 CHAR)  NOT NULL UNIQUE
);
CREATE SEQUENCE s_szerepkor START WITH 1 INCREMENT BY 1;

--munkatars(uuline{id}, uline{felhasznalonev}, nev, idszervezet, idszerepkor, jelszo, salt, aktiv, jelszovaltoztatas)
/*
munkatars
	felhasznalonev          VARCHAR2(50 CHAR)
	jelszo                  VARCHAR2(254 BYTE)
	salt                    VARCHAR2(254 BYTE)
	aktiv                   NUMBER(1,0)
*/
CREATE TABLE munkatars (
	id                NUMBER(19, 0) PRIMARY KEY,
	felhasznalonev    VARCHAR2(50 CHAR)  NOT NULL UNIQUE,
	nev               VARCHAR2(100 CHAR) NOT NULL,
	idszervezet       NUMBER(19, 0) REFERENCES szervezet (id),
	idszerepkor       NUMBER(19, 0) REFERENCES szerepkor (id),
	jelszo            VARCHAR2(500 CHAR) NOT NULL,
	salt              VARCHAR2(500 CHAR),
	aktiv             NUMBER(1) DEFAULT 0 NOT NULL,
	jelszovaltoztatas NUMBER(1) DEFAULT 1 NOT NULL
);
CREATE SEQUENCE s_munkatars START WITH 1 INCREMENT BY 1;
CREATE INDEX ix_aktiv_munkatars ON munkatars (aktiv, felhasznalonev);

--funkcio(uuline{id}, uline{nev}, uwave{kod})
CREATE TABLE funkcio (
	id  NUMBER(19, 0) PRIMARY KEY,
	nev VARCHAR2(100 CHAR) NOT NULL UNIQUE,
	kod VARCHAR2(50 CHAR)  NOT NULL UNIQUE
);
CREATE SEQUENCE s_funkcio START WITH 1 INCREMENT BY 1;

--jogosultsag(uuline{id}, uline{idfunkcio, idszerepkor}, szint)
/*
jogosultsag
	szint                   VARCHAR2(10 CHAR)
*/
CREATE TABLE jogosultsag (
	id          NUMBER(19, 0) PRIMARY KEY,
	idszerepkor NUMBER(19, 0) NOT NULL REFERENCES szerepkor (id),
	idfunkcio   NUMBER(19, 0) NOT NULL REFERENCES funkcio (id),
	szint       VARCHAR2(10 CHAR) NOT NULL,
	CONSTRAINT u_jogosultsag UNIQUE (idszerepkor, idfunkcio)
);
CREATE SEQUENCE s_jogosultsag START WITH 1 INCREMENT BY 1;

--funckió-helper
CREATE OR REPLACE PACKAGE funkcio_helper
IS

	PROCEDURE addFunkcio(i_funkcio_kod VARCHAR2, i_funkcio_nev VARCHAR2);

	PROCEDURE irhassa(i_szerepkor_kod VARCHAR2, i_funkcio_kod VARCHAR2);

	PROCEDURE lathassa(i_szerepkor_kod VARCHAR2, i_funkcio_kod VARCHAR2);

	PROCEDURE remFunkcio(i_funkcio_kod VARCHAR2);

END;
/

CREATE OR REPLACE PACKAGE BODY funkcio_helper
IS
	PROCEDURE addFunkcio(i_funkcio_kod VARCHAR2, i_funkcio_nev VARCHAR2) IS
		BEGIN
			INSERT INTO funkcio (ID, KOD, NEV)
				SELECT seq_helper.nextId('S_funkcio'), i_funkcio_kod, i_funkcio_nev
					FROM dual
			;
			INSERT INTO jogosultsag (ID, IDSZEREPKOR, IDFUNKCIO, SZINT)
				SELECT seq_helper.nextId('s_jogosultsag'), 0, f.id, 'IRHATO'
				FROM funkcio f
				WHERE f.kod= i_funkcio_kod
			;
		END;

	PROCEDURE irhassa(i_szerepkor_kod VARCHAR2, i_funkcio_kod VARCHAR2) IS
		  vanJoga NUMBER;
		BEGIN
			SELECT CASE WHEN exists(SELECT 1
				FROM jogosultsag j
					JOIN szerepkor sz ON sz.id = j.idszerepkor AND sz.kod = i_szerepkor_kod
					JOIN funkcio f ON f.id = j.idfunkcio AND f.kod = i_funkcio_kod
			) THEN 1 ELSE 0 END
			INTO vanJoga
			FROM dual
			;
			IF(vanJoga = 0)
				THEN
					INSERT INTO jogosultsag(ID, IDSZEREPKOR, IDFUNKCIO, SZINT)
						SELECT seq_helper.nextId('s_jogosultsag'), sz.id, f.id, 'IRHATO'
						FROM szerepkor sz, funkcio f
						WHERE sz.kod = i_szerepkor_kod
							AND f.kod = i_funkcio_kod
					;
				ELSE
					UPDATE jogosultsag SET szint = 'IRHATO'
						WHERE idszerepkor = (SELECT id FROM szerepkor WHERE kod = i_szerepkor_kod)
							AND idfunkcio = (SELECT id FROM funkcio WHERE kod = i_funkcio_kod)
					;
			END IF;
		END;

	PROCEDURE lathassa(i_szerepkor_kod VARCHAR2, i_funkcio_kod VARCHAR2) IS
		vanJoga NUMBER;
			BEGIN
				SELECT CASE WHEN exists(SELECT 1
					FROM jogosultsag j
						JOIN szerepkor sz ON sz.id = j.idszerepkor AND sz.kod = i_szerepkor_kod
						JOIN funkcio f ON f.id = j.idfunkcio AND f.kod = i_funkcio_kod
				) THEN 1 ELSE 0 END
				INTO vanJoga
				FROM dual
				;
				IF(vanJoga = 0)
				THEN
					INSERT INTO jogosultsag(ID, IDSZEREPKOR, IDFUNKCIO, SZINT)
						SELECT seq_helper.nextId('s_jogosultsag'), sz.id, f.id, 'LATHATO'
						FROM szerepkor sz, funkcio f
						WHERE sz.kod = i_szerepkor_kod
									AND f.kod = i_funkcio_kod
					;
				ELSE
					UPDATE jogosultsag SET szint = 'LATHATO'
					WHERE idszerepkor = (SELECT id FROM szerepkor WHERE kod = i_szerepkor_kod)
								AND idfunkcio = (SELECT id FROM funkcio WHERE kod = i_funkcio_kod)
					;
				END IF;
		END;

	PROCEDURE remFunkcio(i_funkcio_kod VARCHAR2) IS
		BEGIN
			DELETE FROM jogosultsag
				WHERE idfunkcio = (SELECT id FROM funkcio WHERE kod = i_funkcio_kod)
			;
			DELETE FROM funkcio
				WHERE kod = i_funkcio_kod
			;
		END;

END;
/
