--megvalósításhoz tartozó domain
/*
	id          NUMBER(19,0)
	ervkezdet	DATE
	ervveg		DATE
	kod         VARCHAR2(50 CHAR)
	nev         VARCHAR2(100 CHAR)
	leiras      VARCHAR2(1000 CHAR)
	valid       NUMBER(1,0)
	vegleges    NUMBER(1,0)
	...kredit	NUMBER(3,0)
*/

--tema(uuline{id}, uline{nev, idismeret}, leiras)
CREATE TABLE tema (
	id        NUMBER(19, 0) PRIMARY KEY,
	idismeret NUMBER(19, 0) NULL REFERENCES ismeret (id),
	nev       VARCHAR2(100 CHAR),
	leiras    VARCHAR2(1000 CHAR),
	CONSTRAINT u_tema UNIQUE (idismeret, nev, leiras),
	CONSTRAINT ck_tema CHECK (idismeret IS NOT NULL OR nev IS NOT NULL )
);
CREATE SEQUENCE s_tema START WITH 1 INCREMENT BY 1;

--tantargy(uuline{id}, nev, idszervezet, uline{kod})
CREATE TABLE tantargy (
	id          NUMBER(19, 0) PRIMARY KEY,
	nev         VARCHAR2(100 CHAR) NOT NULL,
	idszervezet NUMBER(19, 0) NULL REFERENCES szervezet (id),
	kod         VARCHAR2(50 CHAR)  NOT NULL UNIQUE
);
CREATE SEQUENCE s_tantargy START WITH 1 INCREMENT BY 1;

--ismeretszint(uuline{id}, uwave{nev}, leiras, uline{kod})
CREATE TABLE ismeretszint (
	id     NUMBER(19, 0) PRIMARY KEY,
	kod    VARCHAR2(50 CHAR)  NOT NULL UNIQUE,
	nev    VARCHAR2(100 CHAR) NOT NULL UNIQUE,
	leiras VARCHAR2(1000 CHAR)
);
CREATE SEQUENCE s_ismeretszint START WITH 1 INCREMENT BY 1;

--tematika(uuline{id}, idtantargy, idtema, ervkezdet, ervveg, idleadottszint, idszamonkertszint, valid, vegleges)
--csak valid tematika véglegesíthető
CREATE TABLE tematika (
	id                NUMBER(19, 0) PRIMARY KEY,
	idtantargy        NUMBER(19, 0) NOT NULL REFERENCES tantargy (id),
	idtema            NUMBER(19, 0) NOT NULL REFERENCES tema (id),
	ervkezdet         DATE,
	ervveg            DATE,
	idleadottszint    NUMBER(19, 0) NULL REFERENCES ismeretszint (id),
	idszamonkertszint NUMBER(19, 0) NULL REFERENCES ismeretszint (id),
	valid             NUMBER(1) DEFAULT 0 NOT NULL,
	vegleges          NUMBER(1) DEFAULT 0 NOT NULL,
	CONSTRAINT ck_tematika_erv CHECK (ervkezdet is NULL or ervveg is null or ervkezdet < ervveg),
	CONSTRAINT ck_tematika_valid CHECK (vegleges <> 1 or valid = 1)
);
CREATE SEQUENCE s_tematika START WITH 1 INCREMENT BY 1;

CREATE INDEX ix_tematikastart ON tematika(idtantargy, idtema, ervkezdet, valid, vegleges);
CREATE INDEX ix_tematikainterval ON tematika(idtantargy, idtema, ervkezdet, ervveg, valid, vegleges);
CREATE INDEX ix_tematikastarterv ON tematika(vegleges, ervkezdet, idtantargy, idtema);
CREATE INDEX ix_tematikaintervalerv ON tematika(vegleges, ervkezdet, ervveg, idtantargy, idtema);

--konkretszak(uuline{id}, uline{idszakkovetelmeny, idszervezet}, szakmaigyakkredit, szabvalkredit, szakiranykredit)
CREATE TABLE konkretszak (
	id                NUMBER(19, 0) PRIMARY KEY,
	idszakkovetelmeny NUMBER(19, 0) NOT NULL REFERENCES szakkovetelmeny (id),
	idszervezet       NUMBER(19, 0) NOT NULL REFERENCES szervezet (id),
	szakmaigyakkredit NUMBER(3, 0),
	szabvalkredit     NUMBER(3, 0),
	szakiranykredit   NUMBER(3, 0),
	CONSTRAINT u_konkretszak UNIQUE (idszakkovetelmeny, idszervezet)
);
CREATE SEQUENCE s_konkretszak START WITH 1 INCREMENT BY 1;

--konkretszakirany(uuline{id}, uline{idszakiranykovetelmeny, nev, uwave{idkonkretszak}}uwave{, kod})
CREATE TABLE konkretszakirany (
	id                     NUMBER(19, 0) PRIMARY KEY,
	idkonkretszak          NUMBER(19, 0) NOT NULL REFERENCES konkretszak (id),
	idszakiranykovetelmeny NUMBER(19, 0) NOT NULL REFERENCES szakiranykovetelmeny (id),
	nev                    VARCHAR2(100 CHAR),
	kod                    VARCHAR2(50 CHAR),
	CONSTRAINT u_konkretszakirany UNIQUE (idkonkretszak, idszakiranykovetelmeny, nev),
	CONSTRAINT u_konkretszakiranykod UNIQUE (idkonkretszak, kod)
);
CREATE SEQUENCE s_konkretszakirany START WITH 1 INCREMENT BY 1;

--ertekelestipus(uuline{id}, uwave{nev}, leiras, uline{kod})
create table ertekelestipus (
	id     NUMBER(19, 0) PRIMARY KEY,
	kod    VARCHAR2(50 CHAR)  NOT NULL UNIQUE,
	nev    VARCHAR2(100 CHAR) NOT NULL UNIQUE,
	leiras VARCHAR2(1000 CHAR)
);
CREATE SEQUENCE s_ertekelestipus START WITH 1 INCREMENT BY 1;

--tanterv(uuline{id}, idkonkretszakirany, idtantargy, ervkezdet, ervveg, statusz, felev, kredit, ea, gyak, lab, idertekelestipus, valid, vegleges)
/*
tanterv
	statusz                 VARCHAR2(10 CHAR)
	felev                   NUMBER(2,0)
	kredit                  NUMBER(2,0)
	ea                      NUMBER(2,0)
	gyak                    NUMBER(2,0)
	lab                     NUMBER(2,0)
*/
CREATE TABLE tanterv (
	id                 NUMBER(19, 0) PRIMARY KEY,
	idkonkretszakirany NUMBER(19, 0) NOT NULL REFERENCES konkretszakirany (id),
	idtantargy         NUMBER(19, 0) NOT NULL REFERENCES tantargy (id),
	ervkezdet          DATE,
	ervveg             DATE,
	statusz            VARCHAR2(10 CHAR),
	felev              NUMBER(2, 0),
	kredit             NUMBER(2, 0),
	ea_ora             NUMBER(2, 0),
	gyak_ora           NUMBER(2, 0),
	lab_ora            NUMBER(2, 0),
	idertekelestipus   NUMBER(19, 0) NULL REFERENCES ertekelestipus (id),
	valid              NUMBER(1) DEFAULT 0 NOT NULL,
	vegleges           NUMBER(1) DEFAULT 0 NOT NULL,
	CONSTRAINT ck_tanterv_erv CHECK (ervkezdet is NULL or ervveg is null or ervkezdet < ervveg),
	CONSTRAINT ck_tanterv_valid CHECK (vegleges <> 1 or valid = 1)
);
CREATE SEQUENCE s_tanterv START WITH 1 INCREMENT BY 1;

CREATE INDEX ix_tantervstart ON tanterv(idkonkretszakirany, idtantargy, ervkezdet, valid, vegleges);
CREATE INDEX ix_tantervinterval ON tanterv(idkonkretszakirany, idtantargy, ervkezdet, ervveg, valid, vegleges);
CREATE INDEX ix_tantervstatusz ON tanterv(statusz, ervkezdet, ervveg, valid, vegleges);
CREATE INDEX ix_tantervfelev ON tanterv(felev, ervkezdet, ervveg, valid, vegleges);
CREATE INDEX ix_tantervstarterv ON tanterv(vegleges, ervkezdet, idkonkretszakirany, idtantargy);
CREATE INDEX ix_tantervintervalerv ON tanterv(vegleges, ervkezdet, ervveg, idkonkretszakirany, idtantargy);
CREATE INDEX ix_tantervstatuszerv ON tanterv(vegleges, ervkezdet, ervveg, statusz);
CREATE INDEX ix_tantervfeleverv ON tanterv(vegleges, ervkezdet, ervveg, felev);
