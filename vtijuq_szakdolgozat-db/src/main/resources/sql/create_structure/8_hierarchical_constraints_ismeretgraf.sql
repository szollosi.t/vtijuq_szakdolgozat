/*
Mentési feltételek
	Ezek a feltételek az adatbázisban közvetlenül is helyet érdemes kapjanak, hiszen a konzisztenciát őrzik.
	A többinél bonyolultabbak és eltérő megoldással ellenőrzöttek, így külön script-ben kaptak helyet.

  általában kell:
	  - temporary table a tranzakcióban érintett, ellenőrizendő adatoknak
	  - after each row trigger, ami tölti a temporary table-t
	  - materialized view, mely frissíttethető a megfelelő tranzakciók végén
	  - before statement trigger a materialized view-n, mellyel elvégezzük a karbantartást
	  - a más jellegű felhasználhatóság érdekében tárolt eljárásba szervezett algoritmus, melyet a trigger hsznál

  Az ismeretgráfban a körmentességet kell elenőrizni
	össz-performancia-javítás reményében felhasználható a topologikus rendezés létét DAG-okban,
	  a körkeresést nem szükséges minden él-beszúrásnál elvégezni,
	  a topologikus rendezés karbantartását is el kell végezni párhuzamosan
	ehhez kell egy új segédtábla (toprend) az ismeret táblához kötve (elkerülendő a mutating trigger error-t),
	  melyre igaznak kell lennie, hogy ha van él két ismeret közt, akkor az él irányában nő a rendszám értéke
	a topologikus rendezés ellenőrzése végezhető az eddigiekben már használt materialized view + check constraint technikával
		a karbantartás lehet egy erre ültetett before (statement) triggerben
	    megjegyzendő, hogy ehhez nincs szükségünk egy teljes topologikus rendezésre, elég egy részben rendezés
*/
--DROP table ism_toprend;
CREATE TABLE ism_toprend (
	id       NUMBER(19, 0) PRIMARY KEY REFERENCES ismeret (id),
	rendszam NUMBER(19, 0) DEFAULT 0
);

--DROP INDEX ix_ism_toprend;
CREATE INDEX ix_ism_toprend ON ism_toprend (rendszam);

--DROP TRIGGER tr_toprend_tolt;
CREATE OR REPLACE TRIGGER tr_toprend_tolt
	AFTER INSERT
	ON ismeret
	FOR EACH ROW
	BEGIN
		INSERT INTO ism_toprend (id)
		VALUES (:new.id)
		;
	END
	;

--DROP TRIGGER tr_toprend_torol;
CREATE OR REPLACE TRIGGER tr_toprend_torol
	BEFORE DELETE
	ON ismeret
	FOR EACH ROW
	BEGIN
		DELETE FROM ism_toprend
			WHERE id = :old.id
		;
	END
	;

--temporary table
--drop table tmp_ismgraf;
CREATE GLOBAL TEMPORARY TABLE tmp_ismgraf (
	idmi   NUMBER(19, 0),
	idhova NUMBER(19, 0)
)
ON COMMIT DELETE ROWS
;

--DROP TRIGGER tr_tmp_ismgraf_tolt;
CREATE OR REPLACE TRIGGER tr_tmp_ismgraf_tolt
	AFTER INSERT
	ON ismeretgraf
	FOR EACH ROW
	BEGIN
		INSERT INTO tmp_ismgraf (idmi, idhova)
		VALUES (:new.idmi, :new.idhova)
		;
	END
	;

--DROP PACKAGE karbantartas_ismeret;
CREATE OR REPLACE PACKAGE karbantartas_ismeret
AS
	PROCEDURE topologikus_rendezes;
END;

CREATE OR REPLACE PACKAGE BODY karbantartas_ismeret AS
	PROCEDURE topologikus_rendezes IS
		BEGIN
			DECLARE
				rossz_toprend NUMBER(1, 0) := 0;
			BEGIN
				MERGE INTO ism_toprend
				USING (
							WITH toprend_start (id, rendszam) AS (
								--a rendezés frissítését, a változás helyén kezdi, ha el is rontotta a rendezést
								-- friss kapcsolatokat is figyelembe véve
								-- javaslatot is számol az új rendszam-hoz
									SELECT
										hova.id, max(mi.rendszam + 1) rendszam
									FROM ism_toprend hova
										JOIN tmp_ismgraf tmp
											ON tmp.idhova = hova.id
										JOIN ism_toprend mi
											ON mi.id = tmp.idmi
											 AND mi.rendszam >= hova.rendszam
									GROUP BY hova.id
							), toprend_javasol (id, rendszam) AS (
								--az új rendezés számolása
								(SELECT
									 ts.id, ts.rendszam
								 FROM toprend_start ts
								)
								UNION ALL (
									--új toprend javaslása
									SELECT
										hova.id, mi.rendszam + 1 rendszam
									FROM toprend_javasol mi
										JOIN ismeretgraf ig
											ON ig.idmi = mi.id
										JOIN ism_toprend hova
											ON hova.id = ig.idhova
											 --kifordult rendezések mentén tovább megy
											 AND hova.rendszam <= mi.rendszam
								)
							)
								SEARCH DEPTH FIRST BY id SET seq
								--ha kört talál jelzi, és nem hibára fut
								CYCLE id SET cycle TO 1 DEFAULT 0
								, toprend_kor (id, rendszam) AS (
								--biztos hibás toprend a körökbe
									SELECT DISTINCT
										id, -1
									FROM toprend_javasol
									WHERE cycle = 1
							), toprend_max (id, rendszam) AS (
								--veszi a javaslatok közül a legnagyobbat
									SELECT
										id, max(rendszam) rendszam
									FROM toprend_javasol
									GROUP BY id
							)
							--a körökbe biztos hibás toprend kerül,
							-- a többiekbe a frissen számolt
							SELECT
								trm.id,
								coalesce(trk.rendszam, trm.rendszam) rendszam
							FROM toprend_max trm
								LEFT JOIN toprend_kor trk
									ON trk.id = trm.id
						) toprendvalt
				ON (toprendvalt.id = ism_toprend.id)
				WHEN MATCHED THEN UPDATE
				SET ism_toprend.rendszam = toprendvalt.rendszam
				;

				SELECT CASE WHEN exists(
						SELECT 1
						FROM ismeretgraf ig, ism_toprend mi, ism_toprend hova
						WHERE mi.id = ig.idmi
								AND hova.id = ig.idhova
								AND hova.rendszam <= mi.rendszam)
					THEN 1
						 ELSE 0 END
				INTO rossz_toprend
				FROM dual
				;

				IF (rossz_toprend = 1)
				THEN
					raise_application_error(-20001, 'Az ismeretgráf megszűnt dag lenni!')
					;
				END IF
				;

			END;
		END;
END
;

--DROP MATERIALIZED VIEW mv_ismeret_belso;
CREATE MATERIALIZED VIEW mv_ismeret_belso
	REFRESH FORCE ON COMMIT
	AS
		SELECT ism.id
		FROM ismeret ism, ismeretgraf ig1, ismeretgraf ig2
		WHERE ism.id = ig1.idhova AND ism.id = ig2.idmi
;

--DROP TRIGGER tr_ismeret_toprend;
CREATE OR REPLACE TRIGGER tr_ismeret_toprend
	BEFORE INSERT OR UPDATE
	--bekerült a tranzakcióban egy belső él
	ON mv_ismeret_belso
	BEGIN
		karbantartas_ismeret.topologikus_rendezes();
	END
;

--szakmaijellemzo-erdő elemeiaihez tartozó ismeretek közt megfelelő utak futnak
--DROP PACKAGE consistency_helper;
CREATE OR REPLACE PACKAGE consistency_helper
AS
  PROCEDURE szakmaijellemzo_reszfa;
END;

CREATE OR REPLACE PACKAGE BODY consistency_helper AS
  PROCEDURE szakmaijellemzo_reszfa IS
    BEGIN
      DECLARE
        nem_reszfa NUMBER(1, 0) := 0;
      BEGIN
        SELECT CASE WHEN exists(SELECT 1
						FROM szakmaijellemzo szj
						--szerencsére gondoltam erre még a szakmaijellemzo hierarchikus view-jának írásakor
						JOIN v_szjellemzo_hierarchia szjh
							ON szjh.id = szj.id AND in_ismeret_tree = 0
				) THEN 1 ELSE 0 END
        INTO nem_reszfa
        FROM dual
        ;

        IF (nem_reszfa = 1)
        THEN
          raise_application_error(-20001, 'A szakmai jellemző erdő megszűnt az ismeretgráf "része" lenni!')
          ;
        END IF
        ;

      END;
    END;
END
;

--DROP MATERIALIZED VIEW mv_aljellemzo;
CREATE MATERIALIZED VIEW mv_aljellemzo
REFRESH FORCE ON COMMIT
  AS
    SELECT szj.id
    FROM szakmaijellemzo szj
    WHERE szj.idfojellemzo IS NOT NULL
;

--DROP TRIGGER tr_szakmaijellemzo_reszfa;
CREATE OR REPLACE TRIGGER tr_szakmaijellemzo_reszfa
  BEFORE INSERT OR UPDATE
  --bekerült legalább egy alárendelt szakmaijellemzo
  ON mv_aljellemzo
  BEGIN
    consistency_helper.szakmaijellemzo_reszfa();
  END
  ;
