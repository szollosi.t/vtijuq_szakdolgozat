--szekvencia-helper a hibernate-tel konform id-generálásért
CREATE OR REPLACE PACKAGE seq_helper
IS

	FUNCTION nextId(i_seq_name VARCHAR2)
		RETURN NUMBER;

	FUNCTION currId(i_seq_name VARCHAR2)
		RETURN NUMBER;

END;
/

CREATE OR REPLACE PACKAGE BODY seq_helper
IS
	TYPE offset_type IS TABLE OF NUMBER INDEX BY VARCHAR2 (64);
	offset offset_type;

	FUNCTION nextId(
		i_seq_name VARCHAR2)
		RETURN NUMBER
	IS
		l_seq    NUMBER := 1;
		l_newid  NUMBER;
		l_offset NUMBER;
		BEGIN
			BEGIN
				l_offset := offset(i_seq_name);
				EXCEPTION WHEN no_data_found THEN
				offset(i_seq_name) := 0;
				l_offset := 0;
			END;
			IF (l_offset = 0)
			THEN
				EXECUTE IMMEDIATE 'SELECT ' || i_seq_name || '.NEXTVAL FROM DUAL' INTO l_seq;
			ELSE
				EXECUTE IMMEDIATE 'SELECT ' || i_seq_name || '.CURRVAL FROM DUAL' INTO l_seq;
			END IF;
			l_newid := l_seq * 50 + l_offset;
			offset(i_seq_name) := offset(i_seq_name) + 1;
			IF (offset(i_seq_name) = 50)
			THEN
				offset(i_seq_name) := 0;
			END IF;
			RETURN l_newid;
		END;

	FUNCTION currId(
		i_seq_name VARCHAR2)
		RETURN NUMBER
	IS
		l_seq   NUMBER := 1;
		l_oldid NUMBER;
		BEGIN
			EXECUTE IMMEDIATE 'SELECT ' || i_seq_name || '.CURRVAL FROM DUAL' INTO l_seq;
			l_oldid := l_seq * 50 + offset(i_seq_name);
			IF (offset(i_seq_name) = 0)
			THEN
				l_oldid := l_oldid + 50;
			END IF;
			l_oldid := l_oldid - 1;
			RETURN l_oldid;
		END;
END;
/
