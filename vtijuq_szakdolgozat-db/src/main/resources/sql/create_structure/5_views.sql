/*
  konzisztencia-ellenőrzéshez,
  akkreditáció-validáláshoz,
  illetékesség-kezeléshez használható view-k
*/
-- ismeret-hierarchia
CREATE OR REPLACE VIEW v_ismeret_hierarchia AS
	WITH leaf AS ( SELECT id FROM ismeret WHERE id NOT IN ( SELECT idhova FROM ismeretgraf )
	), ismeret_hierarchia (id, kod, nev, hierszint, id_root, id_path, kod_path, isleaf) AS (
		( SELECT ism.id, ism.kod, ism.nev, 0 hierszint, ism.id id_root, to_char(ism.id) id_path, ism.kod kod_path,
				CASE WHEN (ism.id IN ( SELECT id FROM leaf ))
					THEN 1 ELSE 0 END isleaf
			FROM ismeret ism
			WHERE ism.id NOT IN ( SELECT idmi FROM ismeretgraf )
		)
		UNION ALL (
			SELECT ism.id, ism.kod, ism.nev, (ih.hierszint + 1) hierszint,
				ih.id_root,
				ih.id_path || '/' || to_char(ism.id) id_path,
				ih.kod_path || '/' || to_char(ism.kod) kod_path,
				CASE WHEN (ism.id IN ( SELECT id FROM leaf ))
					THEN 1 ELSE 0 END isleaf
			FROM ismeret ism
				JOIN ismeretgraf ig ON ig.idmi = ism.id
				JOIN ismeret_hierarchia ih ON ih.id = ig.idhova
		)
	) SELECT * FROM ismeret_hierarchia
;
-- szakirány legfelső szakmai jellemzőinek kredithatárai összeadva
CREATE OR REPLACE VIEW v_szakiranykov_kredit AS
	SELECT
		szk.id idszakkovetelmeny,
		szk.szakiranymaxkredit,
		szk.szakiranyminkredit,
		szkik.id idszakiranykovetelmeny,
		CASE WHEN (szkik.idszakirany IS NULL)
			THEN 1 ELSE 0 END alap,
		sum(coalesce(szj.maxkredit, 0)) sumszjmaxkredit,
		sum(coalesce(szj.minkredit, 0)) sumszjminkredit
	FROM szakkovetelmeny szk
		JOIN szakiranykovetelmeny szkik
			ON szkik.idszakkovetelmeny = szk.id
		JOIN szakmaijellemzo szj
			ON szj.idszakiranykovetelmeny = szkik.id AND szj.idfojellemzo IS NULL
	GROUP BY szk.id, szk.szakiranymaxkredit, szk.szakiranyminkredit, szkik.id, szkik.idszakirany
;
-- szakkövetelményhez tartozó alap szakiránykövetelmény legfelső szakmai jellemzőinek kredithatárai összeadva
CREATE OR REPLACE VIEW v_szk_kredit_kov AS
	SELECT
		szk.id idszakkovetelmeny,
		szk.osszkredit,
		szk.szakdolgozatkredit,
		szk.szabvalminkredit,
		szk.szakiranyminkredit,
		szk.szakiranymaxkredit,
		coalesce(aszik.sumszjmaxkredit, 0) alapszjmaxkredit,
		coalesce(aszik.sumszjminkredit, 0) alapszjminkredit
	FROM szakkovetelmeny szk
		JOIN v_szakiranykov_kredit aszik
			ON aszik.idszakkovetelmeny = szk.id AND aszik.alap = 1
	GROUP BY szk.id,
		szk.osszkredit, szk.szakdolgozatkredit, szk.szabvalminkredit,
		szk.szakiranyminkredit, szk.szakiranymaxkredit,
		aszik.sumszjmaxkredit, aszik.sumszjminkredit
;
-- konkrét szakhoz kapcsolódó szakkövetelményben szereplő kreditek és követelmények
CREATE OR REPLACE VIEW v_akk_szak_kredit AS
	SELECT
		ksz.id idkonkretszak,
		ksz.szabvalkredit,
		szk.szabvalminkredit,
		ksz.szakmaigyakkredit,
		szk.szakmaigyakminkredit,
		ksz.szakiranykredit,
		szk.szakiranyminkredit,
		szk.szakiranymaxkredit
	FROM konkretszak ksz
		JOIN szakkovetelmeny szk
			ON szk.id = ksz.idszakkovetelmeny
;
-- konkrét szakhoz kapcsolódó tantervi bejegyzések kreditértékei
CREATE OR REPLACE VIEW v_akk_szak_tt_kredit AS
	SELECT
		ksz.id idkonkretszak,
		kszi.id idkonkretszakirany,
		case when (szik.idszakirany is null) then 1 else 0 end alap,
		szik.csakkozos,
		tt.id idtanterv,
		tt.statusz,
		tt.kredit,
		tt.ervkezdet,
		tt.ervveg
	FROM konkretszak ksz
		JOIN konkretszakirany kszi
			ON kszi.idkonkretszak = ksz.id
		JOIN szakiranykovetelmeny szik
			ON szik.id = kszi.idszakiranykovetelmeny
		LEFT JOIN tanterv tt
			ON tt.idkonkretszakirany = kszi.id
;
-- szakmai jellemző hierarchia
CREATE OR REPLACE VIEW v_szjellemzo_hierarchia AS
	WITH leaf AS ( SELECT id FROM szakmaijellemzo WHERE id NOT IN ( SELECT idfojellemzo FROM szakmaijellemzo )
	), szj_hierarchia (id, idismeret, kod, hierszint, incycle, id_root, id_path, nev_path, kod_path, in_ismeret_tree, isleaf) AS (
		( SELECT szj.id, szj.idismeret, ism.kod, 0 hierszint, 0 incycle, szj.id id_root, to_char(szj.id) id_path, szj.nev nev_path, ism.kod kod_path, 1 in_ismeret_tree,
				CASE WHEN (szj.id IN ( SELECT id FROM leaf ))
					THEN 1 ELSE 0 END isleaf
			FROM szakmaijellemzo szj
				JOIN ismeret ism ON ism.id = szj.idismeret
			WHERE szj.idfojellemzo IS NULL
		) UNION ALL (
			SELECT szj.id, szj.idismeret, ism.kod, (szjh.hierszint + 1) hierszint,
				CASE WHEN (szjh.id_path LIKE '%/'||to_char(szj.id)||'/%')
					THEN 1 ELSE 0 END incycle,
				szjh.id_root,
				szjh.id_path || '/' || to_char(szj.id) id_path,
				szjh.nev_path || '/' || to_char(szj.nev) nev_path,
				szjh.kod_path || '/' || to_char(ism.kod) kod_path,
				CASE WHEN exists(SELECT 1 FROM v_ismeret_hierarchia ismh
          WHERE ismh.id = szj.idismeret AND szjh.in_ismeret_tree = 1
            AND (ismh.id_root = szjh.idismeret OR ismh.id_path LIKE '%/'||szjh.idismeret||'/%'))
					THEN 1 ELSE 0 END in_ismeret_tree,
				CASE WHEN (szj.id IN ( SELECT id FROM leaf ))
					THEN 1 ELSE 0 END isleaf
			FROM szakmaijellemzo szj
        JOIN ismeret ism ON ism.id = szj.idismeret
				JOIN szj_hierarchia szjh ON szjh.id = szj.idfojellemzo
		)
	) SELECT * FROM szj_hierarchia
;
-- szakmai jellemző - ismeret kapcsolatok
CREATE OR REPLACE VIEW v_szjellemzo_ismeret_kapcs AS
	WITH szj_leaf AS (
		SELECT szjh.id, szjh.idismeret, szjh.id_root, szjh.id_path
		FROM v_szjellemzo_hierarchia szjh
		WHERE szjh.isleaf = 1
	)
	SELECT DISTINCT
		szj.id idszakmaijellemzo,
		ismh.id idismeret
	FROM szj_leaf l -- a szakmaijellemzo-levelek mentén kapcsolódóakra vagyunk kíváncsiak
		JOIN szakmaijellemzo szj
			ON szj.id = l.id
			OR l.id_path LIKE '%/' || to_char(szj.id) || '/%'
			OR szj.id = l.id_root
		JOIN v_ismeret_hierarchia ismh
			ON ismh.id = l.idismeret
			OR ismh.id_path LIKE '%/'||to_char(l.idismeret)||'/%'
			OR ismh.id_root = l.idismeret
;
-- szakmai jellemzőhöz kapcsolódó tantervi elemek kreditértékei
CREATE OR REPLACE VIEW v_akk_szj_tt_kredit AS
	SELECT
		szj.id idszakmaijellemzo,
		tanterv.idkonkretszakirany,
		tematika.id idtematika,
		tanterv.id idtanterv,
		greatest(tematika.ervkezdet, tanterv.ervkezdet) ervkezdet,
		least(tematika.ervveg, tanterv.ervveg) ervveg,
		tanterv.statusz,
		tanterv.kredit
	FROM szakmaijellemzo szj
		JOIN v_szjellemzo_ismeret_kapcs szjism ON szjism.idszakmaijellemzo = szj.id
		JOIN ismeret ism ON ism.id = szjism.idismeret
		JOIN tema ON tema.idismeret = ism.id
		JOIN tematika ON tematika.idtema = tema.id
		JOIN tantargy ON tantargy.id = tematika.idtantargy
		JOIN tanterv ON tanterv.idtantargy = tantargy.id
	    AND (tanterv.ervkezdet BETWEEN tematika.ervkezdet AND tematika.ervveg
				OR tanterv.ervveg BETWEEN tematika.ervkezdet AND tematika.ervveg)
;
-- szervezet hierarchia
CREATE OR REPLACE VIEW v_szervezet_hierarchia AS
WITH szerv_hierarchia (id, rovidites, nev, idtipus, id_root, rov_path, id_path, intezmenyi, hierszint) AS (
	( SELECT sz.id, sz.rovidites, sz.nev, sz.idtipus, sz.id id_root, sz.rovidites rov_path, to_char(sz.id) id_path,
			CASE WHEN (szt.kod = 'INTEZMENY')
				THEN 1 ELSE 0 END intezmenyi,
			0 hierszint
		FROM szervezet sz
			JOIN szervezettipus szt ON szt.id = sz.idtipus
		--a legfőbb adminisztrációt éppen nem szűrjük be, de belőle indulunk ki,
		--rajta kívül nem lehet főszervezet nélküli szervezet,
		--és neki nem lehet főszervezete (és csak 0 típusú lehet)
		--ezt egy check is biztosítja (a 0-s típus ADMIN-ságát pedig a normális működés)
		WHERE sz.idfoszervezet = 0
	) UNION ALL (
		SELECT
			sz.id, sz.rovidites, sz.nev, sz.idtipus, szh.id_root,
			szh.rov_path||sz.rovidites rov_path,
			szh.id_path||to_char(sz.id) id_path,
			CASE WHEN (szh.intezmenyi = 1 OR szt.kod = 'INTEZMENY')
				THEN 1 ELSE 0 END intezmenyi,
			(szh.hierszint + 1) hierszint
		FROM szervezet sz
			JOIN szervezettipus szt ON szt.id = sz.idtipus
			JOIN szerv_hierarchia szh ON szh.id = sz.idfoszervezet
	)
) SELECT * FROM szerv_hierarchia
;
-- jogosultság view
CREATE OR REPLACE VIEW v_munkatars_jogosultsag AS
	SELECT
		m.id idmunkatars,
		m.felhasznalonev,
		m.aktiv,
		j.szint,
		f.kod funkciokod,
		f.nev funkcionev
	FROM munkatars m
	JOIN szerepkor sz on sz.id = m.idszerepkor
	JOIN jogosultsag j on j.idszerepkor = sz.id
	JOIN funkcio f on f.id = j.idfunkcio
;
-- illetekesseg view-k
CREATE OR REPLACE VIEW v_szervezet_illetekesseg AS
	SELECT
		fsz.id idfoszerv,
		szh.id idszervezet
	FROM v_szervezet_hierarchia szh
	JOIN szervezet fsz
		ON szh.id_root = szh.id
		OR szh.id_path LIKE '%/'||to_char(szh.id)||'/%'
;
CREATE OR REPLACE VIEW v_munkatars_illetekesseg AS
	SELECT
		m.id idmunkatars,
		m.felhasznalonev,
		m.aktiv,
		szi.idszervezet
	FROM munkatars m
		JOIN v_szervezet_illetekesseg szi
			ON szi.idfoszerv = m.idszervezet
;
