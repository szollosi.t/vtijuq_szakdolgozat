--megvalósításhoz tartozó domain
-- test konkretszak
--EE-EK sz3 valid
INSERT INTO konkretszak(ID, IDSZAKKOVETELMENY, IDSZERVEZET, SZAKMAIGYAKKREDIT, SZABVALKREDIT, SZAKIRANYKREDIT)
  SELECT seq_helper.NEXTID('s_konkretszak'), szk.id, (SELECT id FROM szervezet WHERE rovidites = 'EE-EK'),
    0, 30, 60
    FROM szakkovetelmeny szk
      JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három' AND szk.valid = 1
;
--EF sz3 valid
INSERT INTO konkretszak(ID, IDSZAKKOVETELMENY, IDSZERVEZET, SZAKMAIGYAKKREDIT, SZABVALKREDIT, SZAKIRANYKREDIT)
  SELECT seq_helper.NEXTID('s_konkretszak'), szk.id, (SELECT id FROM szervezet WHERE rovidites = 'EF'),
    0, 30, 60
  FROM szakkovetelmeny szk
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három' AND szk.valid = 1
;
-- test konkretszakirany
--ksz1 +
INSERT INTO konkretszakirany(ID, IDKONKRETSZAK, IDSZAKIRANYKOVETELMENY, NEV, KOD)
  SELECT seq_helper.NEXTID('s_konkretszakirany'), ksz.id, szik.id, 'Első konkrét szak alap', 'KSZ_1A'
    FROM konkretszak ksz
      JOIN szakkovetelmeny szk ON szk.id = ksz.idszakkovetelmeny AND szk.valid = 1
        JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
      JOIN szakiranykovetelmeny szik ON szik.idszakkovetelmeny = szk.id AND szik.idszakirany IS NULL
      JOIN szervezet sz ON sz.id = ksz.idszervezet AND sz.rovidites = 'EE-EK'
;
--ksz1 szi4
INSERT INTO konkretszakirany(ID, IDKONKRETSZAK, IDSZAKIRANYKOVETELMENY, NEV, KOD)
  SELECT seq_helper.NEXTID('s_konkretszakirany'), ksz.id, szik.id, 'Első konkrét szak spec', 'KSZ_1S'
  FROM konkretszak ksz
    JOIN szakkovetelmeny szk ON szk.id = ksz.idszakkovetelmeny AND szk.valid = 1
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
    JOIN szakiranykovetelmeny szik ON szik.idszakkovetelmeny = szk.id AND szik.idszakirany IS NOT NULL
    JOIN szervezet sz ON sz.id = ksz.idszervezet AND sz.rovidites = 'EE-EK'
;
--ksz2 +
INSERT INTO konkretszakirany(ID, IDKONKRETSZAK, IDSZAKIRANYKOVETELMENY, NEV, KOD)
  SELECT seq_helper.NEXTID('s_konkretszakirany'), ksz.id, szik.id, 'Második konkrét szak alap', 'KSZ_2A'
  FROM konkretszak ksz
    JOIN szakkovetelmeny szk ON szk.id = ksz.idszakkovetelmeny AND szk.valid = 1
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
    JOIN szakiranykovetelmeny szik ON szik.idszakkovetelmeny = szk.id AND szik.idszakirany IS NULL
    JOIN szervezet sz ON sz.id = ksz.idszervezet AND sz.rovidites = 'EF'
;
--ksz2 szi4
INSERT INTO konkretszakirany(ID, IDKONKRETSZAK, IDSZAKIRANYKOVETELMENY, NEV, KOD)
  SELECT seq_helper.NEXTID('s_konkretszakirany'), ksz.id, szik.id, 'Második konkrét szak spec', 'KSZ_2S'
  FROM konkretszak ksz
    JOIN szakkovetelmeny szk ON szk.id = ksz.idszakkovetelmeny AND szk.valid = 1
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
    JOIN szakiranykovetelmeny szik ON szik.idszakkovetelmeny = szk.id AND szik.idszakirany IS NOT NULL
    JOIN szervezet sz ON sz.id = ksz.idszervezet AND sz.rovidites = 'EF'
;
-- test tantargy
--legyenek mondjuk: 6 ISM1, 6 ISM2, 6 ISM3, 8 ISM4
-- -> legyen 22 minta (idszervez = null) és 2*4 nem minta
--minta tantárgyak
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 1', NULL , 'TT_1')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 2', NULL , 'TT_2')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 3', NULL , 'TT_3')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 4', NULL , 'TT_4')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 5', NULL , 'TT_5')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 6', NULL , 'TT_6')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 7', NULL , 'TT_7')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 8', NULL , 'TT_8')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 9', NULL , 'TT_9')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 10', NULL , 'TT_10')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 11', NULL , 'TT_11')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 12', NULL , 'TT_12')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 13', NULL , 'TT_13')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 14', NULL , 'TT_14')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 15', NULL , 'TT_15')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 16', NULL , 'TT_16')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 17', NULL , 'TT_17')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 18', NULL , 'TT_18')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 19', NULL , 'TT_19')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 20', NULL , 'TT_20')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 21', NULL , 'TT_21')
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  VALUES (seq_helper.NEXTID('s_tantargy'), 'Tantárgy 22', NULL , 'TT_22')
;
--nem minta tantárgyak
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  SELECT seq_helper.NEXTID('s_tantargy'), 'Tantárgy 23E', sz.id , 'TT_23E'
    FROM szervezet sz
    WHERE sz.rovidites = 'EE-EK'
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  SELECT seq_helper.NEXTID('s_tantargy'), 'Tantárgy 24E', sz.id , 'TT_24E'
    FROM szervezet sz
    WHERE sz.rovidites = 'EE-EK'
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  SELECT seq_helper.NEXTID('s_tantargy'), 'Tantárgy 25E', sz.id , 'TT_25E'
    FROM szervezet sz
    WHERE sz.rovidites = 'EE-EK'
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  SELECT seq_helper.NEXTID('s_tantargy'), 'Tantárgy 26E', sz.id , 'TT_26E'
    FROM szervezet sz
    WHERE sz.rovidites = 'EE-EK'
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  SELECT seq_helper.NEXTID('s_tantargy'), 'Tantárgy 23F', sz.id , 'TT_23F'
    FROM szervezet sz
    WHERE sz.rovidites = 'EF'
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  SELECT seq_helper.NEXTID('s_tantargy'), 'Tantárgy 24F', sz.id , 'TT_24F'
    FROM szervezet sz
    WHERE sz.rovidites = 'EF'
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  SELECT seq_helper.NEXTID('s_tantargy'), 'Tantárgy 25F', sz.id , 'TT_25F'
    FROM szervezet sz
    WHERE sz.rovidites = 'EF'
;
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  SELECT seq_helper.NEXTID('s_tantargy'), 'Tantárgy 26F', sz.id , 'TT_26F'
    FROM szervezet sz
    WHERE sz.rovidites = 'EF'
;
-- test tema + tematika
--legyenek mondjuk: TT_1-6 ISM1, TT_7-12 ISM2, TT_13-18 ISM3, TT_19-26 ISM4
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 1.1'
    FROM ismeret ism
    WHERE ism.kod = 'ISM1'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_1'
    AND t.nev = 'Téma 1.1'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 1.2'
    FROM ismeret ism
    WHERE ism.kod = 'ISM1'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_2'
    AND t.nev = 'Téma 1.2'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 1.3'
    FROM ismeret ism
    WHERE ism.kod = 'ISM1'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_3'
    AND t.nev = 'Téma 1.3'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 1.4'
    FROM ismeret ism
    WHERE ism.kod = 'ISM1'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_4'
    AND t.nev = 'Téma 1.4'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 1.5'
    FROM ismeret ism
    WHERE ism.kod = 'ISM1'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_5'
    AND t.nev = 'Téma 1.5'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 1.6'
    FROM ismeret ism
    WHERE ism.kod = 'ISM1'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_6'
    AND t.nev = 'Téma 1.6'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 2.1'
    FROM ismeret ism
    WHERE ism.kod = 'ISM2'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_7'
    AND t.nev = 'Téma 2.1'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 2.2'
    FROM ismeret ism
    WHERE ism.kod = 'ISM2'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_8'
    AND t.nev = 'Téma 2.2'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 2.3'
    FROM ismeret ism
    WHERE ism.kod = 'ISM2'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_9'
    AND t.nev = 'Téma 2.3'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 2.4'
    FROM ismeret ism
    WHERE ism.kod = 'ISM2'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_10'
    AND t.nev = 'Téma 2.4'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 2.5'
    FROM ismeret ism
    WHERE ism.kod = 'ISM2'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_11'
    AND t.nev = 'Téma 2.5'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 2.6'
    FROM ismeret ism
    WHERE ism.kod = 'ISM2'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_12'
    AND t.nev = 'Téma 2.6'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 3.1'
    FROM ismeret ism
    WHERE ism.kod = 'ISM3'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_13'
    AND t.nev = 'Téma 3.1'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 3.2'
    FROM ismeret ism
    WHERE ism.kod = 'ISM3'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_14'
    AND t.nev = 'Téma 3.2'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 3.3'
    FROM ismeret ism
    WHERE ism.kod = 'ISM3'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_15'
    AND t.nev = 'Téma 3.3'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 3.4'
    FROM ismeret ism
    WHERE ism.kod = 'ISM3'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_16'
    AND t.nev = 'Téma 3.4'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 3.5'
    FROM ismeret ism
    WHERE ism.kod = 'ISM3'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_17'
    AND t.nev = 'Téma 3.5'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 3.6'
    FROM ismeret ism
    WHERE ism.kod = 'ISM3'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_18'
    AND t.nev = 'Téma 3.6'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.1'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_19'
    AND t.nev = 'Téma 4.1'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.2'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_20'
    AND t.nev = 'Téma 4.2'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.3'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_21'
    AND t.nev = 'Téma 4.3'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.4'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_22'
    AND t.nev = 'Téma 4.4'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.5E1'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_23E'
    AND t.nev = 'Téma 4.5E1'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.5E2'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_24E'
    AND t.nev = 'Téma 4.5E2'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.5F1'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_23F'
    AND t.nev = 'Téma 4.5F1'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.5F2'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_24F'
    AND t.nev = 'Téma 4.5F2'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.6E1'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_25E'
    AND t.nev = 'Téma 4.6E1'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.6E2'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_26E'
    AND t.nev = 'Téma 4.6E2'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.6F1'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_25F'
    AND t.nev = 'Téma 4.6F1'
;
INSERT INTO tema(ID, IDISMERET, NEV)
  SELECT seq_helper.NEXTID('s_tema'), ism.id, 'Téma 4.6F2'
    FROM ismeret ism
    WHERE ism.kod = 'ISM4'
;
INSERT INTO tematika(ID, IDTANTARGY, IDTEMA, ERVKEZDET)
  SELECT seq_helper.NEXTID('s_tematika'), tt.id, t.id, to_date('1999-09-01', 'yyyy-MM-dd')
  FROM tantargy tt, tema t
  WHERE tt.kod = 'TT_26F'
    AND t.nev = 'Téma 4.6F2'
;
--minta tantárgyakat megvalósító konkrét tantárgyak
INSERT INTO tantargy(ID, NEV, IDSZERVEZET, KOD)
  SELECT seq_helper.NEXTID('s_tantargy'), sz.rovidites||' - '||tt.nev, sz.id,
      CASE WHEN (sz.rovidites = 'EF') THEN tt.kod||'F' ELSE tt.kod||'E' END
    FROM szervezet sz, tantargy tt
    WHERE sz.rovidites IN ('EE-EK', 'EF')
      AND tt.idszervezet IS NULL
;
-- test tanterv
--legyenek mondjuk 10 kreditesek
-- összesen több kredit szerezhető, minden félévre jut egy, ISM4-ből kettő az utolsó évre (5-6. félév)
--TT_1-4 KSZ_(1-2)A KOTELEZO
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 1, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
    FROM konkretszakirany kszi, tantargy tt
    WHERE kszi.kod = 'KSZ_1A'
      AND tt.kod = 'TT_1E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 1, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
    FROM konkretszakirany kszi, tantargy tt
    WHERE kszi.kod = 'KSZ_2A'
      AND tt.kod = 'TT_1F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 2, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
    FROM konkretszakirany kszi, tantargy tt
    WHERE kszi.kod = 'KSZ_1A'
      AND tt.kod = 'TT_2E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 2, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
    FROM konkretszakirany kszi, tantargy tt
    WHERE kszi.kod = 'KSZ_2A'
      AND tt.kod = 'TT_2F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 3, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
    FROM konkretszakirany kszi, tantargy tt
    WHERE kszi.kod = 'KSZ_1A'
      AND tt.kod = 'TT_3E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 3, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
    FROM konkretszakirany kszi, tantargy tt
    WHERE kszi.kod = 'KSZ_2A'
      AND tt.kod = 'TT_3F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 4, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
    FROM konkretszakirany kszi, tantargy tt
    WHERE kszi.kod = 'KSZ_1A'
      AND tt.kod = 'TT_4E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 4, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
    FROM konkretszakirany kszi, tantargy tt
    WHERE kszi.kod = 'KSZ_2A'
      AND tt.kod = 'TT_4F'
;
--TT_5-6 KSZ_(1-2)A SZABVAL
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_5E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_5F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_6E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_6F'
;
--TT_7-10 KSZ_(1-2)A KOTELEZO
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 1, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_7E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 1, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_7F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 2, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_8E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 2, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_8F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 3, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_9E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 3, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_9F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 4, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_10E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 4, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_10F'
;
--TT_11-12 KSZ_(1-2)A SZABVAL
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_11E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_11F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_12E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_12F'
;
--TT_13-16 KSZ_(1-2)A KOTELEZO
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 1, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_13E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 1, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_13F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 2, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_14E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 2, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_14F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 3, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_15E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 3, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_15F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 4, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_16E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 4, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_16F'
;
--TT_17-18 KSZ_(1-2)A SZABVAL
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_17E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_17F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1A'
        AND tt.kod = 'TT_18E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'SZABVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2A'
        AND tt.kod = 'TT_18F'
;
--TT_(19-22) KSZ_(1-2)S KOTELEZO
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1S'
        AND tt.kod = 'TT_19E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2S'
        AND tt.kod = 'TT_19F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1S'
        AND tt.kod = 'TT_20E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2S'
        AND tt.kod = 'TT_20F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1S'
        AND tt.kod = 'TT_21E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 5, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2S'
        AND tt.kod = 'TT_21F'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1S'
        AND tt.kod = 'TT_22E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'KOTELEZO', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2S'
        AND tt.kod = 'TT_22F'
;
--TT_(23-26)E KSZ_1S KOTVAL
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'KOTVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1S'
        AND tt.kod = 'TT_23E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'KOTVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1S'
        AND tt.kod = 'TT_24E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'KOTVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1S'
        AND tt.kod = 'TT_25E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'KOTVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_1S'
        AND tt.kod = 'TT_26E'
;
--TT_(23-26)F KSZ_2S KOTVAL
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'KOTVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2S'
        AND tt.kod = 'TT_23E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'KOTVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2S'
        AND tt.kod = 'TT_24E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'KOTVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2S'
        AND tt.kod = 'TT_25E'
;
INSERT INTO tanterv (id, idkonkretszakirany, idtantargy, felev, kredit, statusz, ervkezdet)
  SELECT seq_helper.NEXTID('s_tanterv'), kszi.id, tt.id, 6, 10, 'KOTVAL', to_date('2000-09-01', 'yyyy-MM-dd')
  FROM konkretszakirany kszi, tantargy tt
  WHERE kszi.kod = 'KSZ_2S'
        AND tt.kod = 'TT_26E'
;
