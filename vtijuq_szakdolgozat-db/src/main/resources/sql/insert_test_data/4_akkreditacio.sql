--akkreditációhoz tartozó domain
--test akkreditacio
--  egyik se valid/vegleges
--  mindegyik test konkretszakirany-ra
--  érvényességek a szakkovetelmeny-kből
INSERT INTO akkreditacio(ID, IDKONKRETSZAKIRANY, ERVKEZDET, ERVVEG)
  SELECT seq_helper.NEXTID('s_akkreditacio'), kszi.id, szk.ervkezdet, szk.ervveg
    FROM konkretszakirany kszi
      JOIN szakiranykovetelmeny szik ON szik.id = kszi.idszakiranykovetelmeny
      JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny
;
