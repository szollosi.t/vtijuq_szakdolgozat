-- követelményekhez tartozó domain
-- test kepzesiteruletek
-- KT1
INSERT INTO kepzesiterulet (id, kod, nev)
  VALUES (seq_helper.NEXTID('s_kepzesiterulet'),
          'KT1', 'Első képzési terület')
;
-- KT2
INSERT INTO kepzesiterulet (id, kod, nev)
  VALUES (seq_helper.NEXTID('s_kepzesiterulet'),
          'KT2', 'Második képzési terület')
;
-- test szakok
-- KT1 sz1
INSERT INTO szak (id, nev, idterulet, ciklus)
  SELECT seq_helper.NEXTID('s_szak'), 'Szak Egy', kt.id, 'R'
    FROM kepzesiterulet kt
    WHERE kt.kod = 'KT1'
;
--KT1 sz2
INSERT INTO szak (id, nev, idterulet, ciklus)
  SELECT seq_helper.NEXTID('s_szak'), 'Szak Kettő', kt.id, 'B'
  FROM kepzesiterulet kt
  WHERE kt.kod = 'KT1'
;
--KT2 sz3
INSERT INTO szak (id, nev, idterulet, ciklus)
  SELECT seq_helper.NEXTID('s_szak'), 'Szak Három', kt.id, 'B'
  FROM kepzesiterulet kt
  WHERE kt.kod = 'KT2'
;
-- test szakiranyok
--sz1 szi1
INSERT INTO szakirany (id, idszak, nev, leiras)
  SELECT seq_helper.NEXTID('s_szakirany'), sz.id, 'Szakirány Egy', 'Első szakirány'
    FROM szak sz
    WHERE sz.nev = 'Szak Egy'
;
--sz1 szi2
INSERT INTO szakirany (id, idszak, nev, leiras)
  SELECT seq_helper.NEXTID('s_szakirany'), sz.id, 'Szakirány Kettő', 'Második szakirány'
  FROM szak sz
  WHERE sz.nev = 'Szak Egy'
;
--sz2 szi3
INSERT INTO szakirany (id, idszak, nev, leiras)
  SELECT seq_helper.NEXTID('s_szakirany'), sz.id, 'Szakirány Három', 'Harmadik szakirány'
  FROM szak sz
  WHERE sz.nev = 'Szak Kettő'
;
--sz3 szi4
INSERT INTO szakirany (id, idszak, nev, leiras)
  SELECT seq_helper.NEXTID('s_szakirany'), sz.id, 'Szakirány Négy', 'Negyedik szakirány'
  FROM szak sz
  WHERE sz.nev = 'Szak Három'
;
-- test szakkovetelmenyek
--sz1 szk1
INSERT INTO szakkovetelmeny (id, idszak, oklevelmegjeloles, felevek)
  SELECT seq_helper.NEXTID('s_szakkovetelmeny'), sz.id, 'Első Szakkövetelmény', 4
    FROM szak sz
    WHERE sz.nev = 'Szak Egy'
;
--sz2 szk2
INSERT INTO szakkovetelmeny (id, idszak, oklevelmegjeloles, felevek)
  SELECT seq_helper.NEXTID('s_szakkovetelmeny'), sz.id, 'Második Szakkövetelmény', 6
  FROM szak sz
  WHERE sz.nev = 'Szak Kettő'
;
--sz3 szk3
INSERT INTO szakkovetelmeny (id, idszak, ervkezdet, ervveg, oklevelmegjeloles, felevek, osszkredit, szakdolgozatkredit, szakmaigyakminkredit, szabvalminkredit, szakiranyminkredit, szakiranymaxkredit, szakmaigyakeloirasok)
  SELECT seq_helper.NEXTID('s_szakkovetelmeny'), sz.id,
    to_date('2000-01-01', 'yyyy-MM-dd'), to_date('2005-08-31', 'yyyy-MM-dd'),
    'Harmadik Szakkövetelmény', 6, 180, 30, 0, 30, 60, 120, 'legyen'
  FROM szak sz
  WHERE sz.nev = 'Szak Három'
;
--sz3 szk4
INSERT INTO szakkovetelmeny (id, idszak, ervkezdet, ervveg, oklevelmegjeloles, felevek, osszkredit, szakdolgozatkredit, szakmaigyakminkredit, szabvalminkredit, szakiranyminkredit, szakiranymaxkredit, szakmaigyakeloirasok)
  SELECT seq_helper.NEXTID('s_szakkovetelmeny'), sz.id,
    to_date('2005-09-01', 'yyyy-MM-dd'), NULL,
    'Harmadik Szakkövetelmény2', 7, 210, 30, 0, 30, 60, 120, 'legyen'
  FROM szak sz
  WHERE sz.nev = 'Szak Három'
;
-- test szakiranykovetelmenyek
--szk1 + szik1
INSERT INTO szakiranykovetelmeny (id, idszakkovetelmeny, idszakirany)
  SELECT seq_helper.NEXTID('s_szakiranykovetelmeny'), szk.id, NULL
    FROM szakkovetelmeny szk
  WHERE szk.idszak = (SELECT id FROM szak WHERE nev = 'Szak Egy')
;
--szk2 + szik2
INSERT INTO szakiranykovetelmeny (id, idszakkovetelmeny, idszakirany)
  SELECT seq_helper.NEXTID('s_szakiranykovetelmeny'), szk.id, NULL
  FROM szakkovetelmeny szk
  WHERE szk.idszak = (SELECT id FROM szak WHERE nev = 'Szak Kettő')
;
--szk3 + szik3
INSERT INTO szakiranykovetelmeny (id, idszakkovetelmeny, idszakirany, csakkozos)
  SELECT seq_helper.NEXTID('s_szakiranykovetelmeny'), szk.id, NULL, 0
  FROM szakkovetelmeny szk
  WHERE szk.idszak = (SELECT id FROM szak WHERE nev = 'Szak Három')
    AND szk.ervveg IS NOT NULL
;
--szk3 szi4 szik4
INSERT INTO szakiranykovetelmeny (id, idszakkovetelmeny, idszakirany, csakkozos)
  SELECT seq_helper.NEXTID('s_szakiranykovetelmeny'), szk.id, szi.id, 0
  FROM szakkovetelmeny szk
    JOIN szakirany szi
      ON szi.idszak = szk.idszak AND szi.nev = 'Szakirány Négy' AND szk.ervveg IS NOT NULL
  WHERE szk.idszak = (SELECT id FROM szak WHERE nev = 'Szak Három')
;
--szk4 - szik5
INSERT INTO szakiranykovetelmeny (id, idszakkovetelmeny, idszakirany, csakkozos)
  SELECT seq_helper.NEXTID('s_szakiranykovetelmeny'), szk.id, NULL, 1
  FROM szakkovetelmeny szk
  WHERE szk.idszak = (SELECT id FROM szak WHERE nev = 'Szak Három')
        AND szk.ervveg IS NULL
;
--szk4 szi4 szik6
INSERT INTO szakiranykovetelmeny (id, idszakkovetelmeny, idszakirany, csakkozos)
  SELECT seq_helper.NEXTID('s_szakiranykovetelmeny'), szk.id, szi.id, 0
  FROM szakkovetelmeny szk
    JOIN szakirany szi ON szi.idszak = szk.idszak
     AND szi.nev = 'Szakirány Négy' AND szk.ervveg IS NULL
  WHERE szk.idszak = (SELECT id FROM szak WHERE nev = 'Szak Három')
;
-- test ismeretek
--1
INSERT INTO ismeret (id, nev, leiras, kod)
  VALUES (seq_helper.NEXTID('s_ismeret'), 'Ismeret 1', 'Első ismeret', 'ISM1')
;
--2
INSERT INTO ismeret (id, nev, leiras, kod)
VALUES (seq_helper.NEXTID('s_ismeret'), 'Ismeret 2', 'Második ismeret', 'ISM2')
;
--3
INSERT INTO ismeret (id, nev, leiras, kod)
  VALUES (seq_helper.NEXTID('s_ismeret'), 'Ismeret 3', 'Hármas ismeret', 'ISM3')
;
--4
INSERT INTO ismeret (id, nev, leiras, kod)
  VALUES (seq_helper.NEXTID('s_ismeret'), 'Ismeret 4', 'Négyes ismeret', 'ISM4')
;
--12
INSERT INTO ismeret (id, nev, leiras, kod)
  VALUES (seq_helper.NEXTID('s_ismeret'), 'Ismeret 12', '(Első,Második) ismeret', 'ISM12')
;
--23
INSERT INTO ismeret (id, nev, leiras, kod)
  VALUES (seq_helper.NEXTID('s_ismeret'), 'Ismeret 23', '(Második,Harmadik) ismeret', 'ISM23')
;
--31
INSERT INTO ismeret (id, nev, leiras, kod)
  VALUES (seq_helper.NEXTID('s_ismeret'), 'Ismeret 13', '(Első,Harmadik) ismeret', 'ISM31')
;
--123
INSERT INTO ismeret (id, nev, leiras, kod)
  VALUES (seq_helper.NEXTID('s_ismeret'), 'Ismeret 123', 'Diszciplína', 'ISM123')
;
-- test ismeretgrafok
--1-12
INSERT INTO ismeretgraf (idmi, idhova)
  SELECT mi.id, hova.id
    FROM ismeret mi, ismeret hova
    WHERE mi.kod = 'ISM1'
      AND hova.kod = 'ISM12'
;
--2-12
INSERT INTO ismeretgraf (idmi, idhova)
  SELECT mi.id, hova.id
  FROM ismeret mi, ismeret hova
  WHERE mi.kod = 'ISM2'
    AND hova.kod = 'ISM12'
;
--2-23
INSERT INTO ismeretgraf (idmi, idhova)
  SELECT mi.id, hova.id
  FROM ismeret mi, ismeret hova
  WHERE mi.kod = 'ISM2'
    AND hova.kod = 'ISM23'
;
--3-23
INSERT INTO ismeretgraf (idmi, idhova)
  SELECT mi.id, hova.id
  FROM ismeret mi, ismeret hova
  WHERE mi.kod = 'ISM3'
    AND hova.kod = 'ISM23'
;
--3-31
INSERT INTO ismeretgraf (idmi, idhova)
  SELECT mi.id, hova.id
  FROM ismeret mi, ismeret hova
  WHERE mi.kod = 'ISM3'
    AND hova.kod = 'ISM31'
;
--1-31
INSERT INTO ismeretgraf (idmi, idhova)
  SELECT mi.id, hova.id
  FROM ismeret mi, ismeret hova
  WHERE mi.kod = 'ISM1'
        AND hova.kod = 'ISM31'
;
--12-123
INSERT INTO ismeretgraf (idmi, idhova)
  SELECT mi.id, hova.id
  FROM ismeret mi, ismeret hova
  WHERE mi.kod = 'ISM12'
    AND hova.kod = 'ISM123'
;
--23-123
INSERT INTO ismeretgraf (idmi, idhova)
  SELECT mi.id, hova.id
  FROM ismeret mi, ismeret hova
  WHERE mi.kod = 'ISM23'
    AND hova.kod = 'ISM123'
;
--31-123
INSERT INTO ismeretgraf (idmi, idhova)
  SELECT mi.id, hova.id
  FROM ismeret mi, ismeret hova
  WHERE mi.kod = 'ISM31'
    AND hova.kod = 'ISM123'
;
-- test szakmaijellemzok
-- szk3 120
-- + szik3 60-120
--   ISM123 60-120
INSERT INTO szakmaijellemzo (id, idszakiranykovetelmeny, idismeret, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
  SELECT seq_helper.NEXTID('s_szakmaijellemzo'), szik.id,
      (SELECT id FROM ismeret ism WHERE ism.kod = 'ISM123'),
      NULL, 'SZJ_1', 60, 120, NULL
    FROM szakiranykovetelmeny szik
      JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny AND szk.ervveg IS NOT NULL
        JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
      WHERE szik.idszakirany IS NULL
;
--     ISM12 40-80
INSERT INTO szakmaijellemzo (id, idszakiranykovetelmeny, idismeret, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
  SELECT seq_helper.NEXTID('s_szakmaijellemzo'), szik.id,
      (SELECT id FROM ismeret ism WHERE ism.kod = 'ISM12'),
      (SELECT id FROM szakmaijellemzo szj WHERE szj.nev = 'SZJ_1'),
      'SZJ_1_1', 40, 80, NULL
  FROM szakiranykovetelmeny szik
    JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny AND szk.ervveg IS NOT NULL
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
  WHERE szik.idszakirany IS NULL
;
--     ISM3 20-40
INSERT INTO szakmaijellemzo (id, idszakiranykovetelmeny, idismeret, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
  SELECT seq_helper.NEXTID('s_szakmaijellemzo'), szik.id,
      (SELECT id FROM ismeret ism WHERE ism.kod = 'ISM3'),
      (SELECT id FROM szakmaijellemzo szj WHERE szj.nev = 'SZJ_1'),
      'SZJ_1_2', 20, 40, NULL
  FROM szakiranykovetelmeny szik
    JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny AND szk.ervveg IS NOT NULL
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
  WHERE szik.idszakirany IS NULL
;
-- szi4 szik4 60-120
--   ISM4 60-120
INSERT INTO szakmaijellemzo (id, idszakiranykovetelmeny, idismeret, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
  SELECT seq_helper.NEXTID('s_szakmaijellemzo'), szik.id,
      (SELECT id FROM ismeret ism WHERE ism.kod = 'ISM4'),
      NULL, 'SZJ_2', 60, 120, NULL
  FROM szakiranykovetelmeny szik
    JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny AND szk.ervveg IS NOT NULL
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
  WHERE szik.idszakirany IS NOT NULL
;
--szk4 150
-- - szik5 60-120
--   ISM123 mv2
INSERT INTO szakmaijellemzo (id, idszakiranykovetelmeny, idismeret, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
  SELECT seq_helper.NEXTID('s_szakmaijellemzo'), szik.id,
      (SELECT id FROM ismeret ism WHERE ism.kod = 'ISM123'),
      NULL, 'SZJ_3', 60, 120, 2
  FROM szakiranykovetelmeny szik
    JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny AND szk.ervveg IS NULL
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
  WHERE szik.idszakirany IS NULL
;
--     ISM12 30-60
INSERT INTO szakmaijellemzo (id, idszakiranykovetelmeny, idismeret, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
  SELECT seq_helper.NEXTID('s_szakmaijellemzo'), szik.id,
      (SELECT id FROM ismeret ism WHERE ism.kod = 'ISM12'),
      (SELECT id FROM szakmaijellemzo szj WHERE szj.nev = 'SZJ_3'),
      'SZJ_3_1', 30, 60, NULL
  FROM szakiranykovetelmeny szik
    JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny AND szk.ervveg IS NULL
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
  WHERE szik.idszakirany IS NULL
;
--     ISM23 30-60
INSERT INTO szakmaijellemzo (id, idszakiranykovetelmeny, idismeret, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
  SELECT seq_helper.NEXTID('s_szakmaijellemzo'), szik.id,
      (SELECT id FROM ismeret ism WHERE ism.kod = 'ISM23'),
      (SELECT id FROM szakmaijellemzo szj WHERE szj.nev = 'SZJ_3'),
      'SZJ_3_2', 30, 60, NULL
  FROM szakiranykovetelmeny szik
    JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny AND szk.ervveg IS NULL
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
  WHERE szik.idszakirany IS NULL
;
--       ISM2 15-30
INSERT INTO szakmaijellemzo (id, idszakiranykovetelmeny, idismeret, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
  SELECT seq_helper.NEXTID('s_szakmaijellemzo'), szik.id,
      (SELECT id FROM ismeret ism WHERE ism.kod = 'ISM2'),
      (SELECT id FROM szakmaijellemzo szj WHERE szj.nev = 'SZJ_3_2'),
      'SZJ_3_2_1', 15, 30, NULL
  FROM szakiranykovetelmeny szik
    JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny AND szk.ervveg IS NULL
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
  WHERE szik.idszakirany IS NULL
;
--       ISM3 15-30
INSERT INTO szakmaijellemzo (id, idszakiranykovetelmeny, idismeret, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
  SELECT seq_helper.NEXTID('s_szakmaijellemzo'), szik.id,
      (SELECT id FROM ismeret ism WHERE ism.kod = 'ISM3'),
      (SELECT id FROM szakmaijellemzo szj WHERE szj.nev = 'SZJ_3_2'),
      'SZJ_3_2_2', 15, 30, NULL
  FROM szakiranykovetelmeny szik
    JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny AND szk.ervveg IS NULL
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
  WHERE szik.idszakirany IS NULL
;
--     ISM31 30-60
INSERT INTO szakmaijellemzo (id, idszakiranykovetelmeny, idismeret, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
  SELECT seq_helper.NEXTID('s_szakmaijellemzo'), szik.id,
      (SELECT id FROM ismeret ism WHERE ism.kod = 'ISM31'),
      (SELECT id FROM szakmaijellemzo szj WHERE szj.nev = 'SZJ_3'),
      'SZJ_3_3', 30, 60, NULL
  FROM szakiranykovetelmeny szik
    JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny AND szk.ervveg IS NULL
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
  WHERE szik.idszakirany IS NULL
;
-- szi4 szik6 60-120
--   ISM4 60-120
INSERT INTO szakmaijellemzo (id, idszakiranykovetelmeny, idismeret, idfojellemzo, nev, minkredit, maxkredit, minvalaszt)
  SELECT seq_helper.NEXTID('s_szakmaijellemzo'), szik.id,
      (SELECT id FROM ismeret ism WHERE ism.kod = 'ISM4'),
      NULL, 'SZJ_4', 60, 120, NULL
  FROM szakiranykovetelmeny szik
    JOIN szakkovetelmeny szk ON szk.id = szik.idszakkovetelmeny AND szk.ervveg IS NULL
    JOIN szak sz ON sz.id = szk.idszak AND sz.nev = 'Szak Három'
  WHERE szik.idszakirany IS NOT NULL
;
--szk3 valid és végleges
UPDATE szakkovetelmeny
    SET valid = 1,
        vegleges = 1
  WHERE ervveg IS NOT NULL
    AND idszak = (SELECT id FROM szak sz WHERE sz.nev = 'Szak Három')
;

-- DELETE FROM szakmaijellemzo;
-- DELETE FROM ismeretgraf;
-- DELETE FROM ism_toprend;
-- DELETE FROM ismeret;
-- DELETE FROM szakiranykovetelmeny;
-- DELETE FROM szakkovetelmeny;
-- DELETE FROM szakirany;
-- DELETE FROM szak;
-- DELETE FROM kepzesiterulet;
