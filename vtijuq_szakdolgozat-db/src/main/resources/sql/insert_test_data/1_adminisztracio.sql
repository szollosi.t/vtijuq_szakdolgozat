--adminisztatív domain
-- test szervezettipus
-- Kar
INSERT INTO szervezettipus (id, nev, kod)
  VALUES (seq_helper.nextId('s_szervezettipus'), 'Kar', 'KAR')
;
-- test szerepkör
-- Kar-felelős (tantárgyak)
INSERT INTO szerepkor (id, nev, kod, idszervezettipus)
  SELECT seq_helper.nextId('s_szerepkor'), 'Kar-felelős', 'KAR', szt.id
  FROM SZERVEZETTIPUS szt
  WHERE szt.kod = 'KAR'
;
BEGIN
  funkcio_helper.LATHASSA('INTEZMENY','ISMERET_KARBANTARTO');
  funkcio_helper.LATHASSA('INTEZMENY','ISMERET_SZERKESZTO');
  funkcio_helper.LATHASSA('INTEZMENY','KONKRET_SZERKESZTO');
  funkcio_helper.IRHASSA('INTEZMENY','TANTARGY_KARBANTARTO');
  funkcio_helper.IRHASSA('INTEZMENY','TANTARGY_SZERKESZTO');
END;
/
-- test szervezet
-- MAB főszerv
INSERT INTO szervezet(ID, IDFOSZERVEZET, NEV, ROVIDITES, IDTIPUS, CIM)
  SELECT seq_helper.NEXTID('s_szervezet'), 0, 'MAB', 'MAB - Főszerv', szt.id, 'mab cím1'
  FROM szervezettipus szt
  WHERE szt.kod = 'MAB'
;
-- MAB követelmények
INSERT INTO szervezet(ID, IDFOSZERVEZET, NEV, ROVIDITES, IDTIPUS, CIM)
  SELECT seq_helper.NEXTID('s_szervezet'), fsz.id, 'MAB - Követelmény albizottság', 'MAB-KÖV', szt.id, 'mab cím1'
  FROM szervezettipus szt, szervezet fsz
  WHERE szt.kod = 'MAB'
    AND fsz.rovidites = 'MAB'
;
-- MAB akkreditációk
INSERT INTO szervezet(ID, IDFOSZERVEZET, NEV, ROVIDITES, IDTIPUS, CIM)
  SELECT seq_helper.NEXTID('s_szervezet'), fsz.id, 'MAB - Akkreditációs albizottság', 'MAB-AKKR', szt.id, 'mab cím2'
  FROM szervezettipus szt, szervezet fsz
  WHERE szt.kod = 'MAB'
    AND fsz.rovidites = 'MAB'
;
-- Egy Egyetem egy karral
INSERT INTO szervezet(ID, IDFOSZERVEZET, NEV, ROVIDITES, IDTIPUS, CIM)
  SELECT seq_helper.NEXTID('s_szervezet'), 0, 'Egy Egyetem', 'EE', szt.id, 'Egy Egyetem Címe'
  FROM szervezettipus szt
  WHERE szt.kod = 'INTEZMENY'
;
INSERT INTO szervezet(ID, IDFOSZERVEZET, NEV, ROVIDITES, IDTIPUS, CIM)
  SELECT seq_helper.NEXTID('s_szervezet'), fsz.id, 'Egy Egyetem Egy Kara', 'EE-EK', szt.id, 'Egy Egyetem Címe'
  FROM szervezettipus szt, szervezet fsz
  WHERE szt.kod = 'KAR'
    AND fsz.rovidites = 'EE'
;
-- Egy főiskola
INSERT INTO szervezet(ID, IDFOSZERVEZET, NEV, ROVIDITES, IDTIPUS, CIM)
  SELECT seq_helper.NEXTID('s_szervezet'), 0, 'Egy Főiskola', 'EF', szt.id, 'Egy Főiskola Címe'
  FROM szervezettipus szt
  WHERE szt.kod = 'INTEZMENY'
;
-- test munkatarsak
-- MAB
-- ADMIN
INSERT INTO munkatars(ID, AKTIV, JELSZOVALTOZTATAS, IDSZERVEZET, IDSZEREPKOR, FELHASZNALONEV, NEV, JELSZO)
  SELECT seq_helper.NEXTID('s_munkatars'), 1, 0, sz.id, szk.id, 'MabAdmin', 'MAB admin', 'admin'
    FROM szervezet sz, szerepkor szk
    WHERE sz.rovidites = 'MAB'
      AND szk.kod = 'ADMIN'
;
-- MAB-KÖV
-- ADMIN
INSERT INTO munkatars(ID, AKTIV, JELSZOVALTOZTATAS, IDSZERVEZET, IDSZEREPKOR, FELHASZNALONEV, NEV, JELSZO)
  SELECT seq_helper.NEXTID('s_munkatars'), 1, 0, sz.id, szk.id, 'MabKovAdmin', 'MAB-KÖV admin', 'admin'
  FROM szervezet sz, szerepkor szk
  WHERE sz.rovidites = 'MAB-KÖV'
    AND szk.kod = 'ADMIN'
;
-- MAB
INSERT INTO munkatars(ID, AKTIV, JELSZOVALTOZTATAS, IDSZERVEZET, IDSZEREPKOR, FELHASZNALONEV, NEV, JELSZO)
  SELECT seq_helper.NEXTID('s_munkatars'), 1, 0, sz.id, szk.id, 'MabKovMab', 'MAB-felelős', 'mab'
  FROM szervezet sz, szerepkor szk
  WHERE sz.rovidites = 'MAB-KÖV'
    AND szk.kod = 'MAB'
;
-- MAB-AKKR
-- ADMIN
INSERT INTO munkatars(ID, AKTIV, JELSZOVALTOZTATAS, IDSZERVEZET, IDSZEREPKOR, FELHASZNALONEV, NEV, JELSZO)
  SELECT seq_helper.NEXTID('s_munkatars'), 1, 0, sz.id, szk.id, 'MabAkkrAdmin', 'MAB-AKKR admin', 'admin'
  FROM szervezet sz, szerepkor szk
  WHERE sz.rovidites = 'MAB-AKKR'
    AND szk.kod = 'ADMIN'
;
-- AKKREDITACIO
INSERT INTO munkatars(ID, AKTIV, JELSZOVALTOZTATAS, IDSZERVEZET, IDSZEREPKOR, FELHASZNALONEV, NEV, JELSZO)
  SELECT seq_helper.NEXTID('s_munkatars'), 1, 0, sz.id, szk.id, 'MabAkkreditator', 'MAB akkreditáció karbantarto', 'akkreditator'
  FROM szervezet sz, szerepkor szk
  WHERE sz.rovidites = 'MAB-AKKR'
    AND szk.kod = 'AKKREDITACIO'
;
-- EE
-- ADMIN
INSERT INTO munkatars(ID, AKTIV, JELSZOVALTOZTATAS, IDSZERVEZET, IDSZEREPKOR, FELHASZNALONEV, NEV, JELSZO)
  SELECT seq_helper.NEXTID('s_munkatars'), 1, 0, sz.id, szk.id, 'EeAdmin', 'EE admin', 'admin'
  FROM szervezet sz, szerepkor szk
  WHERE sz.rovidites = 'EE'
    AND szk.kod = 'ADMIN'
;
-- INTEZMENY
INSERT INTO munkatars(ID, AKTIV, JELSZOVALTOZTATAS, IDSZERVEZET, IDSZEREPKOR, FELHASZNALONEV, NEV, JELSZO)
  SELECT seq_helper.NEXTID('s_munkatars'), 1, 0, sz.id, szk.id, 'EeIntezmeny', 'EE intézmény-felelős', 'intezmeny'
  FROM szervezet sz, szerepkor szk
  WHERE sz.rovidites = 'EE'
    AND szk.kod = 'INTEZMENY'
;
-- EE-EK
-- KAR
INSERT INTO munkatars(ID, AKTIV, JELSZOVALTOZTATAS, IDSZERVEZET, IDSZEREPKOR, FELHASZNALONEV, NEV, JELSZO)
  SELECT seq_helper.NEXTID('s_munkatars'), 0, 0, sz.id, szk.id, 'EeEkKar', 'EE-EK kar-felelős', 'kar'
  FROM szervezet sz, szerepkor szk
  WHERE sz.rovidites = 'EE-EK'
    AND szk.kod = 'KAR'
;
-- EF
-- ADMIN
INSERT INTO munkatars(ID, AKTIV, JELSZOVALTOZTATAS, IDSZERVEZET, IDSZEREPKOR, FELHASZNALONEV, NEV, JELSZO)
  SELECT seq_helper.NEXTID('s_munkatars'), 1, 0, sz.id, szk.id, 'EfAdmin', 'EF admin', 'admin'
  FROM szervezet sz, szerepkor szk
  WHERE sz.rovidites = 'EF'
    AND szk.kod = 'ADMIN'
;
-- INTEZMENY
INSERT INTO munkatars(ID, AKTIV, JELSZOVALTOZTATAS, IDSZERVEZET, IDSZEREPKOR, FELHASZNALONEV, NEV, JELSZO)
  SELECT seq_helper.NEXTID('s_munkatars'), 1, 1, sz.id, szk.id, 'EfIntezmeny', 'EF intézmény-felelős', 'intezmeny'
  FROM szervezet sz, szerepkor szk
  WHERE sz.rovidites = 'EF'
    AND szk.kod = 'INTEZMENY'
;
