package vtijuq_szakdoli.mapper;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections4.ListUtils;

import vtijuq_szakdoli.common.dto.AbstractEntitasDTO;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

public abstract class AbstractEditableMapper<
		E extends AbstractEntitasDTO & EntitasDTO,
		F extends AbstractEntitasDTO & EditableDTO,
		G extends AbstractEntitas & EditableByDTO<F> & MappedByDTO<E>>
		extends AbstractMapper<E, G> {

	public abstract G findOrNewEmptyByEditableDTO(F dto);

	public <S extends AbstractEntitasDTO & EditableDTO, V extends AbstractEntitas & EditableByDTO<S>> V findOrNewEmptyByEditableDTO(AbstractEntitasDao<V> dao, S dto, Class<V> persistentClass) {
		return findByDTO(dao, dto, persistentClass);
	}

	public abstract G toEntitas(F dto);

	public void mergeCollection(Collection<F> dtoCollection, List<G> entitasCollection) {
		mergeCollection(dtoCollection, entitasCollection, this::toEntitas);
	}

	private void mergeCollection(Collection<F> dtoCollection, Collection<G> entitasCollection, Function<F, G> mapping) {
		final List<Long> entitasIdList = entitasCollection.stream().map(AbstractEntitas::getId).collect(Collectors.toList());
		final List<Long> dtoIdList = dtoCollection.stream().map(AbstractEntitasDTO::getId).collect(Collectors.toList());
		//remove
		final List<Long> idsToRemove = ListUtils.subtract(entitasIdList, dtoIdList);
		entitasCollection.removeIf(t -> idsToRemove.contains(t.getId()));
		//merge
		final List<F> dtosToMerge = dtoCollection.stream()
				.filter(t -> ListUtils.intersection(entitasIdList, dtoIdList).contains(t.getId()))
				.collect(Collectors.toList());
		final List<Long> idsToMerge = dtosToMerge.stream()
				.map(AbstractEntitasDTO::getId).collect(Collectors.toList());
		entitasCollection.removeIf(t -> idsToMerge.contains(t.getId()));
		entitasCollection.addAll(dtosToMerge.stream()
				.map(mapping)
				.collect(Collectors.toList()));
		//add
		final List<Long> idsToAdd = ListUtils.subtract(dtoIdList, entitasIdList);
		entitasCollection.addAll(dtoCollection.stream()
				.filter(t -> idsToAdd.contains(t.getId()))
				.map(mapping)
				.collect(Collectors.toList()));
	}

	public abstract F toEditableDTO(G entitas);

	public List<F> toEditableDTOList(Collection<G> entitasCollection){
		return entitasCollection.stream().map(this::toEditableDTO).collect(Collectors.toList());
	}

	public Set<F> toEditableDTOSet(Collection<G> entitasCollection){
		return entitasCollection.stream().map(this::toEditableDTO).collect(Collectors.toSet());
	}

}
