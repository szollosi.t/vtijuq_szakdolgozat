package vtijuq_szakdoli.mapper.test;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.dao.entitas.test.SzotarTestDao;
import vtijuq_szakdoli.domain.entitas.test.SzotarTest;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;

public class SzotarTestMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO,
		SzotarTestDTO,
		SzotarTest> {

	@Inject
	private SzotarTestDao szotarTestDao;

	@Override
	public SzotarTest findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO dto) {
		return findByEntitasDTO(szotarTestDao, dto, SzotarTest.class);
	}

	@Override
	public SzotarTest findOrNewEmptyByEditableDTO(SzotarTestDTO dto) {
		return findOrNewEmptyByEditableDTO(szotarTestDao, dto, SzotarTest.class);
	}

	@Override
	public SzotarTest toEntitas(SzotarTestDTO dto) {
		SzotarTest entitas = findOrNewEmptyByEditableDTO(dto);
		entitas = MergeUtil.merge(dto, entitas);
		return entitas;
	}

	@Override
	public SzotarTestDTO toEditableDTO(SzotarTest entitas) {
		SzotarTestDTO dto = new SzotarTestDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO toEntitasDTO(SzotarTest entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO(toEditableDTO(entitas));
	}
}
