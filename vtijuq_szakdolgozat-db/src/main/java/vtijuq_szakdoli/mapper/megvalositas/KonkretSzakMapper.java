package vtijuq_szakdoli.mapper.megvalositas;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.dao.entitas.megvalositas.KonkretSzakDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.KonkretSzak;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;
import vtijuq_szakdoli.mapper.adminisztracio.SzervezetMapper;
import vtijuq_szakdoli.mapper.kovetelmeny.SzakKovetelmenyMapper;

public class KonkretSzakMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO,
		KonkretSzakDTO,
		KonkretSzak> {

	@Inject
	private KonkretSzakDao konkretSzakDao;

	@Inject
	private SzervezetMapper szervezetMapper;

	@Inject
	private SzakKovetelmenyMapper szakKovetelmenyMapper;

	@Override
	public KonkretSzak findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO dto) {
		return findByEntitasDTO(konkretSzakDao, dto, KonkretSzak.class);
	}

	@Override
	public KonkretSzak findOrNewEmptyByEditableDTO(KonkretSzakDTO dto) {
		return findOrNewEmptyByEditableDTO(konkretSzakDao, dto, KonkretSzak.class);
	}

	@Override
	public KonkretSzak toEntitas(KonkretSzakDTO dto) {
		KonkretSzak entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//copy of non-editable fields
		if (dto.getSzervezet() != null) {
			entitas.setSzervezet(szervezetMapper.findByEntitasDTO(dto.getSzervezet()));
		}
		if (dto.getSzakKovetelmeny() != null) {
			entitas.setSzakKovetelmeny(szakKovetelmenyMapper.findByEntitasDTO(dto.getSzakKovetelmeny()));
		}
		return entitas;
	}

	@Override
	public KonkretSzakDTO toEditableDTO(KonkretSzak entitas) {
		KonkretSzakDTO dto = new KonkretSzakDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		dto.setSzakKovetelmeny(szakKovetelmenyMapper.toEntitasDTO(entitas.getSzakKovetelmeny()));
		dto.setSzervezet(szervezetMapper.toEntitasDTO(entitas.getSzervezet()));
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO toEntitasDTO(KonkretSzak entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO(toEditableDTO(entitas));
	}
}
