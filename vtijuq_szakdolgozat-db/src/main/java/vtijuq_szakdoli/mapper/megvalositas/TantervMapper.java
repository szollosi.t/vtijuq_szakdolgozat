package vtijuq_szakdoli.mapper.megvalositas;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.dao.entitas.megvalositas.TantervDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tanterv;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;

public class TantervMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantervDTO,
		TantervDTO,
		Tanterv> {
	@Inject
	private TantervDao tantervDao;

	@Inject
	private ErtekelesTipusMapper ertekelesTipusMapper;

	@Inject
	private KonkretSzakiranyMapper konkretSzakiranyMapper;

	@Inject
	private TantargyMapper tantargyMapper;

	@Override
	public Tanterv findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantervDTO dto) {
		return findByEntitasDTO(tantervDao, dto, Tanterv.class);
	}

	@Override
	public Tanterv findOrNewEmptyByEditableDTO(TantervDTO dto) {
		return findOrNewEmptyByEditableDTO(tantervDao, dto, Tanterv.class);
	}

	@Override
	public Tanterv toEntitas(TantervDTO dto) {
		Tanterv entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//copy of non-editable fields
		if (dto.getErtekelesTipus() != null) {
			entitas.setErtekelesTipus(ertekelesTipusMapper.findByEntitasDTO(dto.getErtekelesTipus()));
		}
		//mapping of transient fields
		entitas.setIdTantargy(dto.getTantargy().getId());
		entitas.setIdKonkretSzakirany(dto.getKonkretSzakirany().getId());
		return entitas;
	}

	@Override
	public TantervDTO toEditableDTO(Tanterv entitas) {
		TantervDTO dto = new TantervDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		if (entitas.getKonkretSzakirany() != null) {
			dto.setKonkretSzakirany(konkretSzakiranyMapper.toEntitasDTO(entitas.getKonkretSzakirany()));
		}
		if (entitas.getTantargy() != null) {
			dto.setTantargy(tantargyMapper.toEntitasDTO(entitas.getTantargy()));
		}
		if (entitas.getErtekelesTipus() != null) {
			dto.setErtekelesTipus(ertekelesTipusMapper.toEntitasDTO(entitas.getErtekelesTipus()));
		}
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantervDTO toEntitasDTO(Tanterv entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantervDTO(toEditableDTO(entitas));
	}
}
