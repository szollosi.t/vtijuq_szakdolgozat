package vtijuq_szakdoli.mapper.megvalositas;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.dao.entitas.megvalositas.TemaDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tema;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;
import vtijuq_szakdoli.mapper.kovetelmeny.IsmeretMapper;

public class TemaMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO,
		TemaDTO,
		Tema> {

	@Inject
	private TemaDao temaDao;

	@Inject
	private IsmeretMapper ismeretMapper;

	@Override
	public Tema findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO dto) {
		return findByEntitasDTO(temaDao, dto, Tema.class);
	}

	@Override
	public Tema findOrNewEmptyByEditableDTO(TemaDTO dto) {
		return findOrNewEmptyByEditableDTO(temaDao, dto, Tema.class);
	}

	@Override
	public Tema toEntitas(TemaDTO dto) {
		Tema entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//copy of non-editable fields
		if (dto.getIsmeret() != null) {
			entitas.setIsmeret(ismeretMapper.findByEntitasDTO(dto.getIsmeret()));
		}
		return entitas;
	}

	@Override
	public TemaDTO toEditableDTO(Tema entitas) {
		TemaDTO dto = new TemaDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		if (entitas.getIsmeret() != null) {
			dto.setIsmeret(ismeretMapper.toEntitasDTO(entitas.getIsmeret()));
		}
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO toEntitasDTO(Tema entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO(toEditableDTO(entitas));
	}
}
