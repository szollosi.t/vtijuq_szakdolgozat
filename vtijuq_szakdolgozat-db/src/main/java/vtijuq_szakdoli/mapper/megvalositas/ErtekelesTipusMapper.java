package vtijuq_szakdoli.mapper.megvalositas;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.ErtekelesTipusDTO;
import vtijuq_szakdoli.dao.entitas.megvalositas.ErtekelesTipusDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.ErtekelesTipus;
import vtijuq_szakdoli.mapper.AbstractMapper;

public class ErtekelesTipusMapper extends AbstractMapper<
		ErtekelesTipusDTO,
		ErtekelesTipus> {
	@Inject
	private ErtekelesTipusDao ertekelesTipusDao;

	@Override
	public ErtekelesTipus findByEntitasDTO(ErtekelesTipusDTO dto) {
		return findByDTO(ertekelesTipusDao, dto, ErtekelesTipus.class);
	}

	@Override
	public ErtekelesTipusDTO toEntitasDTO(ErtekelesTipus entitas) {
		ErtekelesTipusDTO dto = new ErtekelesTipusDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		return dto;
	}
}
