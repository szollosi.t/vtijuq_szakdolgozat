package vtijuq_szakdoli.mapper.megvalositas;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.IsmeretSzintDTO;
import vtijuq_szakdoli.dao.entitas.megvalositas.IsmeretSzintDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.IsmeretSzint;
import vtijuq_szakdoli.mapper.AbstractMapper;

public class IsmeretSzintMapper extends AbstractMapper<
		IsmeretSzintDTO,
		IsmeretSzint> {
	@Inject
	private IsmeretSzintDao ismeretSzintDao;

	@Override
	public IsmeretSzint findByEntitasDTO(IsmeretSzintDTO dto) {
		return findByDTO(ismeretSzintDao, dto, IsmeretSzint.class);
	}

	@Override
	public IsmeretSzintDTO toEntitasDTO(IsmeretSzint entitas) {
		IsmeretSzintDTO dto = new IsmeretSzintDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		return dto;
	}
}
