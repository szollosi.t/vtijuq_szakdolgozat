package vtijuq_szakdoli.mapper.megvalositas;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.dao.entitas.megvalositas.KonkretSzakiranyDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.KonkretSzakirany;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;
import vtijuq_szakdoli.mapper.kovetelmeny.SzakiranyKovetelmenyMapper;

public class KonkretSzakiranyMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO,
		KonkretSzakiranyDTO,
		KonkretSzakirany> {

	@Inject
	private KonkretSzakiranyDao konkretSzakiranyDao;

	@Inject
	private KonkretSzakMapper konkretSzakMapper;

	@Inject
	private SzakiranyKovetelmenyMapper szakiranyKovetelmenyMapper;

	@Override
	public KonkretSzakirany findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO dto) {
		return findByEntitasDTO(konkretSzakiranyDao, dto, KonkretSzakirany.class);
	}

	@Override
	public KonkretSzakirany findOrNewEmptyByEditableDTO(KonkretSzakiranyDTO dto) {
		return findOrNewEmptyByEditableDTO(konkretSzakiranyDao, dto, KonkretSzakirany.class);
	}

	@Override
	public KonkretSzakirany toEntitas(KonkretSzakiranyDTO dto) {
		KonkretSzakirany entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//mapping of transient fields
		entitas.setIdSzakiranyKovetelmeny(dto.getSzakiranyKovetelmeny().getId());
		entitas.setIdKonkretSzak(dto.getKonkretSzak().getId());
		return entitas;
	}

	@Override
	public KonkretSzakiranyDTO toEditableDTO(KonkretSzakirany entitas) {
		KonkretSzakiranyDTO dto = new KonkretSzakiranyDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		if (entitas.getKonkretSzak() != null) {
			dto.setKonkretSzak(konkretSzakMapper.toEntitasDTO(entitas.getKonkretSzak()));
		}
		if (entitas.getSzakiranyKovetelmeny() != null) {
			dto.setSzakiranyKovetelmeny(szakiranyKovetelmenyMapper.toEntitasDTO(entitas.getSzakiranyKovetelmeny()));
		}
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO toEntitasDTO(KonkretSzakirany entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO(toEditableDTO(entitas));
	}
}
