package vtijuq_szakdoli.mapper.megvalositas;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.dao.entitas.megvalositas.TematikaDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tematika;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;

public class TematikaMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TematikaDTO,
		TematikaDTO,
		Tematika> {

	@Inject
	private TematikaDao tematikaDao;

	@Inject
	private TemaMapper temaMapper;

	@Inject
	private IsmeretSzintMapper ismeretSzintMapper;

	@Override
	public Tematika findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TematikaDTO dto) {
		return findByEntitasDTO(tematikaDao, dto, Tematika.class);
	}

	@Override
	public Tematika findOrNewEmptyByEditableDTO(TematikaDTO dto) {
		return findOrNewEmptyByEditableDTO(tematikaDao, dto, Tematika.class);
	}

	@Override
	public Tematika toEntitas(TematikaDTO dto) {
		Tematika entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//optional merge of complex fields
		final TemaDTO tema = dto.getTema();
		if (dto.getTema() instanceof EditableDTO) {
			entitas.setTema(temaMapper.toEntitas((vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO)tema));
		} else if (tema != null){
			entitas.setTema(temaMapper.findByEntitasDTO(tema));
		}
		//copy of non-editable fields
		if (dto.getLeadottSzint() != null) {
			entitas.setLeadottSzint(ismeretSzintMapper.findByEntitasDTO(dto.getLeadottSzint()));
		}
		if (dto.getSzamonkertSzint() != null) {
			entitas.setSzamonkertSzint(ismeretSzintMapper.findByEntitasDTO(dto.getSzamonkertSzint()));
		}
		//mapping of transient fields
		entitas.setIdTantargy(dto.getTantargy().getId());
		return entitas;
	}

	@Override
	public TematikaDTO toEditableDTO(Tematika entitas) {
		TematikaDTO dto = new TematikaDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		dto.setTema(temaMapper.toEntitasDTO(entitas.getTema()));
		if (entitas.getLeadottSzint() != null) {
			dto.setLeadottSzint(ismeretSzintMapper.toEntitasDTO(entitas.getLeadottSzint()));
		}
		if (entitas.getSzamonkertSzint() != null) {
			dto.setSzamonkertSzint(ismeretSzintMapper.toEntitasDTO(entitas.getSzamonkertSzint()));
		}
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TematikaDTO toEntitasDTO(Tematika entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TematikaDTO(toEditableDTO(entitas));
	}
}
