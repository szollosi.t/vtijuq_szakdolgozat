package vtijuq_szakdoli.mapper.megvalositas;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.dao.entitas.megvalositas.TantargyDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tantargy;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;
import vtijuq_szakdoli.mapper.adminisztracio.SzervezetMapper;

public class TantargyMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO,
		TantargyDTO,
		Tantargy> {

	@Inject
	private TantargyDao tantargyDao;

	@Inject
	private SzervezetMapper szervezetMapper;

	@Override
	public Tantargy findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO dto) {
		return findByEntitasDTO(tantargyDao, dto, Tantargy.class);
	}

	@Override
	public Tantargy findOrNewEmptyByEditableDTO(TantargyDTO dto) {
		return findOrNewEmptyByEditableDTO(tantargyDao, dto, Tantargy.class);
	}

	@Override
	public Tantargy toEntitas(TantargyDTO dto) {
		Tantargy entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//mapping of transient fields
		entitas.setIdSzervezet(dto.getSzervezet() != null ? dto.getSzervezet().getId() : null);
		return entitas;
	}

	@Override
	public TantargyDTO toEditableDTO(Tantargy entitas) {
		TantargyDTO dto = new TantargyDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		if (entitas.getSzervezet() != null) {
			dto.setSzervezet(szervezetMapper.toEntitasDTO(entitas.getSzervezet()));
		}
		//dto.getTematikak().addAll(tematikak.stream().map(t -> t.toEntitasDTO()).collect(Collectors.toList()));//lazy
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO toEntitasDTO(Tantargy entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO(toEditableDTO(entitas));
	}
}
