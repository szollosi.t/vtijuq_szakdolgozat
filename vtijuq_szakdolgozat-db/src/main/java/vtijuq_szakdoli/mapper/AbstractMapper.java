package vtijuq_szakdoli.mapper;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import vtijuq_szakdoli.common.dto.AbstractEntitasDTO;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.exception.ConsistencyException;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

public abstract class AbstractMapper<
		E extends AbstractEntitasDTO & EntitasDTO,
		G extends MappedByDTO<E>> {

	public abstract G findByEntitasDTO(E dto);

	protected <S extends AbstractEntitasDTO & EntitasDTO, V extends AbstractEntitas & MappedByDTO<S>> V findByEntitasDTO(AbstractEntitasDao<V> dao, S dto, Class<V> persistentClass) {
		return findByDTO(dao, dto, persistentClass);
	}

	protected <S extends AbstractEntitasDTO, V extends AbstractEntitas> V findByDTO(AbstractEntitasDao<V> dao, S dto, Class<V> persistentClass) {
		if (!dto.getEntitasClass().isAssignableFrom(persistentClass)) {
			throw new ConsistencyException("error.consistency.dto.entity");
		}
		if (dto.getId() == null) {
			if (dto instanceof EditableDTO) {
				try {
					return persistentClass.newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					throw new RuntimeException(e.getMessage(), e);
				}
			} else {
				throw new RuntimeException("error.consistency.create.nonEditable");
			}
		}
		return dao.findById(dto.getId());
	}

	public abstract E toEntitasDTO(G entitas);

	public List<E> toEntitasDTOList(Collection<G> entitasCollection){
		return entitasCollection.stream().map(this::toEntitasDTO).collect(Collectors.toList());
	}

	public Set<E> toEntitasDTOSet(Collection<G> entitasCollection){
		return entitasCollection.stream().map(this::toEntitasDTO).collect(Collectors.toSet());
	}

}
