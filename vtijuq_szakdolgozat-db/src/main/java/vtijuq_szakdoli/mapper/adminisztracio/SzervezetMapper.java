package vtijuq_szakdoli.mapper.adminisztracio;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.dao.entitas.adminisztracio.SzervezetDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;

public class SzervezetMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO,
		SzervezetDTO,
		Szervezet> {

	@Inject
	private SzervezetDao szervezetDao;

	@Inject
	private SzervezetTipusMapper szervezetTipusMapper;

	@Override
	public Szervezet findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO dto) {
		return findByEntitasDTO(szervezetDao, dto, Szervezet.class);
	}

	@Override
	public Szervezet findOrNewEmptyByEditableDTO(SzervezetDTO dto) {
		return findOrNewEmptyByEditableDTO(szervezetDao, dto, Szervezet.class);
	}

	@Override
	public Szervezet toEntitas(SzervezetDTO dto) {
		Szervezet entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//copy of non-editable fields
		if (dto.getSzervezetTipus() != null) {
			entitas.setSzervezetTipus(szervezetTipusMapper.findByEntitasDTO(dto.getSzervezetTipus()));
		}
		return entitas;
	}

	@Override
	public SzervezetDTO toEditableDTO(Szervezet entitas) {
		SzervezetDTO dto = new SzervezetDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		dto.setSzervezetTipus(szervezetTipusMapper.toEntitasDTO(entitas.getSzervezetTipus()));
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO toEntitasDTO(Szervezet entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO(toEditableDTO(entitas));
	}
}
