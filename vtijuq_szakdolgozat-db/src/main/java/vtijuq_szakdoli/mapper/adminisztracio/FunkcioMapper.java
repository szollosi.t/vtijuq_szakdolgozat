package vtijuq_szakdoli.mapper.adminisztracio;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.FunkcioDTO;
import vtijuq_szakdoli.dao.entitas.adminisztracio.FunkcioDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Funkcio;
import vtijuq_szakdoli.mapper.AbstractMapper;

public class FunkcioMapper extends AbstractMapper<
		FunkcioDTO,
		Funkcio> {
	@Inject
	private FunkcioDao szervezetTipusDao;

	@Override
	public Funkcio findByEntitasDTO(FunkcioDTO dto) {
		return findByDTO(szervezetTipusDao, dto, Funkcio.class);
	}

	@Override
	public FunkcioDTO toEntitasDTO(Funkcio entitas) {
		FunkcioDTO dto = new FunkcioDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		return dto;
	}
}
