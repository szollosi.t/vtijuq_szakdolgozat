package vtijuq_szakdoli.mapper.adminisztracio;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.dao.entitas.adminisztracio.MunkatarsDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Munkatars;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;

public class MunkatarsMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.MunkatarsDTO,
		MunkatarsDTO,
		Munkatars> {

	@Inject
	private MunkatarsDao munkatarsDao;

	@Inject
	private SzerepkorMapper szerepkorMapper;

	@Inject
	private SzervezetMapper szervezetMapper;

	@Override
	public Munkatars findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.MunkatarsDTO dto) {
		return findByEntitasDTO(munkatarsDao, dto, Munkatars.class);
	}

	@Override
	public Munkatars findOrNewEmptyByEditableDTO(MunkatarsDTO dto) {
		return findOrNewEmptyByEditableDTO(munkatarsDao, dto, Munkatars.class);
	}

	@Override
	public Munkatars toEntitas(MunkatarsDTO dto) {
		Munkatars entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//copy of non-editable fields
		if (dto.getSzervezet() != null) {
			entitas.setSzervezet(szervezetMapper.findByEntitasDTO(dto.getSzervezet()));
		}
		if (dto.getSzerepkor() != null) {
			entitas.setSzerepkor(szerepkorMapper.findByEntitasDTO(dto.getSzerepkor()));
		}
		//TODO special fields
		if (dto.isJelszovaltoztatas()) {
			entitas.setJelszo("jelszotKellValtanom");
			entitas.setSalt(null);
		}
		return entitas;
	}

	@Override
	public MunkatarsDTO toEditableDTO(Munkatars entitas) {
		MunkatarsDTO dto = new MunkatarsDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		if (entitas.getSzervezet() != null) {
			dto.setSzervezet(szervezetMapper.toEntitasDTO(entitas.getSzervezet()));
		}
		if (entitas.getSzerepkor() != null) {
			dto.setSzerepkor(szerepkorMapper.toEntitasDTO(entitas.getSzerepkor()));
		}
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.MunkatarsDTO toEntitasDTO(Munkatars entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.MunkatarsDTO(toEditableDTO(entitas));
	}
}
