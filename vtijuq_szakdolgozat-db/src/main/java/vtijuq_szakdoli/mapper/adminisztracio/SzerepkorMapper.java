package vtijuq_szakdoli.mapper.adminisztracio;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.dao.entitas.adminisztracio.SzerepkorDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szerepkor;
import vtijuq_szakdoli.mapper.AbstractMapper;

public class SzerepkorMapper extends AbstractMapper<
		SzerepkorDTO,
		Szerepkor> {
	@Inject
	private SzerepkorDao szerepkorDao;
	@Inject
	private SzervezetTipusMapper szervezetTipusMapper;
	@Inject
	private JogosultsagMapper jogosultsagMapper;

	@Override
	public Szerepkor findByEntitasDTO(SzerepkorDTO dto) {
		return findByDTO(szerepkorDao, dto, Szerepkor.class);
	}

	@Override
	public SzerepkorDTO toEntitasDTO(Szerepkor entitas) {
		SzerepkorDTO dto = new SzerepkorDTO(entitas, entitas.getSzervezetTipus() != null ? szervezetTipusMapper.toEntitasDTO(entitas.getSzervezetTipus()) : null);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		dto.getJogosultsagok().addAll(jogosultsagMapper.toEntitasDTOSet(entitas.getJogosultsagok()));
		return dto;
	}
}
