package vtijuq_szakdoli.mapper.adminisztracio;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.dao.entitas.adminisztracio.SzervezetTipusDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.SzervezetTipus;
import vtijuq_szakdoli.mapper.AbstractMapper;

public class SzervezetTipusMapper extends AbstractMapper<
		SzervezetTipusDTO,
		SzervezetTipus> {
	@Inject
	private SzervezetTipusDao szervezetTipusDao;

	@Override
	public SzervezetTipus findByEntitasDTO(SzervezetTipusDTO dto) {
		return findByDTO(szervezetTipusDao, dto, SzervezetTipus.class);
	}

	@Override
	public SzervezetTipusDTO toEntitasDTO(SzervezetTipus entitas) {
		SzervezetTipusDTO dto = new SzervezetTipusDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		return dto;
	}
}
