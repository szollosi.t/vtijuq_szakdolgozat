package vtijuq_szakdoli.mapper.adminisztracio;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.JogosultsagDTO;
import vtijuq_szakdoli.dao.entitas.adminisztracio.JogosultsagDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Jogosultsag;
import vtijuq_szakdoli.mapper.AbstractMapper;

public class JogosultsagMapper extends AbstractMapper<
		JogosultsagDTO,
		Jogosultsag> {
	@Inject
	private JogosultsagDao jogosultsagDao;
	@Inject
	private FunkcioMapper funkcioMapper;

	@Override
	public Jogosultsag findByEntitasDTO(JogosultsagDTO dto) {
		return findByDTO(jogosultsagDao, dto, Jogosultsag.class);
	}

	@Override
	public JogosultsagDTO toEntitasDTO(Jogosultsag entitas) {
		//conversion of complex fields
		JogosultsagDTO dto = new JogosultsagDTO(entitas, funkcioMapper.toEntitasDTO(entitas.getFunkcio()));
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//do not merge szerepkor//lazy
		return dto;
	}
}
