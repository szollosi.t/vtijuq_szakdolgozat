package vtijuq_szakdoli.mapper.kovetelmeny;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakmaiJellemzoDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakmaiJellemzo;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;

public class SzakmaiJellemzoMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO,
		SzakmaiJellemzoDTO,
		SzakmaiJellemzo> {

	@Inject
	private SzakmaiJellemzoDao szakmaiJellemzoDao;

	@Inject
	private IsmeretMapper ismeretMapper;

	@Override
	public SzakmaiJellemzo findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO dto) {
		return findByEntitasDTO(szakmaiJellemzoDao, dto, SzakmaiJellemzo.class);
	}

	@Override
	public SzakmaiJellemzo findOrNewEmptyByEditableDTO(SzakmaiJellemzoDTO dto) {
		return findOrNewEmptyByEditableDTO(szakmaiJellemzoDao, dto, SzakmaiJellemzo.class);
	}

	@Override
	public SzakmaiJellemzo toEntitas(SzakmaiJellemzoDTO dto) {
		SzakmaiJellemzo entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//copy of non-editable fields
//		entitas.setSzakiranyKovetelmeny(dao.findByDTO(dto.getSzakiranyKovetelmeny(), SzakiranyKovetelmeny.class));
//		entitas.setFojellemzo(dao.findByDTO(dto.getFojellemzo(), SzakmaiJellemzo.class));
		//optional merge of complex fields
		final IsmeretDTO ismeret = dto.getIsmeret();
		if (ismeret instanceof EditableDTO) {
			entitas.setIsmeret(ismeretMapper.toEntitas((vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO)ismeret));
		} else if (ismeret != null) {
			entitas.setIsmeret(ismeretMapper.findByEntitasDTO(ismeret));
		}
		//merge of collection fields
//		mergeAljellemzok(dto, entitas);//hierarchikus
		return entitas;
	}

	@Override
	public SzakmaiJellemzoDTO toEditableDTO(SzakmaiJellemzo entitas) {
		SzakmaiJellemzoDTO dto = new SzakmaiJellemzoDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		dto.setIsmeret(ismeretMapper.toEntitasDTO(entitas.getIsmeret()));
		//dto.setFojellemzo(fojellemzo.toEntitasDTO());//lazy
		//dto.getAljellemzok().addAll(aljellemzok.stream().map(j -> j.toEntitasDTO()).collect(Collectors.toList()));//hierarchikus
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO toEntitasDTO(SzakmaiJellemzo entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO(toEditableDTO(entitas));
	}
}
