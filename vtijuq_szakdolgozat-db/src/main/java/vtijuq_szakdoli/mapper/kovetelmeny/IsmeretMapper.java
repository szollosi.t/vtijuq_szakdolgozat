package vtijuq_szakdoli.mapper.kovetelmeny;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.IsmeretDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Ismeret;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;

public class IsmeretMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO,
		IsmeretDTO,
		Ismeret> {

	@Inject
	private IsmeretDao ismeretDao;

	@Override
	public Ismeret findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO dto) {
		return findByEntitasDTO(ismeretDao, dto, Ismeret.class);
	}

	@Override
	public Ismeret findOrNewEmptyByEditableDTO(IsmeretDTO dto) {
		return findOrNewEmptyByEditableDTO(ismeretDao, dto, Ismeret.class);
	}

	@Override
	public Ismeret toEntitas(IsmeretDTO dto) {
		Ismeret entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		return entitas;
	}

	@Override
	public IsmeretDTO toEditableDTO(Ismeret entitas) {
		IsmeretDTO dto = new IsmeretDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO toEntitasDTO(Ismeret entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO(toEditableDTO(entitas));
	}
}
