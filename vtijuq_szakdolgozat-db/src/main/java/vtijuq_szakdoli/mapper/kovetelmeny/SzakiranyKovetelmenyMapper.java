package vtijuq_szakdoli.mapper.kovetelmeny;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakiranyKovetelmenyDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakiranyKovetelmeny;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;

public class SzakiranyKovetelmenyMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO,
		SzakiranyKovetelmenyDTO,
		SzakiranyKovetelmeny> {

	@Inject
	private SzakiranyKovetelmenyDao szakiranyKovetelmenyDao;

	@Inject
	private SzakKovetelmenyMapper szakKovetelmenyMapper;

	@Inject
	private SzakiranyMapper szakiranyMapper;

	@Override
	public SzakiranyKovetelmeny findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO dto) {
		return findByEntitasDTO(szakiranyKovetelmenyDao, dto, SzakiranyKovetelmeny.class);
	}

	@Override
	public SzakiranyKovetelmeny findOrNewEmptyByEditableDTO(SzakiranyKovetelmenyDTO dto) {
		return findOrNewEmptyByEditableDTO(szakiranyKovetelmenyDao, dto, SzakiranyKovetelmeny.class);
	}

	@Override
	public SzakiranyKovetelmeny toEntitas(SzakiranyKovetelmenyDTO dto) {
		SzakiranyKovetelmeny entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//copy of non-editable fields
		entitas.setSzakKovetelmeny(szakKovetelmenyMapper.findByEntitasDTO(dto.getSzakKovetelmeny()));
		//optional merge of complex fields
		final SzakiranyDTO szakirany = dto.getSzakirany();
		if (szakirany instanceof EditableDTO) {
			entitas.setSzakirany(szakiranyMapper.toEntitas((vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO) szakirany));
		} else if (szakirany != null) {
			entitas.setSzakirany(szakiranyMapper.findByEntitasDTO(szakirany));
		}
		return entitas;
	}

	@Override
	public SzakiranyKovetelmenyDTO toEditableDTO(SzakiranyKovetelmeny entitas) {
		SzakiranyKovetelmenyDTO dto = new SzakiranyKovetelmenyDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		dto.setSzakKovetelmeny(szakKovetelmenyMapper.toEntitasDTO(entitas.getSzakKovetelmeny()));
		if (entitas.getSzakirany() != null) {
            dto.setSzakirany(szakiranyMapper.toEntitasDTO(entitas.getSzakirany()));
		}
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO toEntitasDTO(SzakiranyKovetelmeny entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO(toEditableDTO(entitas));
	}
}
