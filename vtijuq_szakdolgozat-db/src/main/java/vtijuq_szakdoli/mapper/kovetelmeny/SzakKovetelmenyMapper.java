package vtijuq_szakdoli.mapper.kovetelmeny;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakKovetelmenyDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakKovetelmeny;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;

public class SzakKovetelmenyMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO,
		SzakKovetelmenyDTO,
		SzakKovetelmeny> {

	@Inject
	private SzakKovetelmenyDao szakKovetelmenyDao;

	@Inject
	private SzakMapper szakMapper;

	@Override
	public SzakKovetelmeny findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO dto) {
		return findByEntitasDTO(szakKovetelmenyDao, dto, SzakKovetelmeny.class);
	}

	@Override
	public SzakKovetelmeny findOrNewEmptyByEditableDTO(SzakKovetelmenyDTO dto) {
		return findOrNewEmptyByEditableDTO(szakKovetelmenyDao, dto, SzakKovetelmeny.class);
	}

	@Override
	public SzakKovetelmeny toEntitas(SzakKovetelmenyDTO dto) {
		SzakKovetelmeny entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//copy of non-editable fields
		if (dto.getSzak() != null) {
			entitas.setSzak(szakMapper.findByEntitasDTO(dto.getSzak()));
		}
		//optional merge of complex fields
		//TODO is it necessary?
//		mergeSzakiranyKovetelmenyek(dto, entitas);
		return entitas;
	}

	@Override
	public SzakKovetelmenyDTO toEditableDTO(SzakKovetelmeny entitas) {
		SzakKovetelmenyDTO dto = new SzakKovetelmenyDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		dto.setSzak(szakMapper.toEntitasDTO(entitas.getSzak()));
		//dto.getSzakiranyKovetelmenyek().addAll(szakiranyKovetelmenyek.stream().map(szik ->szik.toEntitasDTO()).collect(Collectors.toList()));//lazy
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO toEntitasDTO(SzakKovetelmeny entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO(toEditableDTO(entitas));
	}
}
