package vtijuq_szakdoli.mapper.kovetelmeny;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakiranyDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Szakirany;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;

public class SzakiranyMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO,
		SzakiranyDTO,
		Szakirany> {

	@Inject
	private SzakiranyDao szakiranyDao;

	@Inject
	private SzakMapper szakMapper;

	@Override
	public Szakirany findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO dto) {
		return findByEntitasDTO(szakiranyDao, dto, Szakirany.class);
	}

	@Override
	public Szakirany findOrNewEmptyByEditableDTO(SzakiranyDTO dto) {
		return findOrNewEmptyByEditableDTO(szakiranyDao, dto, Szakirany.class);
	}

	@Override
	public Szakirany toEntitas(SzakiranyDTO dto) {
		Szakirany entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//copy of non-editable fields
		if (dto.getSzak() != null) {
			entitas.setSzak(szakMapper.findByEntitasDTO(dto.getSzak()));
		}
		return entitas;
	}

	@Override
	public SzakiranyDTO toEditableDTO(Szakirany entitas) {
		SzakiranyDTO dto = new SzakiranyDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		dto.setSzak(szakMapper.toEntitasDTO(entitas.getSzak()));
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO toEntitasDTO(Szakirany entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO(toEditableDTO(entitas));
	}
}
