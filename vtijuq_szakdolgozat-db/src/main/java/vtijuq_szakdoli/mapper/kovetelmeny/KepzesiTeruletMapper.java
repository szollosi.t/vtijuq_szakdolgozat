package vtijuq_szakdoli.mapper.kovetelmeny;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.KepzesiTeruletDTO;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.KepzesiTeruletDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.KepzesiTerulet;
import vtijuq_szakdoli.mapper.AbstractMapper;

public class KepzesiTeruletMapper extends AbstractMapper<
		KepzesiTeruletDTO,
		KepzesiTerulet> {
	@Inject
	private KepzesiTeruletDao szervezetTipusDao;

	@Override
	public KepzesiTerulet findByEntitasDTO(KepzesiTeruletDTO dto) {
		return findByDTO(szervezetTipusDao, dto, KepzesiTerulet.class);
	}

	@Override
	public KepzesiTeruletDTO toEntitasDTO(KepzesiTerulet entitas) {
		KepzesiTeruletDTO dto = new KepzesiTeruletDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		return dto;
	}
}
