package vtijuq_szakdoli.mapper.kovetelmeny;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Szak;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;

public class SzakMapper extends AbstractEditableMapper<
		vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO,
		SzakDTO,
		Szak> {

	@Inject
	private SzakDao szakDao;

	@Inject
	private KepzesiTeruletMapper kepzesiTeruletMapper;

	@Override
	public Szak findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO dto) {
		return findByEntitasDTO(szakDao, dto, Szak.class);
	}

	@Override
	public Szak findOrNewEmptyByEditableDTO(SzakDTO dto) {
		return findOrNewEmptyByEditableDTO(szakDao, dto, Szak.class);
	}

	@Override
	public Szak toEntitas(SzakDTO dto) {
		Szak entitas = findOrNewEmptyByEditableDTO(dto);
		//copy of simple fields
		entitas = MergeUtil.merge(dto, entitas);
		//copy of non-editable fields
		if (dto.getKepzesiTerulet() != null) {
			entitas.setKepzesiTerulet(kepzesiTeruletMapper.findByEntitasDTO(dto.getKepzesiTerulet()));
		}
		return entitas;
	}

	@Override
	public SzakDTO toEditableDTO(Szak entitas) {
		SzakDTO dto = new SzakDTO(entitas);
		//copy of simple fields
		dto = MergeUtil.merge(entitas, dto);
		//conversion of complex fields
		dto.setKepzesiTerulet(kepzesiTeruletMapper.toEntitasDTO(entitas.getKepzesiTerulet()));
		//dto.getSzakiranyok().addAll(szakiranyok.stream().map(szi -> szi.toEntitasDTO()).collect(Collectors.toList()));//lazy
		return dto;
	}

	@Override
	public vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO toEntitasDTO(Szak entitas) {
		return new vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO(toEditableDTO(entitas));
	}
}
