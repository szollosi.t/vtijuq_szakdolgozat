package vtijuq_szakdoli.mapper.akkreditacio;

import javax.inject.Inject;

import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.dao.entitas.akkreditacio.AkkreditacioDao;
import vtijuq_szakdoli.domain.entitas.akkreditacio.Akkreditacio;
import vtijuq_szakdoli.mapper.AbstractEditableMapper;
import vtijuq_szakdoli.mapper.megvalositas.KonkretSzakiranyMapper;

public class AkkreditacioMapper extends AbstractEditableMapper<
        vtijuq_szakdoli.common.dto.entitasDTO.akkreditacio.AkkreditacioDTO,
        AkkreditacioDTO,
        Akkreditacio> {

    @Inject
    private AkkreditacioDao akkreditacioDao;

    @Inject
    private KonkretSzakiranyMapper konkretSzakiranyMapper;

    @Override
    public Akkreditacio findByEntitasDTO(vtijuq_szakdoli.common.dto.entitasDTO.akkreditacio.AkkreditacioDTO dto) {
        return findByEntitasDTO(akkreditacioDao, dto, Akkreditacio.class);
    }

    @Override
    public Akkreditacio findOrNewEmptyByEditableDTO(AkkreditacioDTO dto) {
        return findOrNewEmptyByEditableDTO(akkreditacioDao, dto, Akkreditacio.class);
    }

    @Override
    public Akkreditacio toEntitas(AkkreditacioDTO dto) {
        Akkreditacio entitas = findOrNewEmptyByEditableDTO(dto);
        //copy of simple fields
        entitas = MergeUtil.merge(dto, entitas);
        //copy of transient fields
        if (dto.getKonkretSzakirany() != null) {
            entitas.setIdKonkretSzakirany(dto.getKonkretSzakirany().getId());
        }
        return entitas;
    }

    @Override
    public AkkreditacioDTO toEditableDTO(Akkreditacio entitas) {
        AkkreditacioDTO dto = new AkkreditacioDTO(entitas);
        //copy of simple fields
        dto = MergeUtil.merge(entitas, dto);
        //conversion of complex fields
        if (entitas.getKonkretSzakirany() != null) {
            dto.setKonkretSzakirany(konkretSzakiranyMapper.toEntitasDTO(entitas.getKonkretSzakirany()));
        }
        return dto;
    }

    @Override
    public vtijuq_szakdoli.common.dto.entitasDTO.akkreditacio.AkkreditacioDTO toEntitasDTO(Akkreditacio entitas) {
        return new vtijuq_szakdoli.common.dto.entitasDTO.akkreditacio.AkkreditacioDTO(toEditableDTO(entitas));
    }
}
