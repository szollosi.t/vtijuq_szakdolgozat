package vtijuq_szakdoli.domain.view.adminisztracio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Munkatars;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szerepkor;

@Getter
@Entity
@Immutable
@Table(name = "MUNKATARS")
public class MunkatarsView implements Munkatars, MappedByDTO<MunkatarsDTO> {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "FELHASZNALONEV", nullable = false, length = 50)
	private String felhasznalonev;

	@Column(name = "NEV", nullable = false, length = 100)
	private String nev;

	@Column(name = "AKTIV")
	private boolean aktiv;

	@Column(name = "JELSZOVALTOZTATAS")
	private boolean jelszovaltoztatas;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZERVEZET", referencedColumnName = "ID", nullable = false)
	private SzervezetHierarchiaView szervezet;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZEREPKOR", referencedColumnName = "ID", nullable = false)
	private Szerepkor szerepkor;
}
