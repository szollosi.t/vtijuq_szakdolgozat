package vtijuq_szakdoli.domain.view.adminisztracio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetHierarchiaDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.domain.entitas.adminisztracio.SzervezetTipus;

@Getter
@Entity
@Immutable
@Table(name = "V_SZERVEZET_HIERARCHIA")
public class SzervezetHierarchiaView implements vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet, Serializable,
                                                MappedByDTO<SzervezetHierarchiaDTO> {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@OneToOne
	private Szervezet szervezet;

	@Column(name = "ROVIDITES", length = 50)
	private String rovidites;

	@Column(name = "NEV", nullable = false, length = 100)
	private String nev;

	@Column(name = "HIERSZINT")
	private Integer hierszint;

	@Column(name = "ID_ROOT")
	private Long idRoot;

	@Column(name = "ROV_PATH", length = 4000)
	private String rovPath;

	@Column(name = "ID_PATH", length = 4000)
	private String idPath;

	@Column(name = "INTEZMENYI")
	private Boolean intezmenyi;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTIPUS", referencedColumnName = "ID", nullable = false)
	private SzervezetTipus szervezetTipus;

	@Transient
	public String getKod() {
		return getSzervezet().getKod();
	}

	@Transient
	public String getCim() {
		return getSzervezet().getCim();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzervezetHierarchiaView that = (SzervezetHierarchiaView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
