package vtijuq_szakdoli.domain.view.adminisztracio;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Jogosultsag.JogosultsagSzint;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Entity
@Immutable
@Table(name = "V_MUNKATARS_JOGOSULTSAG")
public class MunkatarsJogosultsagView implements Serializable {

	@Id @Column(name = "IDMUNKATARS", nullable = false)
	private Long idmunkatars;

	@Column(name = "FELHASZNALONEV", nullable = false, length = 50)
	private String felhasznalonev;

	@Column(name = "AKTIV")
	private Boolean aktiv;

	@Id @Column(name = "FUNKCIOKOD", nullable = false, length = 50)
	private String funkciokod;

	@Column(name = "FUNKCIONEV", nullable = false, length = 50)
	private String funkcionev;

	@Enumerated(EnumType.STRING)
	@Column(name = "SZINT", length = 10)
	private JogosultsagSzint szint;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		MunkatarsJogosultsagView that = (MunkatarsJogosultsagView) o;

		return new EqualsBuilder()
				.append(idmunkatars, that.idmunkatars)
				.append(funkciokod, that.funkciokod)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idmunkatars)
				.append(funkciokod)
				.toHashCode();
	}
}
