package vtijuq_szakdoli.domain.view.adminisztracio;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.domain.entitas.adminisztracio.SzervezetTipus;

@Getter
@Entity
@Immutable
@Table(name = "SZERVEZET")
public class SzervezetView implements Szervezet, MappedByDTO<SzervezetDTO> {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "NEV", nullable = false, length = 100)
	private String nev;

	@Column(name = "ROVIDITES", length = 50)
	private String rovidites;

	@Column(name = "CIM", length = 400)
	private String cim;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTIPUS", referencedColumnName = "ID", nullable = false)
	private SzervezetTipus szervezetTipus;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "IDFOSZERVEZET", referencedColumnName = "ID")
	private SzervezetView foszervezetView;

	@OneToMany(mappedBy = "foszervezetView", fetch = FetchType.EAGER)
	private List<SzervezetView> alszervezetViewk;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzervezetView that = (SzervezetView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
