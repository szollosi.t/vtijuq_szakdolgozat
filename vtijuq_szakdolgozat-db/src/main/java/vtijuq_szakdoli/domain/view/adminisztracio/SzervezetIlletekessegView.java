package vtijuq_szakdoli.domain.view.adminisztracio;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Entity
@Immutable
@Table(name = "V_SZERVEZET_ILLETEKESSEG")
public class SzervezetIlletekessegView implements Serializable {

	@Id @Column(name = "IDFOSZERV", nullable = false)
	private Long idfoszerv;

	@Id @Column(name = "IDSZERVEZET", nullable = false)
	private Long idszervezet;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzervezetIlletekessegView that = (SzervezetIlletekessegView) o;

		return new EqualsBuilder()
				.append(idfoszerv, that.idfoszerv)
				.append(idszervezet, that.idszervezet)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idfoszerv)
				.append(idszervezet)
				.toHashCode();
	}
}
