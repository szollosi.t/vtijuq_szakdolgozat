package vtijuq_szakdoli.domain.view.adminisztracio;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Entity
@Immutable
@Table(name = "V_MUNKATARS_ILLETEKESSEG")
public class MunkatarsIlletekessegView implements Serializable {

	@Id @Column(name = "IDMUNKATARS", nullable = false)
	private Long idmunkatars;

	@Id @Column(name = "IDSZERVEZET", nullable = false)
	private Long idszervezet;

	@Column(name = "FELHASZNALONEV", nullable = false, length = 50)
	private String felhasznalonev;

	@Column(name = "AKTIV")
	private Boolean aktiv;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		MunkatarsIlletekessegView that = (MunkatarsIlletekessegView) o;

		return new EqualsBuilder()
				.append(idmunkatars, that.idmunkatars)
				.append(idszervezet, that.idszervezet)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idmunkatars)
				.append(idszervezet)
				.toHashCode();
	}
}
