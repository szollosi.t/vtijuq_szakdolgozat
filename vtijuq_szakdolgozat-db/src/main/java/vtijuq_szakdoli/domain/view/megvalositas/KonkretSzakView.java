package vtijuq_szakdoli.domain.view.megvalositas;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakWithSzakiranyokDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzak;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.domain.view.kovetelmeny.SzakKovetelmenyView;

@Getter
@Entity
@Immutable
@Table(name = "KONKRETSZAK")
public class KonkretSzakView implements KonkretSzak, MappedByDTO<KonkretSzakWithSzakiranyokDTO> {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "SZAKMAIGYAKKREDIT")
	private Integer szakmaigyakKredit;

	@Column(name = "SZABVALKREDIT")
	private Integer szabvalKredit;

	@Column(name = "SZAKIRANYKREDIT")
	private Integer szakiranyKredit;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZAKKOVETELMENY", referencedColumnName = "ID", nullable = false)
	private SzakKovetelmenyView szakKovetelmeny;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZERVEZET", referencedColumnName = "ID", nullable = false)
	private Szervezet szervezet;

	@OneToMany(mappedBy = "konkretSzak", fetch = FetchType.LAZY)
	private List<KonkretSzakiranyView> konkretSzakiranyok;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		KonkretSzakView that = (KonkretSzakView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
