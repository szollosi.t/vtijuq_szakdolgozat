package vtijuq_szakdoli.domain.view.megvalositas;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Entity
@Immutable
@Table(name = "V_AKK_SZAK_TT_KREDIT")
public class KonkretSzakTantervKreditView implements Serializable {

	@Id @Column(name = "IDKONKRETSZAKIRANY", nullable = false)
	private Long idkonkretszakirany;

	@Id @Column(name = "IDTANTERV", nullable = false)
	private Long idtanterv;

	@Column(name = "IDKONKRETSZAK", nullable = false)
	private Long idkonkretszak;

	@Column(name = "ALAP")
	private Boolean alap;

	@Column(name = "CSAKKOZOS")
	private Boolean csakkozos;

	@Column(name = "STATUSZ", length = 10)
	private String statusz;

	@Column(name = "KREDIT")
	private Integer kredit;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVKEZDET")
	private Date ervkezdet;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVVEG")
	private Date ervveg;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		KonkretSzakTantervKreditView that = (KonkretSzakTantervKreditView) o;

		return new EqualsBuilder()
				.append(idkonkretszakirany, that.idkonkretszakirany)
				.append(idtanterv, that.idtanterv)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idkonkretszakirany)
				.append(idtanterv)
				.toHashCode();
	}
}
