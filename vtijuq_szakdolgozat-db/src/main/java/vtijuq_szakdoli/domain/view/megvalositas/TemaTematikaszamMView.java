package vtijuq_szakdoli.domain.view.megvalositas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import org.hibernate.annotations.Immutable;

@Getter
@Entity
@Immutable
@Table(name = "MV_TEMATEMATIKASZAM")
public class TemaTematikaszamMView {

	@Id @Column(name = "IDTEMA", nullable = false)
	private Long idTema;

	@Column(name = "TEMATIKASZAM", nullable = false)
	private Integer tematikaSzam;
}
