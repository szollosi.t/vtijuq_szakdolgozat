package vtijuq_szakdoli.domain.view.megvalositas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tanterv;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.TantervStatusz;
import vtijuq_szakdoli.domain.entitas.megvalositas.ErtekelesTipus;
import vtijuq_szakdoli.domain.entitas.megvalositas.KonkretSzakirany;

@Getter
@Entity
@Immutable
@Table(name = "TANTERV")
public class TantervTantargybolView implements Tanterv {
	//TODO kell-e ez?

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVKEZDET")
	private Date ervenyessegKezdet;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVVEG")
	private Date ervenyessegVeg;

	@Enumerated(EnumType.STRING)
	@Column(name = "STATUSZ", length = 10)
	private TantervStatusz statusz;

	@Column(name = "FELEV")
	private Integer felev;

	@Column(name = "KREDIT")
	private Integer kredit;

	@Column(name = "EA_ORA")
	private Integer eaOra;

	@Column(name = "GYAK_ORA")
	private Integer gyakOra;

	@Column(name = "LAB_ORA")
	private Integer labOra;

	@Column(name = "VALID")
	private boolean valid;

	@Column(name = "VEGLEGES")
	private boolean vegleges;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDKONKRETSZAKIRANY", referencedColumnName = "ID", nullable = false)
	private KonkretSzakirany konkretSzakirany;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "IDTANTARGY", referencedColumnName = "ID", nullable = false)
	private TantargyView tantargy;

	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDERTEKELESTIPUS", referencedColumnName = "ID")
	private ErtekelesTipus ertekelesTipus;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		TantervTantargybolView that = (TantervTantargybolView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
