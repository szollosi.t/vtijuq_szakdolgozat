package vtijuq_szakdoli.domain.view.megvalositas;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Entity
@Immutable
@Table(name = "V_AKK_SZAK_KREDIT")
public class KonkretSzakKreditView implements Serializable {

	@Id @Column(name = "IDKONKRETSZAK", nullable = false)
	private Long idkonkretszak;

	@Column(name = "SZABVALKREDIT")
	private Integer szabvalkredit;

	@Column(name = "SZABVALMINKREDIT")
	private Integer szabvalminkredit;

	@Column(name = "SZAKMAIGYAKKREDIT")
	private Integer szakmaigyakkredit;

	@Column(name = "SZAKMAIGYAKMINKREDIT")
	private Integer szakmaigyakminkredit;

	@Column(name = "SZAKIRANYKREDIT")
	private Integer szakiranykredit;

	@Column(name = "SZAKIRANYMINKREDIT")
	private Integer szakiranyminkredit;

	@Column(name = "SZAKIRANYMAXKREDIT")
	private Integer szakiranymaxkredit;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		KonkretSzakKreditView that = (KonkretSzakKreditView) o;

		return new EqualsBuilder()
				.append(idkonkretszak, that.idkonkretszak)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idkonkretszak)
				.toHashCode();
	}
}
