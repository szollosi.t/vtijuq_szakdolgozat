package vtijuq_szakdoli.domain.view.megvalositas;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyWithTantervekDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakiranyKovetelmeny;

@Getter
@Entity
@Immutable
@Table(name = "KONKRETSZAKIRANY")
public class KonkretSzakiranyView implements KonkretSzakirany, MappedByDTO<KonkretSzakiranyWithTantervekDTO> {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "NEV", length = 100)
	private String nev;

	@Column(name = "KOD", length = 50)
	private String kod;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDKONKRETSZAK", referencedColumnName = "ID", nullable = false)
	private KonkretSzakView konkretSzak;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZAKIRANYKOVETELMENY", referencedColumnName = "ID", nullable = false)
	private SzakiranyKovetelmeny szakiranyKovetelmeny;

	@OneToMany(mappedBy = "konkretSzakirany", fetch = FetchType.EAGER)
	private List<TantervKonkretSzakiranybolView> tantervek;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		KonkretSzakiranyView that = (KonkretSzakiranyView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
