package vtijuq_szakdoli.domain.view.megvalositas;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tantargy;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tematika;

@Getter
@Entity
@Immutable
@Table(name = "TANTARGY")
public class TantargyView implements Tantargy {
	//TODO kell ez???

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "NEV", nullable = false, length = 100)
	private String nev;

	@Column(name = "KOD", nullable = false, length = 50)
	private String kod;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZERVEZET", referencedColumnName = "ID")
	private Szervezet szervezet;

	@OneToMany(mappedBy = "tantargy", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<TantervTantargybolView> tantervek;

	@OneToMany(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinColumn(name = "IDTANTARGY", referencedColumnName = "ID")
	private List<Tematika> tematikak;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		TantargyView that = (TantargyView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
