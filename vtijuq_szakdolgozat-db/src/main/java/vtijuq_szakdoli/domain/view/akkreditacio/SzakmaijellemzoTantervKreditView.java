package vtijuq_szakdoli.domain.view.akkreditacio;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Entity
@Immutable
@Table(name = "V_AKK_SZJ_TT_KREDIT")
public class SzakmaijellemzoTantervKreditView implements Serializable {

	@Id @Column(name = "IDSZAKMAIJELLEMZO", nullable = false)
	private Long idszakmaijellemzo;

	@Id @Column(name = "IDKONKRETSZAKIRANY", nullable = false)
	private Long idkonkretszakirany;

	@Id @Column(name = "IDTEMATIKA", nullable = false)
	private Long idtematika;

	@Id @Column(name = "IDTANTERV", nullable = false)
	private Long idtanterv;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVKEZDET")
	private Date ervkezdet;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVVEG")
	private Date ervveg;

	@Column(name = "STATUSZ", length = 10)
	private String statusz;

	@Column(name = "KREDIT")
	private Integer kredit;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzakmaijellemzoTantervKreditView that = (SzakmaijellemzoTantervKreditView) o;

		return new EqualsBuilder()
				.append(idszakmaijellemzo, that.idszakmaijellemzo)
				.append(idkonkretszakirany, that.idkonkretszakirany)
				.append(idtanterv, that.idtanterv)
				.append(idtematika, that.idtematika)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idszakmaijellemzo)
				.append(idkonkretszakirany)
				.append(idtanterv)
				.append(idtematika)
				.toHashCode();
	}
}
