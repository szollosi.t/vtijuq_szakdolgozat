package vtijuq_szakdoli.domain.view.akkreditacio;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.dto.entitasDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.akkreditacio.Akkreditacio;
import vtijuq_szakdoli.domain.view.megvalositas.KonkretSzakiranyView;

@Getter
@Entity
@Immutable
@Table(name = "AKKREDITACIO")
public class AkkreditacioView implements Akkreditacio, MappedByDTO<AkkreditacioDTO> {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVKEZDET")
	private Date ervenyessegKezdet;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVVEG")
	private Date ervenyessegVeg;

	@Column(name = "VALID")
	private boolean valid;

	@Column(name = "VEGLEGES")
	private boolean vegleges;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDKONKRETSZAKIRANY", referencedColumnName = "ID", nullable = false)
	private KonkretSzakiranyView konkretSzakirany;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		AkkreditacioView that = (AkkreditacioView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
