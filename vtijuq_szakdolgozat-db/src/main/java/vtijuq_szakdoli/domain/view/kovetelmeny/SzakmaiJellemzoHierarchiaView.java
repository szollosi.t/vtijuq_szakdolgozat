package vtijuq_szakdoli.domain.view.kovetelmeny;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ismeret;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakmaiJellemzo;

@Getter
@Entity
@Immutable
@Table(name = "V_SZJELLEMZO_HIERARCHIA")
public class SzakmaiJellemzoHierarchiaView implements Serializable {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@OneToOne
	private SzakmaiJellemzo szakmaiJellemzo;

	@Column(name = "IDISMERET", nullable = false)
	private Long idismeret;

	@Column(name = "KOD", nullable = false, length = 50)
	private String kod;

	@Column(name = "HIERSZINT")
	private Integer hierszint;

	@Column(name = "ID_ROOT")
	private Long idRoot;

	@Column(name = "ID_PATH", length = 4000)
	private String idPath;

	@Column(name = "ISLEAF")
	private Integer leaf;

	@Transient
	public String getNev() {
		return getSzakmaiJellemzo().getNev();
	}

	@Transient
	public Long getIdSzakiranyKovetelmeny() {
		return getSzakmaiJellemzo().getIdSzakiranyKovetelmeny();
	}

	@Transient
	public Ismeret getIsmeret() {
		return getSzakmaiJellemzo().getIsmeret();
	}

	@Transient
	public Integer getMinKredit() {
		return getSzakmaiJellemzo().getMinKredit();
	}

	@Transient
	public Integer getMaxKredit() {
		return getSzakmaiJellemzo().getMaxKredit();
	}

	@Transient
	public Integer getMinValaszt() {
		return getSzakmaiJellemzo().getMinValaszt();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzakmaiJellemzoHierarchiaView that = (SzakmaiJellemzoHierarchiaView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
