package vtijuq_szakdoli.domain.view.kovetelmeny;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ismeret;

@Getter
@Entity
@Immutable
@Table(name = "ISMERET")
public class IsmeretView implements Ismeret, MappedByDTO<IsmeretDTO> {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "NEV", nullable = false, length = 100)
	private String nev;

	@Column(name = "LEIRAS", length = 1000)
	private String leiras;

	@Column(name = "KOD", nullable = false, length = 50)
	private String kod;

	@ManyToMany(fetch = FetchType.EAGER, targetEntity = IsmeretView.class)
	@JoinTable(name = "ISMERETGRAF",
			joinColumns = {@JoinColumn(name= "IDMI", table = "ISMERET", referencedColumnName = "ID")},
			inverseJoinColumns = @JoinColumn(name= "IDHOVA", table = "ISMERET", referencedColumnName = "ID"))
	private List<IsmeretView> alIsmeretViewk;

}
