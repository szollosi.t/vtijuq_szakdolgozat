package vtijuq_szakdoli.domain.view.kovetelmeny;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Ismeret;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Entity
@Immutable
@Table(name = "V_ISMERET_HIERARCHIA")
public class IsmeretHierarchiaView implements Serializable {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@ManyToOne
	private Ismeret ismeret;

	@Column(name = "KOD", nullable = false, length = 50)
	private String kod;

	@Column(name = "HIERSZINT")
	private Integer hierszint;

	@Column(name = "ID_ROOT")
	private Long idRoot;

	@Column(name = "ID_PATH", length = 4000)
	private String idPath;

	@Column(name = "ISLEAF")
	private Boolean leaf;

	@Transient
	public String getNev() {
		return getIsmeret().getNev();
	}

	@Transient
	public String getLeiras() {
		return getIsmeret().getLeiras();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		IsmeretHierarchiaView that = (IsmeretHierarchiaView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
