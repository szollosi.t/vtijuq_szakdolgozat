package vtijuq_szakdoli.domain.view.kovetelmeny;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyWithSzakmaiJellemzokDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakiranyKovetelmeny;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Szakirany;

@Getter
@Entity
@Immutable
@Table(name = "SZAKIRANYKOVETELMENY")
public class SzakiranyKovetelmenyView implements SzakiranyKovetelmeny, MappedByDTO<SzakiranyKovetelmenyWithSzakmaiJellemzokDTO> {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "CSAKKOZOS")
	private Boolean csakkozos;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZAKKOVETELMENY", referencedColumnName = "ID", nullable = false)
	private SzakKovetelmenyView szakKovetelmeny;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZAKIRANY", referencedColumnName = "ID", nullable = false)
	private Szakirany szakirany;

	@OneToMany(mappedBy = "szakiranyKovetelmeny", fetch = FetchType.EAGER)
	private List<SzakmaiJellemzoView> szakmaiJellemzok;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzakiranyKovetelmenyView that = (SzakiranyKovetelmenyView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
