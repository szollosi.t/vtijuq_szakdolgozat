package vtijuq_szakdoli.domain.view.kovetelmeny;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Entity
@Immutable
@Table(name = "V_SZK_KREDIT_KOV")
public class SzakKovetelmenyKreditKovetelmenyekView implements Serializable {

	@Id @Column(name = "IDSZAKKOVETELMENY", nullable = false)
	private Long idszakkovetelmeny;

	@Column(name = "OSSZKREDIT")
	private Integer osszkredit;

	@Column(name = "SZAKDOLGOZATKREDIT")
	private Integer szakdolgozatkredit;

	@Column(name = "SZABVALMINKREDIT")
	private Integer szabvalminkredit;

	@Column(name = "SZAKIRANYMINKREDIT")
	private Integer szakiranyminkredit;

	@Column(name = "SZAKIRANYMAXKREDIT")
	private Integer szakiranymaxkredit;

	@Column(name = "ALAPSZJMAXKREDIT")
	private Integer alapszjmaxkredit;

	@Column(name = "ALAPSZJMINKREDIT")
	private Integer alapszjminkredit;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzakKovetelmenyKreditKovetelmenyekView that = (SzakKovetelmenyKreditKovetelmenyekView) o;

		return new EqualsBuilder()
				.append(idszakkovetelmeny, that.idszakkovetelmeny)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idszakkovetelmeny)
				.toHashCode();
	}
}
