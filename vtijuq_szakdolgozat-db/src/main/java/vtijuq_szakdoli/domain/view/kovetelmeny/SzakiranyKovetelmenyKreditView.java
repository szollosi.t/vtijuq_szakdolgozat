package vtijuq_szakdoli.domain.view.kovetelmeny;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Entity
@Immutable
@Table(name = "V_SZAKIRANYKOV_KREDIT")
public class SzakiranyKovetelmenyKreditView implements Serializable {

	@Id @Column(name = "IDSZAKIRANYKOVETELMENY", nullable = false)
	private Long idszakiranykovetelmeny;

	@Column(name = "IDSZAKKOVETELMENY", nullable = false)
	private Long idszakkovetelmeny;

	@Column(name = "SZAKIRANYMAXKREDIT")
	private Integer szakiranymaxkredit;

	@Column(name = "SZAKIRANYMINKREDIT")
	private Integer szakiranyminkredit;

	@Column(name = "ALAP")
	private Boolean alap;

	@Column(name = "SUMSZJMAXKREDIT")
	private Integer sumszjmaxkredit;

	@Column(name = "SUMSZJMINKREDIT")
	private Integer sumszjminkredit;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzakiranyKovetelmenyKreditView that = (SzakiranyKovetelmenyKreditView) o;

		return new EqualsBuilder()
				.append(idszakiranykovetelmeny, that.idszakiranykovetelmeny)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idszakiranykovetelmeny)
				.toHashCode();
	}
}
