package vtijuq_szakdoli.domain.view.kovetelmeny;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakmaiJellemzo;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Ismeret;

@Getter
@Entity
@Immutable
@Table(name = "SZAKMAIJELLEMZO")
public class SzakmaiJellemzoView implements SzakmaiJellemzo, MappedByDTO<SzakmaiJellemzoDTO> {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "NEV", length = 100)
	private String nev;

	@Column(name = "MINKREDIT")
	private Integer minKredit;

	@Column(name = "MAXKREDIT")
	private Integer maxKredit;

	@Column(name = "MINVALASZT")
	private Integer minValaszt;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "IDSZAKIRANYKOVETELMENY", referencedColumnName = "ID", nullable = false)
	private SzakiranyKovetelmenyView szakiranyKovetelmeny;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDISMERET", referencedColumnName = "ID", nullable = false)
	private Ismeret ismeret;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "IDFOJELLEMZO", referencedColumnName = "ID")
	private SzakmaiJellemzoView fojellemzo;

	@OneToMany(mappedBy = "fojellemzo", fetch = FetchType.EAGER)
	private List<SzakmaiJellemzoView> aljellemzok;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzakmaiJellemzoView that = (SzakmaiJellemzoView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
