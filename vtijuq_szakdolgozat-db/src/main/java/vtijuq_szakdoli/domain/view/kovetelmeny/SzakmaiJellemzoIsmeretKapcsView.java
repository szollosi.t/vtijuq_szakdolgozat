package vtijuq_szakdoli.domain.view.kovetelmeny;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Entity
@Immutable
@Table(name = "V_SZJELLEMZO_ISMERET_KAPCS")
public class SzakmaiJellemzoIsmeretKapcsView implements Serializable {

	@Id @Column(name = "IDSZAKMAIJELLEMZO", nullable = false)
	private Long idszakmaijellemzo;

	@Id @Column(name = "IDISMERET", nullable = false)
	private Long idismeret;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzakmaiJellemzoIsmeretKapcsView that = (SzakmaiJellemzoIsmeretKapcsView) o;

		return new EqualsBuilder()
				.append(idszakmaijellemzo, that.idszakmaijellemzo)
				.append(idismeret, that.idismeret)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idszakmaijellemzo)
				.append(idismeret)
				.toHashCode();
	}
}
