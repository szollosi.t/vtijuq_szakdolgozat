package vtijuq_szakdoli.domain.view.kovetelmeny;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakKovetelmeny;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Szak;

@Getter
@Entity
@Immutable
@Table(name = "SZAKKOVETELMENY")
public class SzakKovetelmenyView implements SzakKovetelmeny, MappedByDTO<SzakKovetelmenyDTO> {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZAK", referencedColumnName = "ID", nullable = false)
	private Szak szak;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVKEZDET")
	private Date ervenyessegKezdet;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVVEG")
	private Date ervenyessegVeg;

	@Column(name = "VALID")
	private boolean valid;

	@Column(name = "VEGLEGES")
	private boolean vegleges;

	@Column(name = "OKLEVELMEGJELOLES", length = 100)
	private String oklevelMegjeloles;

	@Column(name = "FELEVEK")
	private Integer felevek;

	@Column(name = "OSSZKREDIT")
	private Integer osszKredit;

	@Column(name = "SZAKDOLGOZATKREDIT")
	private Integer szakdolgozatKredit;

	@Column(name = "SZAKMAIGYAKMINKREDIT")
	private Integer szakmaigyakMinKredit;

	@Column(name = "SZABVALMINKREDIT")
	private Integer szabvalMinKredit;

	@Column(name = "SZAKIRANYMINKREDIT")
	private Integer szakiranyMinKredit;

	@Column(name = "SZAKMAIGYAKELOIRASOK", nullable = false, length = 2000)
	private String szakmaigyakEloirasok;

	@Column(name = "SZAKIRANYMAXKREDIT")
	private Integer szakiranyMaxKredit;

	@OneToMany(mappedBy = "szakKovetelmeny", fetch = FetchType.LAZY)
	private List<SzakiranyKovetelmenyView> szakiranyKovetelmenyek;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzakKovetelmenyView that = (SzakKovetelmenyView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
