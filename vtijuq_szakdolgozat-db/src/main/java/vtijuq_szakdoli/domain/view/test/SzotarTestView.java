package vtijuq_szakdoli.domain.view.test;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Immutable;

import vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.test.SzotarTest;

@Getter
@Entity
@Immutable
@Table(name = "SZOTAR")
public class SzotarTestView implements SzotarTest, MappedByDTO<SzotarTestDTO> {

	@Id @Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "IDTIPUS", nullable = false)
	private Long idTipus;

	@Column(name = "KOD", nullable = false, length = 50)
	private String kod;

	@Column(name = "NEV", nullable = false, length = 100)
	private String nev;

	@Column(name = "LEIRAS", length = 400)
	private String leiras;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVKEZDET")
	private Date ervKezdet;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVVEG")
	private Date ervVeg;

	@Column(name = "TECHNIKAI")
	private Boolean technikai;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzotarTestView that = (SzotarTestView) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
