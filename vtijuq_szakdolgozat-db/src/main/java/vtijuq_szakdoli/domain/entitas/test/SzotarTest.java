package vtijuq_szakdoli.domain.entitas.test;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.util.graph.Hierarchical;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Getter @Setter
@Entity @Table(name = "SZOTAR")
public class SzotarTest extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.test.SzotarTest,
		           Hierarchical<SzotarTest>,
		           EditableByDTO<SzotarTestDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO> {
	@Column(name = "IDTIPUS", nullable = false)
	private Long idTipus;
	@Column(name = "KOD", nullable = false, length = 50)
	private String kod;
	@Column(name = "NEV", nullable = false, length = 100)
	private String nev;
	@Column(name = "LEIRAS", length = 400)
	private String leiras;
	@Temporal(TemporalType.DATE)
	@Column(name = "ERVKEZDET")
	private Date ervKezdet;
	@Temporal(TemporalType.DATE)
	@Column(name = "ERVVEG")
	private Date ervVeg;
	@Column(name = "TECHNIKAI")
	private Boolean technikai;

	@Id
	@SequenceGenerator(name = "S_SZOTAR", sequenceName = "S_SZOTAR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_SZOTAR")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

    @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzotarTest that = (SzotarTest) o;
		if (id != null) {
			return id.equals(that.getId());
		} else {
			return Objects.equals(idTipus, that.getIdTipus()) && Objects.equals(kod, that.getKod());
		}
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idTipus)
				.append(kod)
				.toHashCode();
	}
}
