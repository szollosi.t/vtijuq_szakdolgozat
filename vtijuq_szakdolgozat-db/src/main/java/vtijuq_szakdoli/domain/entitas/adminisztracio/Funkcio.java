package vtijuq_szakdoli.domain.entitas.adminisztracio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.FunkcioDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "FUNKCIO")
public class Funkcio extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Funkcio,
		           MappedByDTO<FunkcioDTO> {
	private String nev;
	private String kod;

	@Id
	@SequenceGenerator(name = "S_FUNKCIO", sequenceName = "S_FUNKCIO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_FUNKCIO")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}


	@Column(name = "NEV", nullable = false, length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}


	@Column(name = "KOD", nullable = false, length = 50)
	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}
}
