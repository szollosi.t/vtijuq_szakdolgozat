package vtijuq_szakdoli.domain.entitas.adminisztracio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "MUNKATARS")
public class Munkatars extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Munkatars,
		           EditableByDTO<MunkatarsDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.MunkatarsDTO> {
	private String felhasznalonev;
	private String nev;
	private String jelszo;
	private String salt;
	private boolean aktiv;
	private boolean jelszovaltoztatas;
	private Szervezet szervezet;
	private Szerepkor szerepkor;

	@Id
	@SequenceGenerator(name = "S_MUNKATARS", sequenceName = "S_MUNKATARS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_MUNKATARS")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}


	@Column(name = "FELHASZNALONEV", nullable = false, length = 50)
	public String getFelhasznalonev() {
		return felhasznalonev;
	}

	public void setFelhasznalonev(String felhasznalonev) {
		this.felhasznalonev = felhasznalonev;
	}


	@Column(name = "NEV", nullable = false, length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	@Column(name = "JELSZO", nullable = false, length = 100)
	public String getJelszo() {
		return jelszo;
	}

	public void setJelszo(String jelszo) {
		this.jelszo = jelszo;
	}


	@Column(name = "SALT", length = 100)
	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}


	@Column(name = "AKTIV")
	public boolean isAktiv() {
		return aktiv;
	}

	public void setAktiv(boolean aktiv) {
		this.aktiv = aktiv;
	}


	@Column(name = "JELSZOVALTOZTATAS")
	public boolean isJelszovaltoztatas() {
		return jelszovaltoztatas;
	}

	public void setJelszovaltoztatas(boolean jelszovaltoztatas) {
		this.jelszovaltoztatas = jelszovaltoztatas;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZERVEZET", referencedColumnName = "ID", nullable = false)
	public Szervezet getSzervezet() {
		return szervezet;
	}

	public void setSzervezet(Szervezet szervezet) {
		this.szervezet = szervezet;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZEREPKOR", referencedColumnName = "ID", nullable = false)
	public Szerepkor getSzerepkor() {
		return szerepkor;
	}

	public void setSzerepkor(Szerepkor szerepkor) {
		this.szerepkor = szerepkor;
	}

}
