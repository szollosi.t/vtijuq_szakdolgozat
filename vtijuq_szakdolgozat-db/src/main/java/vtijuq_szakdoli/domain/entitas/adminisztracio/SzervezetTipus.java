package vtijuq_szakdoli.domain.entitas.adminisztracio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "SZERVEZETTIPUS")
public class SzervezetTipus extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.SzervezetTipus,
		           MappedByDTO<SzervezetTipusDTO> {
	private String nev;
	private String kod;

	@Id
	@SequenceGenerator(name = "S_SZERVEZETTIPUS", sequenceName = "S_SZERVEZETTIPUS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_SZERVEZETTIPUS")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}


	@Column(name = "NEV", length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}


	@Column(name = "KOD", length = 50)
	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}
}
