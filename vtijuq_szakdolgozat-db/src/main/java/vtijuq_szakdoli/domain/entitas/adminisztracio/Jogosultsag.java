package vtijuq_szakdoli.domain.entitas.adminisztracio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.JogosultsagDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "JOGOSULTSAG")
public class Jogosultsag extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Jogosultsag,
		           MappedByDTO<JogosultsagDTO> {
	private Szerepkor szerepkor;
	private JogosultsagSzint szint;
	private Funkcio funkcio;

	@Id
	@SequenceGenerator(name = "S_JOGOSULTSAG", sequenceName = "S_JOGOSULTSAG")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_JOGOSULTSAG")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "IDSZEREPKOR", referencedColumnName = "ID", nullable = false)
	public Szerepkor getSzerepkor() {
		return szerepkor;
	}

	public void setSzerepkor(Szerepkor szerepkor) {
		this.szerepkor = szerepkor;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "SZINT", length = 10)
	public JogosultsagSzint getSzint() {
		return szint;
	}

	public void setSzint(JogosultsagSzint szint) {
		this.szint = szint;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDFUNKCIO", referencedColumnName = "ID")
	public Funkcio getFunkcio() {
		return funkcio;
	}

	public void setFunkcio(Funkcio funkcio) {
		this.funkcio = funkcio;
	}
}
