package vtijuq_szakdoli.domain.entitas.adminisztracio;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "SZEREPKOR")
public class Szerepkor extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szerepkor,
		           MappedByDTO<SzerepkorDTO> {
	private String nev;
	private String kod;
	private Set<Jogosultsag> jogosultsagokDb;
	private SzervezetTipus szervezetTipus;

	@Id
	@SequenceGenerator(name = "S_SZEREPKOR", sequenceName = "S_SZEREPKOR")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_SZEREPKOR")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name = "NEV", nullable = false, length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}


	@Column(name = "KOD", nullable = false, length = 50)
	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	@OneToMany(mappedBy = "szerepkor", fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
	public Set<Jogosultsag> getJogosultsagok() {
		if(jogosultsagokDb == null){
			jogosultsagokDb = new HashSet<>();
		}
		return jogosultsagokDb;
	}

	public void setJogosultsagok(Set<Jogosultsag> jogosultsagok) {
		this.jogosultsagokDb = jogosultsagok;
	}

	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZERVEZETTIPUS", referencedColumnName = "ID")
	public SzervezetTipus getSzervezetTipus() {
		return szervezetTipus;
	}

	public void setSzervezetTipus(SzervezetTipus szervezetTipus) {
		this.szervezetTipus = szervezetTipus;
	}
}
