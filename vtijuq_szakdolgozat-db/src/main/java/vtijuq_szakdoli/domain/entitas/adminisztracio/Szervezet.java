package vtijuq_szakdoli.domain.entitas.adminisztracio;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.util.graph.Hierarchical;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "SZERVEZET")
public class Szervezet extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet,
		           Hierarchical<Szervezet>,
		           EditableByDTO<SzervezetDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO> {
	private Long idFoszervezet;
	private String nev;
	private String rovidites;
	private String cim;
	private SzervezetTipus szervezetTipus;

	@Id
	@SequenceGenerator(name = "S_SZERVEZET", sequenceName = "S_SZERVEZET")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_SZERVEZET")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}


	@Column(name = "IDFOSZERVEZET", nullable = false)
	public Long getIdFoszervezet() {
		return idFoszervezet;
	}

	public void setIdFoszervezet(Long idFoszervezet) {
		this.idFoszervezet = idFoszervezet;
	}

	@Column(name = "NEV", nullable = false, length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}


	@Column(name = "ROVIDITES", length = 50)
	public String getRovidites() {
		return rovidites;
	}

	public void setRovidites(String rovidites) {
		this.rovidites = rovidites;
	}


	@Column(name = "CIM", length = 400)
	public String getCim() {
		return cim;
	}

	public void setCim(String cim) {
		this.cim = cim;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTIPUS", referencedColumnName = "ID", nullable = false)
	public SzervezetTipus getSzervezetTipus() {
		return szervezetTipus;
	}

	public void setSzervezetTipus(SzervezetTipus szervezetTipus) {
		this.szervezetTipus = szervezetTipus;
	}

    @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Szervezet that = (Szervezet) o;
		if (id != null) {
			return id.equals(that.getId());
		} else {
			return Objects.equals(idFoszervezet, that.getIdFoszervezet()) && Objects.equals(rovidites, that.getRovidites());
		}
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idFoszervezet)
				.append(rovidites)
				.toHashCode();
	}

	@Override
	public String toString() {
		return String.format("Szervezet{ rovidites='%s', idFoszervezet=%d, szervezetTipus=%s }",
				rovidites, idFoszervezet, szervezetTipus.getKod());
	}
}
