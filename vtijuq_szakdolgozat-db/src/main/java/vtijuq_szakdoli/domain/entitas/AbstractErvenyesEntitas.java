package vtijuq_szakdoli.domain.entitas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import vtijuq_szakdoli.common.interfaces.editable.Ervenyes;

@MappedSuperclass
public abstract class AbstractErvenyesEntitas extends AbstractEntitas implements Ervenyes {

	private Date ervenyessegKezdet;
	private Date ervenyessegVeg;
	private boolean valid;
	private boolean vegleges;

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVKEZDET")
	public Date getErvenyessegKezdet() {
		return ervenyessegKezdet;
	}

	public void setErvenyessegKezdet(Date ervkezdet) {
		this.ervenyessegKezdet = ervkezdet;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "ERVVEG")
	public Date getErvenyessegVeg() {
		return ervenyessegVeg;
	}

	public void setErvenyessegVeg(Date ervveg) {
		this.ervenyessegVeg = ervveg;
	}

	@Column(name = "VALID", nullable = false)
	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	@Column(name = "VEGLEGES", nullable = false)
	public boolean isVegleges() {
		return vegleges;
	}

	public void setVegleges(boolean vegleges) {
		this.vegleges = vegleges;
	}
}
