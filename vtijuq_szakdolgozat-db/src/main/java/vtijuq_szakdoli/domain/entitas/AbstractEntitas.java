package vtijuq_szakdoli.domain.entitas;

import lombok.Setter;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import vtijuq_szakdoli.common.interfaces.Entitas;

public abstract class AbstractEntitas implements Entitas {
	@Setter
	protected Long id;

	public int compareTo(AbstractEntitas other){
		return Long.compare(id, other.id);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		AbstractEntitas that = (AbstractEntitas) o;
		return id.equals(that.id);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
