package vtijuq_szakdoli.domain.entitas.kovetelmeny;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import vtijuq_szakdoli.common.interfaces.subordination.Subordination;

@Entity
@Getter @Setter
@Table(name = "ISMERETGRAF")
@IdClass(IsmeretElPK.class)
public class IsmeretEl implements Subordination<Ismeret> {

	@Id @Column(name = "IDMI", nullable = false)
	private Long idmi;
	@Id @Column(name = "IDHOVA", nullable = false)
	private Long idhova;

	@Transient
	@Override
	public Long getPrincipalId() {
		return getIdhova();
	}

	@Transient
	@Override
	public Long getSubordinateId() {
		return getIdmi();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		IsmeretEl ismeretEl = (IsmeretEl) o;

		return new EqualsBuilder()
				.append(idmi, ismeretEl.idmi)
				.append(idhova, ismeretEl.idhova)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idmi)
				.append(idhova)
				.toHashCode();
	}
}
