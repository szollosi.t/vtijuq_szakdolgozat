package vtijuq_szakdoli.domain.entitas.kovetelmeny;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class IsmeretElPK implements Serializable {
	private Long idmi;
	private Long idhova;

	@Id
	@Column(name = "IDMI", nullable = false)
	public Long getIdmi() {
		return idmi;
	}

	public void setIdmi(Long idmi) {
		this.idmi = idmi;
	}

	@Id
	@Column(name = "IDHOVA", nullable = false)
	public Long getIdhova() {
		return idhova;
	}

	public void setIdhova(Long idhova) {
		this.idhova = idhova;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		IsmeretElPK that = (IsmeretElPK) o;

		return new EqualsBuilder()
				.append(idmi, that.idmi)
				.append(idhova, that.idhova)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idmi)
				.append(idhova)
				.toHashCode();
	}
}
