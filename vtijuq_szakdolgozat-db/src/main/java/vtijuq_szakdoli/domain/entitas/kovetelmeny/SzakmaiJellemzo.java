package vtijuq_szakdoli.domain.entitas.kovetelmeny;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.util.graph.Hierarchical;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "SZAKMAIJELLEMZO")
public class SzakmaiJellemzo extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakmaiJellemzo,
		           Hierarchical<SzakmaiJellemzo>,
		           EditableByDTO<SzakmaiJellemzoDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO> {
	private Long idSzakiranyKovetelmeny;
	private Ismeret ismeret;
	private String nev;
	private Integer minKredit;
	private Integer maxKredit;
	private Integer minValaszt;
	private Long idFojellemzo;

	@Id
	@SequenceGenerator(name = "S_SZAKMAIJELLEMZO", sequenceName = "S_SZAKMAIJELLEMZO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_SZAKMAIJELLEMZO")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name = "IDSZAKIRANYKOVETELMENY", nullable = false)
	public Long getIdSzakiranyKovetelmeny() {
		return idSzakiranyKovetelmeny;
	}

	public void setIdSzakiranyKovetelmeny(Long idSzakiranyKovetelmeny) {
		this.idSzakiranyKovetelmeny = idSzakiranyKovetelmeny;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDISMERET", referencedColumnName = "ID", nullable = false)
	public Ismeret getIsmeret() {
		return ismeret;
	}

	public void setIsmeret(Ismeret ismeret) {
		this.ismeret = ismeret;
	}

	@Column(name = "NEV", length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	@Column(name = "MINKREDIT")
	public Integer getMinKredit() {
		return minKredit;
	}

	public void setMinKredit(Integer minKredit) {
		this.minKredit = minKredit;
	}

	@Column(name = "MAXKREDIT")
	public Integer getMaxKredit() {
		return maxKredit;
	}

	public void setMaxKredit(Integer maxKredit) {
		this.maxKredit = maxKredit;
	}

	@Column(name = "MINVALASZT")
	public Integer getMinValaszt() {
		return minValaszt;
	}

	public void setMinValaszt(Integer minValaszt) {
		this.minValaszt = minValaszt;
	}

	@Column(name = "IDFOJELLEMZO")
	public Long getIdFojellemzo() {
		return idFojellemzo;
	}

	public void setIdFojellemzo(Long idFojellemzo) {
		this.idFojellemzo = idFojellemzo;
	}

    @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SzakmaiJellemzo that = (SzakmaiJellemzo) o;
		if (id != null) {
			return id.equals(that.getId());
		} else {
			return Objects.equals(idSzakiranyKovetelmeny, that.getIdSzakiranyKovetelmeny()) && Objects.equals(ismeret, that.getIsmeret());
		}
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(idSzakiranyKovetelmeny)
				.append(ismeret)
				.toHashCode();
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}
}
