package vtijuq_szakdoli.domain.entitas.kovetelmeny;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "SZAKIRANYKOVETELMENY")
public class SzakiranyKovetelmeny extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakiranyKovetelmeny,
		           EditableByDTO<SzakiranyKovetelmenyDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO> {
	private Boolean csakkozos;
	private SzakKovetelmeny szakKovetelmeny;
	private Szakirany szakirany;

	@Id
	@SequenceGenerator(name = "S_SZAKIRANYKOVETELMENY", sequenceName = "S_SZAKIRANYKOVETELMENY")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_SZAKIRANYKOVETELMENY")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}


	@Column(name = "CSAKKOZOS")
	public Boolean getCsakkozos() {
		return csakkozos;
	}

	public void setCsakkozos(Boolean csakkozos) {
		this.csakkozos = csakkozos;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZAKKOVETELMENY", referencedColumnName = "ID", nullable = false)
	public SzakKovetelmeny getSzakKovetelmeny() {
		return szakKovetelmeny;
	}

	public void setSzakKovetelmeny(SzakKovetelmeny szakKovetelmeny) {
		this.szakKovetelmeny = szakKovetelmeny;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZAKIRANY", referencedColumnName = "ID", nullable = false)
	public Szakirany getSzakirany() {
		return szakirany;
	}

	public void setSzakirany(Szakirany szakirany) {
		this.szakirany = szakirany;
	}

}
