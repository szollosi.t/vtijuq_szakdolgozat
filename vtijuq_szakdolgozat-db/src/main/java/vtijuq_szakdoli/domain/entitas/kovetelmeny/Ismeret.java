package vtijuq_szakdoli.domain.entitas.kovetelmeny;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.util.graph.Hierarchical;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "ISMERET")
public class Ismeret extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ismeret,
		           Hierarchical<Ismeret>,
		           EditableByDTO<IsmeretDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO> {
	private String nev;
	private String leiras;
	private String kod;

	@Id
	@SequenceGenerator(name = "S_ISMERET", sequenceName = "S_ISMERET")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_ISMERET")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}


	@Column(name = "NEV", nullable = false, length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}


	@Column(name = "LEIRAS", length = 1000)
	public String getLeiras() {
		return leiras;
	}

	public void setLeiras(String leiras) {
		this.leiras = leiras;
	}


	@Column(name = "KOD", nullable = false, length = 50)
	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

    @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Ismeret that = (Ismeret) o;
		if (id != null) {
			return id.equals(that.getId());
		} else {
			return Objects.equals(kod, that.getKod());
		}
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(kod)
				.toHashCode();
	}
}
