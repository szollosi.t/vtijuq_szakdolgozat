package vtijuq_szakdoli.domain.entitas.kovetelmeny;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "SZAKIRANY")
public class Szakirany extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szakirany,
		           EditableByDTO<SzakiranyDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO> {
	private Szak szak;
	private String nev;
	private String leiras;

	@Id
	@SequenceGenerator(name = "S_SZAKIRANY", sequenceName = "S_SZAKIRANY")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_SZAKIRANY")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZAK", referencedColumnName = "ID", nullable = false)
	public Szak getSzak() {
		return szak;
	}

	public void setSzak(Szak szak) {
		this.szak = szak;
	}

	@Column(name = "NEV", nullable = false, length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	@Column(name = "LEIRAS", length = 1000)
	public String getLeiras() {
		return leiras;
	}

	public void setLeiras(String leiras) {
		this.leiras = leiras;
	}

}
