package vtijuq_szakdoli.domain.entitas.kovetelmeny;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.KepzesiTeruletDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "KEPZESITERULET")
public class KepzesiTerulet extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.KepzesiTerulet,
		           MappedByDTO<KepzesiTeruletDTO> {
	private String kod;
	private String nev;

	@Id
	@SequenceGenerator(name = "S_KEPZESITERULET", sequenceName = "S_KEPZESITERULET")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_KEPZESITERULET")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}


	@Column(name = "KOD", nullable = false, length = 50)
	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}


	@Column(name = "NEV", nullable = false, length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}
}
