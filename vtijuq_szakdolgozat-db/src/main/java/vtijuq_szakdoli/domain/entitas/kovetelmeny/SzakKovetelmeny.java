package vtijuq_szakdoli.domain.entitas.kovetelmeny;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractErvenyesEntitas;

@Entity
@Table(name = "SZAKKOVETELMENY")
public class SzakKovetelmeny extends AbstractErvenyesEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakKovetelmeny,
		           EditableByDTO<SzakKovetelmenyDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO> {
	private Szak szak;
	private String oklevelMegjeloles;
	private Integer felevek;
	private Integer osszKredit;
	private Integer szakdolgozatKredit;
	private Integer szakmaigyakMinKredit;
	private Integer szabvalMinKredit;
	private Integer szakiranyMinKredit;
	private String szakmaigyakEloirasok;
	private Integer szakiranyMaxKredit;

	@Id
	@SequenceGenerator(name = "S_SZAKKOVETELMENY", sequenceName = "S_SZAKKOVETELMENY")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_SZAKKOVETELMENY")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZAK", referencedColumnName = "ID", nullable = false)
	public Szak getSzak() {
		return szak;
	}

	public void setSzak(Szak szak) {
		this.szak = szak;
	}

	@Column(name = "OKLEVELMEGJELOLES", length = 100)
	public String getOklevelMegjeloles() {
		return oklevelMegjeloles;
	}

	public void setOklevelMegjeloles(String oklevelMegjeloles) {
		this.oklevelMegjeloles = oklevelMegjeloles;
	}

	@Column(name = "FELEVEK")
	public Integer getFelevek() {
		return felevek;
	}

	public void setFelevek(Integer felevek) {
		this.felevek = felevek;
	}

	@Column(name = "OSSZKREDIT")
	public Integer getOsszKredit() {
		return osszKredit;
	}

	public void setOsszKredit(Integer osszKredit) {
		this.osszKredit = osszKredit;
	}

	@Column(name = "SZAKDOLGOZATKREDIT")
	public Integer getSzakdolgozatKredit() {
		return szakdolgozatKredit;
	}

	public void setSzakdolgozatKredit(Integer szakdolgozatKredit) {
		this.szakdolgozatKredit = szakdolgozatKredit;
	}

	@Column(name = "SZAKMAIGYAKMINKREDIT")
	public Integer getSzakmaigyakMinKredit() {
		return szakmaigyakMinKredit;
	}

	public void setSzakmaigyakMinKredit(Integer szakmaigyakMinKredit) {
		this.szakmaigyakMinKredit = szakmaigyakMinKredit;
	}

	@Column(name = "SZABVALMINKREDIT")
	public Integer getSzabvalMinKredit() {
		return szabvalMinKredit;
	}

	public void setSzabvalMinKredit(Integer szabvalMinKredit) {
		this.szabvalMinKredit = szabvalMinKredit;
	}

	@Column(name = "SZAKIRANYMINKREDIT")
	public Integer getSzakiranyMinKredit() {
		return szakiranyMinKredit;
	}

	public void setSzakiranyMinKredit(Integer szakiranyMinKredit) {
		this.szakiranyMinKredit = szakiranyMinKredit;
	}

	@Column(name = "SZAKMAIGYAKELOIRASOK", length = 2000)
	public String getSzakmaigyakEloirasok() {
		return szakmaigyakEloirasok;
	}

	public void setSzakmaigyakEloirasok(String szakmaigyakEloirasok) {
		this.szakmaigyakEloirasok = szakmaigyakEloirasok;
	}

	@Column(name = "SZAKIRANYMAXKREDIT")
	public Integer getSzakiranyMaxKredit() {
		return szakiranyMaxKredit;
	}

	public void setSzakiranyMaxKredit(Integer szakiranyMaxKredit) {
		this.szakiranyMaxKredit = szakiranyMaxKredit;
	}

}
