package vtijuq_szakdoli.domain.entitas.kovetelmeny;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ciklus;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "SZAK")
public class Szak extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szak,
		           EditableByDTO<SzakDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO> {

	private KepzesiTerulet kepzesiTerulet;
	private Ciklus ciklus;
	private String nev;
	private List<Szakirany> szakiranyok;

	@Id
	@SequenceGenerator(name = "S_SZAK", sequenceName = "S_SZAK")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_SZAK")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}


	@Column(name = "NEV", nullable = false, length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTERULET", referencedColumnName = "ID", nullable = false)
	public KepzesiTerulet getKepzesiTerulet() {
		return kepzesiTerulet;
	}

	public void setKepzesiTerulet(KepzesiTerulet kepzesiTerulet) {
		this.kepzesiTerulet = kepzesiTerulet;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "CIKLUS", nullable = false, length = 1)
	public Ciklus getCiklus() {
		return ciklus;
	}

	public void setCiklus(Ciklus ciklus) {
		this.ciklus = ciklus;
	}

	@OneToMany(mappedBy = "szak", fetch = FetchType.LAZY)
	public List<Szakirany> getSzakiranyok() {
		if(szakiranyok == null){
			szakiranyok = new ArrayList<>();
		}
		return szakiranyok;
	}

	public void setSzakiranyok(List<Szakirany> szakiranyok) {
		this.szakiranyok = szakiranyok;
	}

}
