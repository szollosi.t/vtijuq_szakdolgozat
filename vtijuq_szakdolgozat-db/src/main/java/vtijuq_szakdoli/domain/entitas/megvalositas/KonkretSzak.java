package vtijuq_szakdoli.domain.entitas.megvalositas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakKovetelmeny;

@Entity
@Table(name = "KONKRETSZAK")
public class KonkretSzak extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzak,
		           EditableByDTO<KonkretSzakDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO> {

	private Integer szakmaigyakKredit;
	private Integer szabvalKredit;
	private Integer szakiranyKredit;
	private SzakKovetelmeny szakKovetelmeny;
	private Szervezet szervezet;

	@Id
	@SequenceGenerator(name = "S_KONKRETSZAK", sequenceName = "S_KONKRETSZAK")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_KONKRETSZAK")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name = "SZAKMAIGYAKKREDIT")
	public Integer getSzakmaigyakKredit() {
		return szakmaigyakKredit;
	}

	public void setSzakmaigyakKredit(Integer szakmaigyakKredit) {
		this.szakmaigyakKredit = szakmaigyakKredit;
	}

	@Column(name = "SZABVALKREDIT")
	public Integer getSzabvalKredit() {
		return szabvalKredit;
	}

	public void setSzabvalKredit(Integer szabvalKredit) {
		this.szabvalKredit = szabvalKredit;
	}

	@Column(name = "SZAKIRANYKREDIT")
	public Integer getSzakiranyKredit() {
		return szakiranyKredit;
	}

	public void setSzakiranyKredit(Integer szakiranyKredit) {
		this.szakiranyKredit = szakiranyKredit;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZAKKOVETELMENY", referencedColumnName = "ID", nullable = false)
	public SzakKovetelmeny getSzakKovetelmeny() {
		return szakKovetelmeny;
	}

	public void setSzakKovetelmeny(SzakKovetelmeny szakKovetelmeny) {
		this.szakKovetelmeny = szakKovetelmeny;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZERVEZET", referencedColumnName = "ID", nullable = false)
	public Szervezet getSzervezet() {
		return szervezet;
	}

	public void setSzervezet(Szervezet szervezet) {
		this.szervezet = szervezet;
	}

}
