package vtijuq_szakdoli.domain.entitas.megvalositas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.ErtekelesTipusDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

@Entity
@Table(name = "ERTEKELESTIPUS")
public class ErtekelesTipus extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.megvalositas.ErtekelesTipus,
		           MappedByDTO<ErtekelesTipusDTO> {
	private String kod;
	private String nev;
	private String leiras;

	@Id
	@SequenceGenerator(name = "S_ERTEKELESTIPUS", sequenceName = "S_ERTEKELESTIPUS")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_ERTEKELESTIPUS")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}


	@Column(name = "KOD", nullable = false, length = 50)
	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}


	@Column(name = "NEV", nullable = false, length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}


	@Column(name = "LEIRAS", length = 1000)
	public String getLeiras() {
		return leiras;
	}

	public void setLeiras(String leiras) {
		this.leiras = leiras;
	}
}
