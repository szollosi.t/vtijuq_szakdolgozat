package vtijuq_szakdoli.domain.entitas.megvalositas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.TantervStatusz;
import vtijuq_szakdoli.domain.entitas.AbstractErvenyesEntitas;

@Entity
@Table(name = "TANTERV")
public class Tanterv extends AbstractErvenyesEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tanterv,
		           EditableByDTO<TantervDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantervDTO> {
	private Long idKonkretSzakirany;
	private Long idTantargy;
	private TantervStatusz statusz;
	private Integer felev;
	private Integer kredit;
	private Integer eaOra;
	private Integer gyakOra;
	private Integer labOra;
	private ErtekelesTipus ertekelesTipus;

	@Id
	@SequenceGenerator(name = "S_TANTERV", sequenceName = "S_TANTERV")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_TANTERV")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name = "IDKONKRETSZAKIRANY", nullable = false)
	public Long getIdKonkretSzakirany() {
		return idKonkretSzakirany;
	}

	public void setIdKonkretSzakirany(Long idKonkretSzakirany) {
		this.idKonkretSzakirany = idKonkretSzakirany;
	}

	@Column(name = "IDTANTARGY", nullable = false)
	public Long getIdTantargy() {
		return idTantargy;
	}

	public void setIdTantargy(Long idTantargy) {
		this.idTantargy = idTantargy;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "STATUSZ", length = 10)
	public TantervStatusz getStatusz() {
		return statusz;
	}

	public void setStatusz(TantervStatusz statusz) {
		this.statusz = statusz;
	}

	@Column(name = "FELEV")
	public Integer getFelev() {
		return felev;
	}

	public void setFelev(Integer felev) {
		this.felev = felev;
	}

	@Column(name = "KREDIT")
	public Integer getKredit() {
		return kredit;
	}

	public void setKredit(Integer kredit) {
		this.kredit = kredit;
	}

	@Column(name = "EA_ORA")
	public Integer getEaOra() {
		return eaOra;
	}

	public void setEaOra(Integer eaOra) {
		this.eaOra = eaOra;
	}

	@Column(name = "GYAK_ORA")
	public Integer getGyakOra() {
		return gyakOra;
	}

	public void setGyakOra(Integer gyakOra) {
		this.gyakOra = gyakOra;
	}

	@Column(name = "LAB_ORA")
	public Integer getLabOra() {
		return labOra;
	}

	public void setLabOra(Integer labOra) {
		this.labOra = labOra;
	}

	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDERTEKELESTIPUS", referencedColumnName = "ID")
	public ErtekelesTipus getErtekelesTipus() {
		return ertekelesTipus;
	}

	public void setErtekelesTipus(ErtekelesTipus ertekelesTipus) {
		this.ertekelesTipus = ertekelesTipus;
	}

	private KonkretSzakirany konkretSzakirany;
	private Tantargy tantargy;

	@Transient
	public KonkretSzakirany getKonkretSzakirany() {
		return konkretSzakirany;
	}

	public void setKonkretSzakirany(KonkretSzakirany konkretSzakirany) {
		this.konkretSzakirany = konkretSzakirany;
	}

	@Transient
	public Tantargy getTantargy() {
		return tantargy;
	}

	public void setTantargy(Tantargy tantargy) {
		this.tantargy = tantargy;
	}

}
