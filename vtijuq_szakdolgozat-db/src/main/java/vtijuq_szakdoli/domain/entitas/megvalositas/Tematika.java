package vtijuq_szakdoli.domain.entitas.megvalositas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractErvenyesEntitas;

@Entity
@Table(name = "TEMATIKA")
public class Tematika extends AbstractErvenyesEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tematika,
		           EditableByDTO<TematikaDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TematikaDTO> {
	private Long idTantargy;
	private Tema tema;
	private IsmeretSzint leadottSzint;
	private IsmeretSzint szamonkertSzint;

	@Id
	@SequenceGenerator(name = "S_TEMATIKA", sequenceName = "S_TEMATIKA")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_TEMATIKA")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name = "IDTANTARGY", nullable = false)
	public Long getIdTantargy() {
		return idTantargy;
	}

	public void setIdTantargy(Long idTantargy) {
		this.idTantargy = idTantargy;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTEMA", referencedColumnName = "ID", nullable = false)
	public Tema getTema() {
		return tema;
	}

	public void setTema(Tema tema) {
		this.tema = tema;
	}

	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDLEADOTTSZINT", referencedColumnName = "ID")
	public IsmeretSzint getLeadottSzint() {
		return leadottSzint;
	}

	public void setLeadottSzint(IsmeretSzint leadottSzint) {
		this.leadottSzint = leadottSzint;
	}

	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDSZAMONKERTSZINT", referencedColumnName = "ID")
	public IsmeretSzint getSzamonkertSzint() {
		return szamonkertSzint;
	}

	public void setSzamonkertSzint(IsmeretSzint szamonkertSzint) {
		this.szamonkertSzint = szamonkertSzint;
	}

}
