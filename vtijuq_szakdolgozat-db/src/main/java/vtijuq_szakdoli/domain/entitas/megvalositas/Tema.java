package vtijuq_szakdoli.domain.entitas.megvalositas;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Ismeret;

@Entity
@Table(name = "TEMA")
public class Tema extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tema,
		           EditableByDTO<TemaDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO> {
	private String nev;
	private String leiras;
	private Ismeret ismeret;

	@Id
	@SequenceGenerator(name = "S_TEMA", sequenceName = "S_TEMA")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_TEMA")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name = "NEV", length = 100)
	public String getNevDb() {
		return nev;
	}

	public void setNevDb(String nev) {
		this.nev = nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	@Column(name = "LEIRAS", length = 1000)
	public String getLeirasDb() {
		return leiras;
	}

	public void setLeirasDb(String leiras) {
		this.leiras = leiras;
	}

	public void setLeiras(String leiras) {
		this.leiras = leiras;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "IDISMERET", referencedColumnName = "ID", nullable = true)
	public Ismeret getIsmeret() {
		return ismeret;
	}

	public void setIsmeret(Ismeret ismeret) {
		this.ismeret = ismeret;
	}

	@Transient
	public String getKod() {
		return getIsmeret().getKod();
	}

	@Transient
	public String getNev() {
		return isNotBlank(nev) ? nev : getIsmeret().getNev();
	}

	@Transient
	public String getLeiras() {
		return isNotBlank(leiras) ? leiras : getIsmeret().getLeiras();
	}

}
