package vtijuq_szakdoli.domain.entitas.megvalositas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szervezet;

@Entity
@Table(name = "TANTARGY")
public class Tantargy extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tantargy,
		           EditableByDTO<TantargyDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO> {
	private String nev;
	private String kod;
	private Long idSzervezet;

	@Id
	@SequenceGenerator(name = "S_TANTARGY", sequenceName = "S_TANTARGY")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_TANTARGY")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}


	@Column(name = "NEV", nullable = false, length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}


	@Column(name = "KOD", nullable = false, length = 50)
	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	@Column(name = "IDSZERVEZET")
	public Long getIdSzervezet() {
		return idSzervezet;
	}

	public void setIdSzervezet(Long idSzervezet) {
		this.idSzervezet = idSzervezet;
	}

	private Szervezet szervezet;

	@Transient
	@Override
	public Szervezet getSzervezet() {
		return szervezet;
	}

	public void setSzervezet(Szervezet szervezet) {
		this.szervezet = szervezet;
	}

}
