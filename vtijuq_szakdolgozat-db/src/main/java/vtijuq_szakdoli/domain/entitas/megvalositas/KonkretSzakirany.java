package vtijuq_szakdoli.domain.entitas.megvalositas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakiranyKovetelmeny;

@Entity
@Table(name = "KONKRETSZAKIRANY")
public class KonkretSzakirany extends AbstractEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany,
		           EditableByDTO<KonkretSzakiranyDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO> {
	private String nev;
	private String kod;
	private Long idKonkretSzak;
	private Long idSzakiranyKovetelmeny;

	@Id
	@SequenceGenerator(name = "S_KONKRETSZAKIRANY", sequenceName = "S_KONKRETSZAKIRANY")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_KONKRETSZAKIRANY")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name = "NEV", length = 100)
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}


	@Column(name = "KOD", length = 50)
	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	@Column(name = "IDKONKRETSZAK", nullable = false)
	public Long getIdKonkretSzak() {
		return idKonkretSzak;
	}

	public void setIdKonkretSzak(Long idKonkretSzak) {
		this.idKonkretSzak = idKonkretSzak;
	}

	@Column(name = "IDSZAKIRANYKOVETELMENY", nullable = false)
	public Long getIdSzakiranyKovetelmeny() {
		return idSzakiranyKovetelmeny;
	}

	public void setIdSzakiranyKovetelmeny(Long idSzakiranyKovetelmeny) {
		this.idSzakiranyKovetelmeny = idSzakiranyKovetelmeny;
	}

	private KonkretSzak konkretSzak;
	private SzakiranyKovetelmeny szakiranyKovetelmeny;

	@Transient
//	@ManyToOne(optional = false, fetch = FetchType.EAGER)
//	@JoinColumn(name = "IDKONKRETSZAK", referencedColumnName = "ID", nullable = false)
	public KonkretSzak getKonkretSzak() {
		return konkretSzak;
	}

	public void setKonkretSzak(KonkretSzak konkretSzak) {
		this.konkretSzak = konkretSzak;
	}

	@Transient
//	@ManyToOne(optional = false, fetch = FetchType.EAGER)
//	@JoinColumn(name = "IDSZAKIRANYKOVETELMENY", referencedColumnName = "ID", nullable = false)
	public SzakiranyKovetelmeny getSzakiranyKovetelmeny() {
		return szakiranyKovetelmeny;
	}

	public void setSzakiranyKovetelmeny(SzakiranyKovetelmeny szakiranyKovetelmeny) {
		this.szakiranyKovetelmeny = szakiranyKovetelmeny;
	}

}
