package vtijuq_szakdoli.domain.entitas.akkreditacio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.interfaces.EditableByDTO;
import vtijuq_szakdoli.common.interfaces.MappedByDTO;
import vtijuq_szakdoli.domain.entitas.AbstractErvenyesEntitas;
import vtijuq_szakdoli.domain.entitas.megvalositas.KonkretSzakirany;

@Entity
@Table(name = "AKKREDITACIO")
public class Akkreditacio extends AbstractErvenyesEntitas
		implements vtijuq_szakdoli.common.interfaces.entitas.akkreditacio.Akkreditacio,
		           EditableByDTO<AkkreditacioDTO>,
		           MappedByDTO<vtijuq_szakdoli.common.dto.entitasDTO.akkreditacio.AkkreditacioDTO> {

	private Long idKonkretSzakirany;

	@Id
	@SequenceGenerator(name = "S_AKKREDITACIO", sequenceName = "S_AKKREDITACIO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_AKKREDITACIO")
	@Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}

	@Column(name = "IDKONKRETSZAKIRANY", nullable = false)
	public Long getIdKonkretSzakirany() {
		return idKonkretSzakirany;
	}

	public void setIdKonkretSzakirany(Long idKonkretSzakirany) {
		this.idKonkretSzakirany = idKonkretSzakirany;
	}

	private KonkretSzakirany konkretSzakirany;

	@Transient
	public KonkretSzakirany getKonkretSzakirany() {
		return konkretSzakirany;
	}

	public void setKonkretSzakirany(KonkretSzakirany konkretSzakirany) {
		this.konkretSzakirany = konkretSzakirany;
	}

}
