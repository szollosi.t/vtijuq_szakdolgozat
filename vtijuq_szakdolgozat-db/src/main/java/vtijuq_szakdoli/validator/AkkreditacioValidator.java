package vtijuq_szakdoli.validator;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.exception.ValidationException;
import vtijuq_szakdoli.dao.entitas.akkreditacio.AkkreditacioDao;
import vtijuq_szakdoli.dao.entitas.megvalositas.TantervDao;
import vtijuq_szakdoli.dao.entitas.megvalositas.TematikaDao;
import vtijuq_szakdoli.domain.entitas.akkreditacio.Akkreditacio;
import vtijuq_szakdoli.domain.entitas.akkreditacio.QAkkreditacio;
import vtijuq_szakdoli.domain.entitas.megvalositas.QKonkretSzak;
import vtijuq_szakdoli.domain.entitas.megvalositas.QKonkretSzakirany;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTanterv;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTema;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTematika;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tanterv;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tematika;
import vtijuq_szakdoli.domain.view.akkreditacio.QSzakmaijellemzoTantervKreditView;
import vtijuq_szakdoli.domain.view.megvalositas.QKonkretSzakKreditView;
import vtijuq_szakdoli.domain.view.megvalositas.QKonkretSzakTantervKreditView;

@Stateless
public class AkkreditacioValidator extends AbstractValidator<Akkreditacio> {
	//TODO ezeket kell felügyelnie:
	//akkreditacio*, konkretSzak, konkretSzakirany, tanterv*, tantargy, tematika*, tema

	private static final Logger LOG = LoggerFactory.getLogger(AkkreditacioValidator.class);

	@Inject
	private AkkreditacioDao akkreditacioDao;

	@Inject
	private TantervDao tantervDao;

	@Inject
	private TematikaDao tematikaDao;

	@Override
	protected Akkreditacio findById(Long id) {
		return akkreditacioDao.findById(id);
	}

	@Override
	protected Akkreditacio save(Akkreditacio entitas) {
		return akkreditacioDao.save(entitas);
	}

	@Override
	public Set<String> finalise(Long id) throws ValidationException {
		final Akkreditacio entitas = findById(id);
		final Set<String> validationErrors = validate(entitas);
		//TODO ideiglenes
		validationErrors.add("finalisation.disabled");
		final boolean valid = validationErrors.isEmpty();
		findAllTantervByAkkreditacio(entitas).forEach(tt -> {
			findAllTematikaIdByTantervAndErvenyesseg(tt.getTantargy().getId(), entitas.getErvenyessegKezdet(), entitas.getErvenyessegVeg()).forEach(t -> {
				if (!t.isVegleges()) {
					t.setValid(valid);
					if (valid) {
						t.setVegleges(true);
					}
					tematikaDao.save(t);
				}
			});
			if (!tt.isVegleges()) {
				tt.setValid(valid);
				if (valid) {
					tt.setVegleges(true);
				}
				tantervDao.save(tt);
			}
		});
		entitas.setValid(valid);
		if (valid) {
			entitas.setVegleges(true);
		}
		save(entitas);
		return validationErrors;
	}

	@Override
	public Set<String> validate(Long id) throws ValidationException {
		final Akkreditacio entitas = findById(id);
		final Set<String> validationErrors = validate(entitas);
		if (validationErrors.isEmpty()) {
			findAllTantervByAkkreditacio(entitas).forEach(tt -> {
				findAllTematikaIdByTantervAndErvenyesseg(tt.getTantargy().getId(), entitas.getErvenyessegKezdet(), entitas.getErvenyessegVeg()).forEach(t -> {
					t.setValid(true);
					tematikaDao.save(t);
				});
				tt.setValid(true);
				tantervDao.save(tt);
			});
			entitas.setValid(true);
			save(entitas);
		}
		entitas.setValid(validationErrors.isEmpty());
		save(entitas);
		return validationErrors;
	}

	@Override
	protected Set<String> validate(Akkreditacio entitas) {
		final Set<String> validationErrors = super.validate(entitas);
		if (validationErrors.isEmpty()) {
			//TODO simple validation (data presence) //szerencsére kifordult intervallumokat eleve nem engedünk menteni (???)
		}
		if (validationErrors.isEmpty()) {
			//TODO complex validation
			//v_akk_szak_kredit
			QKonkretSzakKreditView konkretSzakKreditView = QKonkretSzakKreditView.konkretSzakKreditView;
			//v_akk_szak_tt_kredit
			QKonkretSzakTantervKreditView konkretSzakTantervKreditView = QKonkretSzakTantervKreditView.konkretSzakTantervKreditView;
			//v_akk_szj_tt_kredit
			QSzakmaijellemzoTantervKreditView szakmaijellemzoTantervKreditView = QSzakmaijellemzoTantervKreditView.szakmaijellemzoTantervKreditView;
		}
		return validationErrors;
	}

	private List<Tematika> findAllTematikaIdByTantervAndErvenyesseg(Long tantargyId, Date ervKezdet, Date ervVeg) {
		QTematika tematika = QTematika.tematika;
		return queryFactory
				.selectFrom(tematika)
				.where(tematika.idTantargy.eq(tantargyId))
				.where(tematika.ervenyessegKezdet.before(ervKezdet))
				.where(tematika.ervenyessegVeg.after(ervVeg))
				.fetch();
	}

	private List<Tanterv> findAllTantervByAkkreditacio(Akkreditacio akkreditacio) {
		QTanterv tanterv = QTanterv.tanterv;
		return queryFactory
				.selectFrom(tanterv)
				.where(tanterv.idKonkretSzakirany.eq(akkreditacio.getKonkretSzakirany().getId()))
				.where(tanterv.ervenyessegKezdet.before(akkreditacio.getErvenyessegKezdet()))
				.where(tanterv.ervenyessegVeg.after(akkreditacio.getErvenyessegVeg()))
				.fetch();
	}

	public boolean isAkkreditacioValid(Long id) {
		if (id == null) return false;
		QAkkreditacio akkr = QAkkreditacio.akkreditacio;
		return BooleanUtils.isTrue(queryFactory
				.select(akkr.valid)
				.from(akkr)
				.where(akkr.id.eq(id))
				.fetchOne());
	}

	public boolean isAkkreditacioFinal(Long id) {
		if (id == null) return false;
		QAkkreditacio akkr = QAkkreditacio.akkreditacio;
		return BooleanUtils.isTrue(queryFactory
				.select(akkr.vegleges)
				.from(akkr)
				.where(akkr.id.eq(id))
				.fetchOne());
	}

	public boolean isKonkretSzakiranyFinal(Long id) {
		if (id == null) return false;
		QAkkreditacio akkr = QAkkreditacio.akkreditacio;
		return BooleanUtils.isTrue(queryFactory
				.select(akkr.vegleges)
				.from(akkr)
				.where(akkr.idKonkretSzakirany.eq(id))
				.where(akkr.vegleges.isTrue())
				.fetchFirst());
	}

	public boolean isKonkretSzakFinal(Long id) {
		if (id == null) return false;
		QAkkreditacio akkr = QAkkreditacio.akkreditacio;
		QKonkretSzakirany kszi = QKonkretSzakirany.konkretSzakirany;
		QKonkretSzak ksz = QKonkretSzak.konkretSzak;
		return BooleanUtils.isTrue(queryFactory
				.select(akkr.vegleges)
				.from(akkr, kszi, ksz)
				.where(akkr.idKonkretSzakirany.eq(kszi.id).and(ksz.id.eq(kszi.idKonkretSzak)).and(ksz.id.eq(id)))
				.where(akkr.vegleges.isTrue())
				.fetchFirst());
	}

	public boolean isTantervFinal(Long id) {
		if (id == null) return false;
		QTanterv tt = QTanterv.tanterv;
		return BooleanUtils.isTrue(queryFactory
				.select(tt.vegleges)
				.from(tt)
				.where(tt.id.eq(id))
				.fetchFirst());
	}

	public boolean isTantargyFinal(Long id) {
		if (id == null) return false;
		QTanterv ttv = QTanterv.tanterv;
		return BooleanUtils.isTrue(queryFactory
				.select(ttv.vegleges)
				.from(ttv)
				.where(ttv.idTantargy.eq(id))
				.where(ttv.vegleges.isTrue())
				.fetchFirst());
	}

	public boolean isTematikaFinal(Long id) {
		if (id == null) return false;
		QTematika tematika = QTematika.tematika;
		return BooleanUtils.isTrue(queryFactory
				.select(tematika.vegleges)
				.from(tematika)
				.where(tematika.id.eq(id))
				.fetchFirst());
	}

	public boolean isTemaFinal(Long id) {
		if (id == null) return false;
		QTematika tematika = QTematika.tematika;
		QTema tema = QTema.tema;
		return BooleanUtils.isTrue(queryFactory
				.select(tematika.vegleges)
				.from(tematika, tema)
				.where(tematika.tema.eq(tema).and(tema.id.eq(id)))
				.where(tematika.vegleges.isTrue())
				.fetchFirst());
	}
}
