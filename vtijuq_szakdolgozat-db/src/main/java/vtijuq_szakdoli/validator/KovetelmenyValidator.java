package vtijuq_szakdoli.validator;

import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.dao.entitas.kovetelmeny.SzakKovetelmenyDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QIsmeret;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzakKovetelmeny;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzakiranyKovetelmeny;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzakmaiJellemzo;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakKovetelmeny;
import vtijuq_szakdoli.domain.view.kovetelmeny.QSzakKovetelmenyKreditKovetelmenyekView;
import vtijuq_szakdoli.domain.view.kovetelmeny.QSzakiranyKovetelmenyKreditView;

@Stateless
public class KovetelmenyValidator extends AbstractValidator<SzakKovetelmeny> {
	//TODO ezeket kell felügyelnie:
	//szakKovetelmeny*, szakiranyKovetelmeny, szakmaiJellemzo, ismeret

	private static final Logger LOG = LoggerFactory.getLogger(KovetelmenyValidator.class);

	@Inject
	private SzakKovetelmenyDao szakKovetelmenyDao;

	@Override
	protected SzakKovetelmeny findById(Long id) {
		return szakKovetelmenyDao.findById(id);
	}

	@Override
	protected SzakKovetelmeny save(SzakKovetelmeny entitas) {
		return szakKovetelmenyDao.save(entitas);
	}

	@Override
	public Set<String> finalise(Long id) {
		final SzakKovetelmeny entitas = findById(id);
		final Set<String> validationErrors = validate(entitas);
		//TODO ideiglenes
		validationErrors.add("finalisation.disabled");
		final boolean valid = validationErrors.isEmpty();
		entitas.setValid(valid);
		if (valid) {
			entitas.setVegleges(true);
		}
		save(entitas);
		return validationErrors;
	}

	@Override
	public Set<String> validate(Long id) {
		final SzakKovetelmeny entitas = findById(id);
		final Set<String> validationErrors = validate(entitas);
		entitas.setValid(validationErrors.isEmpty());
		save(entitas);
		return validationErrors;
	}

	@Override
	protected Set<String> validate(SzakKovetelmeny entitas) {
		final Set<String> validationErrors = super.validate(entitas);
		if (validationErrors.isEmpty()) {
			//TODO simple validation (data presence) //szerencsére kifordult intervallumokat eleve nem engedünk menteni (???)
		}
		if (validationErrors.isEmpty()) {
			//TODO complex validation
			//v_szakiranykov_kredit
			QSzakiranyKovetelmenyKreditView szakiranyKovetelmenyKreditView = QSzakiranyKovetelmenyKreditView.szakiranyKovetelmenyKreditView;
			//v_szk_kredit_kov
			QSzakKovetelmenyKreditKovetelmenyekView szakKovetelmenyKreditKovetelmenyekView = QSzakKovetelmenyKreditKovetelmenyekView.szakKovetelmenyKreditKovetelmenyekView;
		}
		return validationErrors;
	}

	public boolean isSzakKovetelmenyValid(Long id) {
		if (id == null) return false;
		QSzakKovetelmeny szk = QSzakKovetelmeny.szakKovetelmeny;
		return BooleanUtils.isTrue(queryFactory
				.select(szk.valid)
				.from(szk)
				.where(szk.id.eq(id))
				.fetchOne());
	}

	public boolean isSzakKovetelmenyFinal(Long id) {
		if (id == null) return false;
		QSzakKovetelmeny szk = QSzakKovetelmeny.szakKovetelmeny;
		return BooleanUtils.isTrue(queryFactory
				.select(szk.vegleges)
				.from(szk)
				.where(szk.id.eq(id))
				.fetchOne());
	}

	public boolean isSzakiranyKovetelmenyFinal(Long id) {
		if (id == null) return false;
		QSzakKovetelmeny szk = QSzakKovetelmeny.szakKovetelmeny;
		QSzakiranyKovetelmeny szik = QSzakiranyKovetelmeny.szakiranyKovetelmeny;
		return BooleanUtils.isTrue(queryFactory
				.select(szk.vegleges)
				.from(szik, szk)
				.where(szik.szakKovetelmeny.eq(szk))
				.where(szik.id.eq(id))
				.fetchOne());
	}

	public boolean isIsmeretFinal(Long id) {
		if (id == null) return false;
		QSzakKovetelmeny szk = QSzakKovetelmeny.szakKovetelmeny;
		QSzakiranyKovetelmeny szik = QSzakiranyKovetelmeny.szakiranyKovetelmeny;
		QSzakmaiJellemzo szj = QSzakmaiJellemzo.szakmaiJellemzo;
		QIsmeret ism = QIsmeret.ismeret;
		return BooleanUtils.isTrue(queryFactory
				.from(szj, ism, szik, szk)
					.where(szj.ismeret.eq(ism))
					.where(szik.id.eq(szj.idSzakiranyKovetelmeny))
					.where(szk.eq(szik.szakKovetelmeny))
					.where(ism.id.eq(id).and(szk.vegleges.isTrue()))
				.select(szk.vegleges)
				.fetchFirst());
	}
}
