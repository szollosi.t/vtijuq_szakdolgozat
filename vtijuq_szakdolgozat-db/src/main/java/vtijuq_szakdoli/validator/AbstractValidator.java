package vtijuq_szakdoli.validator;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import vtijuq_szakdoli.common.exception.ValidationException;
import vtijuq_szakdoli.common.util.graph.Hierarchical;
import vtijuq_szakdoli.common.util.graph.Hierarchy;
import vtijuq_szakdoli.dao.BaseDao;
import vtijuq_szakdoli.domain.entitas.AbstractErvenyesEntitas;

public abstract class AbstractValidator<T extends AbstractErvenyesEntitas> extends BaseDao {

	protected abstract T findById(Long id);

	protected abstract T save(T entitas);

	public abstract Set<String> finalise(Long id);

	public abstract Set<String> validate(Long id);

	public void invalidate(Long id) throws ValidationException {
		final T entitas = findById(id);
		if (entitas.isVegleges()) {
			throw new ValidationException(Collections.singleton("error.validation.invalidate.final"));
		}
		entitas.setValid(false);
		save(entitas);
	}

	protected Set<String> validate(T entitas) {
		if (entitas.getErvenyessegKezdet() == null || entitas.getErvenyessegVeg() == null) {
			return Collections.singleton("error.validate.openInterval");
		}
		return new HashSet<>();
	}

	protected <H extends Hierarchical<H>> Set<String> validateHierarchy(Hierarchy<H> hierarchy, H cursor, Function<H, Set<String>> validation) {
		final Set<String> validationErors = new HashSet<>(validation.apply(cursor));
		hierarchy.getDirectSubordinates(cursor).forEach( sub ->
				validationErors.addAll(validateHierarchy(hierarchy, sub, validation))
		);
		return validationErors;
	}
}
