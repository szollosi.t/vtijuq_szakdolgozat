package vtijuq_szakdoli.filter.kovetelmeny;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzakiranyKovetelmeny;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakiranyKovetelmeny;
import vtijuq_szakdoli.filter.QueryDslFilter;

public class SzakiranyKovetelmenyFilter extends QueryDslFilter<SzakiranyKovetelmeny> implements vtijuq_szakdoli.common.filter.kovetelmeny.SzakiranyKovetelmenyFilter {

	@EqualCriterion
	@Getter @Setter
	private Boolean csakkozos;

	@EqualCriterion(entityFieldName = "szakKovetelmeny.id")
	@Getter @Setter
	private Long idSzakKovetelmeny;

	@EqualCriterion(entityFieldName = "szakirany.id")
	@Getter @Setter
	private Long idSzakirany;

	@Override
	public EntityPathBase<SzakiranyKovetelmeny> getEntityPath() {
		return QSzakiranyKovetelmeny.szakiranyKovetelmeny;
	}

	@Override
	public String defaultSortField() {
		return "szakKovetelmeny.id";
	}
}
