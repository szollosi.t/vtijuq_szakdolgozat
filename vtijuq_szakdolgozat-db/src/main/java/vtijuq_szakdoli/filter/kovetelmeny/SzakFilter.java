package vtijuq_szakdoli.filter.kovetelmeny;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;

import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ciklus;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzak;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Szak;
import vtijuq_szakdoli.filter.QueryDslFilter;

public class SzakFilter extends QueryDslFilter<Szak> implements vtijuq_szakdoli.common.filter.kovetelmeny.SzakFilter {

	@EqualCriterion(entityFieldName = "ciklus")
	@Getter @Setter
	private Ciklus ciklus;

	@EqualCriterion(entityFieldName = "kepzesiTerulet.id")
	@Getter @Setter
	private Long idKepzesiTerulet;

	@Override
	public EntityPathBase<Szak> getEntityPath() {
		return QSzak.szak;
	}

	@Override
	public String defaultSortField() {
		return "ciklus";
	}
}
