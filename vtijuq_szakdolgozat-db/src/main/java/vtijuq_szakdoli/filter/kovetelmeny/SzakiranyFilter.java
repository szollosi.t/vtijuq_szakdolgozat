package vtijuq_szakdoli.filter.kovetelmeny;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;

import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzakirany;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Szakirany;
import vtijuq_szakdoli.filter.QueryDslFilter;

public class SzakiranyFilter extends QueryDslFilter<Szakirany> implements vtijuq_szakdoli.common.filter.kovetelmeny.SzakiranyFilter {

	@EqualCriterion(entityFieldName = "szak.id")
	@Getter @Setter
	private Long idSzak;

	@Override
	public EntityPathBase<Szakirany> getEntityPath() {
		return QSzakirany.szakirany;
	}

	@Override
	public String defaultSortField() {
		return "idSzak";
	}
}
