package vtijuq_szakdoli.filter.kovetelmeny;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.LikeCriterion;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Ismeret;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QIsmeret;
import vtijuq_szakdoli.filter.QueryDslFilter;

public class IsmeretFilter extends QueryDslFilter<Ismeret> implements vtijuq_szakdoli.common.filter.kovetelmeny.IsmeretFilter {

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String nev;

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String leiras;

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String kod;

	@Override
	public EntityPathBase<Ismeret> getEntityPath() {
		return QIsmeret.ismeret;
	}

	@Override
	public String defaultSortField() {
		return "kod";
	}
}
