package vtijuq_szakdoli.filter.kovetelmeny;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;
import utils.query.filter.GreaterCriterion;
import utils.query.filter.LikeCriterion;
import utils.query.filter.LowerCriterion;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzakKovetelmeny;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakKovetelmeny;
import vtijuq_szakdoli.filter.QueryDslFilter;

import java.util.Date;

public class SzakKovetelmenyFilter extends QueryDslFilter<SzakKovetelmeny> implements vtijuq_szakdoli.common.filter.kovetelmeny.SzakKovetelmenyFilter {

	@EqualCriterion(entityFieldName = "szak.id")
	@Getter @Setter
	private Long idSzak;

	@GreaterCriterion(enableEqual = true, field = "ervenyessegKezdet")
	@Getter @Setter
	private Date ervenyessegKezdet;

	@LowerCriterion(enableEqual = true, field = "ervenyessegVeg")
	@Getter @Setter
	private Date ervenyessegVeg;

	@EqualCriterion
	@Getter @Setter
	private Boolean valid;

	@EqualCriterion
	@Getter @Setter
	private Boolean vegleges;

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String oklevelMegjeloles;

	@Override
	public EntityPathBase<SzakKovetelmeny> getEntityPath() {
		return QSzakKovetelmeny.szakKovetelmeny;
	}

	@Override
	public String defaultSortField() {
		return "idSzak";
	}
}
