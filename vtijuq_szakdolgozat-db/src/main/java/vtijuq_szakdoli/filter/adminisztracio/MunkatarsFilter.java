package vtijuq_szakdoli.filter.adminisztracio;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;
import utils.query.filter.LikeCriterion;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Munkatars;
import vtijuq_szakdoli.domain.entitas.adminisztracio.QMunkatars;
import vtijuq_szakdoli.filter.QueryDslFilter;

public class MunkatarsFilter extends QueryDslFilter<Munkatars> implements vtijuq_szakdoli.common.filter.adminisztracio.MunkatarsFilter {

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String felhasznalonev;

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String nev;

	@EqualCriterion
	@Getter @Setter
	private Boolean aktiv;

	@EqualCriterion(entityFieldName = "szervezet.id")
	@Getter @Setter
	private Long idSzervezet;

	@EqualCriterion(entityFieldName = "szerepkor.id")
	@Getter @Setter
	private Long idSzerepkor;

	@Override
	public EntityPathBase<Munkatars> getEntityPath() {
		return QMunkatars.munkatars;
	}

	@Override
	public String defaultSortField() {
		return "felhasznalonev";
	}
}
