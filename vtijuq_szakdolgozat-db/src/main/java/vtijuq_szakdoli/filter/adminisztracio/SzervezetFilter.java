package vtijuq_szakdoli.filter.adminisztracio;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;
import utils.query.filter.LikeCriterion;
import vtijuq_szakdoli.domain.entitas.adminisztracio.QSzervezet;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.filter.QueryDslFilter;

public class SzervezetFilter extends QueryDslFilter<Szervezet> implements vtijuq_szakdoli.common.filter.adminisztracio.SzervezetFilter {

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String nev;

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String rovidites;

	@EqualCriterion(entityFieldName = "szervezetTipus.id")//TODO???
	@Getter @Setter
	private Long idSzervezetTipus;

	@Override
	public EntityPathBase<Szervezet> getEntityPath() {
		return QSzervezet.szervezet;
	}

	@Override
	public String defaultSortField() {
		return "idSzervezetTipus";
	}
}
