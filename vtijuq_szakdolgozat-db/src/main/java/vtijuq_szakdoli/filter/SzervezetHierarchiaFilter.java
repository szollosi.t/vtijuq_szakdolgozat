package vtijuq_szakdoli.filter;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Munkatars;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.domain.entitas.adminisztracio.SzervezetTipus;
import vtijuq_szakdoli.domain.view.adminisztracio.QSzervezetHierarchiaView;
import vtijuq_szakdoli.domain.view.adminisztracio.SzervezetHierarchiaView;

public class SzervezetHierarchiaFilter extends QueryDslFilter<SzervezetHierarchiaView> {

	@Getter @Setter
	private String nev;

	@Getter @Setter
	private String rovidites;

	@Getter @Setter
	private SzervezetTipus szervezetTipus;

	@Getter @Setter
	private Szervezet illetekesSzerv;

	@Getter @Setter
	private Munkatars illetekesMunkatars;

	@Override
	public EntityPathBase<SzervezetHierarchiaView> getEntityPath() {
		return QSzervezetHierarchiaView.szervezetHierarchiaView;
	}
}
