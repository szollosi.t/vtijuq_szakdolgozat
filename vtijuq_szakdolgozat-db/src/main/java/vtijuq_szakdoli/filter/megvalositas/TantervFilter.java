package vtijuq_szakdoli.filter.megvalositas;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;
import utils.query.filter.GreaterCriterion;
import utils.query.filter.LowerCriterion;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.TantervStatusz;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTanterv;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tanterv;
import vtijuq_szakdoli.filter.QueryDslFilter;

import java.util.Date;

public class TantervFilter extends QueryDslFilter<Tanterv> {

	@GreaterCriterion(enableEqual = true, field = "ervenyessegKezdet")
	@Getter @Setter
	private Date ervenyessegKezdet;

	@LowerCriterion(enableEqual = true, field = "ervenyessegVeg")
	@Getter @Setter
	private Date ervenyessegVeg;

	@EqualCriterion
	@Getter @Setter
	private TantervStatusz statusz;

	@EqualCriterion
	@Getter @Setter
	private Integer felev;

	@EqualCriterion
	@Getter @Setter
	private Boolean valid;

	@EqualCriterion
	@Getter @Setter
	private Boolean vegleges;

	@EqualCriterion(entityFieldName = "konkretSzakirany.id")
	@Getter @Setter
	private Long idKonkretSzakirany;

	@EqualCriterion(entityFieldName = "tantargy.id")
	@Getter @Setter
	private Long idTantargy;

	@Override
	public EntityPathBase<Tanterv> getEntityPath() {
		return QTanterv.tanterv;
	}

	@Override
	public String defaultSortField() {
		return "idKonkretSzakirany";
	}
}
