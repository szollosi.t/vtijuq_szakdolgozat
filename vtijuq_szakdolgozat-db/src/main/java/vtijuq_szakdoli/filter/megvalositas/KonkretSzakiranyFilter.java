package vtijuq_szakdoli.filter.megvalositas;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;
import utils.query.filter.LikeCriterion;
import vtijuq_szakdoli.domain.entitas.megvalositas.KonkretSzakirany;
import vtijuq_szakdoli.domain.entitas.megvalositas.QKonkretSzakirany;
import vtijuq_szakdoli.filter.QueryDslFilter;

public class KonkretSzakiranyFilter extends QueryDslFilter<KonkretSzakirany> implements vtijuq_szakdoli.common.filter.megvalositas.KonkretSzakiranyFilter {

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String nev;

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String kod;

	@EqualCriterion(entityFieldName = "konkretSzak.id")
	@Getter @Setter
	private Long idKonkretSzak;

	@EqualCriterion(entityFieldName = "szakiranyKovetelmeny.id")
	@Getter @Setter
	private Long idSzakiranyKovetelmeny;

	@Override
	public EntityPathBase<KonkretSzakirany> getEntityPath() {
		return QKonkretSzakirany.konkretSzakirany;
	}

	@Override
	public String defaultSortField() {
		return "kod";
	}
}
