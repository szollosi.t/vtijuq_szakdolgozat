package vtijuq_szakdoli.filter.megvalositas;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTematika;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tematika;
import vtijuq_szakdoli.filter.QueryDslFilter;

import java.util.Date;

public class TematikaFilter extends QueryDslFilter<Tematika> {

	@Getter @Setter
	private Date ervenyessegKezdet;
	@Getter @Setter
	private Date ervenyessegVeg;
	@Getter @Setter
	private Boolean valid;
	@Getter @Setter
	private Boolean vegleges;
	@Getter @Setter
	private Long idTantargy;

	@Override
	public EntityPathBase<Tematika> getEntityPath() {
		return QTematika.tematika;
	}

}
