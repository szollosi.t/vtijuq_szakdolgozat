package vtijuq_szakdoli.filter.megvalositas;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;
import utils.query.filter.LikeCriterion;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTantargy;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tantargy;
import vtijuq_szakdoli.filter.QueryDslFilter;

public class TantargyFilter extends QueryDslFilter<Tantargy>
		implements vtijuq_szakdoli.common.filter.megvalositas.TantargyFilter {

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String nev;

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.START)
	@Getter @Setter
	private String kod;

	@EqualCriterion(entityFieldName = "szervezet.id")
	@Getter @Setter
	private Long idSzervezet;

	@Override
	public EntityPathBase<Tantargy> getEntityPath() {
		return QTantargy.tantargy;
	}

	@Override
	public String defaultSortField() {
		return "kod";
	}
}
