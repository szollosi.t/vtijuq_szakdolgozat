package vtijuq_szakdoli.filter.megvalositas;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;
import vtijuq_szakdoli.domain.entitas.megvalositas.KonkretSzak;
import vtijuq_szakdoli.domain.entitas.megvalositas.QKonkretSzak;
import vtijuq_szakdoli.filter.QueryDslFilter;

public class KonkretSzakFilter extends QueryDslFilter<KonkretSzak> implements vtijuq_szakdoli.common.filter.megvalositas.KonkretSzakFilter {

	@EqualCriterion(entityFieldName = "szakKovetelmeny.id")
	@Getter @Setter
	private Long idSzakKovetelmeny;

	@EqualCriterion(entityFieldName = "szervezet.id")
	@Getter @Setter
	private Long idSzervezet;

	@Override
	public EntityPathBase<KonkretSzak> getEntityPath() {
		return QKonkretSzak.konkretSzak;
	}

	@Override
	public String defaultSortField() {
		return "szakKovetelmeny.id";
	}
}
