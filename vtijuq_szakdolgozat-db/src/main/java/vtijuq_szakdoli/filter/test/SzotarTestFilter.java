package vtijuq_szakdoli.filter.test;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;
import utils.query.filter.LikeCriterion;
import vtijuq_szakdoli.domain.entitas.test.QSzotarTest;
import vtijuq_szakdoli.domain.entitas.test.SzotarTest;
import vtijuq_szakdoli.filter.QueryDslFilter;

public class SzotarTestFilter extends QueryDslFilter<SzotarTest> implements vtijuq_szakdoli.common.filter.test.SzotarTestFilter {

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.ANYWHERE)
	@Getter @Setter
	private String kod;

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.ANYWHERE)
	@Getter @Setter
	private String nev;

	@LikeCriterion(ignoreCase = true, matchMode = LikeCriterion.MatchMode.ANYWHERE)
	@Getter @Setter
	private String leiras;

	@EqualCriterion
	@Getter @Setter
	private Long idTipus;

	@EqualCriterion
	@Getter @Setter
	private Boolean technikai;

	@Override
	public EntityPathBase<SzotarTest> getEntityPath() {
		return QSzotarTest.szotarTest;
	}

	@Override
	public String defaultSortField() {
		return "kod";
	}
}
