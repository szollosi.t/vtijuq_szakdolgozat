package vtijuq_szakdoli.filter.akkreditacio;

import com.querydsl.core.types.dsl.EntityPathBase;
import lombok.Getter;
import lombok.Setter;
import utils.query.filter.EqualCriterion;
import utils.query.filter.GreaterCriterion;
import utils.query.filter.LowerCriterion;
import vtijuq_szakdoli.domain.entitas.akkreditacio.Akkreditacio;
import vtijuq_szakdoli.domain.entitas.akkreditacio.QAkkreditacio;
import vtijuq_szakdoli.filter.QueryDslFilter;

import java.util.Date;

public class AkkreditacioFilter extends QueryDslFilter<Akkreditacio> implements vtijuq_szakdoli.common.filter.akkreditacio.AkkreditacioFilter {

	@GreaterCriterion(enableEqual = true, field = "ervenyessegKezdet")
	@Getter @Setter
	private Date ervenyessegKezdet;

	@LowerCriterion(enableEqual = true, field = "ervenyessegVeg")
	@Getter @Setter
	private Date ervenyessegVeg;

	@EqualCriterion
	@Getter @Setter
	private Boolean valid;

	@EqualCriterion
	@Getter @Setter
	private Boolean vegleges;

	@EqualCriterion(entityFieldName = "konkretSzakirany.id")
	@Getter @Setter
	private Long idKonkretSzakirany;

	@Override
	public EntityPathBase<Akkreditacio> getEntityPath() {
		return QAkkreditacio.akkreditacio;
	}

	@Override
	public String defaultSortField() {
		return "idKonkretSzakirany";
	}
}
