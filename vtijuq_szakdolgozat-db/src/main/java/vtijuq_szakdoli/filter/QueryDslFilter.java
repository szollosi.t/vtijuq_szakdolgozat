package vtijuq_szakdoli.filter;

import com.querydsl.core.types.dsl.EntityPathBase;

import utils.query.filter.QuerydslFilter;

public abstract class QueryDslFilter<T> extends QuerydslFilter {

	public abstract EntityPathBase<T> getEntityPath();

	@Override
	public String defaultSortField() {
		return "id";
	}

}
