package vtijuq_szakdoli.dao;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.exception.ConsistencyException;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

public abstract class AbstractEntitasDao<T extends AbstractEntitas> extends AbstractDao<T> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractEntitasDao.class);

	public abstract T findById(@NotNull Long id);

	public T save(@NotNull T entitas) {
		try {
			if (entitas.getId() == null) {
				em.persist(entitas);
				return entitas;
			} else {
				return em.merge(entitas);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new ConsistencyException(e.getMessage());
		}
	}

	/**
	 * Save/delete entities according to membership in collections.
	 * @param oldCollection source collection.
	 * @param newCollection target collection.
	 * @return saved collection.
	 */
	public Set<T> saveCollection(@NotNull Collection<T> oldCollection, @NotNull Collection<T> newCollection) {
		return saveCollection(oldCollection, newCollection, this::save);
	}

	/**
	 * Save/delete entities according to membership in collections.
	 * @param oldCollection source collection.
	 * @param newCollection target collection.
	 * @param save          save operation to use
	 * @return saved collection.
	 */
	protected Set<T> saveCollection(@NotNull Collection<T> oldCollection, @NotNull Collection<T> newCollection, Function<T, T> save) {
		final Set<Long> oldIdSet = oldCollection.stream().map(AbstractEntitas::getId).collect(Collectors.toSet());
		final Set<Long> newIdSet = newCollection.stream()
				.map(AbstractEntitas::getId)
				.filter(Objects::nonNull)
				.collect(Collectors.toSet());
		final Set<T> saved = new HashSet<>();
		//remove
		final Collection<Long> idsToRemove = CollectionUtils.subtract(oldIdSet, newIdSet);
		idsToRemove.forEach(this::delete);
		//merge
		final Collection<Long> idsToMerge = CollectionUtils.intersection(newIdSet, oldIdSet);
		final Set<T> toMerge = oldCollection.stream()
				.filter(t -> idsToMerge.contains(t.getId()))
				.collect(Collectors.toSet());
		saved.addAll(toMerge.stream().map(save).collect(Collectors.toSet()));
		//add
		final Set<T> toAdd = newCollection.stream().filter(e -> e.getId() == null).collect(Collectors.toSet());
		saved.addAll(toAdd.stream().map(save).collect(Collectors.toSet()));
		return saved;
	}

//	public void delete(@NotNull T entitas) {
//		delete(entitas.getId());
//	}

	public abstract void delete(@NotNull Long id);

}
