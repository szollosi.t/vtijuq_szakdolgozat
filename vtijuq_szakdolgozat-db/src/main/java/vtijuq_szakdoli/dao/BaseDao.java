package vtijuq_szakdoli.dao;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseDao {

	private static final Logger LOG = LoggerFactory.getLogger(BaseDao.class);

	@PersistenceContext
	protected EntityManager em;

	protected JPAQueryFactory queryFactory;

	@PostConstruct
	public void init() {
		queryFactory = new JPAQueryFactory(em);
	}

}
