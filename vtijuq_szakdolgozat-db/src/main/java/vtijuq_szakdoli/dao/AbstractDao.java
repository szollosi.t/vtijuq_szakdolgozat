package vtijuq_szakdoli.dao;

import java.util.List;

import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.jpa.JPQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.query.QueryUtil;

import vtijuq_szakdoli.filter.QueryDslFilter;

public abstract class AbstractDao<T> extends BaseDao {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractDao.class);

	protected abstract EntityPathBase<T> getEntityPath();

	public List<T> findAll() {
		return queryFactory.selectFrom(getEntityPath()).fetch();
	}

	public T findByFilter(QueryDslFilter<T> filter) {
		return createQueryByFilter(filter).fetchOne();
	}

	public List<T> findAllByFilter(QueryDslFilter<T> filter) {
		return createQueryByFilter(filter).fetch();
	}

	public boolean existsByFilter(QueryDslFilter<T> filter) {
		return QueryUtil.existsByFilter(queryFactory, getEntityPath(), filter);
	}

	private JPQLQuery<T> createQueryByFilter(QueryDslFilter<T> filter) {
		return QueryUtil.createQueryByFilter(queryFactory, getEntityPath(), filter);
	}

}
