package vtijuq_szakdoli.dao;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections4.CollectionUtils;

import vtijuq_szakdoli.common.util.graph.Hierarchical;
import vtijuq_szakdoli.common.util.graph.Hierarchy;
import vtijuq_szakdoli.common.util.graph.SubordinatesProvider;
import vtijuq_szakdoli.domain.entitas.AbstractEntitas;

public abstract class HierarchicalDao<T extends AbstractEntitas & Hierarchical<T>>
		extends AbstractEntitasDao<T>
        implements SubordinatesProvider<T> {

	public abstract Set<? extends T> findSovereigns();

	@Override
	public Set<? extends T> findSubordinates(T principal) {
		return findSubordinates(principal.getId());
	}

	public abstract Set<? extends T> findSubordinates(@NotNull Long idPrincipal);

	public abstract boolean hasSubordinates(@NotNull Long idPrincipal);

	public abstract boolean isSubordinate(@NotNull Long idPrincipal, @NotNull Long idSubordinate);

	public abstract void saveSubordination(@NotNull Long idPrincipal, @NotNull Long idSubordinate);

	public abstract void removeSubordination(@NotNull Long idPrincipal, @NotNull Long idSubordinate);

	public abstract T save(@NotNull Long idPrincipal, @NotNull T subordinate);

	protected void saveSubordinates(@NotNull Hierarchy<T> hierarchy, @NotNull Hierarchy<T> savedHierarchy, @NotNull T node, @NotNull T savedNode) {
		hierarchy.getDirectSubordinates(node).forEach(subordinate -> {
			//Ha már jártunk az alárendeltnél, akkor csak összekötjük a mostani node-dal.
			if (savedHierarchy.hasNode(subordinate)) {
				final T savedSubordinate = savedHierarchy.getEqualNode(subordinate);
				saveSubordination(savedNode.getId(), savedSubordinate.getId());
				savedHierarchy.addSubordination(savedNode, savedSubordinate);
			} else {
				final T savedSubordinate = save(savedNode.getId(), subordinate);
				savedHierarchy.addNode(savedNode, savedSubordinate);
				saveSubordinates(hierarchy, savedHierarchy, subordinate, savedSubordinate);
			}
		});
	}

	//a törölt principal-k leszármazottait is törli, nem csak az egyes entitásokat
	public Set<T> mergePrincipalCollection(@NotNull Collection<T> oldCollection, @NotNull Collection<T> newCollection) {
		final Set<T> savedSet = new HashSet<>();
		//idSets to know what to do
		final Set<Long> oldIdSet = oldCollection.stream().map(AbstractEntitas::getId).collect(Collectors.toSet());
		final Set<Long> newIdSet = newCollection.stream()
				.map(AbstractEntitas::getId)
				.filter(Objects::nonNull)
				.collect(Collectors.toSet());
		final Set<Long> idsToDelete = new HashSet<>(CollectionUtils.subtract(oldIdSet, newIdSet));
		final Set<Long> idsToMerge = new HashSet<>(CollectionUtils.intersection(newIdSet, oldIdSet));
		//remove
		idsToDelete.forEach(this::deleteSubHierarchy);
		//merge
		final Set<T> toMerge = oldCollection.stream()
				.filter(t -> idsToMerge.contains(t.getId()))
				.collect(Collectors.toSet());
		savedSet.addAll(toMerge.stream().map(this::save).collect(Collectors.toSet()));
		//add
		final Set<T> toAdd = newCollection.stream().filter(e -> e.getId() == null).collect(Collectors.toSet());
		savedSet.addAll(toAdd.stream().map(this::save).collect(Collectors.toSet()));
		return savedSet;
	}

	public void deleteSubHierarchy(@NotNull Long idPrincipal) {
		findSubordinates(idPrincipal).stream()
				.map(AbstractEntitas::getId)
				.forEach(this::deleteSubHierarchy);//rekurzió
		delete(idPrincipal);
	}

}
