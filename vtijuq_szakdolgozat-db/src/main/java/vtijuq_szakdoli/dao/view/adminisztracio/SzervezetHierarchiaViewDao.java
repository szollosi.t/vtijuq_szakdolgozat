package vtijuq_szakdoli.dao.view.adminisztracio;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.adminisztracio.QSzervezetHierarchiaView;
import vtijuq_szakdoli.domain.view.adminisztracio.SzervezetHierarchiaView;

@Stateless
public class SzervezetHierarchiaViewDao extends AbstractDao<SzervezetHierarchiaView> {
    //TODO kell ez???
    @Override
    protected QSzervezetHierarchiaView getEntityPath() {
        return QSzervezetHierarchiaView.szervezetHierarchiaView;
    }
}
