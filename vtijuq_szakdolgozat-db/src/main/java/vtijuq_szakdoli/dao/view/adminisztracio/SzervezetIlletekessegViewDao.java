package vtijuq_szakdoli.dao.view.adminisztracio;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.adminisztracio.QSzervezetIlletekessegView;
import vtijuq_szakdoli.domain.view.adminisztracio.SzervezetIlletekessegView;

@Stateless
public class SzervezetIlletekessegViewDao extends AbstractDao<SzervezetIlletekessegView> {
    //TODO kell ez???
    @Override
    protected QSzervezetIlletekessegView getEntityPath() {
        return QSzervezetIlletekessegView.szervezetIlletekessegView;
    }
}
