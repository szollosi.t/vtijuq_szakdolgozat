package vtijuq_szakdoli.dao.view.adminisztracio;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.adminisztracio.MunkatarsView;
import vtijuq_szakdoli.domain.view.adminisztracio.QMunkatarsView;

@Stateless
public class MunkatarsViewDao extends AbstractDao<MunkatarsView> {
    //TODO kell ez???
    @Override
    protected QMunkatarsView getEntityPath() {
        return QMunkatarsView.munkatarsView;
    }
}
