package vtijuq_szakdoli.dao.view.adminisztracio;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.adminisztracio.QSzervezetView;
import vtijuq_szakdoli.domain.view.adminisztracio.SzervezetView;

@Stateless
public class SzervezetViewDao extends AbstractDao<SzervezetView> {
    //TODO kell ez???
    @Override
    protected QSzervezetView getEntityPath() {
        return QSzervezetView.szervezetView;
    }
}
