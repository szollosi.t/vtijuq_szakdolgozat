package vtijuq_szakdoli.dao.view.adminisztracio;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.adminisztracio.MunkatarsIlletekessegView;
import vtijuq_szakdoli.domain.view.adminisztracio.QMunkatarsIlletekessegView;

@Stateless
public class MunkatarsIlletekessegViewDao extends AbstractDao<MunkatarsIlletekessegView> {
    //TODO kell ez???
    @Override
    protected QMunkatarsIlletekessegView getEntityPath() {
        return QMunkatarsIlletekessegView.munkatarsIlletekessegView;
    }
}
