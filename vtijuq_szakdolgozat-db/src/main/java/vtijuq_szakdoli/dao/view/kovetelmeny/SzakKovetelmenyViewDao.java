package vtijuq_szakdoli.dao.view.kovetelmeny;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.kovetelmeny.QSzakKovetelmenyView;
import vtijuq_szakdoli.domain.view.kovetelmeny.SzakKovetelmenyView;

@Stateless
public class SzakKovetelmenyViewDao extends AbstractDao<SzakKovetelmenyView> {
    //TODO kell ez???
    @Override
    protected QSzakKovetelmenyView getEntityPath() {
        return QSzakKovetelmenyView.szakKovetelmenyView;
    }
}
