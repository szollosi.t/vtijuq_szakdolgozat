package vtijuq_szakdoli.dao.view.kovetelmeny;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.kovetelmeny.IsmeretHierarchiaView;
import vtijuq_szakdoli.domain.view.kovetelmeny.QIsmeretHierarchiaView;

@Stateless
public class IsmeretHierarchiaViewDao extends AbstractDao<IsmeretHierarchiaView> {
    //TODO kell ez???
    @Override
    protected QIsmeretHierarchiaView getEntityPath() {
        return QIsmeretHierarchiaView.ismeretHierarchiaView;
    }
}
