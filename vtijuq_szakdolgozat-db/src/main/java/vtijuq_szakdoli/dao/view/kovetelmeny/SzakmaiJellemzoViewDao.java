package vtijuq_szakdoli.dao.view.kovetelmeny;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.kovetelmeny.QSzakmaiJellemzoView;
import vtijuq_szakdoli.domain.view.kovetelmeny.SzakmaiJellemzoView;

@Stateless
public class SzakmaiJellemzoViewDao extends AbstractDao<SzakmaiJellemzoView> {
    //TODO kell ez???
    @Override
    protected QSzakmaiJellemzoView getEntityPath() {
        return QSzakmaiJellemzoView.szakmaiJellemzoView;
    }
}
