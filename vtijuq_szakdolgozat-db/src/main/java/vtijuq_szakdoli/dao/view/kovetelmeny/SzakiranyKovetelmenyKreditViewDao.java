package vtijuq_szakdoli.dao.view.kovetelmeny;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.kovetelmeny.QSzakiranyKovetelmenyKreditView;
import vtijuq_szakdoli.domain.view.kovetelmeny.SzakiranyKovetelmenyKreditView;

@Stateless
public class SzakiranyKovetelmenyKreditViewDao extends AbstractDao<SzakiranyKovetelmenyKreditView> {
    //TODO kell ez???
    @Override
    protected QSzakiranyKovetelmenyKreditView getEntityPath() {
        return QSzakiranyKovetelmenyKreditView.szakiranyKovetelmenyKreditView;
    }
}
