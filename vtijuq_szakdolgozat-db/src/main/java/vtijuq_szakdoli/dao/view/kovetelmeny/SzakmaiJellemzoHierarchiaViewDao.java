package vtijuq_szakdoli.dao.view.kovetelmeny;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.kovetelmeny.QSzakmaiJellemzoHierarchiaView;
import vtijuq_szakdoli.domain.view.kovetelmeny.SzakmaiJellemzoHierarchiaView;

@Stateless
public class SzakmaiJellemzoHierarchiaViewDao extends AbstractDao<SzakmaiJellemzoHierarchiaView> {
    //TODO kell ez???
    @Override
    protected QSzakmaiJellemzoHierarchiaView getEntityPath() {
        return QSzakmaiJellemzoHierarchiaView.szakmaiJellemzoHierarchiaView;
    }
}
