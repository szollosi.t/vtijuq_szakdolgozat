package vtijuq_szakdoli.dao.view.kovetelmeny;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.kovetelmeny.QSzakmaiJellemzoIsmeretKapcsView;
import vtijuq_szakdoli.domain.view.kovetelmeny.SzakmaiJellemzoIsmeretKapcsView;

@Stateless
public class SzakmaiJellemzoIsmeretKapcsViewDao extends AbstractDao<SzakmaiJellemzoIsmeretKapcsView> {
    //TODO kell ez???
    @Override
    protected QSzakmaiJellemzoIsmeretKapcsView getEntityPath() {
        return QSzakmaiJellemzoIsmeretKapcsView.szakmaiJellemzoIsmeretKapcsView;
    }
}
