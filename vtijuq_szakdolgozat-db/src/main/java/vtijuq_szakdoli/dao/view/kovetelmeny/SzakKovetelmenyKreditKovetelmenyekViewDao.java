package vtijuq_szakdoli.dao.view.kovetelmeny;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.kovetelmeny.QSzakKovetelmenyKreditKovetelmenyekView;
import vtijuq_szakdoli.domain.view.kovetelmeny.SzakKovetelmenyKreditKovetelmenyekView;

@Stateless
public class SzakKovetelmenyKreditKovetelmenyekViewDao extends AbstractDao<SzakKovetelmenyKreditKovetelmenyekView> {
    //TODO kell ez???
    @Override
    protected QSzakKovetelmenyKreditKovetelmenyekView getEntityPath() {
        return QSzakKovetelmenyKreditKovetelmenyekView.szakKovetelmenyKreditKovetelmenyekView;
    }
}
