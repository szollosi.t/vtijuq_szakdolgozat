package vtijuq_szakdoli.dao.view.kovetelmeny;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.kovetelmeny.QSzakiranyKovetelmenyView;
import vtijuq_szakdoli.domain.view.kovetelmeny.SzakiranyKovetelmenyView;

@Stateless
public class SzakiranyKovetelmenyViewDao extends AbstractDao<SzakiranyKovetelmenyView> {
    //TODO kell ez???
    @Override
    protected QSzakiranyKovetelmenyView getEntityPath() {
        return QSzakiranyKovetelmenyView.szakiranyKovetelmenyView;
    }
}
