package vtijuq_szakdoli.dao.view.kovetelmeny;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.kovetelmeny.IsmeretView;
import vtijuq_szakdoli.domain.view.kovetelmeny.QIsmeretView;

@Stateless
public class IsmeretViewDao extends AbstractDao<IsmeretView> {
	//TODO kell ez???
	@Override
	protected QIsmeretView getEntityPath() {
		return QIsmeretView.ismeretView;
	}
}
