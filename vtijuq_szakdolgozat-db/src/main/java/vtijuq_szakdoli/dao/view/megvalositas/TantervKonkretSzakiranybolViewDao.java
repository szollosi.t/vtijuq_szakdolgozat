package vtijuq_szakdoli.dao.view.megvalositas;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.megvalositas.QTantervKonkretSzakiranybolView;
import vtijuq_szakdoli.domain.view.megvalositas.TantervKonkretSzakiranybolView;

@Stateless
public class TantervKonkretSzakiranybolViewDao extends AbstractDao<TantervKonkretSzakiranybolView> {
    //TODO kell ez???
    @Override
    protected QTantervKonkretSzakiranybolView getEntityPath() {
        return QTantervKonkretSzakiranybolView.tantervKonkretSzakiranybolView;
    }
}
