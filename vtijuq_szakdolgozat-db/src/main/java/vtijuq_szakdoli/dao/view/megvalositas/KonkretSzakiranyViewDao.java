package vtijuq_szakdoli.dao.view.megvalositas;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.megvalositas.KonkretSzakiranyView;
import vtijuq_szakdoli.domain.view.megvalositas.QKonkretSzakiranyView;

@Stateless
public class KonkretSzakiranyViewDao extends AbstractDao<KonkretSzakiranyView> {
    //TODO kell ez???
    @Override
    protected QKonkretSzakiranyView getEntityPath() {
        return QKonkretSzakiranyView.konkretSzakiranyView;
    }
}
