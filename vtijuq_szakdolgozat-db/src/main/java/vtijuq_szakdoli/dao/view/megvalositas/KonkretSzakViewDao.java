package vtijuq_szakdoli.dao.view.megvalositas;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.megvalositas.KonkretSzakView;
import vtijuq_szakdoli.domain.view.megvalositas.QKonkretSzakView;

@Stateless
public class KonkretSzakViewDao extends AbstractDao<KonkretSzakView> {
    //TODO kell ez???
    @Override
    protected QKonkretSzakView getEntityPath() {
        return QKonkretSzakView.konkretSzakView;
    }
}
