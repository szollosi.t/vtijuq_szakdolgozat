package vtijuq_szakdoli.dao.view.megvalositas;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.megvalositas.KonkretSzakTantervKreditView;
import vtijuq_szakdoli.domain.view.megvalositas.QKonkretSzakTantervKreditView;

@Stateless
public class KonkretSzakTantervKreditViewDao extends AbstractDao<KonkretSzakTantervKreditView> {
    //TODO kell ez???
    @Override
    protected QKonkretSzakTantervKreditView getEntityPath() {
        return QKonkretSzakTantervKreditView.konkretSzakTantervKreditView;
    }
}
