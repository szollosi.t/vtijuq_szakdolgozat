package vtijuq_szakdoli.dao.view.megvalositas;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.megvalositas.KonkretSzakKreditView;
import vtijuq_szakdoli.domain.view.megvalositas.QKonkretSzakKreditView;

@Stateless
public class KonkretSzakKreditViewDao extends AbstractDao<KonkretSzakKreditView> {
    //TODO kell ez???
    @Override
    protected QKonkretSzakKreditView getEntityPath() {
        return QKonkretSzakKreditView.konkretSzakKreditView;
    }
}
