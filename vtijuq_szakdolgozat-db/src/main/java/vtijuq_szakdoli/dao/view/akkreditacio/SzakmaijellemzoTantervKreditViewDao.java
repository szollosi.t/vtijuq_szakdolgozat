package vtijuq_szakdoli.dao.view.akkreditacio;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.akkreditacio.QSzakmaijellemzoTantervKreditView;
import vtijuq_szakdoli.domain.view.akkreditacio.SzakmaijellemzoTantervKreditView;

@Stateless
public class SzakmaijellemzoTantervKreditViewDao extends AbstractDao<SzakmaijellemzoTantervKreditView> {
    //TODO kell ez???
    @Override
    protected QSzakmaijellemzoTantervKreditView getEntityPath() {
        return QSzakmaijellemzoTantervKreditView.szakmaijellemzoTantervKreditView;
    }
}
