package vtijuq_szakdoli.dao.view.akkreditacio;

import javax.ejb.Stateless;

import vtijuq_szakdoli.dao.AbstractDao;
import vtijuq_szakdoli.domain.view.akkreditacio.AkkreditacioView;
import vtijuq_szakdoli.domain.view.akkreditacio.QAkkreditacioView;

@Stateless
public class AkkreditacioViewDao extends AbstractDao<AkkreditacioView> {
    //TODO kell ez???
    @Override
    protected QAkkreditacioView getEntityPath() {
        return QAkkreditacioView.akkreditacioView;
    }
}
