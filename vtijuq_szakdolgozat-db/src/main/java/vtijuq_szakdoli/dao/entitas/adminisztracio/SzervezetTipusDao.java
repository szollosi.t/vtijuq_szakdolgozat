package vtijuq_szakdoli.dao.entitas.adminisztracio;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.QSzervezetTipus;
import vtijuq_szakdoli.domain.entitas.adminisztracio.SzervezetTipus;

@Stateless
public class SzervezetTipusDao extends AbstractEntitasDao<SzervezetTipus> {

    @Override
    protected QSzervezetTipus getEntityPath() {
        return QSzervezetTipus.szervezetTipus;
    }

    @Override
    public SzervezetTipus findById(@NotNull Long id) {
        final QSzervezetTipus table = QSzervezetTipus.szervezetTipus;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QSzervezetTipus table = QSzervezetTipus.szervezetTipus;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }
}
