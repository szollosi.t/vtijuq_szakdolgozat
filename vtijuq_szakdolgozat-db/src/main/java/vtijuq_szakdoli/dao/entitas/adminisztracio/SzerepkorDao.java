package vtijuq_szakdoli.dao.entitas.adminisztracio;

import static utils.query.QueryUtil.exists;
import static vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Jogosultsag.JogosultsagSzint.IRHATO;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import com.querydsl.jpa.impl.JPAQuery;

import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Jogosultsag.JogosultsagSzint;
import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.QSzerepkor;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szerepkor;
import vtijuq_szakdoli.domain.view.adminisztracio.QMunkatarsJogosultsagView;

@Stateless
public class SzerepkorDao extends AbstractEntitasDao<Szerepkor> {

    @Override
    protected QSzerepkor getEntityPath() {
        return QSzerepkor.szerepkor;
    }

    public Szerepkor findById(@NotNull Long id) {
        final QSzerepkor table = QSzerepkor.szerepkor;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QSzerepkor table = QSzerepkor.szerepkor;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }

    public boolean isEditable(@NotNull String username, @NotNull String funkcio) {
        return isAuthorized(username, funkcio, IRHATO);
    }

    public boolean isAccessible(@NotNull String username, @NotNull String funkcio) {
        return isAuthorized(username, funkcio, JogosultsagSzint.values());
    }

    private boolean isAuthorized(@NotNull String username, @NotNull String funkcio, @NotNull JogosultsagSzint... szint) {
        QMunkatarsJogosultsagView mjView = QMunkatarsJogosultsagView.munkatarsJogosultsagView;
        JPAQuery<Integer> query = queryFactory
                .selectOne().from(mjView)
                .where(mjView.felhasznalonev.eq(username)
                        .and(mjView.aktiv.eq(Boolean.TRUE))
                        .and(mjView.funkcionev.eq(funkcio)
                                .or(mjView.funkciokod.eq(funkcio)))
                        .and(mjView.szint.in(szint))
                );
        return exists(query);
    }
}
