package vtijuq_szakdoli.dao.entitas.adminisztracio;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Funkcio;
import vtijuq_szakdoli.domain.entitas.adminisztracio.QFunkcio;

@Stateless
public class FunkcioDao extends AbstractEntitasDao<Funkcio> {

    @Override
    protected QFunkcio getEntityPath() {
        return QFunkcio.funkcio;
    }

    @Override
    public Funkcio findById(@NotNull Long id) {
        final QFunkcio table = QFunkcio.funkcio;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QFunkcio table = QFunkcio.funkcio;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }
}
