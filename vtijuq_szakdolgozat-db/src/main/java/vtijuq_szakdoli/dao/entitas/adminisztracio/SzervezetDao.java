package vtijuq_szakdoli.dao.entitas.adminisztracio;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.Expressions;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.query.QueryUtil;

import vtijuq_szakdoli.common.exception.ConsistencyException;
import vtijuq_szakdoli.common.util.graph.Dag;
import vtijuq_szakdoli.common.util.graph.Tree;
import vtijuq_szakdoli.dao.HierarchicalDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.QSzervezet;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.domain.view.adminisztracio.QMunkatarsIlletekessegView;
import vtijuq_szakdoli.filter.adminisztracio.SzervezetFilter;

@Stateless
public class SzervezetDao extends HierarchicalDao<Szervezet> {

	private static final Long ID_SZERVEZET_ROOT = 0L;

	private static final Logger LOG = LoggerFactory.getLogger(SzervezetDao.class);

	@Override
	protected EntityPathBase<Szervezet> getEntityPath() {
		return QSzervezet.szervezet;
	}

	@Override
	public Szervezet findById(@NotNull Long id) {
		final QSzervezet table = QSzervezet.szervezet;
		return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
	}

	@Override
	public void delete(@NotNull Long id) {
		final QSzervezet table = QSzervezet.szervezet;
		queryFactory.delete(table)
				.where(table.id.eq(id))
				.execute();
	}

	public boolean isIlletekes(@NotNull String username, @NotNull Long idSzervezet) {
		QMunkatarsIlletekessegView miView = QMunkatarsIlletekessegView.munkatarsIlletekessegView;
		return queryFactory
				.selectOne().from(miView)
				.where(miView.felhasznalonev.eq(username)
				  .and(miView.aktiv)
				  .and(miView.idszervezet.eq(idSzervezet))
				).fetchCount() > 0;
	}

	@Override
	public Set<Szervezet> findSubordinates(@NotNull Long idPrincipal) {
		QSzervezet szervezet = QSzervezet.szervezet;
		return new HashSet<>(queryFactory
             .selectFrom(szervezet)
             .where(szervezet.idFoszervezet.eq(idPrincipal))
             .fetch());
	}

	@Override
	public boolean hasSubordinates(@NotNull Long idPrincipal) {
		QSzervezet szervezet = QSzervezet.szervezet;
		return QueryUtil.exists(queryFactory
				.selectFrom(szervezet)
				.where(szervezet.idFoszervezet.eq(idPrincipal)));
	}

	@Override
	public Set<Szervezet> findSovereigns() {
		return Collections.singleton(findById(0L));
	}

	@Override
	public boolean isSubordinate(@NotNull Long idPrincipal, @NotNull Long idSubordinate) {
		QSzervezet szervezet = QSzervezet.szervezet;
		return BooleanUtils.isTrue(queryFactory.from(szervezet)
				.where(szervezet.id.eq(idSubordinate))
				.select(szervezet.idFoszervezet.when(idPrincipal).then(Expressions.TRUE).otherwise(Expressions.FALSE))
				.fetchFirst());
	}

	public void saveSubordination(@NotNull Long idPrincipal, @NotNull Long idSubordinate) {
		if (ID_SZERVEZET_ROOT.equals(idSubordinate)) {
			throw new ConsistencyException("Szervezet-root can not bee subordinate");
		}
		if (isSubordinate(idPrincipal, idSubordinate)) return;

		QSzervezet szervezet = QSzervezet.szervezet;
		queryFactory.update(szervezet)
				.set(szervezet.idFoszervezet, idPrincipal)
				.where(szervezet.id.eq(idSubordinate))
				.execute();
	}

	@Override
	public void removeSubordination(@NotNull Long idPrincipal, @NotNull Long idSubordinate) {
		if(!isSubordinate(idPrincipal, idSubordinate)) return;

		QSzervezet szervezet = QSzervezet.szervezet;
		queryFactory.update(szervezet)
				.set(szervezet.idFoszervezet, 0L)
				.where(szervezet.id.eq(idSubordinate)
				  .and(szervezet.idFoszervezet.eq(idPrincipal)))
				.execute();
	}

	public Szervezet findSzervezetRoot() {
		return findById(0L);
	}

	@Override
	public Szervezet save(@NotNull Szervezet entitas) {
		if (entitas.getId() == null) {
			return save(ID_SZERVEZET_ROOT, entitas);
		} else {
			return super.save(entitas);
		}
	}

	@Override
	public Szervezet save(@NotNull Long idFoszervezet, @NotNull Szervezet entitas) {
		entitas.setIdFoszervezet(idFoszervezet);
		return super.save(entitas);
	}

	public Tree<Szervezet> save(@NotNull Tree<Szervezet> tree) {
		final Szervezet root = tree.getRoot();
		final Szervezet savedRoot = save(root);
		final Tree<Szervezet> savedTree = new Tree<>(savedRoot);
		saveSubordinates(tree, savedTree, root, savedRoot);
		return savedTree;
	}

	public Tree<Szervezet> findWholeSzervezetTree(){
		return new Tree<>(findSzervezetRoot(), this);
	}

	public Dag<Szervezet> findSzervezetHierarchyByFilter(@NotNull SzervezetFilter filter){
		return new Dag<>(findAllByFilter(filter), this);
	}
}
