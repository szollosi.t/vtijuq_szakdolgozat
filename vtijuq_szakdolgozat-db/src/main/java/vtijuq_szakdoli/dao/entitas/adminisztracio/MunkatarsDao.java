package vtijuq_szakdoli.dao.entitas.adminisztracio;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.exception.VtijuqException;
import vtijuq_szakdoli.common.util.login.PasswordEncryptionService;
import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Munkatars;
import vtijuq_szakdoli.domain.entitas.adminisztracio.QMunkatars;

@Stateless
public class MunkatarsDao extends AbstractEntitasDao<Munkatars> {

	private static final Logger LOG = LoggerFactory.getLogger(MunkatarsDao.class);

	@Override
	protected QMunkatars getEntityPath() {
		return QMunkatars.munkatars;
	}

	@Override
	public Munkatars findById(@NotNull Long id) {
		final QMunkatars table = QMunkatars.munkatars;
		return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
	}

	@Override
	public Munkatars save(Munkatars entitas) {
		if (entitas.isJelszovaltoztatas() || entitas.getSalt() == null) {
			setNewSalt(entitas);
		}
		return super.save(entitas);
	}

	@Override
	public void delete(@NotNull Long id) {
		final QMunkatars table = QMunkatars.munkatars;
		queryFactory.delete(table)
				.where(table.id.eq(id))
				.execute();
	}

	public Munkatars findByFelhasznaloNev(@NotNull String username) {
		QMunkatars munkatars = QMunkatars.munkatars;
		return queryFactory
				.selectFrom(munkatars)
				.where(munkatars.felhasznalonev.eq(username))
				.fetchOne();

	}

	public Munkatars findAktivByFelhasznaloNev(@NotNull String username) {
		QMunkatars munkatars = QMunkatars.munkatars;
		return queryFactory
				.selectFrom(munkatars)
				.where(munkatars.felhasznalonev.eq(username)
				  .and(munkatars.aktiv.isTrue()))
				.fetchOne();
	}

	public List<Munkatars> getUnSalted(){
		QMunkatars munkatars = QMunkatars.munkatars;
		return queryFactory
				.selectFrom(munkatars)
				.where(munkatars.salt.isNull())
				.fetch();
	}

    public void setNewSalt(Munkatars m) {
		try {
			byte[] salt = PasswordEncryptionService.generateSalt();
			m.setSalt(Arrays.toString(salt));
			String jelszo = m.getJelszo();
			String encodedJelszo = Base64.getEncoder().encodeToString(jelszo.getBytes());
			m.setJelszo(Arrays.toString(PasswordEncryptionService.getEncryptedPassword(encodedJelszo, salt)));
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			LOG.error(e.getMessage(), e);
			throw new VtijuqException(e.getMessage(), e);
		}
    }

	public void saltUnsalted() {
		getUnSalted().forEach(m -> {
				setNewSalt(m);
				m.setAktiv(true);
                save(m);
		});
	}
}
