package vtijuq_szakdoli.dao.entitas.adminisztracio;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.Jogosultsag;
import vtijuq_szakdoli.domain.entitas.adminisztracio.QJogosultsag;

@Stateless
public class JogosultsagDao extends AbstractEntitasDao<Jogosultsag> {

    @Override
    protected QJogosultsag getEntityPath() {
        return QJogosultsag.jogosultsag;
    }

    @Override
    public Jogosultsag findById(@NotNull Long id) {
        final QJogosultsag table = QJogosultsag.jogosultsag;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QJogosultsag table = QJogosultsag.jogosultsag;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }
}
