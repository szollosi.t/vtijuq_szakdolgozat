package vtijuq_szakdoli.dao.entitas.test;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.Expressions;
import org.apache.commons.lang3.BooleanUtils;
import utils.query.QueryUtil;

import vtijuq_szakdoli.common.exception.ConsistencyException;
import vtijuq_szakdoli.common.util.graph.Tree;
import vtijuq_szakdoli.dao.HierarchicalDao;
import vtijuq_szakdoli.domain.entitas.test.QSzotarTest;
import vtijuq_szakdoli.domain.entitas.test.SzotarTest;

@Stateless
public class SzotarTestDao
		extends HierarchicalDao<SzotarTest> {

	private static final long ID_SZOTAR_ROOT = 0L;

	@Override
	protected EntityPathBase<SzotarTest> getEntityPath() {
		return QSzotarTest.szotarTest;
	}

	@Override
	public SzotarTest findById(@NotNull Long id) {
		final QSzotarTest table = QSzotarTest.szotarTest;
		return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
	}

	@Override
	public void delete(@NotNull Long id) {
		final QSzotarTest table = QSzotarTest.szotarTest;
		queryFactory.delete(table)
				.where(table.id.eq(id))
				.execute();
	}

	public List<SzotarTest> findAllSzotarTipus() {
		QSzotarTest szotarTest = QSzotarTest.szotarTest;
		return queryFactory
				.selectFrom(szotarTest)
				.where(szotarTest.idTipus.eq(ID_SZOTAR_ROOT)
				.and(szotarTest.id.ne(ID_SZOTAR_ROOT)))
				.fetch();
	}

	public List<SzotarTest> findAllByTipusKod(@NotNull String kod) {
		QSzotarTest view = QSzotarTest.szotarTest;
		QSzotarTest tipus = new QSzotarTest("tipus");
		return queryFactory
				.select(view)
				.from(view, tipus)
				.where(view.idTipus.eq(tipus.id))
				.where(tipus.kod.eq(kod))
				.fetch();
	}

	@Override
	public Set<SzotarTest> findSubordinates(@NotNull Long idPrincipal) {
		QSzotarTest szotarTest = QSzotarTest.szotarTest;
		return new HashSet<>(queryFactory
				.selectFrom(szotarTest)
		        .where(szotarTest.id.ne(ID_SZOTAR_ROOT))
		        .where(szotarTest.idTipus.eq(idPrincipal))
		        .fetch());
	}

	@Override
	public boolean hasSubordinates(@NotNull Long idPrincipal) {
		QSzotarTest szotarTest = QSzotarTest.szotarTest;
		return QueryUtil.exists(queryFactory
				.selectFrom(szotarTest)
		        .where(szotarTest.id.ne(ID_SZOTAR_ROOT))
		        .where(szotarTest.idTipus.eq(idPrincipal)));
	}

	@Override
	public Set<SzotarTest> findSovereigns() {
		return Collections.singleton(findSzotarTestRoot());
	}

	public SzotarTest findSzotarTestRoot() {
		return findById(ID_SZOTAR_ROOT);
	}

	public boolean isSubordinate(@NotNull Long idPrincipal, @NotNull Long idSubordinate) {
		QSzotarTest szotarTest = QSzotarTest.szotarTest;
		return BooleanUtils.isTrue(queryFactory.from(szotarTest)
				.where(szotarTest.id.eq(idSubordinate))
				.select(szotarTest.idTipus.when(idPrincipal).then(Expressions.TRUE).otherwise(Expressions.FALSE))
				.fetchFirst());
	}

	public void saveSubordination(@NotNull Long idPrincipal, @NotNull Long idSubordinate) {
		if (((Long) ID_SZOTAR_ROOT).equals(idSubordinate)) {
			throw new ConsistencyException("Szótár-root can not be subordinate");
		}
		if (isSubordinate(idPrincipal, idSubordinate)) return;

		QSzotarTest szotarTest = QSzotarTest.szotarTest;
		queryFactory.update(szotarTest)
				.set(szotarTest.idTipus, idPrincipal)
				.where(szotarTest.id.eq(idSubordinate))
				.execute();
	}

	@Override
	public void removeSubordination(@NotNull Long idPrincipal, @NotNull Long idSubordinate) {
		if(!isSubordinate(idPrincipal, idSubordinate)){
			return;
		}

		QSzotarTest szotarTest = QSzotarTest.szotarTest;
		queryFactory.update(szotarTest)
				.set(szotarTest.idTipus, 0L)
				.where(szotarTest.id.eq(idSubordinate)
				  .and(szotarTest.idTipus.eq(idPrincipal)))
				.execute();
	}

	@Override
	public SzotarTest save(@NotNull SzotarTest entitas) {
		if (entitas.getId() == null) {
			return save(ID_SZOTAR_ROOT, entitas);
		} else {
			return super.save(entitas);
		}
	}

	public SzotarTest save(@NotNull Long idTipus, @NotNull SzotarTest entitas) {
		entitas.setIdTipus(idTipus);
		return super.save(entitas);
	}

	public Tree<SzotarTest> save(@NotNull Tree<SzotarTest> tree) {
		final SzotarTest root = tree.getRoot();
		final SzotarTest savedRoot = save(root);
		final Tree<SzotarTest> savedTree = new Tree<>(savedRoot);
		saveSubordinates(tree, savedTree, root, savedRoot);
		return savedTree;
	}
}
