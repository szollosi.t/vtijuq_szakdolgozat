package vtijuq_szakdoli.dao.entitas.akkreditacio;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.akkreditacio.Akkreditacio;
import vtijuq_szakdoli.domain.entitas.akkreditacio.QAkkreditacio;
import vtijuq_szakdoli.domain.entitas.megvalositas.KonkretSzakirany;
import vtijuq_szakdoli.domain.entitas.megvalositas.QKonkretSzakirany;
import vtijuq_szakdoli.filter.QueryDslFilter;

@Stateless
public class AkkreditacioDao extends AbstractEntitasDao<Akkreditacio> {

    @Override
    protected QAkkreditacio getEntityPath() {
        return QAkkreditacio.akkreditacio;
    }

    @Override
    public Akkreditacio findById(@NotNull Long id) {
        final QAkkreditacio table = QAkkreditacio.akkreditacio;
        final Akkreditacio akkreditacio = queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
        return akkreditacio != null ? fetchTransientFields(akkreditacio) : null;
    }

    @Override
    public List<Akkreditacio> findAll() {
        return super.findAll().stream().map(this::fetchTransientFields).collect(Collectors.toList());
    }

    @Override
    public Akkreditacio findByFilter(QueryDslFilter<Akkreditacio> filter) {
        final Akkreditacio akkreditacio = super.findByFilter(filter);
        return akkreditacio != null ? fetchTransientFields(akkreditacio) : null;
    }

    @Override
    public List<Akkreditacio> findAllByFilter(QueryDslFilter<Akkreditacio> filter) {
        return super.findAllByFilter(filter).stream().map(this::fetchTransientFields).collect(Collectors.toList());
    }

    private Akkreditacio fetchTransientFields(@NotNull final Akkreditacio entitas) {
        if (entitas.getIdKonkretSzakirany() != null) {
            final QKonkretSzakirany kszi = QKonkretSzakirany.konkretSzakirany;
            final KonkretSzakirany konkretSzakirany = queryFactory
                    .selectFrom(kszi)
                    .where(kszi.id.eq(entitas.getIdKonkretSzakirany()))
                    .fetchOne();
            entitas.setKonkretSzakirany(konkretSzakirany);
        }
        return entitas;
    }

    @Override
    public void delete(@NotNull Long id) {
        final QAkkreditacio table = QAkkreditacio.akkreditacio;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }
}
