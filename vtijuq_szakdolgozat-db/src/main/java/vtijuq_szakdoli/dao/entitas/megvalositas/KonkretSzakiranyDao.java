package vtijuq_szakdoli.dao.entitas.megvalositas;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzakiranyKovetelmeny;
import vtijuq_szakdoli.domain.entitas.megvalositas.KonkretSzakirany;
import vtijuq_szakdoli.domain.entitas.megvalositas.QKonkretSzak;
import vtijuq_szakdoli.domain.entitas.megvalositas.QKonkretSzakirany;
import vtijuq_szakdoli.filter.QueryDslFilter;

@Stateless
public class KonkretSzakiranyDao extends AbstractEntitasDao<KonkretSzakirany> {

    @Override
    protected QKonkretSzakirany getEntityPath() {
        return QKonkretSzakirany.konkretSzakirany;
    }

    @Override
    public List<KonkretSzakirany> findAll() {
        return super.findAll().stream().map(this::fetchTransientFields).collect(Collectors.toList());
    }

    @Override
    public List<KonkretSzakirany> findAllByFilter(QueryDslFilter<KonkretSzakirany> filter) {
        return super.findAllByFilter(filter).stream().map(this::fetchTransientFields).collect(Collectors.toList());
    }

    @Override
    public KonkretSzakirany findById(@NotNull Long id) {
        final QKonkretSzakirany table = QKonkretSzakirany.konkretSzakirany;
        final KonkretSzakirany kszi = queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
        return kszi != null ? fetchTransientFields(kszi) : null;
    }

    @Override
    public KonkretSzakirany findByFilter(QueryDslFilter<KonkretSzakirany> filter) {
        final KonkretSzakirany kszi = super.findByFilter(filter);
        return kszi != null ? fetchTransientFields(kszi): null;
    }

    @Override
    public KonkretSzakirany save(KonkretSzakirany entitas) {
        entitas.setIdKonkretSzak(entitas.getKonkretSzak() != null ? entitas.getKonkretSzak().getId() : null);
        entitas.setIdSzakiranyKovetelmeny(entitas.getSzakiranyKovetelmeny() != null ? entitas.getSzakiranyKovetelmeny().getId() : null);
        final KonkretSzakirany saved = super.save(entitas);
        return fetchTransientFields(saved);
    }

    @Override
    public void delete(@NotNull Long id) {
        final QKonkretSzakirany table = QKonkretSzakirany.konkretSzakirany;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }

    public KonkretSzakirany fetchTransientFields(@NotNull KonkretSzakirany kszi) {
        return fetchKonkretSzak(fetchSzakiranyKovetelmeny(kszi));
    }

    public KonkretSzakirany fetchSzakiranyKovetelmeny(@NotNull KonkretSzakirany kszi) {
        if (kszi.getIdSzakiranyKovetelmeny() != null) {
            final QSzakiranyKovetelmeny szik = QSzakiranyKovetelmeny.szakiranyKovetelmeny;
            kszi.setSzakiranyKovetelmeny(queryFactory.selectFrom(szik).where(szik.id.eq(kszi.getIdSzakiranyKovetelmeny())).fetchOne());
        }
        return kszi;
    }

    public KonkretSzakirany fetchKonkretSzak(@NotNull KonkretSzakirany kszi) {
        if (kszi.getIdKonkretSzak() != null) {
            final QKonkretSzak ksz = QKonkretSzak.konkretSzak;
            kszi.setKonkretSzak(queryFactory.selectFrom(ksz).where(ksz.id.eq(kszi.getIdKonkretSzak())).fetchOne());
        }
        return kszi;
    }

    public Long findAlapSzakiranyIdkByKonkretSzakId(@NotNull Long id) {
        QKonkretSzakirany kszi = QKonkretSzakirany.konkretSzakirany;
        QSzakiranyKovetelmeny szik = QSzakiranyKovetelmeny.szakiranyKovetelmeny;
        return queryFactory.from(kszi, szik)
                .where(kszi.idKonkretSzak.eq(id)
                  .and(szik.id.eq(kszi.idSzakiranyKovetelmeny))
                  .and(szik.szakirany.isNull()))
                .select(kszi.id)
                .fetchOne();
    }

    public List<Long> findKonkretSzakiranyIdkByKonkretSzakId(@NotNull Long id) {
        QKonkretSzakirany kszi = QKonkretSzakirany.konkretSzakirany;
        QSzakiranyKovetelmeny szik = QSzakiranyKovetelmeny.szakiranyKovetelmeny;
        return queryFactory.from(kszi, szik)
                .where(kszi.idKonkretSzak.eq(id)
                  .and(szik.id.eq(kszi.idSzakiranyKovetelmeny))
                  .and(szik.szakirany.isNotNull()))
                .select(kszi.id)
                .fetch();
    }
}
