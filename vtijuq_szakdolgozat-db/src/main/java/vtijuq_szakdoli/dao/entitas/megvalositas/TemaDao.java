package vtijuq_szakdoli.dao.entitas.megvalositas;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import utils.query.QueryUtil;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTema;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tema;
import vtijuq_szakdoli.domain.view.megvalositas.QTemaTematikaszamMView;

@Stateless
public class TemaDao extends AbstractEntitasDao<Tema> {

    @Override
    protected QTema getEntityPath() {
        return QTema.tema;
    }

    public Tema findById(@NotNull Long id) {
        final QTema table = QTema.tema;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QTema table = QTema.tema;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }

    public boolean isSajatTema(@NotNull Long id) {
        QTemaTematikaszamMView mView = QTemaTematikaszamMView.temaTematikaszamMView;
        return QueryUtil.exists(queryFactory.from(mView)
                .where(mView.idTema.eq(id)
                .and(mView.tematikaSzam.eq(1)))
        );
    }
}
