package vtijuq_szakdoli.dao.entitas.megvalositas;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.KonkretSzak;
import vtijuq_szakdoli.domain.entitas.megvalositas.QKonkretSzak;

@Stateless
public class KonkretSzakDao extends AbstractEntitasDao<KonkretSzak> {

    @Override
    protected QKonkretSzak getEntityPath() {
        return QKonkretSzak.konkretSzak;
    }

    @Override
    public KonkretSzak findById(@NotNull Long id) {
        final QKonkretSzak table = QKonkretSzak.konkretSzak;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QKonkretSzak table = QKonkretSzak.konkretSzak;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }
}
