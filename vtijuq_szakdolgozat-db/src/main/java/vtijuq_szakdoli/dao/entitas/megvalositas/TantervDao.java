package vtijuq_szakdoli.dao.entitas.megvalositas;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.QKonkretSzakirany;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTantargy;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTanterv;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tanterv;

@Stateless
public class TantervDao extends AbstractEntitasDao<Tanterv> {

    @Override
    protected QTanterv getEntityPath() {
        return QTanterv.tanterv;
    }

    @Override
    public Tanterv findById(@NotNull Long id) {
        final QTanterv table = QTanterv.tanterv;
        final Tanterv tanterv = queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
        return tanterv != null ? fetchTransientFields(tanterv) : null;
    }

    @Override
    public Tanterv save(Tanterv entitas) {
        return fetchTransientFields(super.save(entitas));
    }

    public Tanterv fetchTransientFields(@NotNull Tanterv tanterv) {
        return fetchKonkretSzakirany(fetchTantargy(tanterv));
    }

    public Tanterv fetchKonkretSzakirany(@NotNull Tanterv tanterv) {
        if (tanterv.getIdKonkretSzakirany() != null) {
            final QKonkretSzakirany kszi = QKonkretSzakirany.konkretSzakirany;
            tanterv.setKonkretSzakirany(queryFactory.selectFrom(kszi).where(kszi.id.eq(tanterv.getIdKonkretSzakirany())).fetchOne());
        }
        return tanterv;
    }

    public Tanterv fetchTantargy(@NotNull Tanterv tanterv) {
        if (tanterv.getIdTantargy() != null) {
            final QTantargy szik = QTantargy.tantargy;
            tanterv.setTantargy(queryFactory.selectFrom(szik).where(szik.id.eq(tanterv.getIdTantargy())).fetchOne());
        }
        return tanterv;
    }

    @Override
    public void delete(@NotNull Long id) {
        final QTanterv table = QTanterv.tanterv;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }

    public List<Tanterv> findTantervekByKonkretSzakiranyId(@NotNull Long konkretSzakiranyId) {
        final QTanterv tanterv = QTanterv.tanterv;
        return queryFactory
                .selectFrom(tanterv)
                .where(tanterv.idKonkretSzakirany.eq(konkretSzakiranyId))
                .fetch().stream()
                .map(this::fetchTantargy)
                .collect(Collectors.toList());
    }

    public List<Tanterv> findTantervekByTantargyId(@NotNull Long idTantargy) {
        final QTanterv tanterv = QTanterv.tanterv;
        return queryFactory
                .selectFrom(tanterv)
                .where(tanterv.idTantargy.eq(idTantargy))
                .fetch().stream()
                .map(this::fetchKonkretSzakirany)
                .collect(Collectors.toList());
    }
}
