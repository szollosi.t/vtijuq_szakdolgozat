package vtijuq_szakdoli.dao.entitas.megvalositas;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.IsmeretSzint;
import vtijuq_szakdoli.domain.entitas.megvalositas.QIsmeretSzint;

@Stateless
public class IsmeretSzintDao extends AbstractEntitasDao<IsmeretSzint> {

    @Override
    protected QIsmeretSzint getEntityPath() {
        return QIsmeretSzint.ismeretSzint;
    }

    @Override
    public IsmeretSzint findById(@NotNull Long id) {
        final QIsmeretSzint table = QIsmeretSzint.ismeretSzint;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QIsmeretSzint table = QIsmeretSzint.ismeretSzint;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }
}
