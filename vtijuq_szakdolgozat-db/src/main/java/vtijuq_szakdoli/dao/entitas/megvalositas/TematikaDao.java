package vtijuq_szakdoli.dao.entitas.megvalositas;

import java.util.List;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import utils.query.QueryUtil;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTema;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTematika;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tematika;
import vtijuq_szakdoli.domain.view.megvalositas.QTemaTematikaszamMView;

@Stateless
public class TematikaDao extends AbstractEntitasDao<Tematika> {

    @Override
    protected QTematika getEntityPath() {
        return QTematika.tematika;
    }

    @Override
    public Tematika findById(@NotNull Long id) {
        final QTematika table = QTematika.tematika;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        //FIXME feljebb vihető-e? -> feljebb viendő a save-beli logikával együtt
        final Long tematikaTemaId = findById(id).getTema().getId();
        final boolean sajatTema = isSajatTema(tematikaTemaId);

        final QTematika tematika = QTematika.tematika;
        queryFactory.delete(tematika)
                .where(tematika.id.eq(id))
                .execute();

        if (sajatTema) {
            deleteTema(tematikaTemaId);
        }
    }

    @Override
    public Tematika save(Tematika tematika) {
        //FIXME feljebb vihető-e? -> feljebb viendő, mert a service is ebben a tranzakcióban van és a tematika attached marad végig
        //FIXME egyébként is mi van, ha egyik tematikából ki, a másikba be
        final Long oldTemaId = tematika.getId() != null ? findById(tematika.getId()).getTema().getId() : null;
        final Long newTemaId = tematika.getTema().getId();
        boolean temaTorlendo = oldTemaId != null && !oldTemaId.equals(newTemaId) && isSajatTema(oldTemaId);

        final Tematika savedTematika = super.save(tematika);

        if (temaTorlendo) {
            deleteTema(oldTemaId);
        }
        return savedTematika;
    }

    public boolean isSajatTema(Long temaId) {
        boolean temaTorlendo;
        final QTemaTematikaszamMView mView = QTemaTematikaszamMView.temaTematikaszamMView;
        temaTorlendo = QueryUtil.exists(queryFactory
                .from(mView)
                .where(mView.idTema.eq(temaId)
                  .and(mView.tematikaSzam.eq(1)))
        );
        return temaTorlendo;
    }

    public void deleteTema(Long tematikaTemaId) {
        QTema tema = QTema.tema;
        queryFactory.delete(tema)
                .where(tema.id.eq(tematikaTemaId))
                .execute();
    }

    public List<Tematika> findTematikakByTantargyId(@NotNull Long idTantargy) {
        QTematika tematika = QTematika.tematika;
        return queryFactory.selectFrom(tematika).where(tematika.idTantargy.eq(idTantargy)).fetch();
    }

    public Tematika save(@NotNull Long idTantargy, @NotNull Tematika tematika) {
        tematika.setIdTantargy(idTantargy);
        return save(tematika);
    }

    public void deleteByTantargyId(@NotNull Long id) {
        final QTema tema = QTema.tema;
        final QTematika tematika = QTematika.tematika;

        final List<Long> temaIds = queryFactory
                .select(tematika.tema.id)
                .from(tematika)
                .where(tematika.idTantargy.eq(id))
                .fetch();
        //ennek megfelelő hívás esetén sz összes tematikát kell megtalálnia,
        // a where feltétel a foreign key constraint-tel együtt garantálja,
        // hogy végleges tematikájú tantárgy ne legyen törölhető
        queryFactory.delete(tematika)
                .where(tematika.idTantargy.eq(id)
                        .and(tematika.vegleges.isNull().or(tematika.vegleges.isFalse())))
                .execute();

        //a csak ehhez a tantárgyhoz tartozó témákat takarítjuk,
        // ha nem akkor az adatbázis megállít minket a tranzakció végén
        if (!QueryUtil.exists(queryFactory
                .selectOne()
                .from(tema, tematika)
                .where(tema.eq(tematika.tema))
                .where(tematika.idTantargy.ne(id)))){
            queryFactory.delete(tema)
                    .where(tema.id.in(temaIds))
                    .execute();
        }
    }
}
