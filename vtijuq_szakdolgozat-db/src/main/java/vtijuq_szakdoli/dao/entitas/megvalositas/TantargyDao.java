package vtijuq_szakdoli.dao.entitas.megvalositas;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.adminisztracio.QSzervezet;
import vtijuq_szakdoli.domain.entitas.megvalositas.QTantargy;
import vtijuq_szakdoli.domain.entitas.megvalositas.Tantargy;
import vtijuq_szakdoli.filter.QueryDslFilter;

@Stateless
public class TantargyDao extends AbstractEntitasDao<Tantargy> {

    @Override
    protected QTantargy getEntityPath() {
        return QTantargy.tantargy;
    }

    @Override
    public List<Tantargy> findAll() {
        return super.findAll().stream()
                .map(this::fetchTransientFields)
                .collect(Collectors.toList());
    }

    @Override
    public Tantargy findByFilter(QueryDslFilter<Tantargy> filter) {
        final Tantargy tantargy = super.findByFilter(filter);
        return tantargy != null ? fetchTransientFields(tantargy) : null;
    }

    @Override
    public List<Tantargy> findAllByFilter(QueryDslFilter<Tantargy> filter) {
        return super.findAllByFilter(filter).stream()
                .map(this::fetchTransientFields)
                .collect(Collectors.toList());
    }

    @Override
    public Tantargy findById(@NotNull Long id) {
        final QTantargy table = QTantargy.tantargy;
        final Tantargy tantargy = queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
        return tantargy != null ? fetchTransientFields(tantargy) : null;
    }

    @Override
    public Tantargy save(Tantargy entitas) {
        entitas.setIdSzervezet(entitas.getSzervezet() != null ? entitas.getSzervezet().getId() : null);
        final Tantargy saved = super.save(entitas);
        return fetchTransientFields(saved);
    }

    @Override
    public void delete(@NotNull Long id) {
        final QTantargy tantargy = QTantargy.tantargy;
        queryFactory.delete(tantargy)
                .where(tantargy.id.eq(id))
                .execute();
    }

    public Tantargy fetchTransientFields(@NotNull Tantargy tantargy) {
        if (tantargy.getIdSzervezet() != null) {
            final QSzervezet sz = QSzervezet.szervezet;
            tantargy.setSzervezet(queryFactory.selectFrom(sz).where(sz.id.eq(tantargy.getIdSzervezet())).fetchOne());
        }
        return tantargy;
    }

    public List<Tantargy> findAllKonkret() {
        QTantargy tantargy = QTantargy.tantargy;
        return queryFactory.selectFrom(tantargy)
                .where(tantargy.idSzervezet.isNotNull())
                .fetch().stream()
                .map(this::fetchTransientFields)
                .collect(Collectors.toList());
    }
}
