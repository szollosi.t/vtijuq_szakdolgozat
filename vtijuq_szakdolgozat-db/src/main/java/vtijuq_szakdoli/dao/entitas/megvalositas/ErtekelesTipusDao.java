package vtijuq_szakdoli.dao.entitas.megvalositas;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.megvalositas.ErtekelesTipus;
import vtijuq_szakdoli.domain.entitas.megvalositas.QErtekelesTipus;

@Stateless
public class ErtekelesTipusDao extends AbstractEntitasDao<ErtekelesTipus> {

    @Override
    protected QErtekelesTipus getEntityPath() {
        return QErtekelesTipus.ertekelesTipus;
    }

    @Override
    public ErtekelesTipus findById(@NotNull Long id) {
        final QErtekelesTipus table = QErtekelesTipus.ertekelesTipus;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QErtekelesTipus table = QErtekelesTipus.ertekelesTipus;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }
}
