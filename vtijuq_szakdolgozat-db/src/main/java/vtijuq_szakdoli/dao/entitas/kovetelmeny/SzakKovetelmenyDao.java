package vtijuq_szakdoli.dao.entitas.kovetelmeny;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzakKovetelmeny;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakKovetelmeny;

@Stateless
public class SzakKovetelmenyDao extends AbstractEntitasDao<SzakKovetelmeny> {

    @Override
    protected QSzakKovetelmeny getEntityPath() {
        return QSzakKovetelmeny.szakKovetelmeny;
    }

    @Override
    public SzakKovetelmeny findById(@NotNull Long id) {
        final QSzakKovetelmeny table = QSzakKovetelmeny.szakKovetelmeny;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QSzakKovetelmeny table = QSzakKovetelmeny.szakKovetelmeny;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }
}
