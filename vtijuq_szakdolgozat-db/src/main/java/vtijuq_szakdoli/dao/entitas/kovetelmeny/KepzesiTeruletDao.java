package vtijuq_szakdoli.dao.entitas.kovetelmeny;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.KepzesiTerulet;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QKepzesiTerulet;

@Stateless
public class KepzesiTeruletDao extends AbstractEntitasDao<KepzesiTerulet> {

    @Override
    protected QKepzesiTerulet getEntityPath() {
        return QKepzesiTerulet.kepzesiTerulet;
    }

    @Override
    public KepzesiTerulet findById(@NotNull Long id) {
        final QKepzesiTerulet table = QKepzesiTerulet.kepzesiTerulet;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QKepzesiTerulet table = QKepzesiTerulet.kepzesiTerulet;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }
}
