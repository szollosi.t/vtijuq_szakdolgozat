package vtijuq_szakdoli.dao.entitas.kovetelmeny;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzak;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Szak;

@Stateless
public class SzakDao extends AbstractEntitasDao<Szak> {

    @Override
    protected QSzak getEntityPath() {
        return QSzak.szak;
    }

    @Override
    public Szak findById(@NotNull Long id) {
        final QSzak table = QSzak.szak;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QSzak table = QSzak.szak;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }
}
