package vtijuq_szakdoli.dao.entitas.kovetelmeny;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import com.querydsl.core.types.dsl.EntityPathBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.query.QueryUtil;

import vtijuq_szakdoli.common.exception.ConsistencyException;
import vtijuq_szakdoli.common.util.graph.Dag;
import vtijuq_szakdoli.dao.HierarchicalDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Ismeret;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.IsmeretEl;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QIsmeret;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QIsmeretEl;

@Stateless
public class IsmeretDao extends HierarchicalDao<Ismeret> {

	private static final Logger LOG = LoggerFactory.getLogger(IsmeretDao.class);

	@Override
	protected EntityPathBase<Ismeret> getEntityPath() {
		return QIsmeret.ismeret;
	}

	@Override
	public Ismeret findById(@NotNull Long id) {
		final QIsmeret ismeret = QIsmeret.ismeret;
		return queryFactory.selectFrom(ismeret).where(ismeret.id.eq(id)).fetchOne();
	}

	@Override
	public Set<Ismeret> findSubordinates(@NotNull Long idPrincipal) {
		QIsmeret ismeret = QIsmeret.ismeret;
		QIsmeretEl ismeretEl = QIsmeretEl.ismeretEl;
		return new HashSet<>(queryFactory
               .select(ismeret)
               .from(ismeret, ismeretEl)
               .where(ismeretEl.idmi.eq(ismeret.id))
               .where(ismeretEl.idhova.eq(idPrincipal))
               .fetch());
	}

	@Override
	public boolean hasSubordinates(@NotNull Long idPrincipal) {
		QIsmeretEl ismeretEl = QIsmeretEl.ismeretEl;
		return QueryUtil.exists(queryFactory
				.selectFrom(ismeretEl)
				.where(ismeretEl.idhova.eq(idPrincipal)));
	}

	@Override
	public Set<? extends Ismeret> findSovereigns() {
		QIsmeret ismeret = QIsmeret.ismeret;
		QIsmeretEl ismeretEl = QIsmeretEl.ismeretEl;
		return new HashSet<>(queryFactory
				                     .selectFrom(ismeret)
				                     .where(ismeret.id.notIn(queryFactory.select(ismeretEl.idmi).from(ismeretEl)))
				                     .fetch());
	}

	@Override
	public boolean isSubordinate(@NotNull Long idPrincipal, @NotNull Long idSubordinate){
		QIsmeretEl el = QIsmeretEl.ismeretEl;
		return QueryUtil.exists(queryFactory
				.from(el)
				.where(el.idmi.eq(idSubordinate)
				  .and(el.idhova.eq(idPrincipal))));
	}

	@Override
	public Ismeret save(@NotNull Long idPrincipal, @NotNull Ismeret entitas) {
		final Ismeret ismeret = save(entitas);
		saveSubordination(idPrincipal, ismeret.getId());
		return ismeret;
	}

	public void saveSubordination(@NotNull Long idPrincipal, @NotNull Long idSubordinate) {
		if(isSubordinate(idPrincipal, idSubordinate)) return;

		IsmeretEl ujEl = new IsmeretEl();
		ujEl.setIdhova(idPrincipal);
		ujEl.setIdmi(idSubordinate);
		save(ujEl);
	}

	@Override
	public void removeSubordination(@NotNull Long idPrincipal, @NotNull Long idSubordinate) {
		if(!isSubordinate(idPrincipal, idSubordinate)) return;

		QIsmeretEl qel = QIsmeretEl.ismeretEl;
		queryFactory.delete(qel)
				.where(qel.idmi.eq(idSubordinate)
				  .and(qel.idhova.eq(idPrincipal)))
				.execute();
	}

	private IsmeretEl save(@NotNull IsmeretEl el){
		try {
			em.persist(el);
			return el;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new ConsistencyException(e.getMessage());
		}
	}

	public void delete(@NotNull Long ismeretId) {
		QIsmeretEl ismeretEl = QIsmeretEl.ismeretEl;
		//kapcsolódó ismeretek átkötése
		List<Long> foIsmeretek = queryFactory
				.select(ismeretEl.idhova).from(ismeretEl).where(ismeretEl.idmi.eq(ismeretId)).fetch();
		List<Long> alIsmeretek = queryFactory
				.select(ismeretEl.idmi).from(ismeretEl).where(ismeretEl.idhova.eq(ismeretId)).fetch();
		if (!foIsmeretek.isEmpty()) {
			queryFactory.delete(ismeretEl)
					.where(ismeretEl.idmi.eq(ismeretId))
					.execute();
		}
		if (!alIsmeretek.isEmpty()) {
			queryFactory.delete(ismeretEl)
					.where(ismeretEl.idhova.eq(ismeretId))
					.execute();
		}
		if (!foIsmeretek.isEmpty() && !alIsmeretek.isEmpty()) {
			foIsmeretek.forEach(foIsmId -> alIsmeretek.forEach(alIsmId -> saveSubordination(foIsmId, alIsmId)));
		}
		QIsmeret ismeret = QIsmeret.ismeret;
		queryFactory.delete(ismeret)
				.where(ismeret.id.eq(ismeretId))
				.execute();
	}

	public Dag<Ismeret> save(@NotNull Dag<Ismeret> dag) {
		final Map<Ismeret, Ismeret> savedSovereigns = dag.getSovereigns().stream()
				.collect(Collectors.toMap(Function.identity(), this::save));
		final Dag<Ismeret> savedDag = new Dag<>(new HashSet<>(savedSovereigns.values()));
		savedSovereigns.forEach((sovereign, savedSovereign) ->
				saveSubordinates(dag, savedDag, sovereign, savedSovereign));
		return savedDag;
	}
}
