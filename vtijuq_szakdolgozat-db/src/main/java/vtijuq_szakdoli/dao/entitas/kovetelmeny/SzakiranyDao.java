package vtijuq_szakdoli.dao.entitas.kovetelmeny;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzakirany;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.Szakirany;

@Stateless
public class SzakiranyDao extends AbstractEntitasDao<Szakirany> {

    @Override
    protected QSzakirany getEntityPath() {
        return QSzakirany.szakirany;
    }

    @Override
    public Szakirany findById(@NotNull Long id) {
        final QSzakirany table = QSzakirany.szakirany;
        return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
    }

    @Override
    public void delete(@NotNull Long id) {
        final QSzakirany table = QSzakirany.szakirany;
        queryFactory.delete(table)
                .where(table.id.eq(id))
                .execute();
    }
}
