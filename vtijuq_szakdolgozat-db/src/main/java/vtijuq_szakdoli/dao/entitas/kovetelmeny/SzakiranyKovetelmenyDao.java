package vtijuq_szakdoli.dao.entitas.kovetelmeny;

import java.util.List;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.dao.AbstractEntitasDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzakiranyKovetelmeny;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakiranyKovetelmeny;

@Stateless
public class SzakiranyKovetelmenyDao extends AbstractEntitasDao<SzakiranyKovetelmeny> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakiranyKovetelmenyDao.class);

	@Override
	protected QSzakiranyKovetelmeny getEntityPath() {
		return QSzakiranyKovetelmeny.szakiranyKovetelmeny;
	}

	public SzakiranyKovetelmeny findById(@NotNull Long id) {
		final QSzakiranyKovetelmeny table = QSzakiranyKovetelmeny.szakiranyKovetelmeny;
		return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
	}

	@Override
	public void delete(@NotNull Long id) {
		final QSzakiranyKovetelmeny table = QSzakiranyKovetelmeny.szakiranyKovetelmeny;
		queryFactory.delete(table)
				.where(table.id.eq(id))
				.execute();
	}

	public SzakiranyKovetelmeny findAlapSzakiranyKovetelmeny(@NotNull Long szakKovetelmenyId) {
		QSzakiranyKovetelmeny szakiranyKovetelmeny = QSzakiranyKovetelmeny.szakiranyKovetelmeny;
		return queryFactory
				.selectFrom(szakiranyKovetelmeny)
				.where(szakiranyKovetelmeny.szakirany.isNull()
				  .and(szakiranyKovetelmeny.szakKovetelmeny.id.eq(szakKovetelmenyId)))
				.fetchOne();
	}

	public Long findAlapSzakiranyIdkBySzakKovetelmenyId(@NotNull Long id) {
		QSzakiranyKovetelmeny szik = QSzakiranyKovetelmeny.szakiranyKovetelmeny;
		return queryFactory.from(szik)
				.where(szik.szakKovetelmeny.id.eq(id)
				  .and(szik.szakirany.isNull()))
				.select(szik.id)
				.fetchOne();
	}

	public List<Long> findSzakiranyKovetelmenyIdkBySzakKovetelmenyId(@NotNull Long id) {
		QSzakiranyKovetelmeny szik = QSzakiranyKovetelmeny.szakiranyKovetelmeny;
		return queryFactory.from(szik)
				.where(szik.szakKovetelmeny.id.eq(id)
				  .and(szik.szakirany.isNotNull()))
				.select(szik.id)
				.fetch();
	}
}
