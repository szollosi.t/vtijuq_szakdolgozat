package vtijuq_szakdoli.dao.entitas.kovetelmeny;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.query.QueryUtil;

import vtijuq_szakdoli.common.exception.ConsistencyException;
import vtijuq_szakdoli.common.util.graph.Dag;
import vtijuq_szakdoli.dao.HierarchicalDao;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.QSzakmaiJellemzo;
import vtijuq_szakdoli.domain.entitas.kovetelmeny.SzakmaiJellemzo;

@Stateless
public class SzakmaiJellemzoDao extends HierarchicalDao<SzakmaiJellemzo> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakmaiJellemzoDao.class);

	@Override
	protected EntityPathBase<SzakmaiJellemzo> getEntityPath() {
		return QSzakmaiJellemzo.szakmaiJellemzo;
	}

	@Override
	public SzakmaiJellemzo findById(@NotNull Long id) {
		final QSzakmaiJellemzo table = QSzakmaiJellemzo.szakmaiJellemzo;
		return queryFactory.selectFrom(table).where(table.id.eq(id)).fetchOne();
	}

	@Override
	public void delete(@NotNull Long id) {
		final QSzakmaiJellemzo table = QSzakmaiJellemzo.szakmaiJellemzo;
		queryFactory.delete(table)
				.where(table.id.eq(id))
				.execute();
	}

	@Override
	public Set<SzakmaiJellemzo> findSubordinates(@NotNull Long idPrincipal) {
		QSzakmaiJellemzo szakmaiJellemzo = QSzakmaiJellemzo.szakmaiJellemzo;
		return new HashSet<>(queryFactory
               .selectFrom(szakmaiJellemzo)
               .where(szakmaiJellemzo.idFojellemzo.eq(idPrincipal))
               .fetch());
	}

	@Override
	public boolean hasSubordinates(@NotNull Long idPrincipal) {
		QSzakmaiJellemzo szakmaiJellemzo = QSzakmaiJellemzo.szakmaiJellemzo;
		return QueryUtil.exists(queryFactory
               .selectFrom(szakmaiJellemzo)
               .where(szakmaiJellemzo.idFojellemzo.eq(idPrincipal)));
	}

	@Override
	public Set<SzakmaiJellemzo> findSovereigns() {
		QSzakmaiJellemzo szakmaiJellemzo = QSzakmaiJellemzo.szakmaiJellemzo;
		return new HashSet<>(queryFactory
             .select(szakmaiJellemzo)
             .from(szakmaiJellemzo)
             .where(szakmaiJellemzo.idFojellemzo.isNull())
             .fetch());
	}

	@Override
	public boolean isSubordinate(@NotNull Long idPrincipal, @NotNull Long idSubordinate) {
		QSzakmaiJellemzo szj = QSzakmaiJellemzo.szakmaiJellemzo;
		final JPAQuery<Boolean> query = queryFactory
				.from(szj)
				.where(szj.id.eq(idSubordinate))
				.select(szj.idFojellemzo.when(idPrincipal).then(Expressions.TRUE).otherwise(Expressions.FALSE));
		return BooleanUtils.isTrue(query.fetchFirst());
	}

	@Override
	public void saveSubordination(@NotNull Long idPrincipal, @NotNull Long idSubordinate) {
		if (isSubordinate(idPrincipal, idSubordinate)) return;

		QSzakmaiJellemzo szj = QSzakmaiJellemzo.szakmaiJellemzo;
		queryFactory.update(szj)
				.set(szj.idFojellemzo, idPrincipal)
				.where(szj.id.eq(idSubordinate))
				.execute();
	}

	@Override
	public void removeSubordination(@NotNull Long idPrincipal, @NotNull Long idSubordinate) {
		if(!isSubordinate(idPrincipal, idSubordinate)) return;

		QSzakmaiJellemzo szj = QSzakmaiJellemzo.szakmaiJellemzo;
		queryFactory.update(szj)
				.setNull(szj.idFojellemzo)
				.where(szj.id.eq(idSubordinate).and(szj.idFojellemzo.eq(idPrincipal)))
				.execute();
	}

	@Override
	public SzakmaiJellemzo save(Long idPrincipal, SzakmaiJellemzo subordinate) {
		subordinate.setIdFojellemzo(idPrincipal);
		return save(subordinate);
	}

	public Dag<SzakmaiJellemzo> save(@NotNull Long id, @NotNull Dag<SzakmaiJellemzo> dag) {
		final Dag<SzakmaiJellemzo> savedDag = findBySzakiranyKovetelmenyId(id);
		//mergeSovereigns
		final Set<SzakmaiJellemzo> savedSovereigns = mergePrincipalCollection(savedDag.getSovereigns(), dag.getSovereigns());
		//adjust savedDag sovereigns
		CollectionUtils.subtract(savedDag.getSovereigns(), savedSovereigns).forEach(savedDag::removeSubHierarchy);
		CollectionUtils.subtract(savedSovereigns, savedDag.getSovereigns()).forEach(savedDag::addNode);
		//map savedSovereigns for saving dag
		final Map<SzakmaiJellemzo, SzakmaiJellemzo> savedSovereignMap = new HashMap<>();
		dag.getSovereigns().forEach(s ->
				savedSovereignMap.put(
						s, savedSovereigns.stream().filter(ss -> ss.equals(s)).findFirst().orElse(null)));
		if (savedSovereignMap.containsValue(null)) {//ilyennek nem kéne lennie a saveCollection alapján
			throw new ConsistencyException("consistency.error.szj.notSavedSovereign");
		}
		//rekurzió indítása
		savedSovereignMap.forEach((sovereign, savedSovereign) ->
				saveSubordinates(dag, savedDag, sovereign, savedSovereign));
		return savedDag;
	}

	public Dag<SzakmaiJellemzo> findBySzakiranyKovetelmenyId(@NotNull Long id) {
		final QSzakmaiJellemzo szj = QSzakmaiJellemzo.szakmaiJellemzo;
		final Set<SzakmaiJellemzo> topSzakmaiJellemzoSet = new HashSet<>(
				queryFactory.selectFrom(szj)
						.where(szj.idSzakiranyKovetelmeny.eq(id)
								.and(szj.idFojellemzo.isNull()))
						.fetch());
		return new Dag<>(topSzakmaiJellemzoSet, this);
	}

	public void deleteBySzakiranyKovetelmenyId(@NotNull Long id){
		QSzakmaiJellemzo szj = QSzakmaiJellemzo.szakmaiJellemzo;
		queryFactory.delete(szj)
				.where(szj.idSzakiranyKovetelmeny.eq(id))
				.execute();
	}

}
