/*
 * FTR Community Edition is a BPM-based lightweight Java application 
 * development framework
 * Copyright (C) 2009-2013 Tigra Kft.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can contact Tigra Kft headquarters at 1115 Budapest, 
 * Bartók Béla út 105-113, Hungary or at email address ftr@tigra.hu. 
 */

package utils.query;

import java.util.Collections;
import java.util.List;

public class QueryResultList<T> {

	private final Long size;
	private final List<T> list;

	/**
	 * empty result
	 */
	public QueryResultList() {
		this.size = 0L;
		this.list = Collections.emptyList();
	}

	public QueryResultList(List<T> list) {
		this.size = null;
		this.list = list;
	}

	public QueryResultList(Long size, List<T> list) {
		this.size = size;
		this.list = list;
	}

	public Long getSize() {
		return size;
	}

	public List<T> getList() {
		return list;
	}
}
