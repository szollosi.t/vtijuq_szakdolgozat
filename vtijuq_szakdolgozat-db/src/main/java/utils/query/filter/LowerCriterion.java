/*
 * FTR Community Edition is a BPM-based lightweight Java application 
 * development framework
 * Copyright (C) 2009-2013 Tigra Kft.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can contact Tigra Kft headquarters at 1115 Budapest, 
 * Bartók Béla út 105-113, Hungary or at email address ftr@tigra.hu. 
 */

package utils.query.filter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LowerCriterion {
	boolean enableEqual() default false;

	String field();

	/**
	 * ha fix dátummal (óra/perc nélküli) szeretnénk szűrni olyan dátumra amely
	 * tartalmaz dátum/percet, akkor 1 nappal elk kell tolni
	 * 
	 * @return
	 */
	int offsetDay() default 0;
}
