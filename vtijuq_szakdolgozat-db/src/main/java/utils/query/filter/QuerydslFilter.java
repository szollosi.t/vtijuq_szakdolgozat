/*
 * FTR Community Edition is a BPM-based lightweight Java application 
 * development framework
 * Copyright (C) 2009-2013 Tigra Kft.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can contact Tigra Kft headquarters at 1115 Budapest, 
 * Bartók Béla út 105-113, Hungary or at email address ftr@tigra.hu. 
 */

package utils.query.filter;

import static org.apache.commons.lang3.StringUtils.capitalize;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.ComparableExpression;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.core.types.dsl.StringExpression;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utils.query.QueryUtil;

public abstract class QuerydslFilter {

	private static final String VALUE_SOURCE_METHOD = "METHOD";
	private static final String VALUE_SOURCE = "querydslfilter.value.source";

	private static final Logger LOG = LoggerFactory.getLogger(QuerydslFilter.class);

	private boolean needCount = true;
	private Long limit;
	private Long offset;
	private Order order = Order.ASC;
	private String sortField;

	public abstract String defaultSortField();

	// Ha a sortField által meghatározott rendezési sorend nem egyértelmű, akkor
	// a uniqueOrderSpecifier segítségével kiegészítő rendezési sorrend adható
	// meg. Ha ez hiányzik a lapozás hibás lehet. Általában az egyedi azonosító
	// lesz a tartalma.
	public OrderSpecifier<?> getUniqueOrderSpecifier() {
		return null;
	}

	public Predicate predicate(EntityPathBase<?> entityPath) {
		return ExpressionUtils.allOf(processAnnotations(entityPath));
	}

	// FIXME Refaktorálni egy kicsit???
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Collection<Predicate> processAnnotations(EntityPathBase entityPath) {
		Set<Predicate> list = new HashSet<>();
		Class cl = this.getClass();
		try {
			// Az osszes field, nem csak az adott osztaly
			List<Field> fields = new ArrayList<>();
			for (Class<?> c = cl; c != null; c = c.getSuperclass()) {
				fields.addAll(Arrays.asList(c.getDeclaredFields()));
			}

			// Customizalas vege
			for (Field f : fields) {
				for (Annotation a : f.getAnnotations()) {
					Object value = getValue(f, this);

					if (a.annotationType() == EqualCriterion.class) {
						EqualCriterion c = (EqualCriterion) a;
						if (value != null && !StringUtils.isBlank(c.filterFieldName())) {
							Field filterField = getField(value, c.filterFieldName());
							filterField.setAccessible(true);
							value = getValue(filterField, value);
						}
						if (value instanceof String && !StringUtils.isBlank((String) value)
								|| !(value instanceof String) && value != null) {
							SimpleExpression exp = getExpression(f.getName(), c.entityFieldName(), entityPath);
							if (c.not()) {
								list.add(exp.ne(value));
							} else {
								list.add(exp.eq(value));
							}
						}
					}

					if (a.annotationType() == LikeCriterion.class) {
						LikeCriterion c = (LikeCriterion) a;
						if (value != null && !StringUtils.isBlank(c.filterFieldName())) {
							Field filterField = getField(value, c.filterFieldName());
							filterField.setAccessible(true);
							value = getValue(filterField, value);
						}
						String svalue = (String) value;
						if (!StringUtils.isBlank(svalue)) {
							StringExpression exp = getExpression(f.getName(), c.entityFieldName(), entityPath);

							String s = c.ignoreCase() ? svalue.toUpperCase() : svalue;
							BooleanExpression likeExp = null;
							switch (c.matchMode()) {
							case ANYWHERE:
								if (c.ignoreCase()) {
									likeExp = exp.containsIgnoreCase(s);
								} else {
									likeExp = exp.contains(s);
								}
								break;
							case END:
								if (c.ignoreCase()) {
									likeExp = exp.endsWithIgnoreCase(s);
								} else {
									likeExp = exp.endsWith(s);
								}
								break;
							case START:
								if (c.ignoreCase()) {
									likeExp = exp.startsWithIgnoreCase(s);
								} else {
									likeExp = exp.startsWith(s);
								}
								break;
							}
							if (likeExp != null) {
								if (c.not()) {
									list.add(likeExp.not());
								} else {
									list.add(likeExp);
								}
							}
						}
					}

					if (a.annotationType() == LowerCriterion.class) {
						LowerCriterion c = (LowerCriterion) a;
						if (value != null) {
							ComparableExpressionBase exp = getExpression(f.getName(), c.field(), entityPath);

							if (c.enableEqual()) {
								if (c.offsetDay() == 0) {
									list.add(loe(exp, value));
								} else {
									list.add(loe(exp, DateUtils.addDays((Date) value, c.offsetDay())));
								}
							} else {
								if (c.offsetDay() == 0) {
									list.add(lt(exp, value));
								} else {
									list.add(lt(exp, DateUtils.addDays((Date) value, c.offsetDay())));
								}
							}
						}
					}

					if (a.annotationType() == GreaterCriterion.class) {
						GreaterCriterion c = (GreaterCriterion) a;
						if (value != null) {
							ComparableExpressionBase exp = getExpression(f.getName(), c.field(), entityPath);
							if (c.enableEqual()) {
								list.add(goe(exp, value));
							} else {
								list.add(gt(exp, value));
							}
						}
					}

					if (a.annotationType() == InCriterion.class) {
						InCriterion c = (InCriterion) a;
						if (value != null) {
							ComparableExpressionBase exp = getExpression(f.getName(), c.entityFieldName(), entityPath);

							if (!StringUtils.isEmpty(c.filterFieldName())) {
								List newValue = new ArrayList();
								if (value instanceof Collection && !((Collection) value).isEmpty()) {
									Collection collection = (Collection) value;
									for (Object o : collection) {
										Object v = getValue(getField(o, c.filterFieldName()), o);
										newValue.add(v);
									}
									value = newValue;
								}
								if (value instanceof Object[] && ((Object[]) value).length > 0) {
									Object[] objects = (Object[]) value;
									for (Object o : objects) {
										Object v = getValue(getField(o, c.filterFieldName()), o);
										newValue.add(v);
									}
									value = newValue;
								}
							}
							if (value instanceof Collection && !((Collection) value).isEmpty()) {
								if (c.not()) {
									list.add(exp.notIn((Collection) value));
								} else {
									list.add(exp.in((Collection) value));
								}
							}
							if (value instanceof Object[] && ((Object[]) value).length > 0) {
								if (c.not()) {
									list.add(exp.notIn((Object[]) value));
								} else {
									list.add(exp.in((Object[]) value));
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Error when processAnnotations!", e);
			throw new RuntimeException("Error when processAnnotations!", e);
		}
		return list;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected <T> T getExpression(String fieldName, String entityFieldName, EntityPathBase entityPath) {
		return (T) entityField(StringUtils.isBlank(entityFieldName) ? fieldName : entityFieldName, entityPath);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private BooleanExpression loe(ComparableExpressionBase exp, Object value) {
		if (exp instanceof ComparableExpression) {
			return ((ComparableExpression) exp).loe((Comparable) value);
		} else if (exp instanceof NumberExpression) {
			return ((NumberExpression) exp).loe((Number) value);
		}
		throw new RuntimeException("invalid expression: " + exp.getClass().getCanonicalName());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private BooleanExpression lt(ComparableExpressionBase exp, Object value) {
		if (exp instanceof ComparableExpression) {
			return ((ComparableExpression) exp).lt((Comparable) value);
		} else if (exp instanceof NumberExpression) {
			return ((NumberExpression) exp).lt((Number) value);
		}
		throw new RuntimeException("invalid expression: " + exp.getClass().getCanonicalName());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private BooleanExpression goe(ComparableExpressionBase exp, Object value) {
		if (exp instanceof ComparableExpression) {
			return ((ComparableExpression) exp).goe((Comparable) value);
		} else if (exp instanceof NumberExpression) {
			return ((NumberExpression) exp).goe((Number) value);
		}
		throw new RuntimeException("invalid expression: " + exp.getClass().getCanonicalName());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private BooleanExpression gt(ComparableExpressionBase exp, Object value) {
		if (exp instanceof ComparableExpression) {
			return ((ComparableExpression) exp).gt((Comparable) value);
		} else if (exp instanceof NumberExpression) {
			return ((NumberExpression) exp).gt((Number) value);
		}
		throw new RuntimeException("invalid expression: " + exp.getClass().getCanonicalName());
	}

	@SuppressWarnings({ "unchecked" })
	private <T> T entityField(String fieldPath, EntityPathBase<?> entityPath) {
		return (T) QueryUtil.getSimpleExpression(fieldPath, entityPath);
	}

	private Field getField(Object o, String name) throws SecurityException {
		for (Class<?> c = o.getClass(); c != null; c = c.getSuperclass()) {
			try {
				return c.getDeclaredField(name);
			} catch (NoSuchFieldException e) {
				LOG.debug("NoSuchFieldException in getField", e);
			}
		}
		return null;
	}

	/**
	 * ertek visszanyerese mezo vagy metodus alapjan
	 */
	private Object getValue(Field field, Object o) throws Exception {
//		if (VALUE_SOURCE_METHOD.equals(FTRConfigUtil.SYSTEM_PROPERTIES.getProperty(VALUE_SOURCE, VALUE_SOURCE_METHOD))) {
		if (true) {
			return getMethodValue(field, o);
		} else {
			field.setAccessible(true);
			return field.get(o);
		}
	}

	private Object getMethodValue(Field field, Object o) throws Exception {
		return o.getClass().getMethod(getterMethodName(field.getName())).invoke(o);
	}

	protected String getterMethodName(String fieldName) {
		return "get" + capitalize(fieldName);
	}

	public void setLimit(long limit) {
		this.limit = limit;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public Long getLimit() {
		return limit;
	}

	public Long getOffset() {
		return offset;
	}

	public String getSortField() {
		return sortField;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public boolean isNeedCount() {
		return needCount;
	}

	public void setNeedCount(boolean needCount) {
		this.needCount = needCount;
	}
}
