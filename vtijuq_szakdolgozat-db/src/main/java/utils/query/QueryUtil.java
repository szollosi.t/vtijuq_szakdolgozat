/*
 * FTR Community Edition is a BPM-based lightweight Java application 
 * development framework
 * Copyright (C) 2009-2013 Tigra Kft.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can contact Tigra Kft headquarters at 1115 Budapest, 
 * Bartók Béla út 105-113, Hungary or at email address ftr@tigra.hu. 
 */

package utils.query;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.hibernate.HibernateQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.query.filter.QuerydslFilter;

import vtijuq_szakdoli.filter.QueryDslFilter;

public interface QueryUtil {

	Logger LOG = LoggerFactory.getLogger(QueryUtil.class);

	static <T> boolean exists(JPAQuery<T> query){
		return query.limit(1).fetchCount() > 0;
	}

	/**
	 * manualis szures utan eredmeny visszanyerese (lapozas, sort, count)
	 */
	static <T> QueryResultList<T> createQueryResultList(JPAQuery<T> query, EntityPathBase<T> entityPath, long first, long pageSize, String sortField, boolean asc) {
		long count = query.fetchCount();
		JPAQuery<T> modifiedQuery = query.limit(first + pageSize).offset(first);
		if (sortField != null) {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			OrderSpecifier<?> orderSpec = new OrderSpecifier(asc ? Order.ASC : Order.DESC, orderByPath(sortField,
					entityPath));
			modifiedQuery = modifiedQuery.orderBy(orderSpec);
		}
		return new QueryResultList<>(count, modifiedQuery.select(entityPath).fetch());
	}

	/**
	 * manualis szures utan eredmeny visszanyerese (lapozas, count)
	 */
	static <T> QueryResultList<T> createQueryResultList(JPAQuery<T> query, EntityPathBase<T> entityPath, long first, long pageSize) {
		return createQueryResultList(query, entityPath, first, pageSize, null, false);
	}

	/**
	 * szures QuerydslFilter-el (lapozas, sort, count)
	 */
	static <T> JPQLQuery<T> createQuery(JPQLQuery<T> query, QuerydslFilter filter, EntityPathBase<T> entityPath) {
		query = query.from(entityPath).where(filter.predicate(entityPath));
		return query;
	}

	/**
	 * szures QuerydslFilter-el, eredmeny visszanyerese (lapozas, sort, count)
	 */
	static <T> QueryResultList<T> createQueryResultList(JPQLQuery<T> query, QuerydslFilter filter, EntityPathBase<T> entityPath) {
		query = createQuery(query, filter, entityPath);
		return createResultList(query, filter, entityPath);
	}

	/**
	 * szures QuerydslFilter-el, ha nem kell a count (lapozas, sort)
	 */
	static <T> List<T> createList(JPQLQuery<T> query, QuerydslFilter filter, EntityPathBase<T> entityPath) {
		query = createQuery(query, filter, entityPath);
		filter.setNeedCount(false);
		return createResultList(query, filter, entityPath).getList();
	}

	/**
	 * Eredmeny visszanyerese
	 */

	static <T> QueryResultList<T> createResultList(JPQLQuery<T> query, QuerydslFilter filter, EntityPathBase<T> entityPath) {
		Long count = null;
		if (filter.isNeedCount()) {
			count = query.fetchCount();
			if (count == 0) {
				return new QueryResultList<>();
			}
		}
		if (filter.getOffset() != null) {
			query = query.limit(filter.getLimit()).offset(filter.getOffset());
		}
		List<OrderSpecifier<?>> orderList = new ArrayList<>();
		String sort = filter.getSortField() != null ? filter.getSortField() : filter.defaultSortField();
		if (sort != null) {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			OrderSpecifier<Comparable<?>> orderSpec = new OrderSpecifier(filter.getOrder(), orderByPath(sort,
					entityPath));
			orderList.add(orderSpec);
		}
		if (filter.getUniqueOrderSpecifier() != null) {
			orderList.add(filter.getUniqueOrderSpecifier());
		}
		if (!orderList.isEmpty()) {
			query = query.orderBy(orderList.toArray(new OrderSpecifier[orderList.size()]));
		}

		/* TODO FTR5 ha üresen jön vissza a lista, de van count akkor újra kéne
		   kérni, úgy hogy az utolsó elemeket mutassa meg(mert a lapozás ilyenkor
		   az utolsó oldalra ufrik)
		*/
		return new QueryResultList<>(count, query.select(entityPath).fetch());
	}

	static ComparableExpressionBase<?> orderByPath(String sortField, EntityPathBase<?> entityPath) {
		return (ComparableExpressionBase<?>) getSimpleExpression(sortField, entityPath);
	}

	static SimpleExpression getSimpleExpression(String fieldSpec, EntityPathBase<?> basePath) {
		final String[] fieldNames = fieldSpec.split("\\.");
		SimpleExpression currentPath = basePath;
		for (String fieldName : fieldNames) {
			try {
				Field field = currentPath.getClass().getField(fieldName);
				currentPath = (SimpleExpression) field.get(currentPath);
			} catch (Exception e) {
				LOG.error("Error when access entityPath field!", e);
				throw new RuntimeException("Error when access entityPath field!", e);
			}
		}
		return currentPath;
	}

	static <T> JPQLQuery<T> makeCacheble(JPAQuery<T> query) {
		query.setHint("org.hibernate.cacheable", true);
		return query;
	}

	static <T> JPQLQuery<T> createCachedQuery(EntityManager em) {
		JPAQuery<T> query = new JPAQuery<>(em);
		query.setHint("org.hibernate.cacheable", true);
		return query;
	}

	static <T> JPQLQuery<T> createCachedQuery(EntityManager em, String region) {
		HibernateQuery<T> query = new HibernateQuery<>(em.unwrap(Session.class));
		query.setCacheable(true);
		query.setCacheRegion(region);
		return query;
	}

	//saját kiegészítések

    static <T> JPQLQuery<T> createQueryByFilter(JPAQueryFactory queryFactory, EntityPathBase<T> entityPath, QueryDslFilter<T> filter) {
		return queryFactory.selectFrom(entityPath).where(filter.predicate(entityPath));
	}

    static <T> boolean existsByFilter(JPAQueryFactory queryFactory, EntityPathBase<T> entityPath, QueryDslFilter<T> filter) {
		final JPAQuery<Integer> query = queryFactory.selectOne().from(entityPath).where(filter.predicate(entityPath)).limit(1);
		return query.fetchCount() > 0;
	}
}
