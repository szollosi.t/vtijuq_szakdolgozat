package vtijuq_szakdoli.login;

import java.io.Serializable;
import java.util.Base64;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.security.auth.login.LoginException;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.util.cdiconfig.messages.MessageResolver;

@SessionScoped
public class LoginBean implements Serializable {

	private static final Logger LOG = LoggerFactory.getLogger(LoginBean.class);

	@Inject
	private MessageResolver messageResolver;

	@Inject
	private LoginService loginService;

	private LoggedInUser loggedInUser;

	public boolean isLoggedIn(){
		return loggedInUser != null;
	}

	public String getUserName() {
		return isLoggedIn() ? loggedInUser.getUsername() : "";
	}

	public void login(String username, char[] password) throws LoginException {
		LoggedInUser loggedInUser = loginService.login(username,
				Base64.getEncoder().encodeToString(new String(password).getBytes()));

		if(loggedInUser == null){
			this.loggedInUser = null;
			Notification.show(messageResolver.getValue("login.failed"), Type.ERROR_MESSAGE);
			return;
		}

		this.loggedInUser = loggedInUser;
	}

	public void logout() {
		loginService.logout(loggedInUser);
		loggedInUser = null;
	}

	public boolean isAccessible(String funkcio){
		return isLoggedIn() && loginService.isAccessible(loggedInUser, funkcio);
	}

	public boolean isEditable(String funkcio){
		return isLoggedIn() && loginService.isEditable(loggedInUser, funkcio);
	}

//	public Boolean isViewInvalid(String viewType){
//		return isLoggedIn() && loginService.isViewInvalid(loggedInUser, viewType);
//	}
}
