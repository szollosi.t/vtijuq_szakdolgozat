package vtijuq_szakdoli.login;

import javax.inject.Inject;
import javax.security.auth.login.LoginException;

import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.LoginForm;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.ui.AbstractRestrictableUI;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.captions.Caption;
import vtijuq_szakdoli.util.cdiconfig.captions.CaptionResolver;
import vtijuq_szakdoli.view.AbstractRestrictableView;
import vtijuq_szakdoli.view.ReachableFromTheMenu;
import vtijuq_szakdoli.view.manipulator.AbstractManipulatorView;

@CDIView(LoginView.NAME)
public class LoginView extends FormLayout implements View, ReachableFromTheMenu {

	private static final Logger LOG = LoggerFactory.getLogger(LoginView.class);
	public static final String NAME = "Login";

	@Inject
	private LoginBean loginBean;

	@Inject
	private CaptionResolver captionResolver;

	@Inject @Caption
	@ConfigurationValue(value = "button.login")
	private String loginCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.logout")
	private String logoutCaption;

	private AbstractRestrictableView previousView;

	@Override
	public String getMenuTitleCaptionKey() {
		return loginBean.isLoggedIn() ? logoutCaption : loginCaption;
	}

	@Override
	public String getName() {
		return NAME;
	}

	private String resolveCaption(String key) {
		return captionResolver.getValue(key);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		final View oldView = event.getOldView();
		if (oldView instanceof AbstractRestrictableView) {
			previousView = (AbstractRestrictableView)oldView;
		}
		removeAllComponents();
		if(loginBean.isLoggedIn()){
			initContentLogout();
		} else {
			initContentLogin();
		}
	}

	private void initContentLogin() {
		LoginForm loginForm = new LoginForm();
		loginForm.setUsernameCaption(resolveCaption("field.username"));
		loginForm.setPasswordCaption(resolveCaption("field.password"));
		loginForm.setLoginButtonCaption(loginCaption);
		loginForm.addLoginListener(loginEvent -> {
			try {
				loginBean.login(
						loginEvent.getLoginParameter("username"),
						loginEvent.getLoginParameter("password").toCharArray());
				refreshMenuAndNavigate();
			} catch (LoginException e) {
				Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
			}
		});
		addComponent(loginForm);
	}

	private void initContentLogout() {
		addComponent(new Label(getTitle()));
		addComponent(new Button(logoutCaption, (clickEvent) -> {
			loginBean.logout();
			refreshMenuAndNavigate();
		}));
	}

	private void refreshMenuAndNavigate() {
		if (previousView != null && !(previousView instanceof AbstractManipulatorView)
                && previousView.isAccessibleByLoggedInUser()) {
            refreshMenuAndNavigateToPrevious();
        } else {
            refreshMenuAndNavigateToDefault();
        }
	}

	protected String getTitle() {
		final StringBuilder sb = new StringBuilder();
		if (loginBean.isLoggedIn()) {
			sb.append(String.format(" %s: %s", resolveCaption("logged.in.as"), loginBean.getUserName()));
		}
		return sb.toString();
	}

	private void refreshMenuAndNavigateToDefault() {
		AbstractRestrictableUI currentUI = (AbstractRestrictableUI) UI.getCurrent();
		currentUI.refreshMenuBar();
		currentUI.navigateToDefaultView();
	}

	private void refreshMenuAndNavigateToPrevious() {
		AbstractRestrictableUI currentUI = (AbstractRestrictableUI) UI.getCurrent();
		currentUI.refreshMenuBar();
		currentUI.getNavigator().navigateTo(previousView.getName());
	}
}
