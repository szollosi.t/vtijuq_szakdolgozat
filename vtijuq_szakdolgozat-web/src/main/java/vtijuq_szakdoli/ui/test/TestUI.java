package vtijuq_szakdoli.ui.test;

import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.ui.VerticalLayout;

import vtijuq_szakdoli.login.LoginView;
import vtijuq_szakdoli.ui.AbstractRestrictableUI;
import vtijuq_szakdoli.view.karbantarto.test.SzotarTestKarbantartoView;
import vtijuq_szakdoli.view.kereso.test.SzotarTestKeresoView;
import vtijuq_szakdoli.view.test.TestView;

@CDIUI("")
@Theme("valo")
public final class TestUI extends AbstractRestrictableUI {

	@Override
	protected VerticalLayout createContent() {
		return new TestView();
	}

	@Override
	protected void initNavigation() {
		initNavigation(TestView.NAME);
	}

	protected void initMenuBar() {
		addNavigatorMenuItem(TestView.NAME);
		addNavigatorMenuItem(SzotarTestKeresoView.NAME);
		addNavigatorMenuItem(SzotarTestKarbantartoView.NAME);
		addNavigatorMenuItem(LoginView.NAME);
	}

}
