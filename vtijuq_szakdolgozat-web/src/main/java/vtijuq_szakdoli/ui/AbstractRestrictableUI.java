package vtijuq_szakdoli.ui;

import javax.inject.Inject;

import com.vaadin.cdi.CDIViewProvider;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Layout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.layout.AbstractRestrictableLayout;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.captions.CaptionResolver;
import vtijuq_szakdoli.util.cdiconfig.messages.Message;
import vtijuq_szakdoli.util.cdiconfig.messages.MessageResolver;
import vtijuq_szakdoli.view.ReachableFromTheMenu;

public abstract class AbstractRestrictableUI extends UI {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractRestrictableUI.class);

	@Inject
	protected CDIViewProvider viewProvider;

	@Inject
	private CaptionResolver captionResolver;

	@Inject
	private MessageResolver messageResolver;

	@Inject @Message
	@ConfigurationValue(value = "accessDenied")
	private String accessDeniedMessage;

	private MenuBar menu;
	private Layout content;
	private String defaultViewName;

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		//a sorrend fontos!
		// Configure the error handler for the UI
		UI.getCurrent().setErrorHandler(new DefaultErrorHandler() {
			@Override
			public void error(com.vaadin.server.ErrorEvent event) {
				// Find the final cause
				StringBuilder cause = new StringBuilder();
				final Throwable relevant = findRelevantThrowable(event.getThrowable());
				if (relevant != null) {
					cause.append("The click failed because:\n");
					cause.append(relevant.getClass().getName());
					cause.append(": ");
					cause.append(relevant.getMessage());
					cause.append("\n");

					final Throwable rootCause = ExceptionUtils.getRootCause(relevant);
					if (rootCause != null) {
						cause.append("Root cause:\n");
						cause.append(rootCause.getClass().getName());
						cause.append(": ");
						cause.append(rootCause.getMessage());
					}
				} else {
					cause.append(resolveMessage(event.getThrowable().getMessage()));
				}
				Notification.show(cause.toString(), Type.ERROR_MESSAGE);
				// Do the default error handling
				doDefault(event);
			}
		});
		initContent();
		initNavigation();
	}

	private void initContent() {
		menu = new MenuBar();
		initMenuBar();
		content = createContent();

		VerticalLayout layout = new VerticalLayout();
		layout.addComponent(menu);
		layout.addComponent(content);
		layout.setExpandRatio(content, 1);
		setContent(layout);
	}

	protected abstract VerticalLayout createContent();

	public final void refreshMenuBar(){
		menu.removeItems();
		initMenuBar();
	}

	protected abstract void initMenuBar();

	protected abstract void initNavigation();

	protected final void initNavigation(String defaultViewName) {
		this.defaultViewName = defaultViewName;

		Navigator navigator = new Navigator(this, content);
		navigator.addProvider(viewProvider);
		navigator.addViewChangeListener(new ViewChangeListener() {

			@Override
			public boolean beforeViewChange(ViewChangeEvent event) {
				final boolean lathatja = ((AbstractRestrictableUI) UI.getCurrent()).isAccessible(event.getNewView());
				if(!lathatja) {
					Notification.show(accessDeniedMessage, Type.ERROR_MESSAGE);
				}
				return lathatja;
			}

			@Override
			public void afterViewChange(ViewChangeEvent event) {
			}

		});
		navigateToDefaultView();
	}

	protected final void addNavigatorMenuItem(String name) {
		ReachableFromTheMenu reachableView = getReachableView(name);
		if (reachableView == null){
			addNonNavigatorMenuItem(name);
		} else if (isAccessible((View)reachableView)) {
			menu.addItem(resolveCaption(reachableView.getMenuTitleCaptionKey()),
			             (menuItem) -> UI.getCurrent().getNavigator().navigateTo(reachableView.getName()));
		}
	}

	protected final MenuItem addNonNavigatorMenuItem(String foMenuTitle) {
		return menu.addItem(foMenuTitle, null, null);
	}

	private ReachableFromTheMenu getReachableView(String viewName) {
		View view = viewProvider.getView(viewName);
		if (view instanceof ReachableFromTheMenu) {
			return (ReachableFromTheMenu)view;
		}
		LOG.error(String.format("NonReachableView: %s", viewName));
		//Notification.show(accessDeniedMessage, Type.ERROR_MESSAGE);
		return null;
	}

	public final void navigateToDefaultView() {
		getNavigator().navigateTo(defaultViewName);
	}

	protected final boolean isAccessible(String m) {
		return isAccessible(viewProvider.getView(m));
	}

	private boolean isAccessible(View view) {
		if (view instanceof AbstractRestrictableLayout) {
			final AbstractRestrictableLayout pageView = (AbstractRestrictableLayout) view;
			if (!pageView.isAccessibleByLoggedInUser()){
				return false;
			}
		}
		return true;
	}

	protected String resolveCaption(final String key) {
		return captionResolver.getValue(key);
	}

	protected String resolveMessage(final String key) {
		return messageResolver.getValue(key);
	}
}
