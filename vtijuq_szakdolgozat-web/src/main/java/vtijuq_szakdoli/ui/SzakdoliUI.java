package vtijuq_szakdoli.ui;

import static java.util.Arrays.asList;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.login.LoginView;
import vtijuq_szakdoli.view.FooldalView;
import vtijuq_szakdoli.view.ReachableFromTheMenu;
import vtijuq_szakdoli.view.karbantarto.adminisztracio.MunkatarsKarbantartoView;
import vtijuq_szakdoli.view.karbantarto.adminisztracio.SzervezetKarbantartoView;
import vtijuq_szakdoli.view.karbantarto.akkreditacio.AkkreditacioKarbantartoView;
import vtijuq_szakdoli.view.karbantarto.kovetelmeny.IsmeretKarbantartoView;
import vtijuq_szakdoli.view.karbantarto.megvalositas.TantargyKarbantartoView;
import vtijuq_szakdoli.view.kereso.kovetelmeny.SzakKovetelmenyKeresoView;
import vtijuq_szakdoli.view.kereso.kovetelmeny.SzakiranyKovetelmenyKeresoView;
import vtijuq_szakdoli.view.kereso.megvalositas.KonkretSzakKeresoView;
import vtijuq_szakdoli.view.kereso.megvalositas.KonkretSzakiranyKeresoView;

@Theme("valo")
@CDIUI("szakdoli")
public final class SzakdoliUI extends AbstractRestrictableUI {

	private static final Logger LOG = LoggerFactory.getLogger(SzakdoliUI.class);

	private static final Map<String, List<String>> FOMENU_MAP = new LinkedHashMap<String, List<String>>(){{
		put("menutitle.main.kovetelmenyek", asList(
				SzakKovetelmenyKeresoView.NAME,
				SzakiranyKovetelmenyKeresoView.NAME,
				IsmeretKarbantartoView.NAME));
		put("menutitle.main.megvalositasok", asList(
				TantargyKarbantartoView.NAME,
				KonkretSzakKeresoView.NAME,
				KonkretSzakiranyKeresoView.NAME));
		put("menutitle.main.akkreditaciok", asList(
				AkkreditacioKarbantartoView.NAME));
		put("menutitle.main.adminisztracio", asList(
				SzervezetKarbantartoView.NAME,
				MunkatarsKarbantartoView.NAME));
	}};

	@Override
	protected VerticalLayout createContent() {
		return new FooldalView();
	}

	protected void initNavigation() {
		initNavigation(FooldalView.NAME);
	}

	protected void initMenuBar() {
		addNavigatorMenuItem(FooldalView.NAME);
		FOMENU_MAP.keySet().stream()
				.filter(foMenuTitle -> FOMENU_MAP.get(foMenuTitle).stream().anyMatch(this::isAccessible))
				.forEach(this::addMainMenuItem);
		addNavigatorMenuItem(LoginView.NAME);
	}

	private MenuItem addMainMenuItem(String foMenuTitleKey) {
		MenuItem foMenuItem = addNonNavigatorMenuItem(resolveCaption(foMenuTitleKey));
		FOMENU_MAP.get(foMenuTitleKey).stream()
				.filter(this::isAccessible)
				.forEach(viewName ->
			         addSubMenuItem(foMenuItem,
	                        (ReachableFromTheMenu) viewProvider.getView(viewName))
		);
		return foMenuItem;
	}

	private MenuItem addSubMenuItem(MenuItem foMenuItem, ReachableFromTheMenu view) {
		return foMenuItem.addItem(
				resolveCaption(view.getMenuTitleCaptionKey()),
				(menuItem) -> UI.getCurrent().getNavigator().navigateTo(view.getName()));
	}
}
