package vtijuq_szakdoli.utils;

import com.vaadin.data.TreeData;
import com.vaadin.data.provider.TreeDataProvider;

import vtijuq_szakdoli.common.util.graph.Hierarchical;
import vtijuq_szakdoli.common.util.graph.Hierarchy;
import vtijuq_szakdoli.common.util.graph.Tree;

public class DataTransformer {
	private DataTransformer() {
	}

	public static <T extends Hierarchical<? super T>> TreeDataProvider<T> toTreeDataProvider(Tree<T> hierarchy) {
		TreeData<T> treeData = new TreeData<>();
		treeData.addItems(hierarchy.getSovereigns(), hierarchy::getDirectSubordinates);
		return new TreeDataProvider<>(treeData);
	}

	public static <T extends Hierarchical<? super T>> TreeDataProvider<HierarchicalDataWrapper<T>> toHierarchicalDataProvider(Hierarchy<T> hierarchy) {
		HierarchicalData<T> hierarchicalData = new HierarchicalData<>();
		hierarchicalData.addHierarchicalItems(hierarchy.getSovereigns(), hierarchy::getDirectSubordinates);
		return new TreeDataProvider<>(hierarchicalData);
	}
}
