package vtijuq_szakdoli.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;

public class MultiColumnFormLayout extends HorizontalLayout {

    private List<String> formCodes = new ArrayList<>();
    private Map<String, FormLayout> forms = new HashMap<>();

    public FormLayout createFormLayout(String newFormCode) {
        if (formCodes.contains(newFormCode)) return forms.get(newFormCode);
        formCodes.add(newFormCode);

        final FormLayout newFormLayout = new FormLayout();
        forms.put(newFormCode, newFormLayout);
        addComponent(newFormLayout);
        forms.forEach((code, form) -> form.setWidth(100/formCodes.size(), Unit.PERCENTAGE));

        return newFormLayout;
    }

    public FormLayout createFormLayout(String newFormCode, Component... components){
        final FormLayout newFormLayout = createFormLayout(newFormCode);
        Stream.of(components).forEach(newFormLayout::addComponent);
        return newFormLayout;
    }

    public FormLayout get(String formCode) {
        return forms.get(formCode);
    }

    public int indexOf(String formCode) {
        return formCodes.indexOf(formCode);
    }
}
