package vtijuq_szakdoli.utils;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;

public class ComplexFormLayout extends VerticalLayout {

    private Layout keyLayout;
    private MultiColumnFormLayout contentFormLayout;

    public ComplexFormLayout(Layout keyLayout) {
        this.keyLayout = keyLayout;
        contentFormLayout = new MultiColumnFormLayout();

        keyLayout.setWidth(100, PERCENTAGE);
        addComponent(keyLayout);
        addComponent(contentFormLayout);
        setExpandRatio(contentFormLayout, 1);
    }

    public FormLayout createContentFormLayout(String newFormCode) {
        return contentFormLayout.createFormLayout(newFormCode);
    }

    public FormLayout createContentFormLayout(String newFormCode, Component... components){
        return contentFormLayout.createFormLayout(newFormCode, components);
    }
}
