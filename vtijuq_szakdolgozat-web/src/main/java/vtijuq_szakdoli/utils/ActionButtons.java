package vtijuq_szakdoli.utils;

import static vtijuq_szakdoli.utils.ActionButtons.BASE_ACTIONS.CREATE;
import static vtijuq_szakdoli.utils.ActionButtons.BASE_ACTIONS.DELETE;
import static vtijuq_szakdoli.utils.ActionButtons.BASE_ACTIONS.EDIT;
import static vtijuq_szakdoli.utils.ActionButtons.BASE_ACTIONS.OPEN;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.function.Consumer;
import java.util.function.Function;

import com.vaadin.ui.Layout;
import lombok.Getter;
import org.apache.commons.lang3.BooleanUtils;

public class ActionButtons<T> {

    public static <T> ActionButtons<T> getStdActionButtons(boolean isGridEditable, Function<T, Boolean> isFinalCallback, Consumer<T> createCallback, Consumer<T> openCallback, Consumer<T> editCallback, Consumer<T> deleteCallback) {
        final EnumMap<BASE_ACTIONS, Consumer<T>> callbacks =
                getOpenEditDelCallbacks(openCallback, editCallback, deleteCallback);
        callbacks.put(CREATE, createCallback);
        return new ActionButtons<T>().setActions(row -> {
            boolean isFinal = isFinalCallback.apply(row);
            final EnumSet<BASE_ACTIONS> actions = getOpenEditDelActions(isGridEditable, null, isFinal);
            if (isGridEditable && !isFinal) {
                actions.add(CREATE);
            }
            return actions;
        }).setCallbacks(callbacks);
    }

    public static <T> ActionButtons<T> getOpenEditDelActionButtons(boolean isGridEditable, Boolean canDelete, Function<T, Boolean> isFinalCallback, Consumer<T> openCallback, Consumer<T> editCallback, Consumer<T> deleteCallback) {
        return new ActionButtons<T>()
                .setActions(row -> getOpenEditDelActions(isGridEditable, canDelete, isFinalCallback.apply(row)))
                .setCallbacks(getOpenEditDelCallbacks(openCallback, editCallback, deleteCallback));
    }

    public static <T> EnumMap<BASE_ACTIONS, Consumer<T>> getOpenEditDelCallbacks(Consumer<T> openCallback, Consumer<T> editCallback, Consumer<T> deleteCallback) {
        return new EnumMap<BASE_ACTIONS, Consumer<T>>(BASE_ACTIONS.class) {{
            put(OPEN, openCallback);
            put(EDIT, editCallback);
            put(DELETE, deleteCallback);
        }};
    }

    public static EnumSet<BASE_ACTIONS> getOpenEditDelActions(boolean isGridEditable, Boolean canDelete, boolean isFinal) {
        final EnumSet<BASE_ACTIONS> actions = EnumSet.noneOf(BASE_ACTIONS.class);
        if (isGridEditable) {
            actions.add(isFinal ? OPEN : EDIT);
            if (!isFinal && BooleanUtils.isNotFalse(canDelete)) {
                actions.add(DELETE);
            }
        } else {
            actions.add(OPEN);
        }
        return actions;
    }

    public enum BASE_ACTIONS {OPEN, EDIT, DELETE, CREATE}

    @Getter
    private Function<T, EnumSet<BASE_ACTIONS>> actions;
    @Getter
    private EnumMap<BASE_ACTIONS, Consumer<T>> callbacks;

    public ActionButtons<T> setActions(Function<T, EnumSet<BASE_ACTIONS>> actions) {
        this.actions = actions;
        return this;
    }

    public ActionButtons<T> setCallbacks(EnumMap<BASE_ACTIONS, Consumer<T>> callbacks) {
        this.callbacks = callbacks;
        return this;
    }

    public Layout addButtons(Layout layout, Function<String, String> captionResolver, T row) {
        actions.apply(row).forEach(ba -> {
            final Consumer<T> callback = callbacks.get(ba);
            switch (ba) {
                case OPEN:
                    ActionButton.openButton(r -> true, callback).addButton(layout, captionResolver, row);
                    break;
                case EDIT:
                    ActionButton.editButton(r -> true, callback).addButton(layout, captionResolver, row);
                    break;
                case DELETE:
                    ActionButton.deleteButton(r -> true, callback).addButton(layout, captionResolver, row);
                    break;
                case CREATE:
                    ActionButton.createButton(r -> true, callback).addButton(layout, captionResolver, row);
                    break;
            }
        });
        return layout;
    }
}
