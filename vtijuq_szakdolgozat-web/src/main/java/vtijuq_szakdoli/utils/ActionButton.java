package vtijuq_szakdoli.utils;

import java.util.function.Consumer;
import java.util.function.Function;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.Layout;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter @AllArgsConstructor
public class ActionButton <T> {

	private String captionKey;
	private VaadinIcons icon;
	private Function<T, Boolean> conditionCallback;
	private Consumer<T> actionCallback;

	public ActionButton() {
	}

	public Layout addButton(Layout layout, Function<String, String> captionResolver, T row) {
		if (actionCallback != null && conditionCallback.apply(row)) {
			layout.addComponent(createButton(captionResolver, row));
		}
		return layout;
	}

	private Button createButton(Function<String, String> captionResolver, T row) {
		final Button button = new Button();
		final String caption = captionResolver.apply(captionKey);
		button.setIcon(icon, caption);
		button.setDescription(caption);
		button.addClickListener(event -> actionCallback.accept(row));
		return button;
	}

	public static <S> ActionButton<S> openButton(Function<S, Boolean> canOpenCallback, Consumer<S> openCallback) {
		return new ActionButton<>("button.open", VaadinIcons.EYE, canOpenCallback, openCallback);
	}

	public static <S> ActionButton<S> editButton(Function<S, Boolean> canEditCallback, Consumer<S> editCallback) {
		return new ActionButton<>("button.edit", VaadinIcons.EDIT, canEditCallback, editCallback);
	}

	public static <S> ActionButton<S> deleteButton(Function<S, Boolean> canDeleteCallback, Consumer<S> deleteCallback) {
		return new ActionButton<>("button.delete", VaadinIcons.TRASH, canDeleteCallback, deleteCallback);
	}

	public static <S> ActionButton<S> createButton(Function<S, Boolean> canCreateCallback, Consumer<S> createCallback) {
		return new ActionButton<>("button.create", VaadinIcons.ENTER_ARROW, canCreateCallback, createCallback);
	}


}
