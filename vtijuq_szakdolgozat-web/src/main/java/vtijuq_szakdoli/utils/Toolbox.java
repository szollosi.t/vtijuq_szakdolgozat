package vtijuq_szakdoli.utils;

import static javax.enterprise.inject.spi.CDI.current;
import static vtijuq_szakdoli.utils.ActionButtons.BASE_ACTIONS.OPEN;

import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Setter;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.vaadin.addons.stackpanel.StackPanel;
import utils.DateConvertUtils;

import vtijuq_szakdoli.common.dto.DTO;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.util.cdiconfig.captions.CaptionResolver;
import vtijuq_szakdoli.util.cdiconfig.messages.MessageResolver;
import vtijuq_szakdoli.utils.ActionButtons.BASE_ACTIONS;

public class Toolbox {

	private static final String REQ_ERROR_MESSAGE = "error.required";
	public static final String CONSTANT_PREFIX = "constant.";

	private CaptionResolver captionResolver;
	private MessageResolver messageResolver;

	private static Toolbox instance = getInstance();

	private Toolbox(CaptionResolver captionResolver, MessageResolver messageResolver) {
		this.captionResolver = captionResolver;
		this.messageResolver = messageResolver;
	}

	private static Toolbox getInstance(){
		return new Toolbox(
				current().select(CaptionResolver.class).get(),
				current().select(MessageResolver.class).get());
	}

	private static String resolveCaption(String captionKey) {
		return instance.captionResolver.getValue(captionKey);
	}

	private static String resolveMessage(String messageKey) {
		return instance.messageResolver.getValue(messageKey);
	}

	public static CheckBox createReadonlyCheckBox(final Boolean value, final String captionKey) {
		CheckBox checkBox = new CheckBox();
		checkBox.setValue(BooleanUtils.isTrue(value));
		checkBox.setCaption(resolveCaption(captionKey));
		checkBox.setEnabled(false);
		return checkBox;
	}

	public static TextField createReadonlyTextField(String value, String captionKey) {
		final TextField foJellemzoField = new TextField(resolveCaption(captionKey));
		foJellemzoField.setValue(value);
		foJellemzoField.setEnabled(false);
		return foJellemzoField;
	}

	public static <T extends DTO> TextField createAndBindTextField(
			final Binder<T> binder, final String captionKey,
			final ValueProvider<T, String> getter, final Setter<T, String> setter) {
		final TextField textField = new TextField(resolveCaption(captionKey));
		binder.forField(textField)
		      .withNullRepresentation("")
		      .bind(getter, setter);
		return textField;
	}

	public static <T extends DTO> TextField createAndBindRequiredTextField(
			final Binder<T> binder, final String captionKey,
			final ValueProvider<T, String> getter, final Setter<T, String> setter) {
		final TextField textField = new TextField(resolveCaption(captionKey));
		binder.forField(textField)
		      .asRequired(resolveMessage(REQ_ERROR_MESSAGE))
		      .withNullRepresentation(StringUtils.EMPTY)
		      .bind(getter, setter);
		return textField;
	}

	public static <T extends DTO> TextField createAndBindIntegerField(
			final Binder<T> binder, final String captionKey,
			final ValueProvider<T, Integer> getter, final Setter<T, Integer> setter) {
		final TextField textField = new TextField(resolveCaption(captionKey));
		binder.forField(textField)
		      .withNullRepresentation(StringUtils.EMPTY)
		      .withConverter(new StringToIntegerConverter(0, resolveMessage("error.integer")))
		      .bind(getter, setter);
		return textField;
	}

	public static <T extends DTO> TextField createAndBindRequiredIntegerField(
			final Binder<T> binder, final String captionKey,
			final ValueProvider<T, Integer> getter, final Setter<T, Integer> setter) {
		final TextField textField = new TextField(resolveCaption(captionKey));
		binder.forField(textField)
		      .asRequired(resolveMessage(REQ_ERROR_MESSAGE))
		      .withNullRepresentation(StringUtils.EMPTY)
		      .withConverter(new StringToIntegerConverter(0, resolveMessage("error.integer")))
		      .bind(getter, setter);
		return textField;
	}

	public static <S extends EntitasDTO, T extends DTO> ComboBox<S> createAndBindComboBoxWithEmpty(
			final Binder<T> binder, final String captionKey, final Map<Long, S> idDtoMap,
			final ValueProvider<T, Long> getter, final Setter<T, Long> setter) {
		final ComboBox<S> comboBox = createComboBoxWithEmpty(captionKey, idDtoMap);
		binder.forField(comboBox)
		      .withConverter(s -> s == null ? -1L : s.getId(), idDtoMap::get)
		      .bind(getter, setter);
		return comboBox;
	}

	public static <S extends EntitasDTO, T extends DTO> ComboBox<S> createAndBindComboBoxWithEmpty(
			final Binder<T> binder, final String captionKey, final Collection<S> dtoCollection,
			final ValueProvider<T, S> getter, final Setter<T, S> setter) {
		final ComboBox<S> comboBox = createComboBoxWithEmpty(captionKey, dtoCollection);
		binder.forField(comboBox).bind(getter, setter);
		return comboBox;
	}

	public static <S extends Enum<S>, T extends DTO> ComboBox<S> createAndBindComboBoxWithEmpty(
			final Binder<T> binder, final String captionKey, final Class<S> enumClass,
			final ValueProvider<T, S> getter, final Setter<T, S> setter) {
		final ComboBox<S> comboBox = createComboBoxWithEmpty(captionKey, enumClass);
		binder.forField(comboBox).bind(getter, setter);
		return comboBox;
	}

	public static <S extends EntitasDTO, T extends DTO> ComboBox<S> createAndBindRequiredComboBoxWithEmpty(
			final Binder<T> binder, final String captionKey, final Map<Long, S> idDtoMap,
			final ValueProvider<T, Long> getter, final Setter<T, Long> setter) {
		final ComboBox<S> comboBox = createComboBoxWithEmpty(captionKey, idDtoMap);
		binder.forField(comboBox)
		      .asRequired(resolveMessage(REQ_ERROR_MESSAGE))
		      .withConverter(s -> s == null ? -1L : s.getId(), idDtoMap::get)
		      .bind(getter, setter);
		return comboBox;
	}

	public static <S extends EntitasDTO, T extends DTO> ComboBox<S> createAndBindRequiredComboBoxWithEmpty(
			final Binder<T> binder, final String captionKey, final Collection<S> dtoCollection,
			final ValueProvider<T, S> getter, final Setter<T, S> setter) {
		final ComboBox<S> comboBox = createComboBoxWithEmpty(captionKey, dtoCollection);
		binder.forField(comboBox)
		      .asRequired(resolveMessage(REQ_ERROR_MESSAGE))
		      .bind(getter, setter);
		return comboBox;
	}

	public static <S extends Enum<S>, T extends DTO> ComboBox<S> createAndBindRequiredComboBoxWithEmpty(
			final Binder<T> binder, final String captionKey, final Class<S> enumClass,
			final ValueProvider<T, S> getter, final Setter<T, S> setter) {
		final ComboBox<S> comboBox = createComboBoxWithEmpty(captionKey, enumClass);
		binder.forField(comboBox)
		      .asRequired(resolveMessage(REQ_ERROR_MESSAGE))
		      .bind(getter, setter);
		return comboBox;
	}

	private static <T extends Entitas> ComboBox<T> createComboBoxWithEmpty(
			final String captionKey, final Map<Long, T> idDtoMap) {
		return createComboBoxWithEmpty(captionKey, idDtoMap.values());
	}

	private static <T> ComboBox<T> createComboBoxWithEmpty(
			final String captionKey, final Collection<T> dtoCollection) {
		final ComboBox<T> comboBox = new ComboBox<>(resolveCaption(captionKey));
		comboBox.setItems(dtoCollection);
		comboBox.setEmptySelectionAllowed(true);
		comboBox.setEmptySelectionCaption("-");
		return comboBox;
	}

	private static <T extends Enum<T>> ComboBox<T> createComboBoxWithEmpty(
			final String captionKey, final Class<T> enumClass) {
		final ComboBox<T> comboBox = new ComboBox<>(resolveCaption(captionKey));
		comboBox.setItems(EnumSet.allOf(enumClass));
		comboBox.setEmptySelectionAllowed(true);
		comboBox.setEmptySelectionCaption("-");
		comboBox.setItemCaptionGenerator(e -> resolveCaption(enumClass.getSimpleName() + "." + e.name()));
		return comboBox;
	}

	public static <T extends DTO> ComboBox<Boolean> createAndBindBooleanComboBox(
			final Binder<T> binder, final String captionKey,
			final ValueProvider<T, Boolean> getter, final Setter<T, Boolean> setter) {
		final ComboBox<Boolean> comboBox = createBooleanComboBox(captionKey);
		binder.forField(comboBox).bind(getter, setter);
		return comboBox;
	}

	public static <T extends DTO> CheckBox createAndBindCheckBox(
			final Binder<T> binder, final String captionKey,
			final ValueProvider<T, Boolean> getter, final Setter<T, Boolean> setter) {
		final CheckBox checkBox = new CheckBox();
		checkBox.setCaption(resolveCaption(captionKey));
		binder.forField(checkBox).bind(getter, setter);
		return checkBox;
	}

	private static ComboBox<Boolean> createBooleanComboBox(final String captionKey){
		ComboBox<Boolean> comboBox = new ComboBox<>(resolveCaption(captionKey));
		comboBox.setEmptySelectionAllowed(true);
		comboBox.setEmptySelectionCaption("-");
		comboBox.setItemCaptionGenerator(item -> resolveCaption(CONSTANT_PREFIX +item.toString()));
		comboBox.setItems(Boolean.TRUE, Boolean.FALSE);
		return comboBox;
	}

	public static <T> DateField createAndBindDateField(
			Binder<T> binder, String captionKey,
			ValueProvider<T, Date> getter, Setter<T, Date> setter) {
		final DateField dateField = createDateField(captionKey);
		binder.forField(dateField)
		      .withConverter(DateConvertUtils::asUtilDate, DateConvertUtils::asLocalDate)
		      .bind(getter, setter);
		return dateField;
	}

	public static DateField createAndBindRequiredDateField(
			Binder<SzotarTestDTO> binder, String captionKey,
			ValueProvider<SzotarTestDTO, Date> getter, Setter<SzotarTestDTO, Date> setter) {
		final DateField dateField = createDateField(captionKey);
		binder.forField(dateField)
		      .asRequired(resolveMessage(REQ_ERROR_MESSAGE))
		      .withConverter(DateConvertUtils::asUtilDate, DateConvertUtils::asLocalDate)
		      .bind(getter, setter);
		return dateField;
	}

	public static DateField createDateField(String captionKey) {
		final DateField dateField = new DateField(resolveCaption(captionKey));
		dateField.setDateFormat("yyyy.MM.dd.");
		return dateField;
	}

    public static Panel createClosedStackPanel(String captionKey, Component component) {
        final Panel gridPanel = new Panel(resolveCaption(captionKey), component);
        final StackPanel gridTab = StackPanel.extend(gridPanel);
        gridTab.setToggleDownIcon(VaadinIcons.CARET_SQUARE_DOWN_O);
        gridTab.setToggleUpIcon(VaadinIcons.CARET_SQUARE_UP_O);
        gridTab.close();
        return gridPanel;
    }

	public static <T, S> Column<T, S> addColumn(Grid<T> grid, String captionKey, ValueProvider<T, S> getTema) {
		return grid.addColumn(getTema).setCaption(resolveCaption(captionKey));
	}

	public static <T, S extends Component> Column<T, S> addComponentColumn(Grid<T> grid, String captionKey, ValueProvider<T, S> componentProvider) {
		return grid.addComponentColumn(componentProvider).setCaption(resolveCaption(captionKey));
	}

    public static <T> Column<T, HorizontalLayout> addActionsColumn(final Grid<T> grid,
			final Consumer<T> openCallback) {
		return addActionsColumn(grid, new ActionButtons<T>()
				.setActions(row -> EnumSet.of(OPEN))
				.setCallbacks(new EnumMap<BASE_ACTIONS, Consumer<T>>(BASE_ACTIONS.class) {{
					put(OPEN, openCallback);
				}})
		);
	}

    public static <T> Column<T, HorizontalLayout> addActionsColumn(final Grid<T> grid,
			final boolean isGridEditable, final Function<T, Boolean> isFinalCallback,
			final Consumer<T> openCallback, final Consumer<T> editCallback, final Consumer<T> deleteCallback) {
		return addActionsColumn(grid, ActionButtons.getOpenEditDelActionButtons(
				isGridEditable, null, isFinalCallback, openCallback, editCallback, deleteCallback));
	}

    public static <T> Column<T, HorizontalLayout> addActionsColumn(final Grid<T> grid,
			final boolean isGridEditable, final Function<T, Boolean> isFinalCallback,
			final Consumer<T> createCallback, final Consumer<T> openCallback, final Consumer<T> editCallback, final Consumer<T> deleteCallback) {
		return addActionsColumn(grid, ActionButtons.getStdActionButtons(isGridEditable, isFinalCallback, createCallback, openCallback, editCallback, deleteCallback));
	}

	public static <T> Column<T, HorizontalLayout> addActionsColumn(final Grid<T> grid,
			final boolean isGridEditable, final Boolean canDelete, final Function<T, Boolean> isFinalCallback,
			final Consumer<T> openCallback, final Consumer<T> editCallback, final Consumer<T> deleteCallback) {
		return addActionsColumn(grid, ActionButtons.getOpenEditDelActionButtons(
				isGridEditable, canDelete, isFinalCallback, openCallback, editCallback, deleteCallback));
	}

	public static <T> Column<T, HorizontalLayout> addActionsColumn(Grid<T> grid, ActionButtons<T> actionButtons) {
		return addComponentColumn(grid, "", row -> {
			final HorizontalLayout actions = new HorizontalLayout();
			actionButtons.addButtons(actions, Toolbox::resolveCaption, row);
			return actions;
		});
	}

	public static <T> Column<T, HorizontalLayout> addActionsColumn(Grid<T> grid, List<ActionButton<T>> actionButtons) {
		return addComponentColumn(grid, "", row -> {
			final HorizontalLayout actions = new HorizontalLayout();
			actionButtons.forEach(ab -> ab.addButton(actions, Toolbox::resolveCaption, row));
			return actions;
		});
	}
}
