package vtijuq_szakdoli.utils;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import vtijuq_szakdoli.common.util.graph.Hierarchical;

public class HierarchicalDataWrapper<T extends Hierarchical<? super T>> {

	private HierarchicalDataWrapper<T> principalWrapper;
	private T data;

	public HierarchicalDataWrapper(HierarchicalDataWrapper<T> principalWrapper, T data) {
		this.principalWrapper = principalWrapper;
		this.data = data;
	}

	public HierarchicalDataWrapper<T> getPrincipalWrapper() {
		return principalWrapper;
	}

	public T getData() {
		return data;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		HierarchicalDataWrapper<?> that = (HierarchicalDataWrapper<?>) o;

		return new EqualsBuilder()
				.append(getPrincipalWrapper(), that.getPrincipalWrapper())
				.append(getData(), that.getData())
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(getPrincipalWrapper())
				.append(getData())
				.toHashCode();
	}
}
