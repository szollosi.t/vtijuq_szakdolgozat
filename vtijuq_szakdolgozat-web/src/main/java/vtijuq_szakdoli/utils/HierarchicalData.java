package vtijuq_szakdoli.utils;

import java.util.Collection;
import java.util.stream.Collectors;

import com.vaadin.data.TreeData;

import vtijuq_szakdoli.common.util.graph.Hierarchical;
import vtijuq_szakdoli.common.util.graph.SubordinatesProvider;

public class HierarchicalData<T extends Hierarchical<? super T>> extends TreeData<HierarchicalDataWrapper<T>> {

	public TreeData<HierarchicalDataWrapper<T>> addHierarchicalItems(Collection<T> principals, SubordinatesProvider<T> subordinateProvider) {
		return super.addItems(
				principals.stream()
				          .map(p -> new HierarchicalDataWrapper<>(null, p))
				          .collect(Collectors.toSet()),
				p -> subordinateProvider.findSubordinates(p.getData())
				                        .stream().map(s -> new HierarchicalDataWrapper<>(p, s))
				                        .collect(Collectors.toSet()));
	}
}
