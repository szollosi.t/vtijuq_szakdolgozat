package vtijuq_szakdoli.view.test;

import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.*;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.messages.Message;
import vtijuq_szakdoli.view.AbstractPublicView;
import vtijuq_szakdoli.view.karbantarto.test.SzotarTestKarbantartoView;

import javax.inject.Inject;

@CDIView(TestView.NAME)
public class TestView extends AbstractPublicView {

	public static final String NAME = "Test";

	@Inject @Message
	@ConfigurationValue(value = "testGreeting", defaultValue = "defVal")
	private String greeting;

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.test";
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	protected HorizontalLayout createTopButtons() {
		//no buttons at the top
		return null;
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();
		content.addComponent(new Button(
				resolveCaption("menutitle.test.karbantarto"),
				(e) -> getUI().getNavigator().navigateTo(SzotarTestKarbantartoView.NAME)));
		return content;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		super.enter(event);
		Notification.show(greeting);
	}
}
