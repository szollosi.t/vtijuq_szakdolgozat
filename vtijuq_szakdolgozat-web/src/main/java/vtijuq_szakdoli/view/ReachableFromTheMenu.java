package vtijuq_szakdoli.view;

public interface ReachableFromTheMenu {

	String getMenuTitleCaptionKey();

	String getName();
}
