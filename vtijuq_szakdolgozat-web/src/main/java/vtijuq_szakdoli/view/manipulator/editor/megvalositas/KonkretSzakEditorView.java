package vtijuq_szakdoli.view.manipulator.editor.megvalositas;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Layout;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.formcreator.editor.megvalositas.KonkretSzakEditor;
import vtijuq_szakdoli.formcreator.editor.megvalositas.KonkretSzakWithSzakiranyokEditor;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.megvalositas.TantervKonkretSzakiranybolCreatorPopup;
import vtijuq_szakdoli.popup.editor.megvalositas.TantervKonkretSzakiranybolEditorPopup;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;
import vtijuq_szakdoli.service.megvalositas.KonkretService;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.kereso.megvalositas.KonkretSzakKeresoView;
import vtijuq_szakdoli.view.manipulator.editor.AbstractEditorView;

@CDIView(KonkretSzakEditorView.NAME)
public class KonkretSzakEditorView extends AbstractEditorView<KonkretSzakDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(KonkretSzakEditorView.class);

	public static final String NAME = "KonkretSzakSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.KONKRET_SZERKESZTO;

	@Inject @Getter(AccessLevel.PROTECTED)
	private KonkretSzakKeresoView keresoView;

//	@Inject
//	private KonkretSzakiranyCreatorPopup konkretSzakiranyCreatorPopup;

	@Inject
	private KonkretService service;

	@Inject
	private KovetelmenyService kovetelmenyService;
	@Inject
	private AdminisztracioService adminisztracioService;

	private List<SzakKovetelmenyDTO> szakKovetelmenyList;
	private List<SzervezetDTO> szervezetList;

	@Inject
	private TantervKonkretSzakiranybolCreatorPopup tantervKonkretSzakiranybolCreatorPopup;
	@Inject
	private TantervKonkretSzakiranybolEditorPopup tantervKonkretSzakiranybolEditorPopup;

	@Override
	public KonkretSzakWithSzakiranyokEditor getDTOManipulator() {
		return (KonkretSzakWithSzakiranyokEditor) editor;
	}

	@Override
	public boolean hasRestrictedVisibility() {
		return false;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.konkretSzak.editor";
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(tantervKonkretSzakiranybolCreatorPopup, tantervKonkretSzakiranybolEditorPopup/*, konkretSzakiranyCreatorPopup*/).collect(Collectors.toSet());
	}

	@Override
	public KonkretSzakEditor getEditorById(Long id) {
		return new KonkretSzakWithSzakiranyokEditor(
				service.getKonkretSzakWithSzakiranyokById(id),
				dto -> service.saveKonkretSzakWithSzakiranyok(dto),
				getSzakKovetelmenyList(), getSzervezetList());
	}

	private List<SzakKovetelmenyDTO> getSzakKovetelmenyList() {
		if( szakKovetelmenyList == null ) {
			szakKovetelmenyList = kovetelmenyService.listAllSzakKovetelmeny();
		}
		return szakKovetelmenyList;
	}

	private List<SzervezetDTO> getSzervezetList() {
		if( szervezetList == null ) {
			szervezetList = adminisztracioService.listAllSzervezet();
		}
		return szervezetList;
	}

	@Override
	protected void delete() {
		service.delete(editor.getEredeti());
		super.delete();
	}

	@Override
	public boolean isFinal(Long id) {
		return getValidatorService().isKonkretSzakFinal(id);
	}

	@Override
	protected Layout createContent() {
		final Layout contentLayout = editor.createForm(!isEditable());

		addButtonsToAlapSzakiranyLayout();
		addButtonsToNemAlapSzakiranyok();
		return contentLayout;
	}

	private void addButtonsToAlapSzakiranyLayout() {
		Toolbox.addActionsColumn(
				getDTOManipulator().getAlapKonkretSzakiranyEditor().getTantervekGrid(),
				isEditable(),
				dto -> getValidatorService().isTantervFinal(dto.getId()),
				this::openTanterv, this::editTanterv, this::deleteTanterv
        );
		if (isEditable()) {
			getDTOManipulator().getAlapKonkretSzakiranyLayout().addComponent(
					new Button(resolveCaption("button.create"), event -> createTanterv()));
		}
	}

	private void addButtonsToNemAlapSzakiranyok() {
		getDTOManipulator().getKonkretSzakiranyEditorok().forEach(kszie ->
				Toolbox.addActionsColumn(
						kszie.getTantervekGrid(),
                		dto -> tantervKonkretSzakiranybolEditorPopup.show(dto, tantervDTO -> {})
                ));
	}

	private void createTanterv() {
		tantervKonkretSzakiranybolCreatorPopup.show(
				getDTOManipulator().getDTO().getAlapKonkretSzakirany(),
				getDTOManipulator().getDTO().getAlapKonkretSzakirany().getTantervek()::add);
	}

	private void openTanterv(TantervDTO dto) {
		tantervKonkretSzakiranybolEditorPopup.show(dto, tantervDTO -> {});
	}

	private void editTanterv(TantervDTO dto) {
		tantervKonkretSzakiranybolEditorPopup.showForEdit(dto, tantervDTO -> {});
	}

	private void deleteTanterv(TantervDTO dto) {
		getDTOManipulator().getDTO().getAlapKonkretSzakirany().getTantervek().remove(dto);
		refresh();
	}
}
