package vtijuq_szakdoli.view.manipulator.editor.kovetelmeny;

import static vtijuq_szakdoli.utils.ActionButtons.BASE_ACTIONS.CREATE;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Layout;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyWithSzakmaiJellemzokDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.formcreator.editor.kovetelmeny.SzakiranyKovetelmenyEditor;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.kovetelmeny.SzakmaiJellemzoCreatorPopup;
import vtijuq_szakdoli.popup.editor.kovetelmeny.SzakmaiJellemzoEditorPopup;
import vtijuq_szakdoli.service.kovetelmeny.IsmeretService;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;
import vtijuq_szakdoli.utils.ActionButtons;
import vtijuq_szakdoli.utils.HierarchicalDataWrapper;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.kereso.kovetelmeny.SzakiranyKovetelmenyKeresoView;
import vtijuq_szakdoli.view.manipulator.editor.AbstractEditorView;

@CDIView(SzakiranyKovetelmenyEditorView.NAME)
public class SzakiranyKovetelmenyEditorView extends AbstractEditorView<SzakiranyKovetelmenyDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakiranyKovetelmenyEditorView.class);

	public static final String NAME = "SzakiranyKovetelmenySzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.KOVETELMENY_SZERKESZTO;

	@Inject @Getter(AccessLevel.PROTECTED)
	private SzakiranyKovetelmenyKeresoView keresoView;

	@Inject
	private SzakmaiJellemzoEditorPopup szakmaiJellemzoEditorPopup;

	@Inject
	private SzakmaiJellemzoCreatorPopup szakmaiJellemzoCreatorPopup;

	@Inject
	private KovetelmenyService service;
	@Inject
	private IsmeretService ismeretService;

	private List<SzakKovetelmenyDTO> szakKovetelmenyList;
	private List<SzakiranyDTO> szakiranyList;

    @Override
    public SzakiranyKovetelmenyEditor getDTOManipulator() {
        return (SzakiranyKovetelmenyEditor)editor;
    }

    @Override
	public boolean hasRestrictedVisibility() {
		return false;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.szakiranyKovetelmeny.editor";
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(szakmaiJellemzoCreatorPopup, szakmaiJellemzoEditorPopup).collect(Collectors.toSet());
	}

	@Override
	public SzakiranyKovetelmenyEditor getEditorById(Long id) {
		return new SzakiranyKovetelmenyEditor(
				service.getSzakiranyKovetelmenyWithSzakmaiJellemzokById(id),
				dto -> service.saveWithSzakmaiJellemzok((SzakiranyKovetelmenyWithSzakmaiJellemzokDTO) dto),
				getSzakKovetelmenyList(), getSzakiranyList());
	}

	@Override
	protected void delete() {
		service.delete(getDTOManipulator().getEredeti());
		super.delete();
	}

	@Override
	public boolean isFinal(Long id) {
		return getValidatorService().isSzakiranyKovetelmenyFinal(id);
	}

	private List<SzakKovetelmenyDTO> getSzakKovetelmenyList() {
		if(szakKovetelmenyList == null ){
			szakKovetelmenyList = service.listAllSzakKovetelmeny();
		}
		return szakKovetelmenyList;
	}

	private List<SzakiranyDTO> getSzakiranyList() {
		if(szakiranyList == null ){
			szakiranyList = service.listAllSzakirany();
		}
		return szakiranyList;
	}

	@Override
	protected Layout createContent() {
		final Layout contentLayout = editor.createForm(!isEditable());
		addButtonsToSzakmaiJellemzokTreeGrid(contentLayout);
		return contentLayout;
	}

	private void addButtonsToSzakmaiJellemzokTreeGrid(Layout contentLayout) {
		final boolean isGridEditable = isEditable();
		final EnumMap<ActionButtons.BASE_ACTIONS, Consumer<HierarchicalDataWrapper<SzakmaiJellemzoDTO>>> callbacks =
				ActionButtons.getOpenEditDelCallbacks(
						row -> openSzakmaiJellemzo(row.getData()),
						row -> editSzakmaiJellemzo(row.getData()),
						row -> deleteSzakmaiJellemzo(row.getData()));
		callbacks.put(CREATE, row -> createSzakmaiJellemzo(row.getData()));

		boolean isReadOnly = !isEditable();
		final ActionButtons<HierarchicalDataWrapper<SzakmaiJellemzoDTO>> actionButtons = new ActionButtons<HierarchicalDataWrapper<SzakmaiJellemzoDTO>>()
				.setActions(row -> {
					final EnumSet<ActionButtons.BASE_ACTIONS> actions = ActionButtons.getOpenEditDelActions(isGridEditable, null, isReadOnly);
					if (isGridEditable && !isReadOnly && ismeretService.hasAlismeretek(row.getData().getIsmeret().getId())) {
						actions.add(CREATE);
					}
					return actions;
				}).setCallbacks(callbacks);

		Toolbox.addActionsColumn(
				getDTOManipulator().getSzakmaiJellemzoTreeGrid(),
                actionButtons);

		if (isEditable()) {
			contentLayout.addComponent(new Button(resolveCaption(
					"button.create.fojellemzo"), event -> createSzakmaiJellemzo()));
		}
	}

	private void createSzakmaiJellemzo() {
		szakmaiJellemzoCreatorPopup.show(createdDTO -> {
			final SzakiranyKovetelmenyWithSzakmaiJellemzokDTO szkWszjDTO = getDTOManipulator().getDTO();
			createdDTO.setIdSzakiranyKovetelmeny(szkWszjDTO.getId());
			szkWszjDTO.getSzakmaiJellemzok().addNode(createdDTO);
		});
	}

	private void createSzakmaiJellemzo(SzakmaiJellemzoDTO dto) {
		szakmaiJellemzoCreatorPopup.show(createdDTO -> {
			final SzakiranyKovetelmenyWithSzakmaiJellemzokDTO szkWszjDTO = getDTOManipulator().getDTO();
			createdDTO.setIdSzakiranyKovetelmeny(szkWszjDTO.getId());
			szkWszjDTO.getSzakmaiJellemzok().addNode(dto, createdDTO);
		});
	}

	private void openSzakmaiJellemzo(SzakmaiJellemzoDTO dto) {
		szakmaiJellemzoEditorPopup.show(dto, szakmaiJellemzoDTO -> {});
	}

	private void editSzakmaiJellemzo(SzakmaiJellemzoDTO dto) {
		szakmaiJellemzoEditorPopup.showForEdit(dto, szakmaiJellemzoDTO -> {});
	}

	private void deleteSzakmaiJellemzo(SzakmaiJellemzoDTO dto) {
		getDTOManipulator().getDTO().getSzakmaiJellemzok().removeSubHierarchy(dto);
		refresh();
	}
}
