package vtijuq_szakdoli.view.manipulator.editor;

import java.util.Objects;

import javax.inject.Inject;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import lombok.AccessLevel;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.layout.Editor;
import vtijuq_szakdoli.service.ValidatorService;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.captions.Caption;
import vtijuq_szakdoli.view.karbantarto.AbstractKarbantartoView;
import vtijuq_szakdoli.view.manipulator.AbstractManipulatorView;

public abstract class AbstractEditorView<T extends EditableDTO> extends AbstractManipulatorView<T> implements Editor<T> {

	@Inject @Caption
	@ConfigurationValue(value = "button.delete")
	protected String deleteCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.cancel")
	protected String cancelCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.back")
	protected String backCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.edit")
	protected String editCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.save")
	protected String saveCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.validate")
	protected String validateCaption;

	@Inject @Getter(AccessLevel.PROTECTED)
	private ValidatorService validatorService;

	private Long id;
	protected EditorFormCreator<T> editor;
	protected boolean editable;
	protected boolean alwaysReadOnly;

	@Override
	public DTOManipulator<T> getDTOManipulator() {
		return editor;
	}

	protected boolean isEditable() {
		return id != null && editable && isEditableByLoggedInUser() && !isFinal(id);
	}

	final public void init(Long id){
		//a sorrend fontos
		alwaysReadOnly = isFinal(id);
		if (!Objects.equals(this.id, id)){
			this.id = id;
			editable = false;
			editor = getEditorById(id);
		}
		init();
	}

	protected boolean isFinal(Long id) {
		return false;
	}

	protected abstract EditorFormCreator<T> getEditorById(Long id);

	@Override
	protected HorizontalLayout createBottomButtons() {
		final HorizontalLayout buttons = new HorizontalLayout();
		if (isEditable()) {
			addEditableButtons(buttons);
		} else {
			addReadOnlyButtons(buttons);
		}
		return buttons;
	}

	private void addReadOnlyButtons(final HorizontalLayout buttons) {
		addButton(buttons, Alignment.BOTTOM_LEFT, backCaption, e -> cancel());
		if (!alwaysReadOnly && isEditableByLoggedInUser()) {
			addButton(buttons, Alignment.BOTTOM_RIGHT, editCaption, e -> changeToEditable());
		}
	}

	protected void addEditableButtons(final HorizontalLayout buttons) {
		if (getKeresoView() instanceof AbstractKarbantartoView ? getKeresoView().isEditableByLoggedInUser() : isEditableByLoggedInUser()) {
			addButton(buttons, Alignment.BOTTOM_LEFT, deleteCaption, e -> { delete(); cancel(); });
		}
		addButton(buttons, Alignment.BOTTOM_LEFT, cancelCaption, e -> cancel());
		if(renderValidateButton()) {
			addButton(buttons, Alignment.BOTTOM_CENTER, validateCaption, e -> validate());
		}
		addButton(buttons, Alignment.BOTTOM_RIGHT, saveCaption, e -> edit());
	}

	private void changeToEditable() {
		editable = true;
		refresh();
	}

	private void changeToReadOnly() {
		editable = false;
		refresh();
	}

	@Override
	public void edit() {
		//explicit az extra elenőrzés nélküli validációt hívjuk
		//TODO ha ErvenyesEntitas, akkor itt a meglévő valid flag alapján extra validáció végzendő
		final boolean valid = validate(this::resolveMessage);
		if (valid) {
			editor.edit();
			changeToReadOnly();
		}
	}

	protected void delete(){
		super.cancel();
	}

	@Override
	protected void cancel() {
		if (isEditable()) {
			editor = getEditorById(id);
			changeToReadOnly();
		} else {
			super.cancel();
		}
	}

	protected Layout createForm() {
		return editor.createForm(!isEditable());
	}

	@Override
	protected boolean validate() {
		return editor.validateForm() && super.validate();
	}
}
