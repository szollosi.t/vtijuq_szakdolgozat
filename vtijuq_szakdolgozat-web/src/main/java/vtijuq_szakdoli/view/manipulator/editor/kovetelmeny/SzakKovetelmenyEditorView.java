package vtijuq_szakdoli.view.manipulator.editor.kovetelmeny;

import static vtijuq_szakdoli.utils.ActionButtons.BASE_ACTIONS.CREATE;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyWithSzakmaiJellemzokDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.formcreator.editor.kovetelmeny.SzakKovetelmenyEditor;
import vtijuq_szakdoli.formcreator.editor.kovetelmeny.SzakKovetelmenyWithSzakiranyokEditor;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.kovetelmeny.SzakmaiJellemzoCreatorPopup;
import vtijuq_szakdoli.popup.editor.kovetelmeny.SzakmaiJellemzoEditorPopup;
import vtijuq_szakdoli.service.kovetelmeny.IsmeretService;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;
import vtijuq_szakdoli.utils.ActionButtons;
import vtijuq_szakdoli.utils.HierarchicalDataWrapper;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.kereso.kovetelmeny.SzakKovetelmenyKeresoView;
import vtijuq_szakdoli.view.manipulator.editor.AbstractEditorView;

@CDIView(SzakKovetelmenyEditorView.NAME)
public class SzakKovetelmenyEditorView extends AbstractEditorView<SzakKovetelmenyDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakKovetelmenyEditorView.class);

	public static final String NAME = "SzakKovetelmenySzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.KOVETELMENY_SZERKESZTO;

	@Inject @Getter(AccessLevel.PROTECTED)
	private SzakKovetelmenyKeresoView keresoView;

	@Inject
	private KovetelmenyService service;

	@Inject
	private IsmeretService ismeretService;

	private List<SzakDTO> szakList;

	@Inject
	private SzakmaiJellemzoEditorPopup szakmaiJellemzoEditorPopup;

	@Inject
	private SzakmaiJellemzoCreatorPopup szakmaiJellemzoCreatorPopup;

	@Override
	public SzakKovetelmenyWithSzakiranyokEditor getDTOManipulator() {
		return (SzakKovetelmenyWithSzakiranyokEditor) editor;
	}

	@Override
	public boolean hasRestrictedVisibility() {
		return false;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.szakKovetelmeny.editor";
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(szakmaiJellemzoCreatorPopup, szakmaiJellemzoEditorPopup).collect(Collectors.toSet());
	}

	@Override
	public SzakKovetelmenyEditor getEditorById(Long id) {
		return new SzakKovetelmenyWithSzakiranyokEditor(
				service.getSzakKovetelmenyWithSzakiranyokById(id),
				dto -> service.saveSzakKovetelmenyWithSzakiranyok(dto),
				getSzakList());
	}

	private List<SzakDTO> getSzakList() {
		if (szakList == null) {
			szakList = service.listAllSzak();
		}
		return szakList;
	}

	@Override
	protected void delete() {
		service.delete(editor.getEredeti());
		super.delete();
	}

	@Override
	public boolean isFinal(Long id) {
		return getValidatorService().isSzakKovetelmenyFinal(id);
	}

	@Override
	protected void addEditableButtons(HorizontalLayout buttons) {
		if (isEditable()) {
			addButton(buttons, Alignment.BOTTOM_LEFT, deleteCaption, e -> { delete(); cancel(); });
		}
		addButton(buttons, Alignment.BOTTOM_LEFT, cancelCaption, e -> cancel());
		if(isEditable()) {
			addButton(buttons, Alignment.BOTTOM_CENTER, validateCaption, e -> validate());
			addButton(buttons, Alignment.BOTTOM_CENTER, resolveCaption("button.finalise"), e -> finalise());
			addButton(buttons, Alignment.BOTTOM_RIGHT, saveCaption, e -> edit());
		}
	}

	private void finalise() {
		final Set<String> validationErrors = getValidatorService().finaliseSzakKovetelmeny(editor.getDTO());
		if(!validationErrors.isEmpty()){
			final String errorMessage = validationErrors.stream()
					.map(this::resolveMessage)
					.collect(Collectors.joining("\n"));
			Notification.show(errorMessage, Type.ERROR_MESSAGE);
		}
	}

	@Override
	public boolean renderValidateButton() {
		return true;
	}

	@Override
	public boolean validate() {
		return validate(this::resolveMessage, dto -> getValidatorService().validateSzakKovetelmeny(dto));
	}

	@Override
	protected Layout createContent() {
		final Layout contentLayout = editor.createForm(!isEditable());

		addButtonsToAlapSzakiranyLayout();
		addButtonsToNemAlapSzakiranyok();
		return contentLayout;
	}

	private void addButtonsToAlapSzakiranyLayout() {
		final boolean isGridEditable = isEditable();
		final EnumMap<ActionButtons.BASE_ACTIONS, Consumer<HierarchicalDataWrapper<SzakmaiJellemzoDTO>>> callbacks =
				ActionButtons.getOpenEditDelCallbacks(
						row -> openSzakmaiJellemzo(row.getData()),
						row -> editSzakmaiJellemzo(row.getData()),
						row -> deleteSzakmaiJellemzo(row.getData()));
		callbacks.put(CREATE, row -> createSzakmaiJellemzo(row.getData()));

		boolean isReadOnly = !isEditable();
		final ActionButtons<HierarchicalDataWrapper<SzakmaiJellemzoDTO>> actionButtons = new ActionButtons<HierarchicalDataWrapper<SzakmaiJellemzoDTO>>()
				.setActions(row -> {
					final EnumSet<ActionButtons.BASE_ACTIONS> actions = ActionButtons.getOpenEditDelActions(isGridEditable, null, isReadOnly);
					if (isGridEditable && !isReadOnly && ismeretService.hasAlismeretek(row.getData().getIsmeret().getId())) {
						actions.add(CREATE);
					}
					return actions;
				}).setCallbacks(callbacks);

		Toolbox.addActionsColumn(
				getDTOManipulator().getAlapSzakiranyKovetelmenyEditor().getSzakmaiJellemzoTreeGrid(),
                actionButtons);

		if (isEditable()) {
			getDTOManipulator().getAlapSzakiranyKovetelmenyLayout().addComponent(
					new Button(resolveCaption("button.create.fojellemzo"),
							event -> createSzakmaiJellemzo()));
		}
	}

	private void addButtonsToNemAlapSzakiranyok() {
		getDTOManipulator().getSzakiranyKovetelmenyEditorok().forEach(szike -> {
			Toolbox.addActionsColumn(
					szike.getSzakmaiJellemzoTreeGrid(),
					row -> openSzakmaiJellemzo(row.getData())
            );
		});
	}

	private void createSzakmaiJellemzo() {
		szakmaiJellemzoCreatorPopup.show(createdDTO -> {
			final SzakiranyKovetelmenyWithSzakmaiJellemzokDTO szkWszjDTO = getDTOManipulator().getDTO().getAlapSzakiranyKovetelmeny();
			createdDTO.setIdSzakiranyKovetelmeny(szkWszjDTO.getId());
			szkWszjDTO.getSzakmaiJellemzok().addNode(createdDTO);
		});
	}

	private void createSzakmaiJellemzo(SzakmaiJellemzoDTO dto) {
		szakmaiJellemzoCreatorPopup.show(createdDTO -> {
			final SzakiranyKovetelmenyWithSzakmaiJellemzokDTO szkWszjDTO = getDTOManipulator().getDTO().getAlapSzakiranyKovetelmeny();
			createdDTO.setIdSzakiranyKovetelmeny(szkWszjDTO.getId());
			szkWszjDTO.getSzakmaiJellemzok().addNode(dto, createdDTO);
		});
	}

	private void openSzakmaiJellemzo(SzakmaiJellemzoDTO dto) {
		szakmaiJellemzoEditorPopup.show(dto, szakmaiJellemzoDTO -> {});
	}

	private void editSzakmaiJellemzo(SzakmaiJellemzoDTO dto) {
		szakmaiJellemzoEditorPopup.showForEdit(dto, szakmaiJellemzoDTO -> {});
	}

	private void deleteSzakmaiJellemzo(SzakmaiJellemzoDTO dto) {
		getDTOManipulator().getDTO().getAlapSzakiranyKovetelmeny().getSzakmaiJellemzok().removeSubHierarchy(dto);
		refresh();
	}
}
