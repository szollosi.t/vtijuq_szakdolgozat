package vtijuq_szakdoli.view.manipulator.editor.akkreditacio;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.formcreator.editor.akkreditacio.AkkreditacioEditor;
import vtijuq_szakdoli.service.akkreditacio.AkkreditacioService;
import vtijuq_szakdoli.service.megvalositas.KonkretService;
import vtijuq_szakdoli.view.karbantarto.akkreditacio.AkkreditacioKarbantartoView;
import vtijuq_szakdoli.view.manipulator.editor.AbstractEditorView;

@CDIView(AkkreditacioEditorView.NAME)
public class AkkreditacioEditorView extends AbstractEditorView<AkkreditacioDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(AkkreditacioEditorView.class);

	public static final String NAME = "AkkreditacioSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.AKKREDITACIO_KARBANTARTO;

	@Inject @Getter(AccessLevel.PROTECTED)
	private AkkreditacioKarbantartoView keresoView;

	@Inject
	private AkkreditacioService service;

	@Inject
	private KonkretService konkretService;

	private List<KonkretSzakiranyDTO> konkretSzakiranyList;

	@Override
	public AkkreditacioEditor getDTOManipulator() {
		return (AkkreditacioEditor) editor;
	}

	@Override
	public boolean hasRestrictedVisibility() {
		return false;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	public AkkreditacioEditor getEditorById(Long id) {
		return new AkkreditacioEditor(service.getAkkreditacioEditableById(id), service::save, getKonkretSzakiranyList());
	}

	@Override
	protected void delete() {
		service.delete(editor.getEredeti());
		cancel();
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.akkreditacio.editor";
	}

	@Override
	public boolean isFinal(Long id) {
		return getValidatorService().isAkkreditacioFinal(id);
	}

	@Override
	public boolean renderValidateButton() {
		return getKeresoView().isEditableByLoggedInUser();
	}

	@Override
	public boolean validate() {
		return validate(this::resolveMessage, dto -> getValidatorService().validateAkkreditacio(dto));
	}

	@Override
	protected void addEditableButtons(HorizontalLayout buttons) {
		if (getKeresoView().isEditableByLoggedInUser() && !isFinal(editor.getDTO().getId())) {
			addButton(buttons, Alignment.BOTTOM_LEFT, deleteCaption, e1 -> { delete(); cancel(); });
		}
		addButton(buttons, Alignment.BOTTOM_LEFT, cancelCaption, e1 -> cancel());
		if(isEditable()) {
			addButton(buttons, Alignment.BOTTOM_CENTER, validateCaption, e1 -> validate());
			addButton(buttons, Alignment.BOTTOM_CENTER, resolveCaption("button.finalise"), e -> finalise());

			addButton(buttons, Alignment.BOTTOM_RIGHT, saveCaption, e1 -> edit());
		}
	}

	private void finalise() {
		final Set<String> validationErrors = getValidatorService().finaliseAkkreditacio(editor.getDTO());
		if(!validationErrors.isEmpty()){
			final String errorMessage = validationErrors.stream()
					.map(this::resolveMessage)
					.collect(Collectors.joining("\n"));
			Notification.show(errorMessage, Type.ERROR_MESSAGE);
		}
	}

	@Override
	protected Layout createContent() {
		final Layout layout = editor.createForm(!isEditable());
		//TODO
		return layout;
	}

	private List<KonkretSzakiranyDTO> getKonkretSzakiranyList() {
		if (konkretSzakiranyList == null) {
			konkretSzakiranyList = konkretService.listAllKonkretSzakirany();
		}
		return konkretSzakiranyList;
	}
}
