package vtijuq_szakdoli.view.manipulator.editor.adminisztracio;

import java.util.List;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Layout;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.formcreator.editor.adminisztracio.MunkatarsEditor;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;
import vtijuq_szakdoli.view.karbantarto.adminisztracio.MunkatarsKarbantartoView;
import vtijuq_szakdoli.view.manipulator.editor.AbstractEditorView;

@CDIView(MunkatarsEditorView.NAME)
public class MunkatarsEditorView extends AbstractEditorView<MunkatarsDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(MunkatarsEditorView.class);

	public static final String NAME = "MunkatarsSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.MUNKATARS_SZERKESZTO;

	@Inject @Getter(AccessLevel.PROTECTED)
	private MunkatarsKarbantartoView keresoView;

	@Inject
	private AdminisztracioService service;

	private List<SzervezetDTO> szervezetList;
	private List<SzerepkorDTO> szerepkorList;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	public MunkatarsEditor getEditorById(Long id) {
		return new MunkatarsEditor(service.getMunkatarsById(id), service::save, getSzervezetList(), getSzerepkorList());
	}

	@Override
	protected void delete() {
		service.inactivate(editor.getEredeti());
		super.delete();
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.munkatars.editor";
	}

	@Override
	protected Layout createContent() {
		final Layout layout = editor.createForm(!isEditable());
		//TODO
		return layout;
	}

	public List<SzervezetDTO> getSzervezetList() {
		if (szervezetList == null) {
			szervezetList = service.listAllSzervezet();
		}
		return szervezetList;
	}

	public List<SzerepkorDTO> getSzerepkorList() {
		if (szerepkorList == null) {
			szerepkorList = service.listAllSzerepkor();
		}
		return szerepkorList;
	}
}
