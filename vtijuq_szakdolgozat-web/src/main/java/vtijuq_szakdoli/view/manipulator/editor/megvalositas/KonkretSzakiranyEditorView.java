package vtijuq_szakdoli.view.manipulator.editor.megvalositas;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Layout;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.formcreator.editor.megvalositas.KonkretSzakiranyEditor;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.megvalositas.TantervKonkretSzakiranybolCreatorPopup;
import vtijuq_szakdoli.popup.editor.megvalositas.TantervKonkretSzakiranybolEditorPopup;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;
import vtijuq_szakdoli.service.megvalositas.KonkretService;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.kereso.megvalositas.KonkretSzakiranyKeresoView;
import vtijuq_szakdoli.view.manipulator.editor.AbstractEditorView;

@CDIView(KonkretSzakiranyEditorView.NAME)
public class KonkretSzakiranyEditorView extends AbstractEditorView<KonkretSzakiranyDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(KonkretSzakiranyEditorView.class);

	public static final String NAME = "KonkretSzakiranySzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.KONKRET_SZERKESZTO;

	@Inject @Getter(AccessLevel.PROTECTED)
	private KonkretSzakiranyKeresoView keresoView;

	@Inject
	private TantervKonkretSzakiranybolCreatorPopup tantervKonkretSzakiranybolCreatorPopup;

	@Inject
	private TantervKonkretSzakiranybolEditorPopup tantervKonkretSzakiranybolEditorPopup;

	@Inject
	private KonkretService service;

	@Inject
	private KovetelmenyService kovetelmenyService;

	private List<KonkretSzakDTO> konkretSzakList;
	private List<SzakiranyKovetelmenyDTO> szakiranyKovetelmenyList;

	@Override
	public KonkretSzakiranyEditor getDTOManipulator() {
		return (KonkretSzakiranyEditor) editor;
	}

	@Override
	public boolean hasRestrictedVisibility() {
		return false;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.konkretSzakirany.editor";
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(tantervKonkretSzakiranybolCreatorPopup, tantervKonkretSzakiranybolEditorPopup).collect(Collectors.toSet());
	}

	@Override
	public KonkretSzakiranyEditor getEditorById(Long id) {
		return new KonkretSzakiranyEditor(service.getKonkretSzakiranyWithTantervekById(id), service::save, getKonkretSzakList(), getSzakiranyKovetelmenyList());
	}

	@Override
	protected void delete() {
		service.delete(editor.getEredeti());
		super.delete();
	}

	@Override
	public boolean isFinal(Long id) {
		return getValidatorService().isKonkretSzakiranyFinal(id);
	}

	private List<KonkretSzakDTO> getKonkretSzakList() {
		if (konkretSzakList == null) {
			konkretSzakList = service.listAllKonkretSzak();
		}
		return konkretSzakList;
	}

	private List<SzakiranyKovetelmenyDTO> getSzakiranyKovetelmenyList() {
		if (szakiranyKovetelmenyList == null) {
			szakiranyKovetelmenyList = kovetelmenyService.listAllSzakiranyKovetelmeny();
		}
		return szakiranyKovetelmenyList;
	}

	@Override
	protected Layout createContent() {
		final Layout contentLayout = editor.createForm(!isEditable());

		addButtonsToTantervekGrid(contentLayout);
		return contentLayout;
	}

	private void addButtonsToTantervekGrid(Layout contentLayout) {
		Toolbox.addActionsColumn(
				getDTOManipulator().getTantervekGrid(),
				isEditableByLoggedInUser(),
				row -> getValidatorService().isTantervFinal(row.getId()),
				this::openTanterv, this::editTanterv, this::deleteTanterv
        );
		if (isEditable()) {
			contentLayout.addComponent(new Button(resolveCaption("button.create"), event ->
					tantervKonkretSzakiranybolCreatorPopup.show(
							getDTOManipulator().getDTO(),
							getDTOManipulator().getDTO().getTantervek()::add)
			));
		}
	}

	private void openTanterv(TantervDTO dto) {
		tantervKonkretSzakiranybolEditorPopup.show(dto, tantervDTO -> {});
	}

	private void editTanterv(TantervDTO dto) {
		tantervKonkretSzakiranybolEditorPopup.showForEdit(dto, tantervDTO -> {});
	}

	private void deleteTanterv(TantervDTO dto) {
		getDTOManipulator().getDTO().getTantervek().remove(dto);
		refresh();
	}
}
