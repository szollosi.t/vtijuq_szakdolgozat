package vtijuq_szakdoli.view.manipulator.editor.adminisztracio;

import java.util.List;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Layout;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.formcreator.editor.adminisztracio.SzervezetEditor;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;
import vtijuq_szakdoli.view.karbantarto.adminisztracio.SzervezetKarbantartoView;
import vtijuq_szakdoli.view.manipulator.editor.AbstractEditorView;

@CDIView(SzervezetEditorView.NAME)
public class SzervezetEditorView extends AbstractEditorView<SzervezetDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzervezetEditorView.class);

	public static final String NAME = "SzervezetSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.SZERVEZET_SZERKESZTO;

	@Inject @Getter(AccessLevel.PROTECTED)
	private SzervezetKarbantartoView keresoView;

	@Inject
	private AdminisztracioService service;

	private List<SzervezetTipusDTO> szervezetTipusList;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	public SzervezetEditor getEditorById(Long id) {
		return new SzervezetEditor(service.getSzervezetById(id), service::save, getSzervezetTipusList());
	}

	@Override
	protected void delete() {
		service.delete(editor.getEredeti());
		super.delete();
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.szervezet.editor";
	}

	@Override
	protected Layout createContent() {
		final Layout layout = editor.createForm(!isEditable());
		//TODO
		return layout;
	}

	private List<SzervezetTipusDTO> getSzervezetTipusList() {
		if (szervezetTipusList == null) {
			szervezetTipusList = service.listAllSzervezetTipus();
		}
		return szervezetTipusList;
	}
}
