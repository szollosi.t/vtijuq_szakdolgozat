package vtijuq_szakdoli.view.manipulator.editor.megvalositas;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Layout;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyWithTantervekAndTematikakDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.formcreator.editor.megvalositas.TantargyEditor;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.megvalositas.TantervTantargybolCreatorPopup;
import vtijuq_szakdoli.popup.creator.megvalositas.TematikaCreatorPopup;
import vtijuq_szakdoli.popup.editor.megvalositas.TantervTantargybolEditorPopup;
import vtijuq_szakdoli.popup.editor.megvalositas.TematikaEditorPopup;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;
import vtijuq_szakdoli.service.megvalositas.TantargyService;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.karbantarto.megvalositas.TantargyKarbantartoView;
import vtijuq_szakdoli.view.manipulator.editor.AbstractEditorView;

@CDIView(TantargyEditorView.NAME)
public class TantargyEditorView extends AbstractEditorView<TantargyDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(TantargyEditorView.class);

	public static final String NAME = "TantargySzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.TANTARGY_SZERKESZTO;

	@Inject @Getter(AccessLevel.PROTECTED)
	private TantargyKarbantartoView keresoView;

	@Inject
	private TematikaCreatorPopup tematikaCreatorPopup;

	@Inject
	private TematikaEditorPopup tematikaEditorPopup;

	@Inject
	private TantervTantargybolCreatorPopup tantervTantargybolCreatorPopup;

	@Inject
	private TantervTantargybolEditorPopup tantervTantargybolEditorPopup;

	@Inject
	private TantargyService service;

	@Inject
	private AdminisztracioService adminisztracioService;

	private List<SzervezetDTO> szervezetList;

	@Override
	public TantargyEditor getDTOManipulator() {
		return (TantargyEditor) editor;
	}

	@Override
	public boolean hasRestrictedVisibility() {
		return false;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.tantargy.editor";
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(
				tematikaCreatorPopup, tematikaEditorPopup,
				tantervTantargybolCreatorPopup, tantervTantargybolEditorPopup
		).collect(Collectors.toSet());
	}

	@Override
	public TantargyEditor getEditorById(Long id) {
		return new TantargyEditor(service.getTantargyWithTantervekAndTematikakById(id),
				dto -> service.saveWithTantervekAndTematikak((TantargyWithTantervekAndTematikakDTO)dto),
				getSzervezetList());
	}

    @Override
	protected void delete() {
		service.delete(getDTOManipulator().getEredeti());
		super.delete();
	}

    @Override
    public boolean isFinal(Long id) {
        return getValidatorService().isTantargyFinal(id);
    }

    private List<SzervezetDTO> getSzervezetList() {
		if (szervezetList == null ) {
			szervezetList = adminisztracioService.listAllSzervezet();
		}
		return szervezetList;
	}

	@Override
	protected Layout createContent() {
		final Layout contentLayout = getDTOManipulator().createForm(!isEditable());
		if (getDTOManipulator().getDTO().getSzervezet() != null) {
			addButtonsToTantervekGrid(contentLayout);
		}

		addButtonsToTematikakGrid(contentLayout);
		//TODO még nem létező témájú tematika hozzáadása -> TematikaWithTemaCreatorPopup
		return contentLayout;
	}

	private void addButtonsToTantervekGrid(Layout contentLayout) {
		Toolbox.addActionsColumn(getDTOManipulator().getTantervekGrid(), isEditable(),
				dto1 -> getValidatorService().isTantervFinal(dto1.getId()),
				this::openTanterv, this::editTanterv, this::deleteTanterv
        );
		if (isEditable()) {
            contentLayout.addComponent(new Button(resolveCaption("button.create.tanterv"), event ->
                    tantervTantargybolCreatorPopup.show(
                    		getDTOManipulator().getDTO(),
							dto -> getDTOManipulator().getDTO().getTantervek().add(dto))));
        }
	}

	private void addButtonsToTematikakGrid(Layout contentLayout) {
		Toolbox.addActionsColumn(getDTOManipulator().getTematikakGrid(), isEditable(),
				row -> getValidatorService().isTematikaFinal(row.getId()),
				this::openTematika, this::editTematika, this::deleteTematika
        );
		if (isEditable()) {
			contentLayout.addComponent(new Button(resolveCaption("button.create.tematika"), event ->
					tematikaCreatorPopup.show(
							dto -> {
								dto.setTantargy(getDTOManipulator().getDTO());
								getDTOManipulator().getDTO().getTematikak().add(dto);
							})));
		}
	}

	private void openTanterv(TantervDTO dto) {
		tantervTantargybolEditorPopup.show(dto, tantervDTO -> {} );
	}

	private void editTanterv(TantervDTO dto) {
		tantervTantargybolEditorPopup.showForEdit(dto, tantervDTO -> {} );
	}

	private void deleteTanterv(TantervDTO dto) {
		getDTOManipulator().getDTO().getTantervek().remove(dto);
		refresh();
	}

	private void openTematika(TematikaDTO dto) {
		tematikaEditorPopup.show(dto, tematikaDTO -> {} );
	}

	private void editTematika(TematikaDTO dto) {
		tematikaEditorPopup.showForEdit(dto, tematikaDTO -> {} );
	}

	private void deleteTematika(TematikaDTO dto) {
		getDTOManipulator().getDTO().getTematikak().remove(dto);
        refresh();
	}
}
