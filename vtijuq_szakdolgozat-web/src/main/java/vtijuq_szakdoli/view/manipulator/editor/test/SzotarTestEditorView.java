package vtijuq_szakdoli.view.manipulator.editor.test;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.formcreator.editor.test.SzotarTestEditor;
import vtijuq_szakdoli.service.test.SzotarTestService;
import vtijuq_szakdoli.view.karbantarto.test.SzotarTestKarbantartoView;
import vtijuq_szakdoli.view.manipulator.editor.AbstractEditorView;

@CDIView(SzotarTestEditorView.NAME)
public class SzotarTestEditorView extends AbstractEditorView<SzotarTestDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzotarTestEditorView.class);

	public static final String NAME = "SzotarTestSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.TEST;

	@Inject @Getter(AccessLevel.PROTECTED)
	private SzotarTestKarbantartoView keresoView;

	@Inject
	private SzotarTestService service;

	@Override
	public boolean hasRestrictedVisibility() {
		return false;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	public SzotarTestEditor getEditorById(Long id) {
		return new SzotarTestEditor(service.getSzotarTestEditableById(id), service::save);
	}

	@Override
	protected void delete() {
		service.delete(editor.getEredeti());
		super.delete();
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.test.editor";
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();
		content.addComponent(createForm());
		return content;
	}

}
