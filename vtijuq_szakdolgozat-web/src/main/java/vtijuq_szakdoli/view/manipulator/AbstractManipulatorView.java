package vtijuq_szakdoli.view.manipulator;

import com.vaadin.ui.HorizontalLayout;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.layout.Manipulator;
import vtijuq_szakdoli.view.AbstractRestrictableView;
import vtijuq_szakdoli.view.kereso.AbstractKeresoView;

public abstract class AbstractManipulatorView<T extends EditableDTO> extends AbstractRestrictableView implements Manipulator<T> {

	protected abstract AbstractKeresoView getKeresoView();

	@Override
	protected HorizontalLayout createTopButtons() {
		return null;
	}

	protected void cancel(){
		navigateTo(getKeresoView().getName());
	}

	protected boolean validate(){
		return validate(this::resolveMessage);
	}

}
