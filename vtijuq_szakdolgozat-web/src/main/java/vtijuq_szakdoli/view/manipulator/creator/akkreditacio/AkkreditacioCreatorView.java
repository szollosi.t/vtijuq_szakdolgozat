package vtijuq_szakdoli.view.manipulator.creator.akkreditacio;

import java.util.List;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.formcreator.creator.akkreditacio.AkkreditacioCreator;
import vtijuq_szakdoli.service.akkreditacio.AkkreditacioService;
import vtijuq_szakdoli.service.megvalositas.KonkretService;
import vtijuq_szakdoli.view.karbantarto.akkreditacio.AkkreditacioKarbantartoView;
import vtijuq_szakdoli.view.manipulator.creator.AbstractCreatorView;
import vtijuq_szakdoli.view.manipulator.editor.akkreditacio.AkkreditacioEditorView;

@CDIView(AkkreditacioCreatorView.NAME)
public class AkkreditacioCreatorView extends AbstractCreatorView<AkkreditacioDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(AkkreditacioCreatorView.class);

	public static final String NAME = "AkkreditacioFelvitel";

	@Inject @Getter(AccessLevel.PROTECTED)
	private AkkreditacioEditorView editorView;

	@Inject @Getter(AccessLevel.PROTECTED)
	private AkkreditacioKarbantartoView keresoView;

	@Inject
	private AkkreditacioService service;

	@Inject
	private KonkretService konkretService;

	private List<KonkretSzakiranyDTO> konkretSzakiranyList;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	protected AkkreditacioCreator createCreator() {
		return new AkkreditacioCreator(createAndNavigateToCreated(service::save), getKonkretSzakiranyList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.akkreditacio.creator";
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();
		content.addComponent(createForm());
		return content;
	}

	public List<KonkretSzakiranyDTO> getKonkretSzakiranyList() {
		if (konkretSzakiranyList == null ) {
			konkretSzakiranyList = konkretService.listAllKonkretSzakirany();
		}
		return konkretSzakiranyList;
	}
}
