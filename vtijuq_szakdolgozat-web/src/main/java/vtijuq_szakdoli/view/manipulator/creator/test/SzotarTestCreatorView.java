package vtijuq_szakdoli.view.manipulator.creator.test;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.formcreator.creator.test.SzotarTestCreator;
import vtijuq_szakdoli.service.test.SzotarTestService;
import vtijuq_szakdoli.view.karbantarto.test.SzotarTestKarbantartoView;
import vtijuq_szakdoli.view.manipulator.creator.AbstractCreatorView;
import vtijuq_szakdoli.view.manipulator.editor.test.SzotarTestEditorView;

@CDIView(SzotarTestCreatorView.NAME)
public class SzotarTestCreatorView extends AbstractCreatorView<SzotarTestDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzotarTestCreatorView.class);

	public static final String NAME = "SzotarTestFelvitel";

	@Inject @Getter(AccessLevel.PROTECTED)
	private SzotarTestEditorView editorView;

	@Inject @Getter(AccessLevel.PROTECTED)
	private SzotarTestKarbantartoView keresoView;

	@Inject
	private SzotarTestService service;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	protected SzotarTestCreator createCreator() {
		return new SzotarTestCreator(createAndNavigateToCreated(service::save));
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.test.creator";
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();
		content.addComponent(createForm());
		return content;
	}
}
