package vtijuq_szakdoli.view.manipulator.creator.adminisztracio;

import java.util.List;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.formcreator.creator.adminisztracio.SzervezetCreator;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;
import vtijuq_szakdoli.view.karbantarto.adminisztracio.SzervezetKarbantartoView;
import vtijuq_szakdoli.view.manipulator.creator.AbstractCreatorView;
import vtijuq_szakdoli.view.manipulator.editor.adminisztracio.SzervezetEditorView;

@CDIView(SzervezetCreatorView.NAME)
public class SzervezetCreatorView extends AbstractCreatorView<SzervezetDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzervezetCreatorView.class);

	public static final String NAME = "SzervezetFelvitel";

	@Inject @Getter(AccessLevel.PROTECTED)
	private SzervezetEditorView editorView;

	@Inject @Getter(AccessLevel.PROTECTED)
	private SzervezetKarbantartoView keresoView;

	@Inject
	private AdminisztracioService service;

	private List<SzervezetTipusDTO> szervezetTipusList;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	protected SzervezetCreator createCreator() {
		return new SzervezetCreator(createAndNavigateToCreated(service::save), getSzervezetTipusList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.szervezet.creator";
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();
		content.addComponent(createForm());
		return content;
	}

	public List<SzervezetTipusDTO> getSzervezetTipusList() {
		if (szervezetTipusList == null ) {
			szervezetTipusList = service.listAllSzervezetTipus();
		}
		return szervezetTipusList;
	}
}
