package vtijuq_szakdoli.view.manipulator.creator;

import java.util.function.Consumer;
import java.util.function.Function;

import javax.inject.Inject;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.layout.Creator;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.captions.Caption;
import vtijuq_szakdoli.view.karbantarto.AbstractKarbantartoView;
import vtijuq_szakdoli.view.kereso.AbstractKeresoView;
import vtijuq_szakdoli.view.manipulator.AbstractManipulatorView;
import vtijuq_szakdoli.view.manipulator.editor.AbstractEditorView;

public abstract class AbstractCreatorView<T extends EditableDTO> extends AbstractManipulatorView<T> implements Creator<T> {

	@Inject @Caption
	@ConfigurationValue(value = "button.cancel")
	private String cancelCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.validate")
	private String validateCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.create")
	private String createCaption;

	protected CreatorFormCreator<T> creator;

	@Override
	public String getFunkcioNev() {
		if (getKeresoView() instanceof AbstractKarbantartoView) {
			return ((AbstractKarbantartoView)getKeresoView()).getFunkcioNev();
		} else {
			return getEditorView().getFunkcioNev();
		}
	}

	@Override
	public void enter(ViewChangeEvent event) {
		//a sorrend fontos
		creator = createCreator();
		super.enter(event);
	}

	protected abstract AbstractEditorView<T> getEditorView();

	protected abstract AbstractKeresoView getKeresoView();

	protected abstract CreatorFormCreator<T> createCreator();

	protected Consumer<T> createAndNavigateToCreated(Function<T, T> creatorFunction){
		return dto -> {
			final T created = creatorFunction.apply(dto);
			navigateTo(getEditorView().getName(), created.getId());
		};
	}

	@Override
	public DTOManipulator<T> getDTOManipulator() {
		return creator;
	}

	private void create(){
		final boolean valid = validate();
		if (valid) {
			creator.create();
		}
	}

	@Override
	protected HorizontalLayout createBottomButtons() {
		final HorizontalLayout buttons = new HorizontalLayout();
		addButton(buttons, Alignment.BOTTOM_LEFT, cancelCaption, e -> cancel());
		if(renderValidateButton()) {
			addButton(buttons, Alignment.BOTTOM_CENTER, validateCaption, e -> validate());
		}
		addButton(buttons, Alignment.BOTTOM_RIGHT, createCaption, e -> create());
		return buttons;
	}

	protected Layout createForm() {
		return creator.createForm(false);
	}

	@Override
	protected boolean validate() {
		return creator.validateForm() && super.validate();
	}

}
