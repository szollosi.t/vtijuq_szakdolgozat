package vtijuq_szakdoli.view.manipulator.creator.adminisztracio;

import java.util.List;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.formcreator.creator.adminisztracio.MunkatarsCreator;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;
import vtijuq_szakdoli.view.karbantarto.adminisztracio.MunkatarsKarbantartoView;
import vtijuq_szakdoli.view.manipulator.creator.AbstractCreatorView;
import vtijuq_szakdoli.view.manipulator.editor.adminisztracio.MunkatarsEditorView;

@CDIView(MunkatarsCreatorView.NAME)
public class MunkatarsCreatorView extends AbstractCreatorView<MunkatarsDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(MunkatarsCreatorView.class);

	public static final String NAME = "MunkatarsFelvitel";

	@Inject @Getter(AccessLevel.PROTECTED)
	private MunkatarsEditorView editorView;

	@Inject @Getter(AccessLevel.PROTECTED)
	private MunkatarsKarbantartoView keresoView;

	@Inject
	private AdminisztracioService service;

	private List<SzervezetDTO> szervezetList;
	private List<SzerepkorDTO> szerepkorList;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	protected MunkatarsCreator createCreator() {
		return new MunkatarsCreator(createAndNavigateToCreated(service::save), getSzervezetList(), getSzerepkorList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.munkatars.creator";
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();
		content.addComponent(createForm());
		return content;
	}

	public List<SzervezetDTO> getSzervezetList() {
		if (szervezetList == null ) {
			szervezetList = service.listAllSzervezet();
		}
		return szervezetList;
	}

	public List<SzerepkorDTO> getSzerepkorList() {
		if (szerepkorList == null ) {
			szerepkorList = service.listAllSzerepkor();
		}
		return szerepkorList;
	}
}
