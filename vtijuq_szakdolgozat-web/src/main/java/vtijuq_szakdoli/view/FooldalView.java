package vtijuq_szakdoli.view;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CDIView(FooldalView.NAME)
public class FooldalView extends AbstractPublicView {

	private static final Logger LOG = LoggerFactory.getLogger(FooldalView.class);

	public static final String NAME = "fooldal";

	@Override
	public String getMenuTitleCaptionKey() {
		return resolveCaption("menutitle.fooldal");
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	protected HorizontalLayout createTopButtons() {
		//no buttons at the top
		return null;
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();
		content.addComponent(new Label("Üdvözöljük oldalunkon!"));
		if(getLoginBean().isLoggedIn()) {
			content.addComponent(new Label(getLoginBean().getUserName()));
		}
		return content;
	}
}
