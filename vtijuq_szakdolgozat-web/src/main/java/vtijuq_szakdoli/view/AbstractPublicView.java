package vtijuq_szakdoli.view;

import com.vaadin.ui.HorizontalLayout;

public abstract class AbstractPublicView extends AbstractRestrictableView implements ReachableFromTheMenu {

	@Override
	protected HorizontalLayout createBottomButtons() {
		//at standard no buttons at the bottom
		return null;
	}

	@Override
	public String getTitleCaptionKey(){
		return getMenuTitleCaptionKey();
	}
}
