package vtijuq_szakdoli.view.karbantarto.adminisztracio;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TreeGrid;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.common.dto.filterDTO.adminisztracio.SzervezetFilterDTO;
import vtijuq_szakdoli.formcreator.filter.adminisztracio.SzervezetFilter;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;
import vtijuq_szakdoli.utils.DataTransformer;
import vtijuq_szakdoli.utils.HierarchicalDataWrapper;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.karbantarto.AbstractKarbantartoView;
import vtijuq_szakdoli.view.manipulator.creator.adminisztracio.SzervezetCreatorView;
import vtijuq_szakdoli.view.manipulator.editor.adminisztracio.SzervezetEditorView;

@CDIView(SzervezetKarbantartoView.NAME)
public class SzervezetKarbantartoView extends AbstractKarbantartoView<SzervezetDTO, SzervezetFilterDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzervezetKarbantartoView.class);

	public static final String NAME = FunctionNames.SZERVEZET_KARBANTARTO;

	@Inject
	private AdminisztracioService service;

	private TreeDataProvider<HierarchicalDataWrapper<SzervezetDTO>> dataProvider;

	private Map<Long, SzervezetTipusDTO> szervezetTipusMap;

	@Getter(AccessLevel.PROTECTED)
	private SzervezetFilter filter;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.szervezet.karbantarto";
	}

	@Override
	protected void create() {
		navigateTo(SzervezetCreatorView.NAME);
	}

	@Override
	protected void edit(SzervezetDTO dto) {
		navigateTo(SzervezetEditorView.NAME, dto.getId());
	}

	@Override
	protected void delete(SzervezetDTO dto) {
		service.delete(dto);
		refresh();
	}

	@Override
	protected void search() {
		dataProvider = filter.hasConditions()
				? DataTransformer.toHierarchicalDataProvider(service.getEditableHierarchyBelowFiltered(filter.getDTO()))
				: DataTransformer.toHierarchicalDataProvider(service.getWholeSzervezetTreeEditable());
	}

	@Override
	protected void init() {
		if (dataProvider == null) {
			dataProvider = DataTransformer.toHierarchicalDataProvider(service.getWholeSzervezetTreeEditable());
		}
		if (szervezetTipusMap == null) {
			szervezetTipusMap = service.listAllSzervezetTipus().stream()
			                           .collect(Collectors.toMap(SzervezetTipusDTO::getId, Function.identity()));
		}
		if (filter == null) {
			filter = new SzervezetFilter(szervezetTipusMap);
		}
		super.init();
	}

	@Override
	protected Grid<HierarchicalDataWrapper<SzervezetDTO>> createGrid() {
		TreeGrid<HierarchicalDataWrapper<SzervezetDTO>> treeGrid = new TreeGrid<>();

		Toolbox.addColumn(treeGrid, "field.organizationType", wrapper -> wrapper.getData().getSzervezetTipus());
		Toolbox.addColumn(treeGrid, "field.code", wrapper -> wrapper.getData().getKod());
		Toolbox.addColumn(treeGrid, "field.name", wrapper -> wrapper.getData().getNev());
		Toolbox.addColumn(treeGrid, "field.rovidites", wrapper -> wrapper.getData().getRovidites());
		Toolbox.addColumn(treeGrid, "field.cim", wrapper -> wrapper.getData().getCim());

		Toolbox.addActionsColumn(treeGrid,
				getLoginBean().isEditable(FunctionNames.SZERVEZET_SZERKESZTO),
				isEditableByLoggedInUser(),
				row -> false,
				row -> open(row.getData()),
				row -> edit(row.getData()),
				row -> delete(row.getData())
        );
		treeGrid.setDataProvider(dataProvider);
		treeGrid.setWidth(100, PERCENTAGE);
		return treeGrid;
	}
}
