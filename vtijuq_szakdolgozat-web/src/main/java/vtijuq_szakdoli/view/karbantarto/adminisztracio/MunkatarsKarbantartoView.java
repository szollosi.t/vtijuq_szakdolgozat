package vtijuq_szakdoli.view.karbantarto.adminisztracio;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Grid;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.filterDTO.adminisztracio.MunkatarsFilterDTO;
import vtijuq_szakdoli.formcreator.filter.adminisztracio.MunkatarsFilter;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;
import vtijuq_szakdoli.utils.ActionButton;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.karbantarto.AbstractKarbantartoView;
import vtijuq_szakdoli.view.manipulator.creator.adminisztracio.MunkatarsCreatorView;
import vtijuq_szakdoli.view.manipulator.editor.adminisztracio.MunkatarsEditorView;

@CDIView(MunkatarsKarbantartoView.NAME)
public class MunkatarsKarbantartoView extends AbstractKarbantartoView<MunkatarsDTO, MunkatarsFilterDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(MunkatarsKarbantartoView.class);

	public static final String NAME = FunctionNames.MUNKATARS_KARBANTARTO;

	@Inject
	private AdminisztracioService service;

	private ListDataProvider<MunkatarsDTO> dataProvider;

	private Map<Long, SzervezetDTO> szervezetMap;
	private Map<Long, SzerepkorDTO> szerepkorMap;

	@Getter(AccessLevel.PROTECTED)
	private MunkatarsFilter filter;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.munkatars.karbantarto";
	}

	@Override
	protected void create() {
		navigateTo(MunkatarsCreatorView.NAME);
	}

	@Override
	protected void edit(MunkatarsDTO dto) {
		navigateTo(MunkatarsEditorView.NAME, dto.getId());
	}

	@Override
	protected void delete(MunkatarsDTO dto) {
		service.inactivate(dto);
		refresh();
	}

	@Override
	protected void search() {
		dataProvider = filter.hasConditions()
				? DataProvider.ofCollection(service.listAllMunkatarsByFilterEditable(filter.getDTO()))
				: DataProvider.ofCollection(service.listAllMunkatarsEditable());
		refresh();
	}

	@Override
	protected void init() {
		if (dataProvider == null) {
			dataProvider = DataProvider.ofCollection(service.listAllMunkatarsEditable());
		}
		if (szervezetMap == null) {
			szervezetMap = service.listAllSzervezet().stream()
			                      .collect(Collectors.toMap(SzervezetDTO::getId, Function.identity()));
		}
		if (szerepkorMap == null) {
			szerepkorMap = service.listAllSzerepkor().stream()
			                      .collect(Collectors.toMap(SzerepkorDTO::getId, Function.identity()));
		}
		if (filter == null) {
			filter = new MunkatarsFilter(szervezetMap, szerepkorMap);
		}
		super.init();
	}

	@Override
	protected Grid<MunkatarsDTO> createGrid() {
		Grid<MunkatarsDTO> grid = new Grid<>();

		Toolbox.addColumn(grid, "field.username", MunkatarsDTO::getFelhasznalonev);
		Toolbox.addColumn(grid, "field.organization", MunkatarsDTO::getSzervezet);
		Toolbox.addColumn(grid, "field.role", MunkatarsDTO::getSzerepkor);
		Toolbox.addComponentColumn(grid, "field.active", dto -> Toolbox.createReadonlyCheckBox(
				dto.isAktiv(), StringUtils.EMPTY));

		;
		final boolean editable = getLoginBean().isEditable(FunctionNames.MUNKATARS_SZERKESZTO);
		final boolean canDelete = isEditableByLoggedInUser();
		Toolbox.addActionsColumn(grid, Arrays.asList(
				ActionButton.openButton(row -> !editable, this::open),
				ActionButton.editButton(row -> editable, this::edit),
				ActionButton.deleteButton(row -> canDelete, this::delete)
		));

		grid.setDataProvider(dataProvider);
		grid.setWidth(100, PERCENTAGE);
		return grid;
	}
}
