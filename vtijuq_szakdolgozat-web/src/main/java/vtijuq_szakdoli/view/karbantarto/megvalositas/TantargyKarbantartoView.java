package vtijuq_szakdoli.view.karbantarto.megvalositas;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Grid;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.TantargyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.megvalositas.TantargyFilter;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.megvalositas.TantargyCreatorPopup;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;
import vtijuq_szakdoli.service.megvalositas.TantargyService;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.karbantarto.AbstractKarbantartoView;
import vtijuq_szakdoli.view.manipulator.editor.megvalositas.TantargyEditorView;

@CDIView(TantargyKarbantartoView.NAME)
public class TantargyKarbantartoView extends AbstractKarbantartoView<TantargyDTO, TantargyFilterDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(TantargyKarbantartoView.class);

	public static final String NAME = "TantargyKarbantarto";

	@Inject
	private TantargyCreatorPopup creatorPopup;

	@Inject
	private TantargyService service;

	@Inject
	private AdminisztracioService adminisztracioService;

	private ListDataProvider<TantargyDTO> dataProvider;

	private Map<Long, SzervezetDTO> szervezetMap;

	@Getter(AccessLevel.PROTECTED)
	private TantargyFilter filter;

	@Override
	public boolean hasRestrictedVisibility() {
		return false;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.tantargy.karbantarto";
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(creatorPopup).collect(Collectors.toSet());
	}

	@Override
	protected void create() {
		creatorPopup.show(service::save);
	}

	@Override
	protected void edit(TantargyDTO dto) {
		navigateTo(TantargyEditorView.NAME, dto.getId());
	}

	@Override
	protected void delete(TantargyDTO dto) {
		service.delete(dto);
		refresh();
	}

	@Override
	protected void search() {
		dataProvider = filter.hasConditions()
				? DataProvider.ofCollection(service.listTantargyEditableByFilter(filter.getDTO()))
				: DataProvider.ofCollection(service.listAllTantargyEditable());
	}

	@Override
	protected void init() {
		if (dataProvider == null) {
			dataProvider = DataProvider.ofCollection(service.listAllTantargyEditable());
		}
		if (szervezetMap == null) {
			szervezetMap = adminisztracioService.listAllSzervezet().stream()
			                      .collect(Collectors.toMap(SzervezetDTO::getId, Function.identity()));
		}
		if (filter == null) {
			filter = new TantargyFilter(szervezetMap);
		}
		super.init();
	}

	@Override
	protected Grid<TantargyDTO> createGrid() {
		Grid<TantargyDTO> grid = new Grid<>();

		Toolbox.addColumn(grid, "field.code", TantargyDTO::getKod);
		Toolbox.addColumn(grid, "field.name", TantargyDTO::getNev);
		Toolbox.addColumn(grid, "field.organization", TantargyDTO::getSzervezet);
		Toolbox.addActionsColumn(grid, isEditableByLoggedInUser(),
				row -> getValidatorService().isTantargyFinal(row.getId()),
				this::open, this::edit, this::delete
        );

		grid.setDataProvider(dataProvider);
		grid.setWidth(100, PERCENTAGE);
		return grid;
	}
}
