package vtijuq_szakdoli.view.karbantarto.test;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Tree;
import com.vaadin.ui.TreeGrid;
import com.vaadin.ui.VerticalLayout;
import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.dto.filterDTO.test.SzotarTestFilterDTO;
import vtijuq_szakdoli.common.util.graph.Dag;
import vtijuq_szakdoli.common.util.graph.Hierarchy;
import vtijuq_szakdoli.formcreator.filter.test.SzotarTestFilter;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.test.SzotarTestCreatorPopup;
import vtijuq_szakdoli.popup.editor.test.SzotarTestEditorPopup;
import vtijuq_szakdoli.service.test.SzotarTestService;
import vtijuq_szakdoli.utils.DataTransformer;
import vtijuq_szakdoli.utils.HierarchicalDataWrapper;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.karbantarto.AbstractKarbantartoView;

@CDIView(SzotarTestKarbantartoView.NAME)
public class SzotarTestKarbantartoView extends AbstractKarbantartoView<SzotarTestDTO, SzotarTestFilterDTO> {

	public static final String NAME = "SzotarTestKarbantarto";
	private static final String FUNKCIO_NEV = FunctionNames.TEST;

	@Inject
	private SzotarTestCreatorPopup creatorPopup;

	@Inject
	private SzotarTestEditorPopup editorPopup;

	@Inject
	private SzotarTestEditorPopup showPopup;

	@Inject
	private SzotarTestService service;

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.test.karbantarto";
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected void init() {
		showPopup.init(true);
		super.init();
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(creatorPopup, editorPopup, showPopup).collect(Collectors.toSet());
	}

	private void create(SzotarTestDTO row) {
		creatorPopup.show(dto -> service.save(row, dto));
	}

	@Override
	protected void create() {
		creatorPopup.show(dto -> service.save(dto));
	}

	@Override
	protected void edit(SzotarTestDTO dto) {
		editorPopup.show(dto, editedDTO -> service.save(editedDTO));
	}

	@Override
	protected void delete(SzotarTestDTO dto) {
		service.delete(dto);
		refresh();
	}

	@Override
	protected HorizontalLayout createTopButtons() {
		final HorizontalLayout buttons = new HorizontalLayout();
		if (isEditableByLoggedInUser()) {
			addButton(buttons, Alignment.TOP_RIGHT, resolveCaption("button.create"), e -> create());
		}
		return buttons;
	}

	@Override
	protected SzotarTestFilter getFilter() {
		//itt szándékosan nincs implementálva
		return null;
	}

	@Override
	protected void search() {
		//itt szándékosan nincs implementálva
	}

	@Override
	protected void emptySearch() {
		//itt szándékosan nincs implementálva
	}

	@Override
	protected Layout createSearchForm() {
		//szándékosan nincs implementálva
		//TODO helyette a táblázaton belüli szűrést kéne prototipizálni ebben a view-ban
		return null;
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();

		Accordion accordion = new Accordion();

		accordion.addTab(createGridTab(), "Grid");
		accordion.addTab(createViewGridTab(), "View Grid");
		accordion.addTab(createTreeGridTab(), "TreeGrid");
		accordion.addTab(createHierarchicalGridTab(), "HierarchicalGrid");

		//TODO warning Panel-ben a Grid cellák tartalma nem jelenik meg, ezért csak a fás reprezentációt lehet stackPanel-be tenni.
		VerticalLayout noGridSheet = new VerticalLayout();
		final Panel treePanel = Toolbox.createClosedStackPanel("Tree", createTreeTab());
		noGridSheet.addComponent(treePanel);
		final Panel hierarchicalPanel = Toolbox.createClosedStackPanel("Hierarchical", createHierarchicalTab());
		noGridSheet.addComponent(hierarchicalPanel);
		accordion.addTab(noGridSheet, "No Grid");

		content.addComponentsAndExpand(accordion);
		return content;
	}

	@Override
	protected Grid createGrid() {
		//itt szándékosan nincs implementálva
		return null;
	}

	private Component createGridTab() {
		Grid<SzotarTestDTO> grid = new Grid<>();

		Toolbox.addColumn(grid, "field.code", SzotarTestDTO::getKod);
		Toolbox.addColumn(grid, "field.name", SzotarTestDTO::getNev);
		Toolbox.addColumn(grid, "field.description", SzotarTestDTO::getLeiras);
		Toolbox.addColumn(grid, "field.valStart", SzotarTestDTO::getErvKezdet);
		Toolbox.addColumn(grid, "field.valEnd", SzotarTestDTO::getErvVeg);
		Toolbox.addComponentColumn(grid, "field.technical",
				dto -> Toolbox.createReadonlyCheckBox(dto.getTechnikai(), StringUtils.EMPTY));
		Toolbox.addActionsColumn(grid, isEditableByLoggedInUser(), row -> false,
				this::open, this::edit, this::delete
        );

		grid.setItems(service.listAllEditable());
		grid.setWidth(100, PERCENTAGE);
		return grid;
	}

	private Component createViewGridTab() {
		Grid<vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO> grid = new Grid<>();

		Toolbox.addColumn(grid, "field.code", vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO::getKod);
		Toolbox.addColumn(grid, "field.name", vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO::getNev);
		Toolbox.addColumn(grid, "field.description", vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO::getLeiras);
		Toolbox.addColumn(grid, "field.valStart", vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO::getErvKezdet);
		Toolbox.addColumn(grid, "field.valEnd", vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO::getErvVeg);
		Toolbox.addComponentColumn(grid, "field.technical",
				szotarTestDTO -> Toolbox.createReadonlyCheckBox(szotarTestDTO.getTechnikai(), StringUtils.EMPTY));

		Toolbox.addActionsColumn(grid, isEditableByLoggedInUser(), row -> false,
				null,
				row -> showPopup.show(new SzotarTestDTO(row), dto -> {}),
				null
        );
		grid.setItems(service.listAllEditable());
		grid.setWidth(100, PERCENTAGE);
		return grid;
	}

	private Component createTreeGridTab() {
		TreeGrid<SzotarTestDTO> treeGrid = new TreeGrid<>();

		Toolbox.addColumn(treeGrid, "field.code", SzotarTestDTO::getKod);
		Toolbox.addColumn(treeGrid, "field.name", SzotarTestDTO::getNev);
		Toolbox.addColumn(treeGrid, "field.description", SzotarTestDTO::getLeiras);
		Toolbox.addColumn(treeGrid, "field.valStart", SzotarTestDTO::getErvKezdet);
		Toolbox.addColumn(treeGrid, "field.valEnd", SzotarTestDTO::getErvVeg);
		Toolbox.addComponentColumn(treeGrid, "field.technical",
				sz -> Toolbox.createReadonlyCheckBox(sz.getTechnikai(), StringUtils.EMPTY));
		Toolbox.addActionsColumn(treeGrid, isEditableByLoggedInUser(), row -> false,
				this::create, this::open, this::edit, this::delete
        );

		treeGrid.setDataProvider(DataTransformer.toTreeDataProvider(service.getWholeTreeEditable()));
		treeGrid.setWidth(100, PERCENTAGE);
		return treeGrid;
	}

	private Component createHierarchicalGridTab() {
		Hierarchy<SzotarTestDTO> szotarTestTree = service.getWholeTreeEditable();
		Hierarchy<SzotarTestDTO> subTree = service.getSubTreeEditable(service.listAllTipusEditable().get(0));
		Dag<SzotarTestDTO> dag = new Dag<>(szotarTestTree);
		dag.addIsolatedSubgraph(subTree);

		TreeGrid<HierarchicalDataWrapper<SzotarTestDTO>> treeGrid = new TreeGrid<>();
		Toolbox.addColumn(treeGrid, "field.code", w -> w.getData().getKod());
		Toolbox.addColumn(treeGrid, "field.name", w -> w.getData().getNev());
		Toolbox.addColumn(treeGrid, "field.description", w -> w.getData().getLeiras());
		Toolbox.addColumn(treeGrid, "field.valStart", w -> w.getData().getErvKezdet());
		Toolbox.addColumn(treeGrid, "field.valEnd", w -> w.getData().getErvVeg());
		Toolbox.addComponentColumn(treeGrid, "field.technical",
				w -> Toolbox.createReadonlyCheckBox(w.getData().getTechnikai(), StringUtils.EMPTY));

		treeGrid.setDataProvider(DataTransformer.toHierarchicalDataProvider(dag));
		treeGrid.setWidth(100, PERCENTAGE);
		return treeGrid;
	}

	private Component createTreeTab() {
		Tree<SzotarTestDTO> tree = new Tree<>();

		tree.setItemCaptionGenerator( sz -> String.format("%s - %s", sz.getKod(), sz.getNev()));
		tree.setDataProvider(DataTransformer.toTreeDataProvider(service.getWholeTreeEditable()));

		tree.setWidth(100, PERCENTAGE);
		return tree;
	}

	private Component createHierarchicalTab() {
		Hierarchy<SzotarTestDTO> szotarTestTree = service.getWholeTreeEditable();
		Hierarchy<SzotarTestDTO> subTree = service.getSubTreeEditable(service.listAllTipusEditable().get(0));
		Dag<SzotarTestDTO> dag = new Dag<>(szotarTestTree);
		dag.addIsolatedSubgraph(subTree);

		Tree<HierarchicalDataWrapper<SzotarTestDTO>> tree = new Tree<>();
		tree.setDataProvider(DataTransformer.toHierarchicalDataProvider(dag));
		tree.setItemCaptionGenerator( sz -> String.format("%s - %s", sz.getData().getKod(), sz.getData().getNev()));

		tree.setWidth(100, PERCENTAGE);
		return tree;
	}
}
