package vtijuq_szakdoli.view.karbantarto;

import javax.inject.Inject;

import lombok.AccessLevel;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.layout.Restricted;
import vtijuq_szakdoli.service.ValidatorService;
import vtijuq_szakdoli.view.kereso.AbstractKeresoView;

public abstract class AbstractKarbantartoView<T extends EditableDTO, S extends FilterDTO> extends AbstractKeresoView<T, S> implements Restricted {

	@Inject
	@Getter(AccessLevel.PROTECTED)
	private ValidatorService validatorService;

	public String getFunkcioNev(){
		return getName();
	}

	protected abstract void edit(T dto);

	protected void open(T dto){
		edit(dto);
	}

	protected abstract void delete(T dto);

}
