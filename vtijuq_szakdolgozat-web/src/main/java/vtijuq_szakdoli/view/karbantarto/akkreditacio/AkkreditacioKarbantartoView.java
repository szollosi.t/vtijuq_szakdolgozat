package vtijuq_szakdoli.view.karbantarto.akkreditacio;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.text.DateFormat;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.filterDTO.akkreditacio.AkkreditacioFilterDTO;
import vtijuq_szakdoli.formcreator.filter.akkreditacio.AkkreditacioFilter;
import vtijuq_szakdoli.service.akkreditacio.AkkreditacioService;
import vtijuq_szakdoli.service.megvalositas.KonkretService;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.karbantarto.AbstractKarbantartoView;
import vtijuq_szakdoli.view.manipulator.creator.akkreditacio.AkkreditacioCreatorView;
import vtijuq_szakdoli.view.manipulator.editor.akkreditacio.AkkreditacioEditorView;

@CDIView(AkkreditacioKarbantartoView.NAME)
public class AkkreditacioKarbantartoView extends AbstractKarbantartoView<AkkreditacioDTO, AkkreditacioFilterDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(AkkreditacioKarbantartoView.class);

	public static final String NAME = FunctionNames.AKKREDITACIO_KARBANTARTO;

	@Inject
	private AkkreditacioService service;

	@Inject
	private KonkretService konkretService;

	private ListDataProvider<AkkreditacioDTO> dataProvider;

	private Map<Long, KonkretSzakiranyDTO> konkretSzakiranyMap;

	@Getter(AccessLevel.PROTECTED)
	private AkkreditacioFilter filter;

	@Override
	public boolean hasRestrictedVisibility() {
		return false;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.akkreditacio.karbantarto";
	}

	@Override
	protected void create() {
		navigateTo(AkkreditacioCreatorView.NAME);
	}

	@Override
	protected void edit(AkkreditacioDTO dto) {
		navigateTo(AkkreditacioEditorView.NAME, dto.getId());
	}

	@Override
	protected void delete(AkkreditacioDTO dto) {
		service.delete(dto);
		refresh();
	}

	@Override
	protected void search() {
		dataProvider = filter.hasConditions()
				? DataProvider.ofCollection(service.listAkkreditacioEditableByFilter(filter.getDTO()))
				: DataProvider.ofCollection(service.listAllAkkreditacioEditable());
	}

	@Override
	protected void init() {
		if (dataProvider == null) {
			dataProvider = DataProvider.ofCollection(service.listAllAkkreditacioEditable());
		}
		if (konkretSzakiranyMap == null) {
			konkretSzakiranyMap = konkretService.listAllKonkretSzakirany().stream()
			                                    .collect(Collectors.toMap(KonkretSzakiranyDTO::getId, Function.identity()));
		}
		if (filter == null) {
			filter = new AkkreditacioFilter(konkretSzakiranyMap);
		}
		super.init();
	}

	@Override
	protected Grid<AkkreditacioDTO> createGrid() {
		Grid<AkkreditacioDTO> grid = new Grid<>();

		Toolbox.addColumn(grid, "field.konkretSzakirany", AkkreditacioDTO::getKonkretSzakirany);
		Toolbox.addColumn(grid, "field.valStart", AkkreditacioDTO::getErvenyessegKezdet)
				.setRenderer(new DateRenderer(DateFormat.getDateInstance()));
		Toolbox.addColumn(grid, "field.valEnd", AkkreditacioDTO::getErvenyessegVeg)
				.setRenderer(new DateRenderer(DateFormat.getDateInstance()));
		Toolbox.addComponentColumn(grid, "field.valid",
				dto -> Toolbox.createReadonlyCheckBox(dto.isValid(), StringUtils.EMPTY));
		Toolbox.addComponentColumn(grid, "field.vegleges",
				dto -> Toolbox.createReadonlyCheckBox(dto.isVegleges(), StringUtils.EMPTY));

		Toolbox.addActionsColumn(grid,
				getLoginBean().isEditable(FunctionNames.AKKREDITACIO_KARBANTARTO),
				isEditableByLoggedInUser(),
				row -> getValidatorService().isAkkreditacioFinal(row.getId()),
				this::open, this::edit, this::delete
        );
		grid.setDataProvider(dataProvider);

		grid.setWidth(100, PERCENTAGE);
		return grid;
	}
}
