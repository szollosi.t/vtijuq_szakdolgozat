package vtijuq_szakdoli.view.karbantarto.kovetelmeny;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TreeGrid;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.IsmeretFilterDTO;
import vtijuq_szakdoli.common.dto.subordinationDTO.SubordinationDTO;
import vtijuq_szakdoli.formcreator.filter.kovetelmeny.IsmeretFilter;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.kovetelmeny.IsmeretCreatorPopup;
import vtijuq_szakdoli.popup.creator.subordination.IsmeretSubordinationCreatorPopup;
import vtijuq_szakdoli.popup.editor.kovetelmeny.IsmeretEditorPopup;
import vtijuq_szakdoli.service.kovetelmeny.IsmeretService;
import vtijuq_szakdoli.utils.ActionButton;
import vtijuq_szakdoli.utils.ActionButtons;
import vtijuq_szakdoli.utils.DataTransformer;
import vtijuq_szakdoli.utils.HierarchicalDataWrapper;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.karbantarto.AbstractKarbantartoView;

@CDIView(IsmeretKarbantartoView.NAME)
public class IsmeretKarbantartoView extends AbstractKarbantartoView<IsmeretDTO, IsmeretFilterDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(IsmeretKarbantartoView.class);

	public static final String NAME = "IsmeretKarbantarto";

	@Inject
	private IsmeretEditorPopup editorPopup;

	@Inject
	private IsmeretCreatorPopup creatorPopup;

	@Inject
	private IsmeretSubordinationCreatorPopup subordinationCreatorPopup;

	@Inject
	private IsmeretService service;

	private TreeDataProvider<HierarchicalDataWrapper<IsmeretDTO>> dataProvider;

	private IsmeretFilter filter;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.ismeret.karbantarto";
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(creatorPopup, editorPopup, subordinationCreatorPopup).collect(Collectors.toSet());
	}

	@Override
	protected void search() {
		dataProvider = filter.hasConditions()
				? DataTransformer.toHierarchicalDataProvider(service.getHierarchyBelowFilteredEditable(getFilter().getDTO()))
				: DataTransformer.toHierarchicalDataProvider(service.getWholeTreeEditable());
	}

	@Override
	protected HorizontalLayout createTopButtons() {
		final HorizontalLayout buttons = super.createTopButtons();
		if (isEditableByLoggedInUser()) {
			addButton(buttons, Alignment.TOP_RIGHT, resolveCaption("button.create.subordination.ismeret"),
					e -> subordinationCreatorPopup.show(dto -> {
						service.createSubordination(dto);
						initDataProvider();
					})
			);
		}
		return buttons;
	}

	protected TreeDataProvider<HierarchicalDataWrapper<IsmeretDTO>> getDataProvider() {
		if (dataProvider == null) {
			initDataProvider();
		}
		return dataProvider;
	}

	private void initDataProvider() {
		dataProvider = DataTransformer.toHierarchicalDataProvider(service.getWholeTreeEditable());
	}

	@Override
	protected IsmeretFilter getFilter() {
		if (filter == null) {
			filter = new IsmeretFilter();
		}
		return filter;
	}

	@Override
	protected Grid<HierarchicalDataWrapper<IsmeretDTO>> createGrid() {
		TreeGrid<HierarchicalDataWrapper<IsmeretDTO>> treeGrid = new TreeGrid<>();

		Toolbox.addColumn(treeGrid, "field.code", wrapper -> wrapper.getData().getKod());
		Toolbox.addColumn(treeGrid, "field.name", wrapper -> wrapper.getData().getNev());
		Toolbox.addColumn(treeGrid, "field.description", wrapper -> wrapper.getData().getLeiras());

		addActionsColumn(treeGrid);
		treeGrid.setDataProvider(getDataProvider());
		treeGrid.setWidth(100, PERCENTAGE);
		return treeGrid;
	}

	private void addActionsColumn(TreeGrid<HierarchicalDataWrapper<IsmeretDTO>> treeGrid) {
		final boolean isGridEditable = isEditableByLoggedInUser();
		final ActionButtons<HierarchicalDataWrapper<IsmeretDTO>> stdActionButtons =
				ActionButtons.getStdActionButtons(isGridEditable, this::isIsmeretFinal,
						this::create, row -> open(row.getData()), row -> edit(row.getData()), this::delete
		);
		final ActionButton<HierarchicalDataWrapper<IsmeretDTO>> removeSubordinationButton = new ActionButton<>(
				"button.remove.subordination.ismeret", VaadinIcons.BOLT,
				row -> isGridEditable							//szerkeszthető
						&& row.getPrincipalWrapper() != null 	//van mi alól kisorolni
						&& !isIsmeretFinal(row),				//se ő, se a fölé rendelt ismeretek nem kapcsolódnak véglegesített szakKovetelmeny-hez
				this::removeSubordination
		);
		Toolbox.addComponentColumn(treeGrid, StringUtils.EMPTY, row -> {
			final HorizontalLayout actions = new HorizontalLayout();
			stdActionButtons.addButtons(actions, this::resolveCaption, row);
			removeSubordinationButton.addButton(actions, this::resolveCaption, row);
			return actions;
		});
	}

	private boolean isIsmeretFinal(HierarchicalDataWrapper<IsmeretDTO> row) {
		//TODO megérné inkább flaggel jelezni, enterprise esetben pedig megfelelő mview készíthető erre a célra
		return getValidatorService().isIsmeretFinal(row.getData().getId())
				|| (row.getPrincipalWrapper() != null && isIsmeretFinal(row.getPrincipalWrapper()));
	}

	private void create(HierarchicalDataWrapper<IsmeretDTO> principalWrapper) {
		creatorPopup.show(dto -> {
			service.save(dto, principalWrapper.getData().getId());
			getDataProvider().getTreeData().addItem(principalWrapper, new HierarchicalDataWrapper<>(principalWrapper, dto));
		});
	}

	@Override
	protected void create() {
		creatorPopup.show(dto -> {
			service.save(dto);
			getDataProvider().getTreeData().addItem(null, new HierarchicalDataWrapper<>(null, dto));
		});
	}

	@Override
	protected void edit(IsmeretDTO dto) {
		editorPopup.showForEdit(dto, service::save);
	}

	protected void delete(HierarchicalDataWrapper<IsmeretDTO> wrapper) {
		delete(wrapper.getData());
		//az elem törlésével az alá besorolt elemek eggyel feljebb vándorolnak
		getDataProvider().getTreeData().getChildren(wrapper).forEach( child ->
				getDataProvider().getTreeData().setParent(child, wrapper.getPrincipalWrapper()));
		getDataProvider().getTreeData().removeItem(wrapper);
		refresh();
	}

	@Override
	protected void delete(IsmeretDTO dto) {
		service.delete(dto);
	}

	private void removeSubordination(HierarchicalDataWrapper<IsmeretDTO> wrapper) {
		service.removeSubordination(new SubordinationDTO<>(
				wrapper.getPrincipalWrapper().getData(), wrapper.getData()));
		initDataProvider();
		refresh();
	}
}
