package vtijuq_szakdoli.view.kereso.kovetelmeny;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.SzakiranyKovetelmenyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.kovetelmeny.SzakiranyKovetelmenyFilter;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.kovetelmeny.SzakiranyCreatorPopup;
import vtijuq_szakdoli.popup.creator.kovetelmeny.SzakiranyKovetelmenyCreatorPopup;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.kereso.AbstractKeresoView;
import vtijuq_szakdoli.view.manipulator.editor.kovetelmeny.SzakiranyKovetelmenyEditorView;

@CDIView(SzakiranyKovetelmenyKeresoView.NAME)
public class SzakiranyKovetelmenyKeresoView extends AbstractKeresoView<SzakiranyKovetelmenyDTO, SzakiranyKovetelmenyFilterDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakiranyKovetelmenyKeresoView.class);

	public static final String NAME = "SzakiranyKovetelmenyKereso";

	@Inject
	private SzakiranyKovetelmenyCreatorPopup creatorPopup;

	@Inject
	private SzakiranyCreatorPopup szakiranyCreatorPopup;

	@Inject
	private KovetelmenyService service;

	private ListDataProvider<SzakiranyKovetelmenyDTO> dataProvider;

	private Map<Long, SzakKovetelmenyDTO> szakKovetelmenyMap;
	private Map<Long, SzakiranyDTO> szakiranyMap;

	@Getter(AccessLevel.PROTECTED)
	private SzakiranyKovetelmenyFilter filter;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.szakiranyKovetelmeny.kereso";
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(creatorPopup, szakiranyCreatorPopup).collect(Collectors.toSet());
	}

	@Override
	protected void open(SzakiranyKovetelmenyDTO dto) {
		navigateTo(SzakiranyKovetelmenyEditorView.NAME, dto.getId());
	}

	@Override
	protected void search() {
		dataProvider = filter.hasConditions()
				? DataProvider.ofCollection(service.listAllSzakiranyKovetelmenyByFilter(filter.getDTO()))
				: DataProvider.ofCollection(service.listAllSzakiranyKovetelmeny());
	}

	@Override
	protected HorizontalLayout createTopButtons() {
		final HorizontalLayout topButtons = super.createTopButtons();
		if (isEditableByLoggedInUser()) {
			addButton(topButtons, Alignment.TOP_RIGHT,
					resolveCaption("button.create.szakirany"),
					e -> createSzakirany());
		}
		return topButtons;
	}

	@Override
	public boolean isEditableByLoggedInUser() {
		return getLoginBean().isEditable(FunctionNames.KOVETELMENY_SZERKESZTO);
	}

	@Override
	protected void create() {
		creatorPopup.show(dto -> {service.save(dto); open(dto); });
	}

	private void createSzakirany() {
		szakiranyCreatorPopup.show(dto -> service.save(dto));
	}

	@Override
	protected void init() {
		if (dataProvider == null) {
			dataProvider = DataProvider.ofCollection(service.listAllSzakiranyKovetelmeny());
		}
		if (szakKovetelmenyMap == null) {
			szakKovetelmenyMap = service.listAllSzakKovetelmeny().stream()
			                            .collect(Collectors.toMap(SzakKovetelmenyDTO::getId, Function.identity()));
		}
		if (szakiranyMap == null) {
			szakiranyMap = service.listAllSzakirany().stream()
			                      .collect(Collectors.toMap(SzakiranyDTO::getId, Function.identity()));
		}
		if (filter == null) {
			filter = new SzakiranyKovetelmenyFilter(szakKovetelmenyMap, szakiranyMap);
		}
		super.init();
	}

	@Override
	protected Grid<SzakiranyKovetelmenyDTO> createGrid() {
		Grid<SzakiranyKovetelmenyDTO> grid = new Grid<>();

		Toolbox.addComponentColumn(grid, "field.csakkozos",
				dto ->Toolbox.createReadonlyCheckBox(dto.getCsakkozos(), StringUtils.EMPTY)
		);
		Toolbox.addColumn(grid, "field.szakKovetelmeny", SzakiranyKovetelmenyDTO::getSzakKovetelmeny);
		Toolbox.addColumn(grid, "field.szakirany", SzakiranyKovetelmenyDTO::getSzakirany);

		Toolbox.addActionsColumn(grid, this::open);
		grid.setDataProvider(dataProvider);

		grid.setWidth(100, PERCENTAGE);
		return grid;
	}
}
