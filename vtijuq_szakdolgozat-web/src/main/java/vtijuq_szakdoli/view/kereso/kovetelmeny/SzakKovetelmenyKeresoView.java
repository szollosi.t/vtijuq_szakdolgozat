package vtijuq_szakdoli.view.kereso.kovetelmeny;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.text.DateFormat;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.renderers.DateRenderer;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.SzakKovetelmenyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.kovetelmeny.SzakKovetelmenyFilter;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.kovetelmeny.SzakCreatorPopup;
import vtijuq_szakdoli.popup.creator.kovetelmeny.SzakKovetelmenyCreatorPopup;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.kereso.AbstractKeresoView;
import vtijuq_szakdoli.view.manipulator.editor.kovetelmeny.SzakKovetelmenyEditorView;

@CDIView(SzakKovetelmenyKeresoView.NAME)
public class SzakKovetelmenyKeresoView extends AbstractKeresoView<SzakKovetelmenyDTO, SzakKovetelmenyFilterDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakKovetelmenyKeresoView.class);

	public static final String NAME = "SzakKovetelmenyKereso";

	@Inject
	private SzakKovetelmenyCreatorPopup creatorPopup;

	@Inject
	private SzakCreatorPopup szakCreatorPopup;

	@Inject
	private KovetelmenyService service;

	private ListDataProvider<SzakKovetelmenyDTO> dataProvider;

	private Map<Long, SzakDTO> szakMap;

	@Getter(AccessLevel.PROTECTED)
	private SzakKovetelmenyFilter filter;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.szakKovetelmeny.kereso";
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(creatorPopup, szakCreatorPopup).collect(Collectors.toSet());
	}

	@Override
	protected void open(SzakKovetelmenyDTO dto) {
		navigateTo(SzakKovetelmenyEditorView.NAME, dto.getId());
	}

	@Override
	protected void search() {
		dataProvider = filter.hasConditions()
				? DataProvider.ofCollection(service.listAllSzakKovetelmenyByFilter(filter.getDTO()))
				: DataProvider.ofCollection(service.listAllSzakKovetelmeny());
	}

	@Override
	protected HorizontalLayout createTopButtons() {
		final HorizontalLayout topButtons = super.createTopButtons();
		if (isEditableByLoggedInUser()) {
			addButton(topButtons, Alignment.TOP_RIGHT, resolveCaption("button.create.szak"), e -> createSzak());
		}
		return topButtons;
	}

	@Override
	public boolean isEditableByLoggedInUser() {
		return getLoginBean().isEditable(FunctionNames.KOVETELMENY_SZERKESZTO);
	}

	@Override
	protected void create() {
		creatorPopup.show(dto -> {service.save(dto); open(dto); });
	}

	private void createSzak() {
		szakCreatorPopup.show(dto -> service.save(dto));
	}

	@Override
	protected void init() {
		if (dataProvider == null) {
			dataProvider = DataProvider.ofCollection(service.listAllSzakKovetelmeny());
		}
		if (szakMap == null) {
			szakMap = service.listAllSzak().stream()
			                 .collect(Collectors.toMap(SzakDTO::getId, Function.identity()));
		}
		if (filter == null) {
			filter = new SzakKovetelmenyFilter(szakMap);
		}
		super.init();
	}

	@Override
	protected Grid<SzakKovetelmenyDTO> createGrid() {
		Grid<SzakKovetelmenyDTO> grid = new Grid<>();

		Toolbox.addColumn(grid, "field.szak", SzakKovetelmenyDTO::getSzak);
		Toolbox.addColumn(grid, "field.valStart", SzakKovetelmenyDTO::getErvenyessegKezdet)
				.setRenderer(new DateRenderer(DateFormat.getDateInstance()));
		Toolbox.addColumn(grid, "field.valEnd", SzakKovetelmenyDTO::getErvenyessegVeg)
				.setRenderer(new DateRenderer(DateFormat.getDateInstance()));
		Toolbox.addComponentColumn(grid, "field.valid",
				dto -> Toolbox.createReadonlyCheckBox(dto.isValid(), StringUtils.EMPTY));
		Toolbox.addComponentColumn(grid, "field.vegleges",
				dto -> Toolbox.createReadonlyCheckBox(dto.isVegleges(), StringUtils.EMPTY));
		Toolbox.addColumn(grid, "field.oklevelMegjeloles", SzakKovetelmenyDTO::getOklevelMegjeloles);

		Toolbox.addActionsColumn(grid, this::open);
		grid.setDataProvider(dataProvider);

		grid.setWidth(100, PERCENTAGE);
		return grid;
	}
}
