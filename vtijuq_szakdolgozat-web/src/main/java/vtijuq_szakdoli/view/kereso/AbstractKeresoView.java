package vtijuq_szakdoli.view.kereso;

import javax.inject.Inject;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.captions.Caption;
import vtijuq_szakdoli.view.AbstractPublicView;

public abstract class AbstractKeresoView<T extends EntitasDTO, S extends FilterDTO> extends AbstractPublicView {

	@Inject @Caption
	@ConfigurationValue(value = "button.search")
	private String searchCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.emptySearch")
	private String emptySearchCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.create")
	private String addCaption;

	protected abstract FilterFormCreator<S> getFilter();

	protected abstract void search();

	protected void emptySearch() {
		getFilter().clear();
		search();
		refresh();
	}

	protected Layout createSearchForm() {
		return getFilter().createForm();
	}

	protected abstract void create();

	protected abstract void open(T dto);

	@Override
	protected Layout createHead() {
		Layout searchLayout = null;
		final Layout searchForm = createSearchForm();
		if (searchForm != null) {
			searchLayout = new VerticalLayout();
			searchLayout.addComponent(searchForm);
			final Layout searchButtonLayout = new HorizontalLayout();
			searchButtonLayout.addComponent(new Button(searchCaption, e -> {
				search();
				refresh();
			}));
			searchButtonLayout.addComponent(new Button(emptySearchCaption, e -> emptySearch()));
			searchLayout.addComponent(searchButtonLayout);
		}
		return searchLayout;
	}

	@Override
	protected HorizontalLayout createTopButtons() {
		final HorizontalLayout buttons = new HorizontalLayout();
		if (isEditableByLoggedInUser()) {
			addButton(buttons, Alignment.TOP_RIGHT, addCaption, e -> create());
		}
		return buttons;
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();
		final Grid grid = createGrid();
		content.addComponent(grid);
		content.setExpandRatio(grid, 1);
		return content;
	}

	protected abstract Grid createGrid();
}
