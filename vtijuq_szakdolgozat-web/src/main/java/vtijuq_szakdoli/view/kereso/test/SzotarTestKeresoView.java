package vtijuq_szakdoli.view.kereso.test;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TreeGrid;
import com.vaadin.ui.VerticalLayout;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.dto.filterDTO.test.SzotarTestFilterDTO;
import vtijuq_szakdoli.formcreator.filter.test.SzotarTestFilter;
import vtijuq_szakdoli.service.test.SzotarTestService;
import vtijuq_szakdoli.utils.DataTransformer;
import vtijuq_szakdoli.utils.HierarchicalDataWrapper;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.kereso.AbstractKeresoView;
import vtijuq_szakdoli.view.manipulator.creator.test.SzotarTestCreatorView;
import vtijuq_szakdoli.view.manipulator.editor.test.SzotarTestEditorView;

@CDIView(SzotarTestKeresoView.NAME)
public class SzotarTestKeresoView extends AbstractKeresoView<SzotarTestDTO, SzotarTestFilterDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzotarTestKeresoView.class);

	public static final String NAME = "SzotarTestKereso";

	@Inject
	private SzotarTestService service;

	private TreeDataProvider<HierarchicalDataWrapper<SzotarTestDTO>> dataProvider;

	private Map<Long, SzotarTestDTO> tipusMap;

	private SzotarTestFilter filter;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.test.kereso";
	}

	protected TreeDataProvider<HierarchicalDataWrapper<SzotarTestDTO>> getDataProvider() {
		if (dataProvider == null) {
			dataProvider = DataTransformer.toHierarchicalDataProvider(service.getWholeTree());
		}
		return dataProvider;
	}

	protected Map<Long, SzotarTestDTO> getTipusMap() {
		if (tipusMap == null) {
			tipusMap = service.listAllTipus().stream()
					.collect(Collectors.toMap(SzotarTestDTO::getId, Function.identity()));
		}
		return tipusMap;
	}

	protected SzotarTestFilter getFilter() {
		if (filter == null) {
			filter = new SzotarTestFilter(getTipusMap());
		}
		return filter;
	}

	@Override
	protected void open(SzotarTestDTO dto) {
		navigateTo(SzotarTestEditorView.NAME, dto.getId());
	}

	@Override
	protected void search() {
		dataProvider = filter.hasConditions()
				? DataTransformer.toHierarchicalDataProvider(service.getEntitasHierarchyBelowFiltered(getFilter().getDTO()))
				: DataTransformer.toHierarchicalDataProvider(service.getWholeTree());
	}

	@Override
	public boolean isEditableByLoggedInUser() {
		return getLoginBean().isEditable(FunctionNames.TEST);
	}

	@Override
	protected void create() {
		navigateTo(SzotarTestCreatorView.NAME);
	}

	private boolean existsByFilter() {
		return !getFilter().hasConditions() || service.existsByFilter(getFilter().getDTO());
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();
		if (existsByFilter()) {
			final Grid grid = createGrid();
			content.addComponent(grid);
			content.setExpandRatio(grid, 1);
		} else {
			content.addComponent(new Label("Nincs megjeleníthető találat!"));
		}
		return content;
	}

	protected TreeGrid<HierarchicalDataWrapper<SzotarTestDTO>> createGrid() {
		TreeGrid<HierarchicalDataWrapper<SzotarTestDTO>> treeGrid = new TreeGrid<>();

		Toolbox.addColumn(treeGrid, "field.code", wrapper -> wrapper.getData().getKod());
		Toolbox.addColumn(treeGrid, "field.name", wrapper -> wrapper.getData().getNev());
		Toolbox.addColumn(treeGrid, "field.description", wrapper -> wrapper.getData().getLeiras());
		Toolbox.addColumn(treeGrid, "field.valStart", wrapper -> wrapper.getData().getErvKezdet());
		Toolbox.addColumn(treeGrid, "field.valEnd", wrapper -> wrapper.getData().getErvVeg());
		Toolbox.addComponentColumn(treeGrid, "field.technical",
				wrapper -> Toolbox.createReadonlyCheckBox(wrapper.getData().getTechnikai(), StringUtils.EMPTY));

		Toolbox.addActionsColumn(treeGrid, row -> open(row.getData()));
		treeGrid.setDataProvider(getDataProvider());

		treeGrid.setWidth(100, PERCENTAGE);
		return treeGrid;
	}
}
