package vtijuq_szakdoli.view.kereso.megvalositas;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Grid;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.KonkretSzakFilterDTO;
import vtijuq_szakdoli.formcreator.filter.megvalositas.KonkretSzakFilter;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.megvalositas.KonkretSzakCreatorPopup;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;
import vtijuq_szakdoli.service.megvalositas.KonkretService;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.kereso.AbstractKeresoView;
import vtijuq_szakdoli.view.manipulator.editor.megvalositas.KonkretSzakEditorView;

@CDIView(KonkretSzakKeresoView.NAME)
public class KonkretSzakKeresoView extends AbstractKeresoView<KonkretSzakDTO, KonkretSzakFilterDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(KonkretSzakKeresoView.class);

	public static final String NAME = "KonkretSzakKereso";

	@Inject
	private KonkretSzakCreatorPopup creatorPopup;

	@Inject
	private KonkretService service;

	@Inject
	private KovetelmenyService kovetelmenyService;

	@Inject
	private AdminisztracioService adminisztracioService;

	private ListDataProvider<KonkretSzakDTO> dataProvider;

	private Map<Long, SzakKovetelmenyDTO> szakKovetelmenyMap;
	private Map<Long, SzervezetDTO> szervezetMap;

	@Getter(AccessLevel.PROTECTED)
	private KonkretSzakFilter filter;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.konkretSzak.kereso";
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(creatorPopup).collect(Collectors.toSet());
	}

	@Override
	protected void open(KonkretSzakDTO dto) {
		navigateTo(KonkretSzakEditorView.NAME, dto.getId());
	}

	@Override
	protected void search() {
		dataProvider = filter.hasConditions()
				? DataProvider.ofCollection(service.listKonkretSzakByFilter(filter.getDTO()))
				: DataProvider.ofCollection(service.listAllKonkretSzak());
	}

	@Override
	public boolean isEditableByLoggedInUser() {
		return getLoginBean().isEditable(FunctionNames.KONKRET_SZERKESZTO);
	}

	@Override
	protected void create() {
		creatorPopup.show(dto -> {service.save(dto); open(dto); });
	}

	@Override
	protected void init() {
		if (dataProvider == null) {
			dataProvider = DataProvider.ofCollection(service.listAllKonkretSzak());
		}
		if (szakKovetelmenyMap == null) {
			szakKovetelmenyMap = kovetelmenyService.listAllSzakKovetelmeny().stream()
			                            .collect(Collectors.toMap(SzakKovetelmenyDTO::getId, Function.identity()));
		}
		if (szervezetMap == null) {
			szervezetMap = adminisztracioService.listAllSzervezet().stream()
			                      .collect(Collectors.toMap(SzervezetDTO::getId, Function.identity()));
		}
		if (filter == null) {
			filter = new KonkretSzakFilter(szakKovetelmenyMap, szervezetMap);
		}
		super.init();
	}

	@Override
	protected Grid<KonkretSzakDTO> createGrid() {
		Grid<KonkretSzakDTO> grid = new Grid<>();

		Toolbox.addColumn(grid, "field.szakKovetelmeny", KonkretSzakDTO::getSzakKovetelmeny);
		Toolbox.addColumn(grid, "field.organization", KonkretSzakDTO::getSzervezet);

		Toolbox.addActionsColumn(grid, row -> open(row));
		grid.setDataProvider(dataProvider);

		grid.setWidth(100, PERCENTAGE);
		return grid;
	}
}
