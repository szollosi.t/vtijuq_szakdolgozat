package vtijuq_szakdoli.view.kereso.megvalositas;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Grid;
import lombok.AccessLevel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.KonkretSzakiranyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.megvalositas.KonkretSzakiranyFilter;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.megvalositas.KonkretSzakiranyCreatorPopup;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;
import vtijuq_szakdoli.service.megvalositas.KonkretService;
import vtijuq_szakdoli.utils.Toolbox;
import vtijuq_szakdoli.view.kereso.AbstractKeresoView;
import vtijuq_szakdoli.view.manipulator.editor.megvalositas.KonkretSzakiranyEditorView;

@CDIView(KonkretSzakiranyKeresoView.NAME)
public class KonkretSzakiranyKeresoView extends AbstractKeresoView<KonkretSzakiranyDTO, KonkretSzakiranyFilterDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(KonkretSzakiranyKeresoView.class);

	public static final String NAME = "KonkretSzakiranyKereso";

	@Inject
	private KonkretSzakiranyCreatorPopup creatorPopup;

	@Inject
	private KonkretService service;

	@Inject
	private KovetelmenyService kovetelmenyService;

	private ListDataProvider<KonkretSzakiranyDTO> dataProvider;

	private Map<Long, KonkretSzakDTO> konkretSzakMap;
	private Map<Long, SzakiranyKovetelmenyDTO> szakiranyKovetelmenyMap;

	@Getter(AccessLevel.PROTECTED)
	private KonkretSzakiranyFilter filter;

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getMenuTitleCaptionKey() {
		return "menutitle.konkretSzakirany.kereso";
	}

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(creatorPopup).collect(Collectors.toSet());
	}

	@Override
	protected void open(KonkretSzakiranyDTO dto) {
		navigateTo(KonkretSzakiranyEditorView.NAME, dto.getId());
	}

	@Override
	protected void search() {
		dataProvider = filter.hasConditions()
				? DataProvider.ofCollection(service.listAllKonkretSzakiranyByFilter(filter.getDTO()))
				: DataProvider.ofCollection(service.listAllKonkretSzakirany());
	}

	@Override
	public boolean isEditableByLoggedInUser() {
		return getLoginBean().isEditable(FunctionNames.KONKRET_SZERKESZTO);
	}

	@Override
	protected void create() {
		creatorPopup.show(dto -> {service.save(dto); open(dto); });
	}

	@Override
	protected void init() {
		if (dataProvider == null) {
			dataProvider = DataProvider.ofCollection(service.listAllKonkretSzakirany());
		}
		if (konkretSzakMap == null) {
			konkretSzakMap = service.listAllKonkretSzak().stream()
			                        .collect(Collectors.toMap(KonkretSzakDTO::getId, Function.identity()));
		}
		if (szakiranyKovetelmenyMap == null) {
			szakiranyKovetelmenyMap = kovetelmenyService.listAllSzakiranyKovetelmeny().stream()
			                                 .collect(Collectors.toMap(SzakiranyKovetelmenyDTO::getId, Function.identity()));
		}
		if (filter == null) {
			filter = new KonkretSzakiranyFilter(konkretSzakMap, szakiranyKovetelmenyMap);
		}
		super.init();
	}

	@Override
	protected Grid<KonkretSzakiranyDTO> createGrid() {
		Grid<KonkretSzakiranyDTO> grid = new Grid<>();

		Toolbox.addColumn(grid, "field.code", KonkretSzakiranyDTO::getKod);
		Toolbox.addColumn(grid, "field.name", KonkretSzakiranyDTO::getNev);
		Toolbox.addColumn(grid, "field.konkretSzak", KonkretSzakiranyDTO::getKonkretSzak);
		Toolbox.addColumn(grid, "field.szakiranyKovetelmeny", KonkretSzakiranyDTO::getSzakiranyKovetelmeny);

		Toolbox.addActionsColumn(grid, this::open);

		grid.setDataProvider(dataProvider);

		grid.setWidth(100, PERCENTAGE);
		return grid;
	}
}
