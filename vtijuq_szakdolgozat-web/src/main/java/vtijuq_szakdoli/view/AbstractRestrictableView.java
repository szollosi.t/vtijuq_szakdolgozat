package vtijuq_szakdoli.view;

import java.util.Map;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.UI;

import vtijuq_szakdoli.layout.AbstractRestrictableLayout;
import vtijuq_szakdoli.view.manipulator.editor.AbstractEditorView;

public abstract class AbstractRestrictableView extends AbstractRestrictableLayout implements View {

	private static final String PARAM_KEY_ID = "id";

	public abstract String getName();

	@Override
	protected String getTitle() {
		final StringBuilder sb = new StringBuilder();
		sb.append(resolveCaption(super.getTitle()));
		if (getLoginBean().isLoggedIn()) {
			sb.append(String.format(" (%s: %s)", resolveCaption("logged.in.as"), getLoginBean().getUserName()));
		}
		return sb.toString();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		final Map<String, String> parameterMap = event.getParameterMap();
		if (this instanceof AbstractEditorView) {
			AbstractEditorView thisView = (AbstractEditorView)this;
			thisView.init(Long.parseLong(parameterMap.getOrDefault(PARAM_KEY_ID, "-1")));
		} else {
			init();
		}
	}

	protected void navigateTo(final String viewName) {
		navigateTo(viewName, null);
	}

	protected void navigateTo(final String viewName, final Long id){
		final StringBuilder navigationState = new StringBuilder();
		navigationState.append(viewName);
		if (id != null) {
			navigationState.append("/id=");
			navigationState.append(id);
		}
		UI.getCurrent().getNavigator().navigateTo(navigationState.toString());
	}
}
