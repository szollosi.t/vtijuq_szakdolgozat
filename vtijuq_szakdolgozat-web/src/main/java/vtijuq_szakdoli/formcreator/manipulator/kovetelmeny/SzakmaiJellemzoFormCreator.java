package vtijuq_szakdoli.formcreator.manipulator.kovetelmeny;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public interface SzakmaiJellemzoFormCreator extends ManipulatorFormCreator<SzakmaiJellemzoDTO> {

	@Override
	default FormLayout createForm(final boolean readOnly) {
		final FormLayout form = new FormLayout();
		if (getFoJellemzo() != null) {
			form.addComponent(createFoJellemzoField());
		}

		form.addComponent(createIsmeretField());
		form.addComponent(createNameField());
		form.addComponent(createMinKreditField());
		form.addComponent(createMaxKreditField());
		form.addComponent(createMinValasztField());

		getBinder().setReadOnly(readOnly);
		return form;
	}

	List<IsmeretDTO> getIsmeretList();

	vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO getFoJellemzo();

	default TextField createFoJellemzoField() {
		return Toolbox.createReadonlyTextField(
				getFoJellemzo().getNev(), "field.fojellemzo"
		);
	}

	default TextField createNameField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.name",
				SzakmaiJellemzoDTO::getNev, SzakmaiJellemzoDTO::setNev);
	}

	default ComboBox<IsmeretDTO> createIsmeretField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.ismeret", getIsmeretList(),
				SzakmaiJellemzoDTO::getIsmeret, SzakmaiJellemzoDTO::setIsmeret
		);
	}

	default TextField createMinKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.minKredit",
				SzakmaiJellemzoDTO::getMinKredit, SzakmaiJellemzoDTO::setMinKredit
		);
	}

	default TextField createMaxKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.maxKredit",
				SzakmaiJellemzoDTO::getMaxKredit, SzakmaiJellemzoDTO::setMaxKredit
		);
	}

	default TextField createMinValasztField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.minValaszt",
				SzakmaiJellemzoDTO::getMinValaszt, SzakmaiJellemzoDTO::setMinValaszt
		);
	}

}
