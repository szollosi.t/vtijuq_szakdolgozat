package vtijuq_szakdoli.formcreator.manipulator.kovetelmeny;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.ComplexFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface SzakKovetelmenyFormCreator extends ManipulatorFormCreator<SzakKovetelmenyDTO> {

	List<SzakDTO> getSzakList();

	@Override
	default Layout createForm(final boolean readOnly) {

		final HorizontalLayout complexFormKeyLayout = new HorizontalLayout();
		complexFormKeyLayout.addComponent(createSzakField());
		complexFormKeyLayout.addComponent(createOklevelMegjelolesField());
		complexFormKeyLayout.addComponent(createFelevekField());

		final ComplexFormLayout complexFormLayout = new ComplexFormLayout(complexFormKeyLayout);

		complexFormLayout.createContentFormLayout("kreditek",
				createOsszKreditField(),
				createSzakiranyMinKreditField(),
				createSzakiranyMaxKreditField(),
				createSzabvalMinKreditField(),
				createSzakdolgozatKreditField()
		);

		complexFormLayout.createContentFormLayout("szakmaiGyak",
				createSzakmaigyakMinKreditField(),
				createSzakmaigyakEloirasokField()
		);

		complexFormLayout.createContentFormLayout("validInfo",
				createValStartField(),
				createValEndField(),
				createtValidField(),
				createVeglegesField()
		);

		getBinder().setReadOnly(readOnly);
		return complexFormLayout;
	}

	default ComboBox<SzakDTO> createSzakField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.szak", getSzakList(),
				SzakKovetelmenyDTO::getSzak, SzakKovetelmenyDTO::setSzak
		);
	}

	default DateField createValStartField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valStart",
				SzakKovetelmenyDTO::getErvenyessegKezdet, SzakKovetelmenyDTO::setErvenyessegKezdet);
	}

	default DateField createValEndField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valEnd",
				SzakKovetelmenyDTO::getErvenyessegVeg, SzakKovetelmenyDTO::setErvenyessegVeg);
	}

	default ComboBox<Boolean> createtValidField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.valid",
				SzakKovetelmenyDTO::isValid, SzakKovetelmenyDTO::setValid
        );
	}

	default ComboBox<Boolean> createVeglegesField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.vegleges",
				SzakKovetelmenyDTO::isVegleges, SzakKovetelmenyDTO::setVegleges
        );
	}

	default TextField createOklevelMegjelolesField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.oklevelMegjeloles",
				SzakKovetelmenyDTO::getOklevelMegjeloles, SzakKovetelmenyDTO::setOklevelMegjeloles);
	}

	default TextField createSzakmaigyakEloirasokField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.szakmaigyakEloirasok",
				SzakKovetelmenyDTO::getSzakmaigyakEloirasok, SzakKovetelmenyDTO::setSzakmaigyakEloirasok);
	}

	default TextField createFelevekField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.felevek",
				SzakKovetelmenyDTO::getFelevek, SzakKovetelmenyDTO::setFelevek
		);
	}

	default TextField createOsszKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.osszKredit",
				SzakKovetelmenyDTO::getOsszKredit, SzakKovetelmenyDTO::setOsszKredit
		);
	}

	default TextField createSzakdolgozatKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.szakdolgozatKredit",
				SzakKovetelmenyDTO::getSzakdolgozatKredit, SzakKovetelmenyDTO::setSzakdolgozatKredit
		);
	}

	default TextField createSzakmaigyakMinKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.szakmaigyakMinKredit",
				SzakKovetelmenyDTO::getSzakmaigyakMinKredit, SzakKovetelmenyDTO::setSzakmaigyakMinKredit
		);
	}

	default TextField createSzabvalMinKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.szabvalMinKredit",
				SzakKovetelmenyDTO::getSzabvalMinKredit, SzakKovetelmenyDTO::setSzabvalMinKredit
		);
	}

	default TextField createSzakiranyMinKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.szakiranyMinKredit",
				SzakKovetelmenyDTO::getSzakiranyMinKredit, SzakKovetelmenyDTO::setSzakiranyMinKredit
		);
	}

	default TextField createSzakiranyMaxKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.szakiranyMaxKredit",
				SzakKovetelmenyDTO::getSzakiranyMaxKredit, SzakKovetelmenyDTO::setSzakiranyMaxKredit
		);
	}

}
