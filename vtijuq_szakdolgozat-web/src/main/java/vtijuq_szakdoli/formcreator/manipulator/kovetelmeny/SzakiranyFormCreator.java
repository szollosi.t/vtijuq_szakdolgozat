package vtijuq_szakdoli.formcreator.manipulator.kovetelmeny;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public interface SzakiranyFormCreator extends ManipulatorFormCreator<SzakiranyDTO> {

	@Override
	default FormLayout createForm(final boolean readOnly) {
		final FormLayout form = new FormLayout();
		//TODO
		form.addComponent(createCodeField());
		form.addComponent(createNameField());
		form.addComponent(createDescriptionField());
		form.addComponent(createSzakField());
		getBinder().setReadOnly(readOnly);
		return form;
	}

	List<SzakDTO> getSzakList();

	default TextField createCodeField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.code",
				SzakiranyDTO::getKod, SzakiranyDTO::setKod);
	}

	default TextField createNameField() {
		return Toolbox.createAndBindRequiredTextField(
				getBinder(), "field.name",
				SzakiranyDTO::getNev, SzakiranyDTO::setNev
		);
	}

	default TextField createDescriptionField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.description",
				SzakiranyDTO::getLeiras, SzakiranyDTO::setLeiras);
	}

	default ComboBox<SzakDTO> createSzakField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.szak", getSzakList(),
				SzakiranyDTO::getSzak, SzakiranyDTO::setSzak
		);
	}

}
