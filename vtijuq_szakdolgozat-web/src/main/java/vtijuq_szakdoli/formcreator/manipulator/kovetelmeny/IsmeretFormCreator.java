package vtijuq_szakdoli.formcreator.manipulator.kovetelmeny;

import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public interface IsmeretFormCreator extends ManipulatorFormCreator<IsmeretDTO> {

	@Override
	default FormLayout createForm(final boolean readOnly) {
		final FormLayout form = new FormLayout();
		if (getFoIsmeret() != null) {
			form.addComponent(createFoIsmeretField());
		}
		form.addComponent(createNameField());
		form.addComponent(createDescriptionField());
		form.addComponent(createCodeField());

		getBinder().setReadOnly(readOnly);
		return form;
	}

	vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO getFoIsmeret();

	default TextField createFoIsmeretField() {
		return Toolbox.createReadonlyTextField(getFoIsmeret().getNev(), "field.foismeret");
	}

	default TextField createNameField() {
		return Toolbox.createAndBindRequiredTextField(
				getBinder(), "field.name",
				IsmeretDTO::getNev, IsmeretDTO::setNev
        );
	}

	default TextField createDescriptionField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.description",
				IsmeretDTO::getLeiras, IsmeretDTO::setLeiras);
	}

	default TextField createCodeField() {
		return Toolbox.createAndBindRequiredTextField(
				getBinder(), "field.code",
				IsmeretDTO::getKod, IsmeretDTO::setKod
        );
	}

}
