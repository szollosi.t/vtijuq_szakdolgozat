package vtijuq_szakdoli.formcreator.manipulator.kovetelmeny;

import java.util.List;

import com.vaadin.ui.ComboBox;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public interface SzakiranyKovetelmenyFormCreator extends ManipulatorFormCreator<SzakiranyKovetelmenyDTO> {

	List<SzakKovetelmenyDTO> getSzakKovetelmenyList();

	List<SzakiranyDTO> getSzakiranyList();

	default ComboBox<Boolean> createCsakkozosField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.csakkozos",
				SzakiranyKovetelmenyDTO::getCsakkozos, SzakiranyKovetelmenyDTO::setCsakkozos
        );
	}

	default ComboBox<SzakKovetelmenyDTO> createSzakKovetelmenyField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.szakKovetelmeny", getSzakKovetelmenyList(),
				SzakiranyKovetelmenyDTO::getSzakKovetelmeny, SzakiranyKovetelmenyDTO::setSzakKovetelmeny
		);
	}

	default ComboBox<SzakiranyDTO> createSzakiranyField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.szakirany", getSzakiranyList(),
				SzakiranyKovetelmenyDTO::getSzakirany, SzakiranyKovetelmenyDTO::setSzakirany
		);
	}

}
