package vtijuq_szakdoli.formcreator.manipulator.kovetelmeny;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.KepzesiTeruletDTO;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ciklus;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public interface SzakFormCreator extends ManipulatorFormCreator<SzakDTO> {

	List<KepzesiTeruletDTO> getKepzesiTeruletList();

	@Override
	default Layout createForm(final boolean readOnly) {
		final FormLayout form = new FormLayout();
		form.addComponent(createNameField());
		form.addComponent(createKepzesiTeruletField());
		form.addComponent(createCiklusField());
		getBinder().setReadOnly(readOnly);
		return form;
	}

	default TextField createNameField() {
		return Toolbox.createAndBindRequiredTextField(
				getBinder(), "field.name",
				SzakDTO::getNev, SzakDTO::setNev
		);
	}

	default ComboBox<KepzesiTeruletDTO> createKepzesiTeruletField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.kepzesiTerulet", getKepzesiTeruletList(),
				SzakDTO::getKepzesiTerulet, SzakDTO::setKepzesiTerulet
		);
	}

	default ComboBox<Ciklus> createCiklusField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.ciklus", Ciklus.class,
				SzakDTO::getCiklus, SzakDTO::setCiklus
		);
	}

}
