package vtijuq_szakdoli.formcreator.manipulator.adminisztracio;

import java.util.List;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface MunkatarsFormCreator extends ManipulatorFormCreator<MunkatarsDTO> {

	@Override
	default Layout createForm(final boolean readOnly) {
		final MultiColumnFormLayout form = new MultiColumnFormLayout();

		form.createFormLayout("adatok",
				createFelhasznalonevField(),
				createNameField(),
				createSzervezetField(),
				createSzerepkorField());

		form.createFormLayout("flagek",
				createAktivField(),
				createJelszovaltoztatasField());

		getBinder().setReadOnly(readOnly);
		return form;
	}

	List<SzervezetDTO> getSzervezetList();

	List<SzerepkorDTO> getSzerepkorList();

	default TextField createFelhasznalonevField() {
		return Toolbox.createAndBindRequiredTextField(
				getBinder(), "field.username",
				MunkatarsDTO::getFelhasznalonev, MunkatarsDTO::setFelhasznalonev
		);
	}

	default TextField createNameField() {
		return Toolbox.createAndBindRequiredTextField(
				getBinder(), "field.name",
				MunkatarsDTO::getNev, MunkatarsDTO::setNev
		);
	}

	default CheckBox createAktivField() {
		return Toolbox.createAndBindCheckBox(
				getBinder(), "field.active",
				MunkatarsDTO::isAktiv, MunkatarsDTO::setAktiv
        );
	}

	default ComboBox<SzervezetDTO> createSzervezetField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.organization", getSzervezetList(),
				MunkatarsDTO::getSzervezet, MunkatarsDTO::setSzervezet
		);
	}

	default ComboBox<SzerepkorDTO> createSzerepkorField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.role", getSzerepkorList(),
				MunkatarsDTO::getSzerepkor, MunkatarsDTO::setSzerepkor
		);
	}

	default CheckBox createJelszovaltoztatasField() {
		return Toolbox.createAndBindCheckBox(
				getBinder(), "field.jelszovaltoztatas",
				MunkatarsDTO::isJelszovaltoztatas, MunkatarsDTO::setJelszovaltoztatas
        );
	}

}
