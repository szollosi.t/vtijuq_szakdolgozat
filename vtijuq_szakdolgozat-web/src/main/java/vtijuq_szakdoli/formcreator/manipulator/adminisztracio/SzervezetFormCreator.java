package vtijuq_szakdoli.formcreator.manipulator.adminisztracio;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public interface SzervezetFormCreator extends ManipulatorFormCreator<SzervezetDTO> {

	@Override
	default FormLayout createForm(final boolean readOnly) {
		final FormLayout form = new FormLayout();
		if (getFoSzervezet() != null) {
			form.addComponent(createFoSzervezetField());
		}
		form.addComponent(createCodeField());
		form.addComponent(createNameField());
		form.addComponent(createRoviditesField());
		form.addComponent(createSzervezetTipusField());
		form.addComponent(createCimField());
		getBinder().setReadOnly(readOnly);
		return form;
	}

	List<SzervezetTipusDTO> getSzervezetTipusList();

	vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO getFoSzervezet();

	default TextField createFoSzervezetField() {
		return Toolbox.createReadonlyTextField(getFoSzervezet().getNev(), "field.foszervezet");
	}

	default TextField createNameField() {
		return Toolbox.createAndBindRequiredTextField(
				getBinder(), "field.name",
				SzervezetDTO::getNev, SzervezetDTO::setNev
		);
	}

	default TextField createRoviditesField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.rovidites",
				SzervezetDTO::getRovidites, SzervezetDTO::setRovidites);
	}

	default ComboBox<SzervezetTipusDTO> createSzervezetTipusField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.organizationType", getSzervezetTipusList(),
				SzervezetDTO::getSzervezetTipus, SzervezetDTO::setSzervezetTipus
		);
	}

	default TextField createCodeField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.code",
				SzervezetDTO::getKod, SzervezetDTO::setKod);
	}

	default TextField createCimField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.cim",
				SzervezetDTO::getCim, SzervezetDTO::setCim);
	}

}
