package vtijuq_szakdoli.formcreator.manipulator;

import com.vaadin.ui.Layout;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.formcreator.FormCreator;

public interface ManipulatorFormCreator<T extends EditableDTO> extends FormCreator<T> {

	default void setReadOnly(final boolean readOnly) {
		getBinder().setReadOnly(readOnly);
	}

	default boolean validateForm() {
		//TODO jól jelöli, hogy kivel van probléma?
//		final BinderValidationStatus<T> validationStatus = getBinder().validate();
//		validationStatus.getBeanValidationErrors().forEach(
//				error -> Notification.show(error.getErrorMessage(), Notification.Type.ERROR_MESSAGE)
//		);
//		return validationStatus.isOk();
		return getBinder().isValid();
	}

	Layout createForm(final boolean readOnly);

}
