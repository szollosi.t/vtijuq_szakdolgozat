package vtijuq_szakdoli.formcreator.manipulator.megvalositas;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.ComplexFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface KonkretSzakiranyFormCreator extends ManipulatorFormCreator<KonkretSzakiranyDTO> {

	@Override
	default Layout createForm(final boolean readOnly) {
		final ComplexFormLayout form = new ComplexFormLayout(new HorizontalLayout());

		form.createContentFormLayout("alap",
				createKonkretSzakField(),
				createSzakiranyKovetelmenyField());

		form.createContentFormLayout("azon",
				createCodeField(),
				createNameField());

		getBinder().setReadOnly(readOnly);
		return form;
	}

	List<KonkretSzakDTO> getKonkretSzakList();

	List<SzakiranyKovetelmenyDTO> getSzakiranyKovetelmenyList();

	default TextField createNameField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.name",
				KonkretSzakiranyDTO::getNev, KonkretSzakiranyDTO::setNev);
	}

	default TextField createCodeField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.code",
				KonkretSzakiranyDTO::getKod, KonkretSzakiranyDTO::setKod);
	}

	default ComboBox<KonkretSzakDTO> createKonkretSzakField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.konkretSzak", getKonkretSzakList(),
				KonkretSzakiranyDTO::getKonkretSzak, KonkretSzakiranyDTO::setKonkretSzak
		);
	}

	default ComboBox<SzakiranyKovetelmenyDTO> createSzakiranyKovetelmenyField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.szakiranyKovetelmeny", getSzakiranyKovetelmenyList(),
				KonkretSzakiranyDTO::getSzakiranyKovetelmeny, KonkretSzakiranyDTO::setSzakiranyKovetelmeny
		);
	}

}
