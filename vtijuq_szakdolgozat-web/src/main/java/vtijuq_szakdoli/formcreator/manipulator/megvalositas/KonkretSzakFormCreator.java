package vtijuq_szakdoli.formcreator.manipulator.megvalositas;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.ComplexFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface KonkretSzakFormCreator extends ManipulatorFormCreator<KonkretSzakDTO> {

	@Override
	default Layout createForm(final boolean readOnly) {
		final ComplexFormLayout form = new ComplexFormLayout(new HorizontalLayout());

		form.createContentFormLayout("alap",
				createSzakKovetelmenyField(),
				createSzervezetField());

		form.createContentFormLayout("kreditek",
				createSzakiranyKreditField(),
				createSzabvalKreditField(),
				createSzakmaigyakKreditField());

		getBinder().setReadOnly(readOnly);
		return form;
	}

	List<SzakKovetelmenyDTO> getSzakKovetelmenyList();

	List<SzervezetDTO> getSzervezetList();

	default ComboBox<SzakKovetelmenyDTO> createSzakKovetelmenyField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.szakKovetelmeny", getSzakKovetelmenyList(),
				KonkretSzakDTO::getSzakKovetelmeny, KonkretSzakDTO::setSzakKovetelmeny
		);
	}

	default ComboBox<SzervezetDTO> createSzervezetField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.organization", getSzervezetList(),
				KonkretSzakDTO::getSzervezet, KonkretSzakDTO::setSzervezet
		);
	}

	default TextField createSzakmaigyakKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.szakmaigyakKredit",
				KonkretSzakDTO::getSzakmaigyakKredit, KonkretSzakDTO::setSzakmaigyakKredit
		);
	}

	default TextField createSzabvalKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.szabvalKredit",
				KonkretSzakDTO::getSzabvalKredit, KonkretSzakDTO::setSzabvalKredit
		);
	}

	default TextField createSzakiranyKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.szakiranyKredit",
				KonkretSzakDTO::getSzakiranyKredit, KonkretSzakDTO::setSzakiranyKredit
		);
	}

}
