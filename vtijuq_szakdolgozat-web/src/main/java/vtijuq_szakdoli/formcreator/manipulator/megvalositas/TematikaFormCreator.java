package vtijuq_szakdoli.formcreator.manipulator.megvalositas;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Layout;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.IsmeretSzintDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface TematikaFormCreator extends ManipulatorFormCreator<TematikaDTO> {

	@Override
	default Layout createForm(final boolean readOnly) {
		final MultiColumnFormLayout multiColForm = new MultiColumnFormLayout();
		multiColForm.createFormLayout("alap",
				createTemaField(),
				createLeadottSzintField(),
				createSzamonkertSzintField());

		multiColForm.createFormLayout("validInfo",
				createValStartField(),
				createValEndField(),
				createtValidField(),
				createVeglegesField());

		getBinder().setReadOnly(readOnly);
		return multiColForm;
	}

	List<TemaDTO> getTemaList();

	List<IsmeretSzintDTO> getLeadottSzintList();

	List<IsmeretSzintDTO> getSzamonkertSzintList();

	default ComboBox<TemaDTO> createTemaField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.tema", getTemaList(),
				TematikaDTO::getTema, TematikaDTO::setTema
		);
	}

	default ComboBox<IsmeretSzintDTO> createLeadottSzintField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.leadottSzint", getLeadottSzintList(),
				TematikaDTO::getLeadottSzint, TematikaDTO::setLeadottSzint
		);
	}

	default ComboBox<IsmeretSzintDTO> createSzamonkertSzintField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.szamonkertSzint", getSzamonkertSzintList(),
				TematikaDTO::getSzamonkertSzint, TematikaDTO::setSzamonkertSzint
		);
	}

	default DateField createValStartField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valStart",
				TematikaDTO::getErvenyessegKezdet, TematikaDTO::setErvenyessegKezdet);
	}

	default DateField createValEndField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valEnd",
				TematikaDTO::getErvenyessegVeg, TematikaDTO::setErvenyessegVeg);
	}

	default ComboBox<Boolean> createtValidField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.valid",
				TematikaDTO::isValid, TematikaDTO::setValid
        );
	}

	default ComboBox<Boolean> createVeglegesField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.vegleges",
				TematikaDTO::isVegleges, TematikaDTO::setVegleges
        );
	}

}
