package vtijuq_szakdoli.formcreator.manipulator.megvalositas;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface TantargyFormCreator extends ManipulatorFormCreator<TantargyDTO> {

	List<SzervezetDTO> getSzervezetList();

	@Override
	default Layout createForm(final boolean readOnly) {
		final MultiColumnFormLayout form = new MultiColumnFormLayout();

		form.createFormLayout("tantargy",
				createCodeField(),
				createNameField());

		form.createFormLayout("szerv",
				createSzervezetField());

		getBinder().setReadOnly(readOnly);
		return form;
	}

	default TextField createNameField() {
		return Toolbox.createAndBindRequiredTextField(
				getBinder(), "field.name",
				TantargyDTO::getNev, TantargyDTO::setNev
		);
	}

	default TextField createCodeField() {
		return Toolbox.createAndBindRequiredTextField(
				getBinder(), "field.code",
				TantargyDTO::getKod, TantargyDTO::setKod
		);
	}

	default ComboBox<SzervezetDTO> createSzervezetField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.organization", getSzervezetList(),
				TantargyDTO::getSzervezet, TantargyDTO::setSzervezet
        );
	}

}
