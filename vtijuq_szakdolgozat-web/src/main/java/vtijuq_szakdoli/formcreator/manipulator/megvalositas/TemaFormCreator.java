package vtijuq_szakdoli.formcreator.manipulator.megvalositas;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public interface TemaFormCreator extends ManipulatorFormCreator<TemaDTO> {

	@Override
	default FormLayout createForm(final boolean readOnly) {
		final FormLayout form = new FormLayout();
		form.addComponent(createNameField());
		form.addComponent(createDescriptionField());
		form.addComponent(createIsmeretField());
		getBinder().setReadOnly(readOnly);
		return form;
	}

	List<IsmeretDTO> getIsmeretList();

	default TextField createNameField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.name",
				TemaDTO::getNev, TemaDTO::setNev);
	}

	default TextField createDescriptionField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.description",
				TemaDTO::getLeiras, TemaDTO::setLeiras);
	}

	default ComboBox<IsmeretDTO> createIsmeretField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.ismeret", getIsmeretList(),
				TemaDTO::getIsmeret, TemaDTO::setIsmeret
		);
	}

}
