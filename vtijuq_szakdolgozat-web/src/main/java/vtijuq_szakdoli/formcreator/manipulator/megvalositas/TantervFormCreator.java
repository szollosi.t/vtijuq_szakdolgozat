package vtijuq_szakdoli.formcreator.manipulator.megvalositas;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.ErtekelesTipusDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.TantervStatusz;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.ComplexFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface TantervFormCreator extends ManipulatorFormCreator<TantervDTO> {

	@Override
	default Layout createForm(final boolean readOnly) {
		final HorizontalLayout complexFormKeyLayout = new HorizontalLayout();
		final ComboBox<KonkretSzakiranyDTO> konkretSzakiranyField = createKonkretSzakiranyField();
		konkretSzakiranyField.setWidth(50, PERCENTAGE);
		complexFormKeyLayout.addComponent(konkretSzakiranyField);

		final ComboBox<TantargyDTO> tantargyField = createTantargyField();
		tantargyField.setWidth(50, PERCENTAGE);
		complexFormKeyLayout.addComponent(tantargyField);

		final ComplexFormLayout complexFormLayout = new ComplexFormLayout(complexFormKeyLayout);

		complexFormLayout.createContentFormLayout(
				"alapadat",
				createFelevField(),
				createStatuszField(),
				createKreditField(),
				createErtekelesTipusField()
		);

		complexFormLayout.createContentFormLayout(
				"orak",
				createEaOraField(),
				createGyakOraField(),
				createLabOraField()
		);

		complexFormLayout.createContentFormLayout(
				"validInfo",
				createValStartField(),
				createValEndField(),
				createtValidField(),
				createVeglegesField()
		);

		getBinder().setReadOnly(readOnly);
		return complexFormLayout;
	}

	List<KonkretSzakiranyDTO> getKonkretSzakiranyList();

	List<TantargyDTO> getTantargyList();

	List<ErtekelesTipusDTO> getErtekelesTipusList();

	default DateField createValStartField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valStart",
				TantervDTO::getErvenyessegKezdet, TantervDTO::setErvenyessegKezdet);
	}

	default DateField createValEndField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valEnd",
				TantervDTO::getErvenyessegVeg, TantervDTO::setErvenyessegVeg);
	}

	default ComboBox<Boolean> createtValidField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.valid",
				TantervDTO::isValid, TantervDTO::setValid
        );
	}

	default ComboBox<Boolean> createVeglegesField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.vegleges",
				TantervDTO::isVegleges, TantervDTO::setVegleges
        );
	}

	default ComboBox<TantervStatusz> createStatuszField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.statusz", TantervStatusz.class,
				TantervDTO::getStatusz, TantervDTO::setStatusz
		);
	}

	default ComboBox<KonkretSzakiranyDTO> createKonkretSzakiranyField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.konkretSzakirany", getKonkretSzakiranyList(),
				TantervDTO::getKonkretSzakirany, TantervDTO::setKonkretSzakirany
		);
	}

	default ComboBox<TantargyDTO> createTantargyField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.tantargy", getTantargyList(),
				TantervDTO::getTantargy, TantervDTO::setTantargy
		);
	}

	default ComboBox<ErtekelesTipusDTO> createErtekelesTipusField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.ertekelesTipus", getErtekelesTipusList(),
				TantervDTO::getErtekelesTipus, TantervDTO::setErtekelesTipus
		);
	}

	default TextField createFelevField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.felev",
				TantervDTO::getFelev, TantervDTO::setFelev
		);
	}

	default TextField createKreditField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.kredit",
				TantervDTO::getKredit, TantervDTO::setKredit
		);
	}

	default TextField createEaOraField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.eaOra",
				TantervDTO::getEaOra, TantervDTO::setEaOra
		);
	}

	default TextField createGyakOraField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.gyakOra",
				TantervDTO::getGyakOra, TantervDTO::setGyakOra
		);
	}

	default TextField createLabOraField() {
		return Toolbox.createAndBindIntegerField(
				getBinder(), "field.labOra",
				TantervDTO::getLabOra, TantervDTO::setLabOra
		);
	}

}
