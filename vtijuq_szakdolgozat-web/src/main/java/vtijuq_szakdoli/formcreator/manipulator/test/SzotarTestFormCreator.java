package vtijuq_szakdoli.formcreator.manipulator.test;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public interface SzotarTestFormCreator extends ManipulatorFormCreator<SzotarTestDTO> {

	@Override
	default FormLayout createForm(final boolean readOnly) {
		FormLayout form = new FormLayout();
		if (getTipus() != null) {
			form.addComponent(createTipusField());
		}
		form.addComponent(createCodeField());
		form.addComponent(createNameField());
		form.addComponent(createDescriptionField());
		form.addComponent(createValStartField());
		form.addComponent(createValEndField());
		form.addComponent(createTechnikaiField());
		setReadOnly(readOnly);
		return form;
	}

	vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO getTipus();

	default TextField createTipusField() {
		return Toolbox.createReadonlyTextField("field.tipus", getTipus().getNev());
	}

	default TextField createCodeField() {
		return Toolbox.createAndBindRequiredTextField(
				getBinder(), "field.code",
				SzotarTestDTO::getKod, SzotarTestDTO::setKod
		);
	}

	default TextField createNameField() {
		return Toolbox.createAndBindRequiredTextField(
				getBinder(), "field.name",
				SzotarTestDTO::getNev, SzotarTestDTO::setNev
		);
	}

	default TextField createDescriptionField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.description",
				SzotarTestDTO::getLeiras, SzotarTestDTO::setLeiras);
	}

	default DateField createValStartField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valStart",
				SzotarTestDTO::getErvKezdet, SzotarTestDTO::setErvKezdet);
	}

	default DateField createValEndField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valEnd",
				SzotarTestDTO::getErvVeg, SzotarTestDTO::setErvVeg);
	}

	default CheckBox createTechnikaiField() {
		return Toolbox.createAndBindCheckBox(
				getBinder(), "field.technical",
				SzotarTestDTO::getTechnikai, SzotarTestDTO::setTechnikai
        );
	}
}
