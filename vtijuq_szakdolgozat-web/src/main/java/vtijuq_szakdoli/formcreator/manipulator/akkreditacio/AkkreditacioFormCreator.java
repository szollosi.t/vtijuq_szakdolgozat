package vtijuq_szakdoli.formcreator.manipulator.akkreditacio;

import java.util.List;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Layout;

import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface AkkreditacioFormCreator extends ManipulatorFormCreator<AkkreditacioDTO> {

	@Override
	default Layout createForm(final boolean readOnly) {
		final MultiColumnFormLayout form = new MultiColumnFormLayout();

		form.createFormLayout("szakirany",
				createKonkretSzakiranyField());

		form.createFormLayout("interval",
				createValStartField(),
				createValEndField());

		form.createFormLayout("validity",
				createtValidField(),
				createVeglegesField());

		getBinder().setReadOnly(readOnly);
		return form;
	}

	List<KonkretSzakiranyDTO> getKonkretSzakiranyList();

	default DateField createValStartField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valStart",
				AkkreditacioDTO::getErvenyessegKezdet, AkkreditacioDTO::setErvenyessegKezdet);
	}

	default DateField createValEndField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valEnd",
				AkkreditacioDTO::getErvenyessegVeg, AkkreditacioDTO::setErvenyessegVeg);
	}

	default ComboBox<Boolean> createtValidField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.valid",
				AkkreditacioDTO::isValid, AkkreditacioDTO::setValid
        );
	}

	default ComboBox<Boolean> createVeglegesField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.vegleges",
				AkkreditacioDTO::isVegleges, AkkreditacioDTO::setVegleges
        );
	}

	default ComboBox<KonkretSzakiranyDTO> createKonkretSzakiranyField() {
		return Toolbox.createAndBindRequiredComboBoxWithEmpty(
				getBinder(), "field.konkretSzakirany", getKonkretSzakiranyList(),
				AkkreditacioDTO::getKonkretSzakirany, AkkreditacioDTO::setKonkretSzakirany
		);
	}

}
