package vtijuq_szakdoli.formcreator.filter.adminisztracio;

import com.vaadin.data.Binder;
import lombok.Getter;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.common.dto.filterDTO.adminisztracio.SzervezetFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;

import java.util.Map;

public class SzervezetFilter implements SzervezetFilterFormCreator, FilterFormCreator<SzervezetFilterDTO> {

	private final SzervezetFilterDTO dto;
	@Getter
	private final Binder<SzervezetFilterDTO> binder;
	@Getter
	private final Map<Long, SzervezetTipusDTO> szervezetTipusMap;

	public SzervezetFilter(Map<Long, SzervezetTipusDTO> szervezetTipusMap) {
		dto = new SzervezetFilterDTO();
		this.szervezetTipusMap = szervezetTipusMap;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public SzervezetFilterDTO getDTO() {
		return dto;
	}

}
