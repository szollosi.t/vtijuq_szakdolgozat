package vtijuq_szakdoli.formcreator.filter.adminisztracio;

import java.util.Map;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.common.dto.filterDTO.adminisztracio.SzervezetFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface SzervezetFilterFormCreator extends FilterFormCreator<SzervezetFilterDTO> {

	Map<Long, SzervezetTipusDTO> getSzervezetTipusMap();

	@Override
	default Layout createForm(){
		final MultiColumnFormLayout filterForm = new MultiColumnFormLayout();

		filterForm.createFormLayout("szerv",
				createNameField(),
				createRoviditesField());

		filterForm.createFormLayout("tipus",
				createSzervezetTipusField());

		return filterForm;
	}

	default TextField createNameField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.name",
				SzervezetFilterDTO::getNev, SzervezetFilterDTO::setNev);
	}

	default TextField createRoviditesField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.rovidites",
				SzervezetFilterDTO::getRovidites, SzervezetFilterDTO::setRovidites);
	}

	default ComboBox<SzervezetTipusDTO> createSzervezetTipusField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.organizationType", getSzervezetTipusMap(),
				SzervezetFilterDTO::getIdSzervezetTipus, SzervezetFilterDTO::setIdSzervezetTipus
        );
	}
}
