package vtijuq_szakdoli.formcreator.filter.adminisztracio;

import com.vaadin.data.Binder;
import lombok.Getter;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.filterDTO.adminisztracio.MunkatarsFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;

import java.util.Map;

public class MunkatarsFilter implements MunkatarsFilterFormCreator, FilterFormCreator<MunkatarsFilterDTO> {

	private final MunkatarsFilterDTO dto;
	@Getter
	private final Binder<MunkatarsFilterDTO> binder;
	@Getter
	private final Map<Long, SzervezetDTO> szervezetMap;
	@Getter
	private final Map<Long, SzerepkorDTO> szerepkorMap;

	public MunkatarsFilter(Map<Long, SzervezetDTO> szervezetMap, Map<Long, SzerepkorDTO> szerepkorMap) {
		dto = new MunkatarsFilterDTO();
		this.szervezetMap = szervezetMap;
		this.szerepkorMap = szerepkorMap;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public MunkatarsFilterDTO getDTO() {
		return dto;
	}

}
