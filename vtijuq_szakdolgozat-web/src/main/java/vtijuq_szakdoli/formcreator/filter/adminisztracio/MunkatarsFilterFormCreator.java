package vtijuq_szakdoli.formcreator.filter.adminisztracio;

import java.util.Map;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.filterDTO.adminisztracio.MunkatarsFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface MunkatarsFilterFormCreator extends FilterFormCreator<MunkatarsFilterDTO> {

	Map<Long, SzervezetDTO> getSzervezetMap();

	Map<Long, SzerepkorDTO> getSzerepkorMap();

	@Override
	default Layout createForm(){
		final MultiColumnFormLayout filterForm = new MultiColumnFormLayout();

		filterForm.createFormLayout("alap",
				createFelhasznalonevField(),
				createNameField());

		filterForm.createFormLayout("role",
				createSzervezetField(),
				createSzerepkorField());

		filterForm.createFormLayout("active",
				createAktivField());

		return filterForm;
	}

	default TextField createFelhasznalonevField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.username",
				MunkatarsFilterDTO::getFelhasznalonev, MunkatarsFilterDTO::setFelhasznalonev);
	}

	default TextField createNameField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.name",
				MunkatarsFilterDTO::getNev, MunkatarsFilterDTO::setNev);
	}

	default ComboBox<Boolean> createAktivField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.active",
				MunkatarsFilterDTO::getAktiv, MunkatarsFilterDTO::setAktiv
        );
	}

	default ComboBox<SzervezetDTO> createSzervezetField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.organization", getSzervezetMap(),
				MunkatarsFilterDTO::getIdSzervezet, MunkatarsFilterDTO::setIdSzervezet
		);
	}

	default ComboBox<SzerepkorDTO> createSzerepkorField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.role", getSzerepkorMap(),
				MunkatarsFilterDTO::getIdSzerepkor, MunkatarsFilterDTO::setIdSzerepkor
		);
	}
}
