package vtijuq_szakdoli.formcreator.filter.akkreditacio;

import com.vaadin.data.Binder;
import lombok.Getter;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.filterDTO.akkreditacio.AkkreditacioFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;

import java.util.Map;

public class AkkreditacioFilter implements AkkreditacioFilterFormCreator, FilterFormCreator<AkkreditacioFilterDTO> {

	private final AkkreditacioFilterDTO dto;
	@Getter
	private final Binder<AkkreditacioFilterDTO> binder;
	@Getter
	private final Map<Long, KonkretSzakiranyDTO> konkretSzakiranyMap;

	public AkkreditacioFilter(Map<Long, KonkretSzakiranyDTO> konkretSzakiranyMap) {
		dto = new AkkreditacioFilterDTO();
		this.konkretSzakiranyMap = konkretSzakiranyMap;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public AkkreditacioFilterDTO getDTO() {
		return dto;
	}

}
