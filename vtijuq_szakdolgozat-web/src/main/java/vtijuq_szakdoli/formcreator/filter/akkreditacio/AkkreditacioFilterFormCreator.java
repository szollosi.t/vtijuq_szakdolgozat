package vtijuq_szakdoli.formcreator.filter.akkreditacio;

import java.util.Map;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Layout;

import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.filterDTO.akkreditacio.AkkreditacioFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface AkkreditacioFilterFormCreator extends FilterFormCreator<AkkreditacioFilterDTO> {

	Map<Long, KonkretSzakiranyDTO> getKonkretSzakiranyMap();

	@Override
	default Layout createForm(){
		final MultiColumnFormLayout filterForm = new MultiColumnFormLayout();

		filterForm.createFormLayout("szakirany",
				createKonkretSzakiranyField());

		filterForm.createFormLayout("interval",
				createValStartField(),
				createValEndField());

		filterForm.createFormLayout("validity",
				createtValidField(),
				createVeglegesField());

		return filterForm;
	}

	default DateField createValStartField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valStart",
				AkkreditacioFilterDTO::getErvenyessegKezdet, AkkreditacioFilterDTO::setErvenyessegKezdet);
	}

	default DateField createValEndField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valEnd",
				AkkreditacioFilterDTO::getErvenyessegVeg, AkkreditacioFilterDTO::setErvenyessegVeg);
	}

	default ComboBox<Boolean> createtValidField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.valid",
				AkkreditacioFilterDTO::getValid, AkkreditacioFilterDTO::setValid
        );
	}

	default ComboBox<Boolean> createVeglegesField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.vegleges",
				AkkreditacioFilterDTO::getVegleges, AkkreditacioFilterDTO::setVegleges
        );
	}

	default ComboBox<KonkretSzakiranyDTO> createKonkretSzakiranyField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.konkretSzakirany", getKonkretSzakiranyMap(),
				AkkreditacioFilterDTO::getIdKonkretSzakirany, AkkreditacioFilterDTO::setIdKonkretSzakirany
		);
	}
}
