package vtijuq_szakdoli.formcreator.filter.kovetelmeny;

import com.vaadin.data.Binder;
import lombok.Getter;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.IsmeretFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;

public class IsmeretFilter implements IsmeretFilterFormCreator, FilterFormCreator<IsmeretFilterDTO> {

	private final IsmeretFilterDTO dto;
	@Getter
	private final Binder<IsmeretFilterDTO> binder;

	public IsmeretFilter() {
		dto = new IsmeretFilterDTO();
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public IsmeretFilterDTO getDTO() {
		return dto;
	}

}
