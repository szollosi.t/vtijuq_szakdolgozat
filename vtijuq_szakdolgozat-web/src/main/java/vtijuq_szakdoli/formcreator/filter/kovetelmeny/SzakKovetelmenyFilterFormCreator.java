package vtijuq_szakdoli.formcreator.filter.kovetelmeny;

import java.util.Map;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.SzakKovetelmenyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface SzakKovetelmenyFilterFormCreator extends FilterFormCreator<SzakKovetelmenyFilterDTO> {

	Map<Long, SzakDTO> getSzakMap();

	@Override
	default Layout createForm(){
		final MultiColumnFormLayout filterForm = new MultiColumnFormLayout();

		filterForm.createFormLayout("alap",
				createOklevelMegjelolesField(),
				createSzakField());

		filterForm.createFormLayout("validInterval",
				createValStartField(),
				createValEndField());

		filterForm.createFormLayout("validInfo",
				createtValidField(),
				createVeglegesField());

		return filterForm;
	}

	default ComboBox<SzakDTO> createSzakField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.szak", getSzakMap(),
				SzakKovetelmenyFilterDTO::getIdSzak, SzakKovetelmenyFilterDTO::setIdSzak
		);
	}

	default DateField createValStartField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valStart",
				SzakKovetelmenyFilterDTO::getErvenyessegKezdet, SzakKovetelmenyFilterDTO::setErvenyessegKezdet);
	}

	default DateField createValEndField() {
		return Toolbox.createAndBindDateField(
				getBinder(), "field.valEnd",
				SzakKovetelmenyFilterDTO::getErvenyessegVeg, SzakKovetelmenyFilterDTO::setErvenyessegVeg);
	}

	default ComboBox<Boolean> createtValidField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.valid",
				SzakKovetelmenyFilterDTO::getValid, SzakKovetelmenyFilterDTO::setValid
        );
	}

	default ComboBox<Boolean> createVeglegesField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.vegleges",
				SzakKovetelmenyFilterDTO::getVegleges, SzakKovetelmenyFilterDTO::setVegleges
        );
	}

	default TextField createOklevelMegjelolesField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.oklevelMegjeloles",
				SzakKovetelmenyFilterDTO::getOklevelMegjeloles, SzakKovetelmenyFilterDTO::setOklevelMegjeloles);
	}
}
