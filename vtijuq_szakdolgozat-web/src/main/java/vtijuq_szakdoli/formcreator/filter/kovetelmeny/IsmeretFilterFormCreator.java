package vtijuq_szakdoli.formcreator.filter.kovetelmeny;

import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.IsmeretFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface IsmeretFilterFormCreator extends FilterFormCreator<IsmeretFilterDTO> {

	@Override
	default Layout createForm(){
		final MultiColumnFormLayout filterForm = new MultiColumnFormLayout();

		filterForm.createFormLayout("kod", createCodeField());

		filterForm.createFormLayout("nev", createNameField());

		filterForm.createFormLayout("leiras", createDescriptionField());

		return filterForm;
	}

	default TextField createNameField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.name",
				IsmeretFilterDTO::getNev, IsmeretFilterDTO::setNev);
	}

	default TextField createDescriptionField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.description",
				IsmeretFilterDTO::getLeiras, IsmeretFilterDTO::setLeiras);
	}

	default TextField createCodeField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.code",
				IsmeretFilterDTO::getKod, IsmeretFilterDTO::setKod);
	}
}
