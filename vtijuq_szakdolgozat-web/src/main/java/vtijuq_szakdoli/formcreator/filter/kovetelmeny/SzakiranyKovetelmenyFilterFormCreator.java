package vtijuq_szakdoli.formcreator.filter.kovetelmeny;

import java.util.Map;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Layout;

import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.SzakiranyKovetelmenyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface SzakiranyKovetelmenyFilterFormCreator extends FilterFormCreator<SzakiranyKovetelmenyFilterDTO> {

	Map<Long, SzakKovetelmenyDTO> getSzakKovetelmenyMap();

	Map<Long, SzakiranyDTO> getSzakiranyMap();

	@Override
	default Layout createForm(){
		final MultiColumnFormLayout filterForm = new MultiColumnFormLayout();

		filterForm.createFormLayout("szak",
				createSzakKovetelmenyField());
		filterForm.createFormLayout("szakirany",
				createSzakiranyField());
		filterForm.createFormLayout("csakkozos",
				createCsakkozosField());

		return filterForm;
	}

	default ComboBox<Boolean> createCsakkozosField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.csakkozos",
				SzakiranyKovetelmenyFilterDTO::getCsakkozos, SzakiranyKovetelmenyFilterDTO::setCsakkozos
        );
	}

	default ComboBox<SzakKovetelmenyDTO> createSzakKovetelmenyField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.szakKovetelmeny", getSzakKovetelmenyMap(),
				SzakiranyKovetelmenyFilterDTO::getIdSzakKovetelmeny, SzakiranyKovetelmenyFilterDTO::setIdSzakKovetelmeny
		);
	}

	default ComboBox<SzakiranyDTO> createSzakiranyField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.szakirany", getSzakiranyMap(),
				SzakiranyKovetelmenyFilterDTO::getIdSzakirany, SzakiranyKovetelmenyFilterDTO::setIdSzakirany
		);
	}
}
