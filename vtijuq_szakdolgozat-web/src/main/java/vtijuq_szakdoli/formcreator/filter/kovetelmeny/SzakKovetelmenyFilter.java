package vtijuq_szakdoli.formcreator.filter.kovetelmeny;

import com.vaadin.data.Binder;
import lombok.Getter;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.SzakKovetelmenyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;

import java.util.Map;

public class SzakKovetelmenyFilter implements SzakKovetelmenyFilterFormCreator, FilterFormCreator<SzakKovetelmenyFilterDTO> {

	private final SzakKovetelmenyFilterDTO dto;
	@Getter
	private final Binder<SzakKovetelmenyFilterDTO> binder;
	@Getter
	private final Map<Long, SzakDTO> szakMap;

	public SzakKovetelmenyFilter(Map<Long, SzakDTO> szakMap) {
		dto = new SzakKovetelmenyFilterDTO();
		this.szakMap = szakMap;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public SzakKovetelmenyFilterDTO getDTO() {
		return dto;
	}

}
