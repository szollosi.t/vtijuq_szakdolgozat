package vtijuq_szakdoli.formcreator.filter.kovetelmeny;

import com.vaadin.data.Binder;
import lombok.Getter;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny.SzakiranyKovetelmenyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;

import java.util.Map;

public class SzakiranyKovetelmenyFilter implements SzakiranyKovetelmenyFilterFormCreator, FilterFormCreator<SzakiranyKovetelmenyFilterDTO> {

	private final SzakiranyKovetelmenyFilterDTO dto;
	@Getter
	private final Binder<SzakiranyKovetelmenyFilterDTO> binder;
	@Getter
	private final Map<Long, SzakKovetelmenyDTO> szakKovetelmenyMap;
	@Getter
	private final Map<Long, SzakiranyDTO> szakiranyMap;

	public SzakiranyKovetelmenyFilter(Map<Long, SzakKovetelmenyDTO> szakKovetelmenyMap, Map<Long, SzakiranyDTO> szakiranyMap) {
		dto = new SzakiranyKovetelmenyFilterDTO();
		this.szakKovetelmenyMap = szakKovetelmenyMap;
		this.szakiranyMap = szakiranyMap;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public SzakiranyKovetelmenyFilterDTO getDTO() {
		return dto;
	}

}
