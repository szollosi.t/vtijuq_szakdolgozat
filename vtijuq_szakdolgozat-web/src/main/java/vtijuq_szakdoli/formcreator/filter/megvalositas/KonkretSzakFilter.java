package vtijuq_szakdoli.formcreator.filter.megvalositas;

import com.vaadin.data.Binder;
import lombok.Getter;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.KonkretSzakFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;

import java.util.Map;

public class KonkretSzakFilter implements KonkretSzakFilterFormCreator, FilterFormCreator<KonkretSzakFilterDTO> {

	private final KonkretSzakFilterDTO dto;
	@Getter
	private final Binder<KonkretSzakFilterDTO> binder;
	@Getter
	private final Map<Long, SzakKovetelmenyDTO> szakKovetelmenyMap;
	@Getter
	private final Map<Long, SzervezetDTO> szervezetMap;

	public KonkretSzakFilter(Map<Long, SzakKovetelmenyDTO> szakKovetelmenyMap, Map<Long, SzervezetDTO> szervezetMap) {
		dto = new KonkretSzakFilterDTO();
		this.szakKovetelmenyMap = szakKovetelmenyMap;
		this.szervezetMap = szervezetMap;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public KonkretSzakFilterDTO getDTO() {
		return dto;
	}

}
