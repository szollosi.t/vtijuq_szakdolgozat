package vtijuq_szakdoli.formcreator.filter.megvalositas;

import com.vaadin.data.Binder;
import lombok.Getter;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.KonkretSzakiranyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;

import java.util.Map;

public class KonkretSzakiranyFilter implements KonkretSzakiranyFilterFormCreator, FilterFormCreator<KonkretSzakiranyFilterDTO> {

	private final KonkretSzakiranyFilterDTO dto;
	@Getter
	private final Binder<KonkretSzakiranyFilterDTO> binder;
	@Getter
	private final Map<Long, KonkretSzakDTO> konkretSzakMap;
	@Getter
	private final Map<Long, SzakiranyKovetelmenyDTO> szakiranyKovetelmenyMap;

	public KonkretSzakiranyFilter(Map<Long, KonkretSzakDTO> konkretSzakMap, Map<Long, SzakiranyKovetelmenyDTO> szakiranyKovetelmenyMap) {
		dto = new KonkretSzakiranyFilterDTO();
		this.konkretSzakMap = konkretSzakMap;
		this.szakiranyKovetelmenyMap = szakiranyKovetelmenyMap;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public KonkretSzakiranyFilterDTO getDTO() {
		return dto;
	}
}
