package vtijuq_szakdoli.formcreator.filter.megvalositas;

import java.util.Map;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.KonkretSzakiranyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface KonkretSzakiranyFilterFormCreator extends FilterFormCreator<KonkretSzakiranyFilterDTO> {

	Map<Long, KonkretSzakDTO> getKonkretSzakMap();

	Map<Long, SzakiranyKovetelmenyDTO> getSzakiranyKovetelmenyMap();

	@Override
	default Layout createForm(){
		final MultiColumnFormLayout filterForm = new MultiColumnFormLayout();
		filterForm.createFormLayout("szak",
				createKonkretSzakField(),
				createSzakiranyKovetelmenyField());
		filterForm.createFormLayout("szakirany",
				createCodeField(),
				createNameField());
		return filterForm;
	}

	default TextField createNameField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.name",
				KonkretSzakiranyFilterDTO::getNev, KonkretSzakiranyFilterDTO::setNev);
	}

	default TextField createCodeField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.code",
				KonkretSzakiranyFilterDTO::getKod, KonkretSzakiranyFilterDTO::setKod);
	}

	default ComboBox<KonkretSzakDTO> createKonkretSzakField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.konkretSzak", getKonkretSzakMap(),
				KonkretSzakiranyFilterDTO::getIdKonkretSzak, KonkretSzakiranyFilterDTO::setIdKonkretSzak
        );
	}

	default ComboBox<SzakiranyKovetelmenyDTO> createSzakiranyKovetelmenyField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.szakiranyKovetelmeny", getSzakiranyKovetelmenyMap(),
				KonkretSzakiranyFilterDTO::getIdSzakiranyKovetelmeny, KonkretSzakiranyFilterDTO::setIdSzakiranyKovetelmeny
        );
	}
}
