package vtijuq_szakdoli.formcreator.filter.megvalositas;

import java.util.Map;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Layout;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.KonkretSzakFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface KonkretSzakFilterFormCreator extends FilterFormCreator<KonkretSzakFilterDTO> {

	Map<Long, SzakKovetelmenyDTO> getSzakKovetelmenyMap();

	Map<Long, SzervezetDTO> getSzervezetMap();

	@Override
	default Layout createForm(){
		final MultiColumnFormLayout filterForm = new MultiColumnFormLayout();
		filterForm.createFormLayout("szak", createSzakKovetelmenyField());
		filterForm.createFormLayout("szerv", createSzervezetField());
		return filterForm;
	}

	default ComboBox<SzakKovetelmenyDTO> createSzakKovetelmenyField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.szakKovetelmeny", getSzakKovetelmenyMap(),
				KonkretSzakFilterDTO::getIdSzakKovetelmeny, KonkretSzakFilterDTO::setIdSzakKovetelmeny
        );
	}

	default ComboBox<SzervezetDTO> createSzervezetField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.organization", getSzervezetMap(),
				KonkretSzakFilterDTO::getIdSzervezet, KonkretSzakFilterDTO::setIdSzervezet
        );
	}
}
