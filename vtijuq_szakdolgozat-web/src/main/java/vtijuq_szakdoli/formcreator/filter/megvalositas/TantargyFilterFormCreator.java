package vtijuq_szakdoli.formcreator.filter.megvalositas;

import java.util.Map;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.TantargyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;
import vtijuq_szakdoli.utils.MultiColumnFormLayout;
import vtijuq_szakdoli.utils.Toolbox;

public interface TantargyFilterFormCreator extends FilterFormCreator<TantargyFilterDTO> {

	@Override
	default Layout createForm(){
		final MultiColumnFormLayout filterForm = new MultiColumnFormLayout();

		filterForm.createFormLayout("tantargy",
				createCodeField(),
				createNameField());

		filterForm.createFormLayout("szerv",
				createSzervezetField());

		return filterForm;
	}

	Map<Long, SzervezetDTO> getSzervezetMap();

	default TextField createNameField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.name",
				TantargyFilterDTO::getNev, TantargyFilterDTO::setNev);
	}

	default TextField createCodeField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.code",
				TantargyFilterDTO::getKod, TantargyFilterDTO::setKod);
	}

	default ComboBox<SzervezetDTO> createSzervezetField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.organization", getSzervezetMap(),
				TantargyFilterDTO::getIdSzervezet, TantargyFilterDTO::setIdSzervezet
        );
	}
}
