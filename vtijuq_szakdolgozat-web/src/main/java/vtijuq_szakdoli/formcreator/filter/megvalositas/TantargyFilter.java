package vtijuq_szakdoli.formcreator.filter.megvalositas;

import com.vaadin.data.Binder;
import lombok.Getter;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.filterDTO.megvalositas.TantargyFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;

import java.util.Map;

public class TantargyFilter implements TantargyFilterFormCreator, FilterFormCreator<TantargyFilterDTO> {

	private final TantargyFilterDTO dto;
	@Getter
	private final Binder<TantargyFilterDTO> binder;
	@Getter
	private final Map<Long, SzervezetDTO> szervezetMap;

	public TantargyFilter(Map<Long, SzervezetDTO> szervezetMap) {
		dto = new TantargyFilterDTO();
		this.szervezetMap = szervezetMap;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public TantargyFilterDTO getDTO() {
		return dto;
	}

}
