package vtijuq_szakdoli.formcreator.filter;

import com.vaadin.ui.Layout;

import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.formcreator.FormCreator;

public interface FilterFormCreator<T extends FilterDTO> extends FormCreator<T> {

	Layout createForm();

	default boolean hasConditions(){
		return getDTO().hasConditions();
	}

	default void clear(){
		getDTO().clear();
	}

}
