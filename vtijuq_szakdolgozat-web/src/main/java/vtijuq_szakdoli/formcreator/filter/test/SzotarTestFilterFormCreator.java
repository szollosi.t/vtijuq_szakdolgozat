package vtijuq_szakdoli.formcreator.filter.test;

import java.util.Map;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

import vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.dto.filterDTO.test.SzotarTestFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public interface SzotarTestFilterFormCreator extends FilterFormCreator<SzotarTestFilterDTO> {

	@Override
	default FormLayout createForm() {
		final FormLayout filterForm = new FormLayout();
		filterForm.addComponent(createTipusField());
		filterForm.addComponent(createCodeField());
		filterForm.addComponent(createNameField());
		filterForm.addComponent(createDescriptionField());
		filterForm.addComponent(createTechnikaiField());
		return filterForm;
	}

	Map<Long, SzotarTestDTO> getTipusMap();

	default TextField createCodeField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.code",
				SzotarTestFilterDTO::getKod, SzotarTestFilterDTO::setKod);
	}

	default TextField createNameField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.name",
				SzotarTestFilterDTO::getNev, SzotarTestFilterDTO::setNev);
	}

	default TextField createDescriptionField() {
		return Toolbox.createAndBindTextField(
				getBinder(), "field.description",
				SzotarTestFilterDTO::getLeiras, SzotarTestFilterDTO::setLeiras);
	}

	default ComboBox<SzotarTestDTO> createTipusField() {
		return Toolbox.createAndBindComboBoxWithEmpty(
				getBinder(), "field.tipus", getTipusMap(),
				SzotarTestFilterDTO::getIdTipus, SzotarTestFilterDTO::setIdTipus
		);
	}

	default ComboBox<Boolean> createTechnikaiField() {
		return Toolbox.createAndBindBooleanComboBox(
				getBinder(), "field.technical",
				SzotarTestFilterDTO::getTechnikai, SzotarTestFilterDTO::setTechnikai
        );
	}
}
