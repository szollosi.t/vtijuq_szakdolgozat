package vtijuq_szakdoli.formcreator.filter.test;

import com.vaadin.data.Binder;
import lombok.Getter;
import vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.dto.filterDTO.test.SzotarTestFilterDTO;
import vtijuq_szakdoli.formcreator.filter.FilterFormCreator;

import java.util.Map;

public class SzotarTestFilter implements SzotarTestFilterFormCreator, FilterFormCreator<SzotarTestFilterDTO> {

	private final SzotarTestFilterDTO dto;
	@Getter
	private final Binder<SzotarTestFilterDTO> binder;
	@Getter
	private final Map<Long, SzotarTestDTO> tipusMap;

	public SzotarTestFilter(final Map<Long, SzotarTestDTO> tipusMap) {
		dto = new SzotarTestFilterDTO();
		this.tipusMap = tipusMap;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public SzotarTestFilterDTO getDTO() {
		return dto;
	}
}
