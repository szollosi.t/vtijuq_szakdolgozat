package vtijuq_szakdoli.formcreator;

import com.vaadin.data.Binder;

import vtijuq_szakdoli.common.dto.DTO;

public interface FormCreator<T extends DTO> {

	T getDTO();

	Binder<T> getBinder();

}
