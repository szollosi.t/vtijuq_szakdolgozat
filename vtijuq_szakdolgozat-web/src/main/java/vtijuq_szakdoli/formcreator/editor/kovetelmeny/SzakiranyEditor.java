package vtijuq_szakdoli.formcreator.editor.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.manipulator.editor.kovetelmeny.SzakiranyDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.SzakiranyFormCreator;

public class SzakiranyEditor extends SzakiranyDTOEditor implements SzakiranyFormCreator, EditorFormCreator<SzakiranyDTO> {

	@Getter
	private Binder<SzakiranyDTO> binder;
	@Getter
	private List<SzakDTO> szakList;

	public SzakiranyEditor(SzakiranyDTO dto, Consumer<SzakiranyDTO> editedDTOHandler, List<SzakDTO> szakList) {
		super(dto, editedDTOHandler);
		this.szakList = szakList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
