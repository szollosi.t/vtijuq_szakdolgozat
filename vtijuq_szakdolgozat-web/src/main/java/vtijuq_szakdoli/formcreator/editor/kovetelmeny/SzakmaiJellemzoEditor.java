package vtijuq_szakdoli.formcreator.editor.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.manipulator.editor.kovetelmeny.SzakmaiJellemzoDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.SzakmaiJellemzoFormCreator;

public class SzakmaiJellemzoEditor extends SzakmaiJellemzoDTOEditor implements SzakmaiJellemzoFormCreator, EditorFormCreator<SzakmaiJellemzoDTO> {

	@Getter
	private Binder<SzakmaiJellemzoDTO> binder;
	@Getter
	private List<IsmeretDTO> ismeretList;
	@Getter
	private vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO foJellemzo;

	public SzakmaiJellemzoEditor(SzakmaiJellemzoDTO dto, Consumer<SzakmaiJellemzoDTO> editedDTOHandler, List<IsmeretDTO> ismeretList) {
		super(dto, editedDTOHandler);
		this.ismeretList = ismeretList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public SzakmaiJellemzoEditor(SzakmaiJellemzoDTO dto, Consumer<SzakmaiJellemzoDTO> editedDTOHandler, List<IsmeretDTO> ismeretList, vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO foJellemzo) {
		this(dto, editedDTOHandler, ismeretList);
		this.foJellemzo = foJellemzo;
	}

}
