package vtijuq_szakdoli.formcreator.editor.kovetelmeny;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import com.vaadin.data.Binder;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.KepzesiTeruletDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.manipulator.editor.kovetelmeny.SzakDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.SzakFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public class SzakEditor extends SzakDTOEditor implements SzakFormCreator, EditorFormCreator<SzakDTO> {

	@Getter
	private Binder<SzakDTO> binder;
	@Getter
	private List<KepzesiTeruletDTO> kepzesiTeruletList;

	public SzakEditor(SzakDTO dto, Consumer<SzakDTO> editedDTOHandler, List<KepzesiTeruletDTO> kepzesiTeruletList) {
		super(dto, editedDTOHandler);
		this.kepzesiTeruletList = kepzesiTeruletList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	//TODO???
	public Layout createSzakiranyokLayout(final Function<String, String> captionResolver){
		final VerticalLayout szakiranyokLayout = new VerticalLayout();
		final Grid<SzakiranyDTO> grid = new Grid<>();
		Toolbox.addColumn(grid, "field.name", SzakiranyDTO::getNev);
		Toolbox.addColumn(grid, "field.description", SzakiranyDTO::getLeiras);

		grid.setDataProvider(DataProvider.ofCollection(getDTO().getSzakiranyok()));
		szakiranyokLayout.addComponent(grid);
		szakiranyokLayout.setExpandRatio(grid, 1);
		szakiranyokLayout.setWidth(100, PERCENTAGE);

		return szakiranyokLayout;
	}
}
