package vtijuq_szakdoli.formcreator.editor.kovetelmeny;

import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.manipulator.editor.kovetelmeny.IsmeretDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.IsmeretFormCreator;

public class IsmeretEditor extends IsmeretDTOEditor implements IsmeretFormCreator, EditorFormCreator<IsmeretDTO> {

	@Getter
	private Binder<IsmeretDTO> binder;
	@Getter
	private vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO foIsmeret;

	public IsmeretEditor(IsmeretDTO dto, Consumer<IsmeretDTO> editedDTOHandler) {
		super(dto, editedDTOHandler);
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public IsmeretEditor(IsmeretDTO dto, Consumer<IsmeretDTO> editedDTOHandler, vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO foIsmeret) {
		this(dto, editedDTOHandler);
		this.foIsmeret = foIsmeret;
	}

}
