package vtijuq_szakdoli.formcreator.editor.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.manipulator.editor.kovetelmeny.SzakKovetelmenyDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.SzakKovetelmenyFormCreator;

public class SzakKovetelmenyEditor extends SzakKovetelmenyDTOEditor implements SzakKovetelmenyFormCreator, EditorFormCreator<SzakKovetelmenyDTO> {

	@Getter
	private Binder<SzakKovetelmenyDTO> binder;
	@Getter
	private List<SzakDTO> szakList;

	public SzakKovetelmenyEditor(SzakKovetelmenyDTO dto, Consumer<SzakKovetelmenyDTO> editedDTOHandler, List<SzakDTO> szakList) {
		super(dto, editedDTOHandler);
		this.szakList = szakList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public SzakKovetelmenyEditor(SzakKovetelmenyDTO eredeti, SzakKovetelmenyDTO dto, Consumer<SzakKovetelmenyDTO> editedDTOHandler, List<SzakDTO> szakList) {
		super(eredeti, dto, editedDTOHandler);
		this.szakList = szakList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}
}
