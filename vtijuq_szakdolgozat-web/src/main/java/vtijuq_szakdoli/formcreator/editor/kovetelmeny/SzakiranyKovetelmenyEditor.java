package vtijuq_szakdoli.formcreator.editor.kovetelmeny;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TreeGrid;
import com.vaadin.ui.VerticalLayout;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyWithSzakmaiJellemzokDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.manipulator.editor.kovetelmeny.SzakiranyKovetelmenyDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.SzakiranyKovetelmenyFormCreator;
import vtijuq_szakdoli.utils.DataTransformer;
import vtijuq_szakdoli.utils.HierarchicalDataWrapper;
import vtijuq_szakdoli.utils.Toolbox;

public class SzakiranyKovetelmenyEditor extends SzakiranyKovetelmenyDTOEditor implements SzakiranyKovetelmenyFormCreator, EditorFormCreator<SzakiranyKovetelmenyDTO> {

	@Getter
	private Binder<SzakiranyKovetelmenyDTO> binder;
	@Getter
	private TreeGrid<HierarchicalDataWrapper<SzakmaiJellemzoDTO>> szakmaiJellemzoTreeGrid;
	@Getter
	private List<SzakKovetelmenyDTO> szakKovetelmenyList;
	@Getter
	private List<SzakiranyDTO> szakiranyList;
	@Getter
	private boolean alapSzakirany;

	@Override
	public SzakiranyKovetelmenyWithSzakmaiJellemzokDTO getDTO() {
		return (SzakiranyKovetelmenyWithSzakmaiJellemzokDTO)super.getDTO();
	}

	@Override
	public SzakiranyKovetelmenyWithSzakmaiJellemzokDTO getEredeti() {
		return (SzakiranyKovetelmenyWithSzakmaiJellemzokDTO)super.getEredeti();
	}

	public SzakiranyKovetelmenyEditor(SzakiranyKovetelmenyWithSzakmaiJellemzokDTO eredeti, SzakiranyKovetelmenyWithSzakmaiJellemzokDTO dto,
			Consumer<SzakiranyKovetelmenyDTO> editedDTOHandler, boolean alapSzakirany) {
		super(eredeti, dto, editedDTOHandler);
		this.alapSzakirany = alapSzakirany;
		this.szakKovetelmenyList = Collections.singletonList(dto.getSzakKovetelmeny());
		this.szakiranyList = Collections.singletonList(dto.getSzakirany());
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public SzakiranyKovetelmenyEditor(SzakiranyKovetelmenyWithSzakmaiJellemzokDTO dto, Consumer<SzakiranyKovetelmenyDTO> editedDTOHandler,
									  List<SzakKovetelmenyDTO> szakKovetelmenyList, List<SzakiranyDTO> szakiranyList) {
		super(dto, new SzakiranyKovetelmenyWithSzakmaiJellemzokDTO(dto), editedDTOHandler);
		this.alapSzakirany = false;
		this.szakKovetelmenyList = szakKovetelmenyList;
		this.szakiranyList = szakiranyList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public VerticalLayout createForm(boolean readOnly) {
		final VerticalLayout content = new VerticalLayout();
		if (!alapSzakirany) {
			final HorizontalLayout keyLayout = new HorizontalLayout();
			keyLayout.addComponent(createSzakKovetelmenyField());
			keyLayout.addComponent(createSzakiranyField());
			keyLayout.addComponent(createCsakkozosField());
			getBinder().setReadOnly(readOnly);
			content.addComponent(keyLayout);
		}

		szakmaiJellemzoTreeGrid = createSzakmaiJellemzoTree();
		content.addComponent(szakmaiJellemzoTreeGrid);
		return content;
	}

	private TreeGrid<HierarchicalDataWrapper<SzakmaiJellemzoDTO>> createSzakmaiJellemzoTree() {
		final TreeGrid<HierarchicalDataWrapper<SzakmaiJellemzoDTO>> treeGrid = new TreeGrid<>();

		Toolbox.addColumn(treeGrid, "field.name", wrapper -> wrapper.getData().getNev());
		Toolbox.addColumn(treeGrid, "field.ismeret", wrapper -> wrapper.getData().getIsmeret());
		Toolbox.addColumn(treeGrid, "field.minKredit", wrapper -> wrapper.getData().getMinKredit());
		Toolbox.addColumn(treeGrid, "field.maxKredit", wrapper -> wrapper.getData().getMaxKredit());
		Toolbox.addColumn(treeGrid, "field.minValaszt", wrapper -> wrapper.getData().getMinValaszt());

		treeGrid.setDataProvider(DataTransformer.toHierarchicalDataProvider(getDTO().getSzakmaiJellemzok()));
		treeGrid.setWidth(100, PERCENTAGE);
		return treeGrid;
	}

}
