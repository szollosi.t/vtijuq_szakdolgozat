package vtijuq_szakdoli.formcreator.editor.kovetelmeny;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.vaadin.ui.Accordion;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;

import vtijuq_szakdoli.common.dto.AbstractEntitasDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyWithSzakiranyokDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyWithSzakmaiJellemzokDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;

public class SzakKovetelmenyWithSzakiranyokEditor extends SzakKovetelmenyEditor {

	@Getter
	private SzakiranyKovetelmenyEditor alapSzakiranyKovetelmenyEditor;
	@Getter
	private List<SzakiranyKovetelmenyEditor> szakiranyKovetelmenyEditorok;

	private Consumer<SzakKovetelmenyWithSzakiranyokDTO> compositeDTOHandler;

	@Getter
	private Layout alapSzakiranyKovetelmenyLayout;

	public SzakKovetelmenyWithSzakiranyokEditor(SzakKovetelmenyWithSzakiranyokDTO eredeti, Consumer<SzakKovetelmenyWithSzakiranyokDTO> editedDTOHandler,
			List<SzakDTO> szakList) {
		super(eredeti, new SzakKovetelmenyWithSzakiranyokDTO(eredeti), null, szakList);
		alapSzakiranyKovetelmenyEditor = new SzakiranyKovetelmenyEditor(
				getEredeti().getAlapSzakiranyKovetelmeny(), getDTO().getAlapSzakiranyKovetelmeny(), null, true);
		//egyébként nem módosulnak elvileg, csak egységességi okokból használjuk itt a másolataikat
		final Map<Long, SzakiranyKovetelmenyWithSzakmaiJellemzokDTO> szakiranyKovetelmenyDTOMap = getDTO().getSzakiranyKovetelmenyek().stream()
				.collect(Collectors.toMap(AbstractEntitasDTO::getId, Function.identity()));
		if (CollectionUtils.isNotEmpty(getEredeti().getSzakiranyKovetelmenyek())) {
			szakiranyKovetelmenyEditorok = getEredeti().getSzakiranyKovetelmenyek().stream()
					.map(kszi -> new SzakiranyKovetelmenyEditor(kszi, szakiranyKovetelmenyDTOMap.get(kszi.getId()), null, false))
					.collect(Collectors.toList());
		} else {
			szakiranyKovetelmenyEditorok = Collections.emptyList();
		}
		//csak együttes handler van
		compositeDTOHandler = editedDTOHandler;
	}

	@Override
	public SzakKovetelmenyWithSzakiranyokDTO getEredeti() {
		return (SzakKovetelmenyWithSzakiranyokDTO)super.getEredeti();
	}

	@Override
	public SzakKovetelmenyWithSzakiranyokDTO getDTO() {
		return (SzakKovetelmenyWithSzakiranyokDTO)super.getDTO();
	}

	@Override
	public Consumer<SzakKovetelmenyDTO> getDTOHandler() {
		//a getDTO-val szinkronban specializált DTO-osztályt fogyaszt
		//TODO szebb megoldás
		return (ksz) -> compositeDTOHandler.accept((SzakKovetelmenyWithSzakiranyokDTO)ksz);
	}

	@Override
	public void edit() {
		super.edit();
		alapSzakiranyKovetelmenyEditor.edit();
	}

	@Override
	public boolean validateForm() {
		return super.validateForm() && alapSzakiranyKovetelmenyEditor.validateForm();
	}

	@Override
	public Layout createForm(boolean readOnly) {
		final VerticalLayout content = new VerticalLayout();

		final Layout szakKovetelmenyLayout = super.createForm(readOnly);
		content.addComponent(szakKovetelmenyLayout);

		Accordion szakiranyokAccordion = new Accordion();
		//alapszakirany az első lapon
		alapSzakiranyKovetelmenyLayout = alapSzakiranyKovetelmenyEditor.createForm(readOnly);
		szakiranyokAccordion.addTab(alapSzakiranyKovetelmenyLayout, alapSzakiranyKovetelmenyEditor.getDTO().getNev());
		//többi szakirány a további lapokon
		szakiranyKovetelmenyEditorok.forEach(szike -> {
			final VerticalLayout szakiranyKovetelmenyLayout = szike.createForm(true);
			szakiranyokAccordion.addTab(szakiranyKovetelmenyLayout, szike.getDTO().getNev());
		});
		content.addComponent(szakiranyokAccordion);
		return content;
	}
}
