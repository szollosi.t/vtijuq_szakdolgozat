package vtijuq_szakdoli.formcreator.editor.test;

import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.manipulator.editor.test.SzotarTestDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.test.SzotarTestFormCreator;

public class SzotarTestEditor extends SzotarTestDTOEditor implements SzotarTestFormCreator, EditorFormCreator<SzotarTestDTO> {

	@Getter
	private Binder<SzotarTestDTO> binder;
	@Getter
	private vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO tipus;

	public SzotarTestEditor(SzotarTestDTO dto, Consumer<SzotarTestDTO> editedDTOHandler) {
		super(dto, editedDTOHandler);
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public SzotarTestEditor(SzotarTestDTO dto, Consumer<SzotarTestDTO> editedDTOHandler, vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO tipus) {
		this(dto, editedDTOHandler);
		this.tipus = tipus;
	}
}
