package vtijuq_szakdoli.formcreator.editor.akkreditacio;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.manipulator.editor.akkreditacio.AkkreditacioDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.akkreditacio.AkkreditacioFormCreator;

public class AkkreditacioEditor extends AkkreditacioDTOEditor implements AkkreditacioFormCreator, EditorFormCreator<AkkreditacioDTO> {

	@Getter
	private Binder<AkkreditacioDTO> binder;
	@Getter
	private List<KonkretSzakiranyDTO> konkretSzakiranyList;

	public AkkreditacioEditor(AkkreditacioDTO dto, Consumer<AkkreditacioDTO> editedDTOHandler, List<KonkretSzakiranyDTO> konkretSzakiranyList) {
		super(dto, editedDTOHandler);
		this.konkretSzakiranyList = konkretSzakiranyList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
