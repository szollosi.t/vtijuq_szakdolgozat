package vtijuq_szakdoli.formcreator.editor;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;

/**
 * Generic DTO editor class.
 * @param <T> the type of entity, this editor can edit. By cconvention, subclasses should implement it also.
 */
public interface EditorFormCreator<T extends EditableDTO> extends DTOEditor<T>, ManipulatorFormCreator<T> {

	@Override
	default void edit() {
		DTOEditor.super.edit();
		getBinder().writeBeanIfValid(getEredeti());
	}

}
