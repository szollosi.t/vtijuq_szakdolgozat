package vtijuq_szakdoli.formcreator.editor.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.IsmeretSzintDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.manipulator.editor.megvalositas.TematikaDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.TematikaFormCreator;

public class TematikaEditor extends TematikaDTOEditor implements TematikaFormCreator, EditorFormCreator<TematikaDTO> {

	@Getter
	private Binder<TematikaDTO> binder;
	@Getter
	private List<IsmeretSzintDTO> szamonkertSzintList;
	@Getter
	private List<IsmeretSzintDTO> leadottSzintList;
	@Getter
	private List<TemaDTO> temaList;

	public TematikaEditor(TematikaDTO dto, Consumer<TematikaDTO> editedDTOHandler, List<IsmeretSzintDTO> szamonkertSzintList, List<IsmeretSzintDTO> leadottSzintList, List<TemaDTO> temaList) {
		super(dto, editedDTOHandler);
		this.szamonkertSzintList = szamonkertSzintList;
		this.leadottSzintList = leadottSzintList;
		this.temaList = temaList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
