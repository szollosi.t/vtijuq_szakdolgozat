package vtijuq_szakdoli.formcreator.editor.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.manipulator.editor.megvalositas.KonkretSzakDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.KonkretSzakFormCreator;

public class KonkretSzakEditor extends KonkretSzakDTOEditor implements KonkretSzakFormCreator, EditorFormCreator<KonkretSzakDTO> {

	@Getter
	private Binder<KonkretSzakDTO> binder;
	@Getter
	private List<SzakKovetelmenyDTO> szakKovetelmenyList;
	@Getter
	private List<SzervezetDTO> szervezetList;

	public KonkretSzakEditor(KonkretSzakDTO eredeti, Consumer<KonkretSzakDTO> editedDTOHandler, List<SzakKovetelmenyDTO> szakKovetelmenyList, List<SzervezetDTO> szervezetList) {
		super(eredeti, editedDTOHandler);
		this.szakKovetelmenyList = szakKovetelmenyList;
		this.szervezetList = szervezetList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public KonkretSzakEditor(KonkretSzakDTO eredeti, KonkretSzakDTO dto, Consumer<KonkretSzakDTO> editedDTOHandler, List<SzakKovetelmenyDTO> szakKovetelmenyList, List<SzervezetDTO> szervezetList) {
		super(eredeti, dto, editedDTOHandler);
		this.szakKovetelmenyList = szakKovetelmenyList;
		this.szervezetList = szervezetList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
