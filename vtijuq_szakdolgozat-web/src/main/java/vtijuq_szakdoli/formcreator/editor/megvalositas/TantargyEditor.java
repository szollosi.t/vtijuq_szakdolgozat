package vtijuq_szakdoli.formcreator.editor.megvalositas;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.text.DateFormat;
import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.DateRenderer;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyWithTantervekAndTematikakDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.manipulator.editor.megvalositas.TantargyDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.TantargyFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public class TantargyEditor extends TantargyDTOEditor implements TantargyFormCreator, EditorFormCreator<TantargyDTO> {

	@Getter
	private Binder<TantargyDTO> binder;
	@Getter
	private List<SzervezetDTO> szervezetList;
	@Getter
	private Grid<TematikaDTO> tematikakGrid;
	@Getter
	private Grid<TantervDTO> tantervekGrid;

	@Override
	public TantargyWithTantervekAndTematikakDTO getEredeti() {
		return (TantargyWithTantervekAndTematikakDTO)super.getEredeti();
	}

	@Override
	public TantargyWithTantervekAndTematikakDTO getDTO() {
		return (TantargyWithTantervekAndTematikakDTO)super.getDTO();
	}

	public TantargyEditor(TantargyWithTantervekAndTematikakDTO dto, Consumer<TantargyDTO> editedDTOHandler, List<SzervezetDTO> szervezetList) {
		super(dto, new TantargyWithTantervekAndTematikakDTO(dto), editedDTOHandler);
		this.szervezetList = szervezetList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public VerticalLayout createForm(boolean readOnly) {
		final VerticalLayout content = new VerticalLayout();
		//Létrehozáskor meghatározható kulcs-jellegű adatok, szerkesztéskor inkább ne lehessen megváltoztatni őket
		final Layout form = TantargyFormCreator.super.createForm(true);
		content.addComponent(form);
		if (getDTO().getSzervezet() != null) {
			tantervekGrid = createTantervekGrid();
			content.addComponent(tantervekGrid);
		}

		tematikakGrid = createTematikakGrid();
		content.addComponent(tematikakGrid);
		return content;
	}

	private Grid<TematikaDTO> createTematikakGrid() {
		final Grid<TematikaDTO> grid = new Grid<>();
		Toolbox.addColumn(grid, "field.tema", TematikaDTO::getTema);
		Toolbox.addColumn(grid, "field.valStart", TematikaDTO::getErvenyessegKezdet)
				.setRenderer(new DateRenderer(DateFormat.getDateInstance()));
		Toolbox.addColumn(grid, "field.valEnd", TematikaDTO::getErvenyessegVeg)
				.setRenderer(new DateRenderer(DateFormat.getDateInstance()));
		Toolbox.addComponentColumn(grid, "field.valid",
				dto -> Toolbox.createReadonlyCheckBox(dto.isValid(), StringUtils.EMPTY));
		Toolbox.addComponentColumn(grid, "field.vegleges",
				dto -> Toolbox.createReadonlyCheckBox(dto.isVegleges(), StringUtils.EMPTY));

		grid.setDataProvider(DataProvider.ofCollection(getDTO().getTematikak()));
		grid.setWidth(100, PERCENTAGE);
		return grid;
	}

	private Grid<TantervDTO> createTantervekGrid() {
		Grid<TantervDTO> grid = new Grid<>();

		Toolbox.addColumn(grid, "field.konkretSzakirany", TantervDTO::getKonkretSzakirany);
		Toolbox.addColumn(grid, "field.statusz", TantervDTO::getStatusz);
		Toolbox.addColumn(grid, "field.kredit", TantervDTO::getKredit);
		Toolbox.addColumn(grid, "field.valStart", TantervDTO::getErvenyessegKezdet)
				.setRenderer(new DateRenderer(DateFormat.getDateInstance()));
		Toolbox.addColumn(grid, "field.valEnd", TantervDTO::getErvenyessegVeg)
				.setRenderer(new DateRenderer(DateFormat.getDateInstance()));

		grid.setDataProvider(DataProvider.ofCollection(getDTO().getTantervek()));
		grid.setWidth(100, PERCENTAGE);
		return grid;
	}
}
