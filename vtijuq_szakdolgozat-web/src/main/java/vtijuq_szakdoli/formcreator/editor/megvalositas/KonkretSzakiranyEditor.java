package vtijuq_szakdoli.formcreator.editor.megvalositas;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

import java.text.DateFormat;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.DateRenderer;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyWithTantervekDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.manipulator.editor.megvalositas.KonkretSzakiranyDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.KonkretSzakiranyFormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public class KonkretSzakiranyEditor extends KonkretSzakiranyDTOEditor implements KonkretSzakiranyFormCreator, EditorFormCreator<KonkretSzakiranyDTO> {

	@Getter
	private boolean alapSzakirany;
	@Getter
	private Binder<KonkretSzakiranyDTO> binder;
	@Getter
	private Grid<TantervDTO> tantervekGrid;
	@Getter
	private List<KonkretSzakDTO> konkretSzakList;
	@Getter
	private List<SzakiranyKovetelmenyDTO> szakiranyKovetelmenyList;

	@Override
	public KonkretSzakiranyWithTantervekDTO getDTO() {
		return (KonkretSzakiranyWithTantervekDTO)super.getDTO();
	}

	public KonkretSzakiranyEditor(KonkretSzakiranyWithTantervekDTO eredeti, KonkretSzakiranyWithTantervekDTO dto,
			Consumer<KonkretSzakiranyDTO> editedDTOHandler, boolean alapSzakirany) {
		super(eredeti, dto, editedDTOHandler);
		this.alapSzakirany = alapSzakirany;
		this.konkretSzakList = Collections.singletonList(dto.getKonkretSzak());
		this.szakiranyKovetelmenyList = Collections.singletonList(dto.getSzakiranyKovetelmeny());
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public KonkretSzakiranyEditor(KonkretSzakiranyWithTantervekDTO dto, Consumer<KonkretSzakiranyDTO> editedDTOHandler,
			List<KonkretSzakDTO> konkretSzakList, List<SzakiranyKovetelmenyDTO> szakiranyKovetelmenyList) {
		super(dto, new KonkretSzakiranyWithTantervekDTO(dto), editedDTOHandler);
		alapSzakirany = false;
		this.konkretSzakList = konkretSzakList;
		this.szakiranyKovetelmenyList = szakiranyKovetelmenyList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public VerticalLayout createForm(boolean readOnly) {
		final VerticalLayout content = new VerticalLayout();
		if (!alapSzakirany) {
			final Layout form = KonkretSzakiranyFormCreator.super.createForm(readOnly);
			content.addComponent(form);
		}

		tantervekGrid = createTantervekGrid();
		content.addComponent(tantervekGrid);
		return content;
	}

	private Grid<TantervDTO> createTantervekGrid() {
		Grid<TantervDTO> grid = new Grid<>();

		Toolbox.addColumn(grid, "field.tantargy", TantervDTO::getTantargy);
		Toolbox.addColumn(grid, "field.statusz", TantervDTO::getStatusz);
		Toolbox.addColumn(grid, "field.kredit", TantervDTO::getKredit);
		Toolbox.addColumn(grid, "field.valStart", TantervDTO::getErvenyessegKezdet)
				.setRenderer(new DateRenderer(DateFormat.getDateInstance()));
		Toolbox.addColumn(grid, "field.valEnd", TantervDTO::getErvenyessegVeg)
				.setRenderer(new DateRenderer(DateFormat.getDateInstance()));

		grid.setDataProvider(DataProvider.ofCollection(getDTO().getTantervek()));
		grid.setWidth(100, PERCENTAGE);
		return grid;
	}
}
