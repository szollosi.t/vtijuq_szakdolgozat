package vtijuq_szakdoli.formcreator.editor.megvalositas;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.vaadin.ui.Accordion;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;

import vtijuq_szakdoli.common.dto.AbstractEntitasDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakWithSzakiranyokDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyWithTantervekDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;

public class KonkretSzakWithSzakiranyokEditor extends KonkretSzakEditor {

	@Getter
	private KonkretSzakiranyEditor alapKonkretSzakiranyEditor;
	@Getter
	private List<KonkretSzakiranyEditor> konkretSzakiranyEditorok;

	private Consumer<KonkretSzakWithSzakiranyokDTO> compositeDTOHandler;

	@Getter
	private Layout alapKonkretSzakiranyLayout;

	public KonkretSzakWithSzakiranyokEditor(KonkretSzakWithSzakiranyokDTO eredeti, Consumer<KonkretSzakWithSzakiranyokDTO> editedDTOHandler,
			List<SzakKovetelmenyDTO> szakKovetelmenyList, List<SzervezetDTO> szervezetList) {
		super(eredeti, new KonkretSzakWithSzakiranyokDTO(eredeti), null, szakKovetelmenyList, szervezetList);
		alapKonkretSzakiranyEditor = new KonkretSzakiranyEditor(getEredeti().getAlapKonkretSzakirany(), getDTO().getAlapKonkretSzakirany(), null, true);
		//egyébként nem módosulnak elvileg, csak egységességi okokból használjuk itt a másolataikat
		final Map<Long, KonkretSzakiranyWithTantervekDTO> konkretSzakiranyDTOMap = getDTO().getKonkretSzakiranyok().stream()
				.collect(Collectors.toMap(AbstractEntitasDTO::getId, Function.identity()));
		if (CollectionUtils.isNotEmpty(getEredeti().getKonkretSzakiranyok())) {
			konkretSzakiranyEditorok = getEredeti().getKonkretSzakiranyok().stream()
					.map(kszi -> new KonkretSzakiranyEditor(kszi, konkretSzakiranyDTOMap.get(kszi.getId()), null, false))
					.collect(Collectors.toList());
		}
		//csak együttes handler van
		compositeDTOHandler = editedDTOHandler;
	}

	@Override
	public KonkretSzakWithSzakiranyokDTO getEredeti() {
		return (KonkretSzakWithSzakiranyokDTO)super.getEredeti();
	}

	@Override
	public KonkretSzakWithSzakiranyokDTO getDTO() {
		return (KonkretSzakWithSzakiranyokDTO)super.getDTO();
	}

	@Override
	public Consumer<KonkretSzakDTO> getDTOHandler() {
		//a getDTO-val szinkronban specializált DTO-osztályt fogyaszt
		//TODO szebb megoldás
		return (ksz) -> compositeDTOHandler.accept((KonkretSzakWithSzakiranyokDTO)ksz);
	}

	@Override
	public void edit() {
		super.edit();
		alapKonkretSzakiranyEditor.edit();
	}

	@Override
	public boolean validateForm() {
		return super.validateForm() && alapKonkretSzakiranyEditor.validateForm();
	}

	@Override
	public Layout createForm(boolean readOnly) {
		final VerticalLayout content = new VerticalLayout();

		final Layout konkretSzakLayout = super.createForm(readOnly);
		content.addComponent(konkretSzakLayout);

		Accordion szakiranyokAccordion = new Accordion();
		//alapszakirany az első lapon
		alapKonkretSzakiranyLayout = alapKonkretSzakiranyEditor.createForm(readOnly);
		szakiranyokAccordion.addTab(alapKonkretSzakiranyLayout, alapKonkretSzakiranyEditor.getDTO().getNev());
		//többi szakirány a további lapokon
		konkretSzakiranyEditorok.forEach(ksze -> {
			final VerticalLayout konkretSzakiranyLayout = ksze.createForm(true);
			szakiranyokAccordion.addTab(konkretSzakiranyLayout, ksze.getDTO().getNev());
		});
		content.addComponent(szakiranyokAccordion);
		return content;
	}
}
