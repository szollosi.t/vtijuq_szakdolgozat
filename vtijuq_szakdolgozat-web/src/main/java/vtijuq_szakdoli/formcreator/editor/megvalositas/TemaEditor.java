package vtijuq_szakdoli.formcreator.editor.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.manipulator.editor.megvalositas.TemaDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.TemaFormCreator;

public class TemaEditor extends TemaDTOEditor implements TemaFormCreator, EditorFormCreator<TemaDTO> {

	@Getter
	private Binder<TemaDTO> binder;
	@Getter
	private List<IsmeretDTO> ismeretList;

	public TemaEditor(TemaDTO dto, Consumer<TemaDTO> editedDTOHandler, List<IsmeretDTO> ismeretList) {
		super(dto, editedDTOHandler);
		this.ismeretList = ismeretList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
