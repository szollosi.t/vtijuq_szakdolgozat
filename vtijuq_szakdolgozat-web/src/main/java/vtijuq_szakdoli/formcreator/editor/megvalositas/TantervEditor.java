package vtijuq_szakdoli.formcreator.editor.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.ErtekelesTipusDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.manipulator.editor.megvalositas.TantervDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.TantervFormCreator;

public class TantervEditor extends TantervDTOEditor implements TantervFormCreator, EditorFormCreator<TantervDTO> {

	@Getter
	private Binder<TantervDTO> binder;
	@Getter
	private List<KonkretSzakiranyDTO> konkretSzakiranyList;
	@Getter
	private List<TantargyDTO> tantargyList;
	@Getter
	private List<ErtekelesTipusDTO> ertekelesTipusList;

	public TantervEditor(TantervDTO dto, Consumer<TantervDTO> editedDTOHandler, List<KonkretSzakiranyDTO> konkretSzakiranyList, List<TantargyDTO> tantargyList, List<ErtekelesTipusDTO> ertekelesTipusList) {
		super(dto, editedDTOHandler);
		this.konkretSzakiranyList = konkretSzakiranyList;
		this.tantargyList = tantargyList;
		this.ertekelesTipusList = ertekelesTipusList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
