package vtijuq_szakdoli.formcreator.editor.adminisztracio;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.common.manipulator.editor.adminisztracio.SzervezetDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.adminisztracio.SzervezetFormCreator;

public class SzervezetEditor extends SzervezetDTOEditor implements SzervezetFormCreator, EditorFormCreator<SzervezetDTO> {

	@Getter
	private Binder<SzervezetDTO> binder;
	@Getter
	private List<SzervezetTipusDTO> szervezetTipusList;
	@Getter
	private vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO foSzervezet;

	public SzervezetEditor(SzervezetDTO dto, Consumer<SzervezetDTO> editedDTOHandler, List<SzervezetTipusDTO> szervezetTipusList) {
		super(dto, editedDTOHandler);
		this.szervezetTipusList = szervezetTipusList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public SzervezetEditor(SzervezetDTO dto, Consumer<SzervezetDTO> editedDTOHandler, List<SzervezetTipusDTO> szervezetTipusList, vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO foSzervezet) {
		this(dto, editedDTOHandler, szervezetTipusList);
		this.foSzervezet = foSzervezet;
	}

}
