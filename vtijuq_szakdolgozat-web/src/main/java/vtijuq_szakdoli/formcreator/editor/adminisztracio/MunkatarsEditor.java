package vtijuq_szakdoli.formcreator.editor.adminisztracio;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.manipulator.editor.adminisztracio.MunkatarsDTOEditor;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.adminisztracio.MunkatarsFormCreator;

public class MunkatarsEditor extends MunkatarsDTOEditor implements MunkatarsFormCreator, EditorFormCreator<MunkatarsDTO> {
	//TODO jelszóváltoztatási felület

	@Getter
	private Binder<MunkatarsDTO> binder;
	@Getter
	private List<SzervezetDTO> szervezetList;
	@Getter
	private List<SzerepkorDTO> szerepkorList;

	public MunkatarsEditor(MunkatarsDTO dto, Consumer<MunkatarsDTO> editedDTOHandler, List<SzervezetDTO> szervezetList, List<SzerepkorDTO> szerepkorList) {
		super(dto, editedDTOHandler);
		this.szervezetList = szervezetList;
		this.szerepkorList = szerepkorList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
