package vtijuq_szakdoli.formcreator.creator.adminisztracio;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.manipulator.creator.adminisztracio.MunkatarsDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.adminisztracio.MunkatarsFormCreator;

public class MunkatarsCreator extends MunkatarsDTOCreator implements MunkatarsFormCreator, CreatorFormCreator<MunkatarsDTO> {

	@Getter
	private Binder<MunkatarsDTO> binder;
	@Getter
	private List<SzervezetDTO> szervezetList;
	@Getter
	private List<SzerepkorDTO> szerepkorList;

	public MunkatarsCreator(Consumer<MunkatarsDTO> createdDTOHandler, List<SzervezetDTO> szervezetList, List<SzerepkorDTO> szerepkorList) {
		super(createdDTOHandler);
		this.szervezetList = szervezetList;
		this.szerepkorList = szerepkorList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
