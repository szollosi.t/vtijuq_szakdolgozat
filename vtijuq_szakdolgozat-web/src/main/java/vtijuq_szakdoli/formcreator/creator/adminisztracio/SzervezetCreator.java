package vtijuq_szakdoli.formcreator.creator.adminisztracio;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.common.manipulator.creator.adminisztracio.SzervezetDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.adminisztracio.SzervezetFormCreator;

public class SzervezetCreator extends SzervezetDTOCreator implements SzervezetFormCreator, CreatorFormCreator<SzervezetDTO> {

	@Getter
	private Binder<SzervezetDTO> binder;
	@Getter
	private List<SzervezetTipusDTO> szervezetTipusList;
	@Getter
	private vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO foSzervezet;

	public SzervezetCreator(Consumer<SzervezetDTO> createdDTOHandler, List<SzervezetTipusDTO> szervezetTipusList) {
		super(createdDTOHandler);
		this.szervezetTipusList = szervezetTipusList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public SzervezetCreator(Consumer<SzervezetDTO> createdDTOHandler, List<SzervezetTipusDTO> szervezetTipusList, vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO foSzervezet) {
		this(createdDTOHandler, szervezetTipusList);
		this.foSzervezet = foSzervezet;
	}

}
