package vtijuq_szakdoli.formcreator.creator.akkreditacio;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.manipulator.creator.akkreditacio.AkkreditacioDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.akkreditacio.AkkreditacioFormCreator;

public class AkkreditacioCreator extends AkkreditacioDTOCreator implements AkkreditacioFormCreator, CreatorFormCreator<AkkreditacioDTO> {

	@Getter
	private Binder<AkkreditacioDTO> binder;
	@Getter
	private List<KonkretSzakiranyDTO> konkretSzakiranyList;

	public AkkreditacioCreator(Consumer<AkkreditacioDTO> createdDTOHandler, List<KonkretSzakiranyDTO> konkretSzakiranyList) {
		super(createdDTOHandler);
		this.konkretSzakiranyList = konkretSzakiranyList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
