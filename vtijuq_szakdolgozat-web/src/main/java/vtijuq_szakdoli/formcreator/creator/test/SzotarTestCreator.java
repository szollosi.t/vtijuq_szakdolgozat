package vtijuq_szakdoli.formcreator.creator.test;

import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.manipulator.creator.test.SzotarTestDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.test.SzotarTestFormCreator;

public class SzotarTestCreator extends SzotarTestDTOCreator implements SzotarTestFormCreator, CreatorFormCreator<SzotarTestDTO> {

	@Getter
	private Binder<SzotarTestDTO> binder;
	@Getter
	private vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO tipus;

	public SzotarTestCreator(Consumer<SzotarTestDTO> createdDTOHandler) {
		super(createdDTOHandler);
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public SzotarTestCreator(Consumer<SzotarTestDTO> createdDTOHandler, vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO tipus) {
		this(createdDTOHandler);
		this.tipus = tipus;
	}
}
