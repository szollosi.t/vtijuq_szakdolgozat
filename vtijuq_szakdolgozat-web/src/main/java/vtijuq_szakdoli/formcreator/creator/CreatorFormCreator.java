package vtijuq_szakdoli.formcreator.creator;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;
import vtijuq_szakdoli.formcreator.manipulator.ManipulatorFormCreator;

public interface CreatorFormCreator<T extends EditableDTO> extends DTOCreator<T>, ManipulatorFormCreator<T> {

}
