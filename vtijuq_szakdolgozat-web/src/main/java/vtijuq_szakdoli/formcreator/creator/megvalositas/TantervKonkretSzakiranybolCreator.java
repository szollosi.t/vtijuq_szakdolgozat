package vtijuq_szakdoli.formcreator.creator.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.ErtekelesTipusDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.manipulator.creator.megvalositas.TantervDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.TantervFormCreator;

public class TantervKonkretSzakiranybolCreator extends TantervDTOCreator implements TantervFormCreator, CreatorFormCreator<TantervDTO> {

	@Getter
	private Binder<TantervDTO> binder;
	@Getter
	private List<KonkretSzakiranyDTO> konkretSzakiranyList;
	@Getter
	private List<TantargyDTO> tantargyList;
	@Getter
	private List<ErtekelesTipusDTO> ertekelesTipusList;

	public TantervKonkretSzakiranybolCreator(Consumer<TantervDTO> createdDTOHandler, List<KonkretSzakiranyDTO> konkretSzakiranyList, List<TantargyDTO> tantargyList, List<ErtekelesTipusDTO> ertekelesTipusList) {
		super(createdDTOHandler);
		this.konkretSzakiranyList = konkretSzakiranyList;
		this.tantargyList = tantargyList;
		this.ertekelesTipusList = ertekelesTipusList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
