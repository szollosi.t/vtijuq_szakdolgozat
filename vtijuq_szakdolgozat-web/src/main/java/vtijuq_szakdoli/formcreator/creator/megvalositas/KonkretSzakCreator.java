package vtijuq_szakdoli.formcreator.creator.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.manipulator.creator.megvalositas.KonkretSzakDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.KonkretSzakFormCreator;

public class KonkretSzakCreator extends KonkretSzakDTOCreator implements KonkretSzakFormCreator, CreatorFormCreator<KonkretSzakDTO> {

	@Getter
	private Binder<KonkretSzakDTO> binder;
	@Getter
	private List<SzakKovetelmenyDTO> szakKovetelmenyList;
	@Getter
	private List<SzervezetDTO> szervezetList;

	public KonkretSzakCreator(Consumer<KonkretSzakDTO> createdDTOHandler, List<SzakKovetelmenyDTO> szakKovetelmenyList, List<SzervezetDTO> szervezetList) {
		super(createdDTOHandler);
		this.szakKovetelmenyList = szakKovetelmenyList;
		this.szervezetList = szervezetList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
