package vtijuq_szakdoli.formcreator.creator.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.manipulator.creator.megvalositas.TemaDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.TemaFormCreator;

public class TemaCreator extends TemaDTOCreator implements TemaFormCreator, CreatorFormCreator<TemaDTO> {

	@Getter
	private Binder<TemaDTO> binder;
	@Getter
	private List<IsmeretDTO> ismeretList;

	public TemaCreator(Consumer<TemaDTO> createdDTOHandler, List<IsmeretDTO> ismeretList) {
		super(createdDTOHandler);
		this.ismeretList = ismeretList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}
}
