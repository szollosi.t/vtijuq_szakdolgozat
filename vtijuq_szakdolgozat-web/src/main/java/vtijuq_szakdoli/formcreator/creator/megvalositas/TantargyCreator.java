package vtijuq_szakdoli.formcreator.creator.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.manipulator.creator.megvalositas.TantargyDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.TantargyFormCreator;

public class TantargyCreator extends TantargyDTOCreator implements TantargyFormCreator, CreatorFormCreator<TantargyDTO> {

	@Getter
	private Binder<TantargyDTO> binder;
	@Getter
	private List<SzervezetDTO> szervezetList;

	public TantargyCreator(Consumer<TantargyDTO> createdDTOHandler, List<SzervezetDTO> szervezetList) {
		super(createdDTOHandler);
		this.szervezetList = szervezetList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}
}
