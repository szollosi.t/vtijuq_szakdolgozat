package vtijuq_szakdoli.formcreator.creator.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.manipulator.creator.megvalositas.KonkretSzakiranyDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.KonkretSzakiranyFormCreator;

public class KonkretSzakiranyCreator extends KonkretSzakiranyDTOCreator implements KonkretSzakiranyFormCreator, CreatorFormCreator<KonkretSzakiranyDTO> {

	@Getter
	private Binder<KonkretSzakiranyDTO> binder;
	@Getter
	private List<KonkretSzakDTO> konkretSzakList;
	@Getter
	private List<SzakiranyKovetelmenyDTO> szakiranyKovetelmenyList;

	public KonkretSzakiranyCreator(Consumer<KonkretSzakiranyDTO> createdDTOHandler, List<KonkretSzakDTO> konkretSzakList, List<SzakiranyKovetelmenyDTO> szakiranyKovetelmenyList) {
		super(createdDTOHandler);
		this.konkretSzakList = konkretSzakList;
		this.szakiranyKovetelmenyList = szakiranyKovetelmenyList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
