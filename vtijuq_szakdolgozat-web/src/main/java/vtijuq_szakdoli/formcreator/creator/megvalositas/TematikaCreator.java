package vtijuq_szakdoli.formcreator.creator.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.IsmeretSzintDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.manipulator.creator.megvalositas.TematikaDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.megvalositas.TematikaFormCreator;

public class TematikaCreator extends TematikaDTOCreator implements TematikaFormCreator, CreatorFormCreator<TematikaDTO> {

	@Getter
	private Binder<TematikaDTO> binder;
	@Getter
	private List<TemaDTO> temaList;
	@Getter
	private List<IsmeretSzintDTO> leadottSzintList;
	@Getter
	private List<IsmeretSzintDTO> szamonkertSzintList;

	public TematikaCreator(Consumer<TematikaDTO> createdDTOHandler, List<TemaDTO> temaList, List<IsmeretSzintDTO> leadottSzintList, List<IsmeretSzintDTO> szamonkertSzintList) {
		super(createdDTOHandler);
		this.temaList = temaList;
		this.leadottSzintList = leadottSzintList;
		this.szamonkertSzintList = szamonkertSzintList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}
}
