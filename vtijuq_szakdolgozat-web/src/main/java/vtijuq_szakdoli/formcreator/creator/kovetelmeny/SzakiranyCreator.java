package vtijuq_szakdoli.formcreator.creator.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.manipulator.creator.kovetelmeny.SzakiranyDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.SzakiranyFormCreator;

public class SzakiranyCreator extends SzakiranyDTOCreator implements SzakiranyFormCreator, CreatorFormCreator<SzakiranyDTO> {

	@Getter
	private Binder<SzakiranyDTO> binder;
	@Getter
	private List<SzakDTO> szakList;

	public SzakiranyCreator(Consumer<SzakiranyDTO> createdDTOHandler, List<SzakDTO> szakList) {
		super(createdDTOHandler);
		this.szakList = szakList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

}
