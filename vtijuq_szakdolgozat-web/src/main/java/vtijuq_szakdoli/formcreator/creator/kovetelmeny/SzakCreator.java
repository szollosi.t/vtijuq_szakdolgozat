package vtijuq_szakdoli.formcreator.creator.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.KepzesiTeruletDTO;
import vtijuq_szakdoli.common.manipulator.creator.kovetelmeny.SzakDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.SzakFormCreator;

public class SzakCreator extends SzakDTOCreator implements SzakFormCreator, CreatorFormCreator<SzakDTO> {

	@Getter
	private Binder<SzakDTO> binder;
	@Getter
	private List<KepzesiTeruletDTO> kepzesiTeruletList;

	public SzakCreator(Consumer<SzakDTO> createdDTOHandler, List<KepzesiTeruletDTO> kepzesiTeruletList) {
		super(createdDTOHandler);
		this.kepzesiTeruletList = kepzesiTeruletList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}
}
