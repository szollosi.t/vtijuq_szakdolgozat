package vtijuq_szakdoli.formcreator.creator.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.manipulator.creator.kovetelmeny.SzakKovetelmenyDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.SzakKovetelmenyFormCreator;

public class SzakKovetelmenyCreator extends SzakKovetelmenyDTOCreator implements SzakKovetelmenyFormCreator, CreatorFormCreator<SzakKovetelmenyDTO> {

	@Getter
	private Binder<SzakKovetelmenyDTO> binder;
	@Getter
	private List<SzakDTO> szakList;

	public SzakKovetelmenyCreator(Consumer<SzakKovetelmenyDTO> createdDTOHandler, List<SzakDTO> szakList) {
		super(createdDTOHandler);
		this.szakList = szakList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}
}
