package vtijuq_szakdoli.formcreator.creator.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Layout;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.manipulator.creator.kovetelmeny.SzakiranyKovetelmenyDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.SzakiranyKovetelmenyFormCreator;

public class SzakiranyKovetelmenyCreator extends SzakiranyKovetelmenyDTOCreator implements SzakiranyKovetelmenyFormCreator, CreatorFormCreator<SzakiranyKovetelmenyDTO> {

	@Getter
	private Binder<SzakiranyKovetelmenyDTO> binder;
	@Getter
	private List<SzakKovetelmenyDTO> szakKovetelmenyList;
	@Getter
	private List<SzakiranyDTO> szakiranyList;

	public SzakiranyKovetelmenyCreator(Consumer<SzakiranyKovetelmenyDTO> createdDTOHandler, List<SzakKovetelmenyDTO> szakKovetelmenyList, List<SzakiranyDTO> szakiranyList) {
		super(createdDTOHandler);
		this.szakKovetelmenyList = szakKovetelmenyList;
		this.szakiranyList = szakiranyList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	@Override
	public Layout createForm(final boolean readOnly) {
		final FormLayout form = new FormLayout();
		form.addComponent(createSzakKovetelmenyField());
		form.addComponent(createSzakiranyField());
		form.addComponent(createCsakkozosField());
		getBinder().setReadOnly(readOnly);
		return form;
	}
}
