package vtijuq_szakdoli.formcreator.creator.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.manipulator.creator.kovetelmeny.SzakmaiJellemzoDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.SzakmaiJellemzoFormCreator;

public class SzakmaiJellemzoCreator extends SzakmaiJellemzoDTOCreator implements SzakmaiJellemzoFormCreator, CreatorFormCreator<SzakmaiJellemzoDTO> {

	@Getter
	private Binder<SzakmaiJellemzoDTO> binder;
	@Getter
	private List<IsmeretDTO> ismeretList;
	@Getter
	private vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO foJellemzo;

	public SzakmaiJellemzoCreator(Consumer<SzakmaiJellemzoDTO> createdDTOHandler, List<IsmeretDTO> ismeretList) {
		super(createdDTOHandler);
		this.ismeretList = ismeretList;
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public SzakmaiJellemzoCreator(Consumer<SzakmaiJellemzoDTO> createdDTOHandler, List<IsmeretDTO> ismeretList, vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO foJellemzo) {
		this(createdDTOHandler, ismeretList);
		this.foJellemzo = foJellemzo;
	}

}
