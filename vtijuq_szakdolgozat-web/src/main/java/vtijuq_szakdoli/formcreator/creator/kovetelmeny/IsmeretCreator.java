package vtijuq_szakdoli.formcreator.creator.kovetelmeny;

import java.util.function.Consumer;

import com.vaadin.data.Binder;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.manipulator.creator.kovetelmeny.IsmeretDTOCreator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.formcreator.manipulator.kovetelmeny.IsmeretFormCreator;

public class IsmeretCreator extends IsmeretDTOCreator implements IsmeretFormCreator, CreatorFormCreator<IsmeretDTO> {

	@Getter
	private Binder<IsmeretDTO> binder;
	@Getter
	private vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO foIsmeret;

	public IsmeretCreator(Consumer<IsmeretDTO> createdDTOHandler) {
		super(createdDTOHandler);
		binder = new Binder<>();
		getBinder().setBean(getDTO());
	}

	public IsmeretCreator(Consumer<IsmeretDTO> createdDTOHandler, vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO foIsmeret) {
		this(createdDTOHandler);
		this.foIsmeret = foIsmeret;
	}

}
