package vtijuq_szakdoli.formcreator.subordination;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.data.Binder;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Layout;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.subordinationDTO.SubordinationDTO;
import vtijuq_szakdoli.common.manipulator.subordination.SubordinationDTOCreator;
import vtijuq_szakdoli.common.util.graph.Hierarchical;
import vtijuq_szakdoli.formcreator.FormCreator;
import vtijuq_szakdoli.utils.Toolbox;

public class SubordinationCreator<T extends EntitasDTO & Hierarchical<T>> extends SubordinationDTOCreator<T> implements FormCreator<SubordinationDTO<T>> {

    @Getter
    private Binder<SubordinationDTO<T>> binder;
    @Getter
    private List<T> verticesList;

    public SubordinationCreator(Consumer<SubordinationDTO<T>> createdDTOHandler, List<T> verticesList) {
        super(createdDTOHandler);
        this.verticesList = verticesList;
        binder = new Binder<>();
        getBinder().setBean(getDTO());
    }

    public Layout createForm(final String principalFieldCaptionKey, final String subordinateFieldCaptionKey) {
        final FormLayout form = new FormLayout();
        form.addComponent(createPrincipalField(principalFieldCaptionKey));
        form.addComponent(createSubordinateField(subordinateFieldCaptionKey));
        return form;
    }

    private ComboBox<T> createPrincipalField(String principalFieldCaptionKey) {
        return Toolbox.createAndBindRequiredComboBoxWithEmpty(
                getBinder(), principalFieldCaptionKey, getVerticesList(),
                SubordinationDTO<T>::getPrincipalDTO, SubordinationDTO<T>::setPrincipalDTO
        );
    }

    private ComboBox<T> createSubordinateField(final String subordinateFieldCaptionKey) {
        return Toolbox.createAndBindRequiredComboBoxWithEmpty(
                getBinder(), subordinateFieldCaptionKey, getVerticesList(),
                SubordinationDTO<T>::getSubordinateDTO, SubordinationDTO<T>::setSubordinateDTO
        );
    }
}
