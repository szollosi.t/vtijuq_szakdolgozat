package vtijuq_szakdoli.layout;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;

public interface Editor<T extends EditableDTO> extends Manipulator<T> {

	void edit();

}
