package vtijuq_szakdoli.layout;

import java.util.function.Function;

public interface ValidatableLayout {

	boolean validate(Function<String, String> messageResolver);

	default boolean renderValidateButton(){
		return false;
	}
	
}
