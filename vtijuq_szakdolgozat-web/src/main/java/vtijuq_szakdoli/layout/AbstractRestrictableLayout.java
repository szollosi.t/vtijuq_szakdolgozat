package vtijuq_szakdoli.layout;

import java.util.Set;

import javax.inject.Inject;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.login.LoginBean;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.util.cdiconfig.captions.CaptionResolver;
import vtijuq_szakdoli.util.cdiconfig.messages.MessageResolver;
import vtijuq_szakdoli.utils.Toolbox;

public abstract class AbstractRestrictableLayout extends VerticalLayout implements Restrictable {

	@Inject
	@Getter(AccessLevel.PROTECTED)
	private LoginBean loginBean;

	@Inject
	private CaptionResolver captionResolver;

	@Inject
	private MessageResolver messageResolver;

	private Layout content;
	private Layout head;
	private HorizontalLayout topButtons;
	private HorizontalLayout bottomButtons;

	public void refresh(){
		removeAllComponents();
		init();
	}

	protected void init() {
		if (StringUtils.isNotBlank(getTitle())) {
			addAndExpandHorizontally(new Label(getTitle()));
		}

		head = createHead();
		if (head != null) {
			addAndExpandHorizontally(head);
		}

		topButtons = createTopButtons();
		if (topButtons != null) {
			addAndExpandHorizontally(topButtons);
		}

		content = createContent();
		addAndExpandHorizontally(content);

		bottomButtons = createBottomButtons();
		if (bottomButtons != null) {
			addAndExpandHorizontally(bottomButtons);
		}
		setExpandRatio(content,1);
		getPopups().forEach(popup -> popup.addPopupView(this));
	}

	protected String getTitle() {
		return resolveCaption(getTitleCaptionKey());
	}

	private <T extends Component & Sizeable> void addAndExpandHorizontally(T sizeableComponent) {
		addComponent(sizeableComponent);
		sizeableComponent.setWidth(100, Unit.PERCENTAGE);
	}

	protected abstract String getTitleCaptionKey();

	protected Layout createHead(){
		return null;
	}

	protected abstract Layout createContent();

	protected abstract HorizontalLayout createTopButtons();

	protected abstract HorizontalLayout createBottomButtons();

	protected Set<AbstractPopup> getPopups(){
		return SetUtils.emptySet();
	}

	protected final void addButton(HorizontalLayout buttons, Alignment alignment, String caption, ClickListener action) {
		Button button = new Button(caption, action);
		buttons.addComponent(button);
		buttons.setComponentAlignment(button, alignment);
	}

	public boolean isAccessibleByLoggedInUser(){
		if (this instanceof Restricted) {
			Restricted restricted = (Restricted)this;
			return !restricted.hasRestrictedVisibility() || loginBean.isAccessible(restricted.getFunkcioNev());
		}
		return true;
	}

	public boolean isEditableByLoggedInUser(){
		if (this instanceof Restricted) {
			Restricted restricted = (Restricted)this;
			return loginBean.isEditable(restricted.getFunkcioNev());
		}
		return true;
	}

	protected String resolveCaption(final String key) {
		return captionResolver.getValue(key);
	}

	protected String resolveConstantCaption(final Object constantValue) {
		return captionResolver.getValue(Toolbox.CONSTANT_PREFIX + constantValue);
	}

	protected String resolveMessage(final String key) {
		return messageResolver.getValue(key);
	}
}
