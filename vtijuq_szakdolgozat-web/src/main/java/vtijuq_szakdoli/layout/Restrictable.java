package vtijuq_szakdoli.layout;

public interface Restrictable {

	default boolean hasRestrictedVisibility() {
		return false;
	}

	default boolean isAccessibleByLoggedInUser() {
		return !hasRestrictedVisibility();
	}

	boolean isEditableByLoggedInUser();

}
