package vtijuq_szakdoli.layout;

public interface Restricted extends Restrictable {

	@Override
	default boolean hasRestrictedVisibility() {
		return true;
	}

	String getFunkcioNev();

}
