package vtijuq_szakdoli.layout;

import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public interface Manipulator<T extends EditableDTO> extends Restricted, ValidatableLayout {

	DTOManipulator<T> getDTOManipulator();

	@Override
	default boolean validate(Function<String, String> messageResolver) {
		return validate(messageResolver, null);
	}

	default boolean validate(Function<String, String> messageResolver, Function<T, Set<String>> extraValidation) {
		final Set<String> errors = getDTOManipulator().getDTO().getErrors();
		if (errors.isEmpty() && extraValidation != null) {
			errors.addAll(extraValidation.apply(getDTOManipulator().getDTO()));
		}
		if(!errors.isEmpty()){
			final String errorMessage = errors.stream().map(messageResolver).collect(Collectors.joining("\n"));
			Notification.show(errorMessage, Type.ERROR_MESSAGE);
		}
		return errors.isEmpty();
	}

}
