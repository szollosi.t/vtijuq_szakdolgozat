package vtijuq_szakdoli.popup;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.layout.Manipulator;

public interface ManipulatorPopup<T extends EditableDTO> extends Popup, Manipulator<T> {}
