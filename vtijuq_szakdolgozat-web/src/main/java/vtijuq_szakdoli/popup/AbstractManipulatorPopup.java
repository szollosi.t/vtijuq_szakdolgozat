package vtijuq_szakdoli.popup;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.layout.Manipulator;

public abstract class AbstractManipulatorPopup<T extends EditableDTO> extends AbstractPopup implements Manipulator<T> {

	protected boolean validate(){
		return validate(this::resolveMessage);
	}
}
