package vtijuq_szakdoli.popup.creator;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.layout.Creator;
import vtijuq_szakdoli.popup.ManipulatorPopup;

public interface CreatorPopup<T extends EditableDTO> extends Creator<T>, ManipulatorPopup<T> {}
