package vtijuq_szakdoli.popup.creator.subordination;

import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import lombok.AccessLevel;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.subordinationDTO.SubordinationDTO;
import vtijuq_szakdoli.common.util.graph.Hierarchical;
import vtijuq_szakdoli.formcreator.subordination.SubordinationCreator;
import vtijuq_szakdoli.layout.Restricted;
import vtijuq_szakdoli.layout.ValidatableLayout;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.service.ValidatorService;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.captions.Caption;

public abstract class AbstractSubordinationCreatorPopup<T extends EntitasDTO & Hierarchical<T>> extends AbstractPopup implements Restricted, ValidatableLayout {

	@Inject @Caption
	@ConfigurationValue(value = "button.cancel")
	private String cancelCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.create")
	private String createCaption;

	@Inject
	@Getter(AccessLevel.PROTECTED)
	private ValidatorService validatorService;

	@Getter
	protected SubordinationCreator<T> creator;

	public void show(Consumer<SubordinationDTO<T>> createdDTOHandler) {
		creator = createCreator(createdDTOHandler);
		show();
	}

	protected abstract SubordinationCreator<T> createCreator(Consumer<SubordinationDTO<T>> createdDTOHandler);

	@Override
	public final boolean isAccessibleByLoggedInUser() {
		return isEditableByLoggedInUser();
	}

	@Override
	protected HorizontalLayout createBottomButtons() {
		final HorizontalLayout buttons = new HorizontalLayout();
		addButton(buttons, Alignment.BOTTOM_LEFT, cancelCaption, e -> hide());
		addButton(buttons, Alignment.BOTTOM_RIGHT, createCaption, e -> create());
		return buttons;
	}

	private void create() {
		final boolean valid = validate();
		if (valid) {
			creator.create();
			hide();
		}
	}

	protected boolean validate() {
		return validate(this::resolveMessage);
	}

	@Override
	public boolean validate(Function<String, String> messageResolver) {
		final Set<String> errors = getCreator().getDTO().getErrors();
		if(!errors.isEmpty()){
			final String errorMessage = errors.stream().map(messageResolver).collect(Collectors.joining("\n"));
			Notification.show(errorMessage, Type.ERROR_MESSAGE);
		}
		return errors.isEmpty();
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(getPrincipalFieldCaptionKey(), getSubordinateFieldCaptionKey());
	}

	protected abstract String getPrincipalFieldCaptionKey();

	protected abstract String getSubordinateFieldCaptionKey();

}
