package vtijuq_szakdoli.popup.creator.subordination;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.dto.subordinationDTO.SubordinationDTO;
import vtijuq_szakdoli.formcreator.subordination.SubordinationCreator;
import vtijuq_szakdoli.service.kovetelmeny.IsmeretService;

public class IsmeretSubordinationCreatorPopup extends AbstractSubordinationCreatorPopup<IsmeretDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(IsmeretSubordinationCreatorPopup.class);
	private static final String NAME = "IsmeretBesorolas";
	private static final String FUNKCIO_NEV = FunctionNames.ISMERET_KARBANTARTO;

	@Inject
	private IsmeretService ismeretService;

	private List<IsmeretDTO> ismeretList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected SubordinationCreator<IsmeretDTO> createCreator(Consumer<SubordinationDTO<IsmeretDTO>> createdDTOHandler) {
		return new SubordinationCreator<>(createdDTOHandler, getIsmeretList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.ismeret.subordinationCreator";
	}

	@Override
	protected String getPrincipalFieldCaptionKey() {
		return "field.foismeret";
	}

	@Override
	protected String getSubordinateFieldCaptionKey() {
		return "field.alismeret";
	}

	private List<IsmeretDTO> getIsmeretList() {
		if (ismeretList == null) {
			//TODO jobban megoldani, letolni adatbázis-szintre a szűrést
			ismeretList = ismeretService.listAllIsmeret().stream()
					.filter(dto -> !getValidatorService().isIsmeretFinal(dto.getId()))
					.collect(Collectors.toList());
		}
		return ismeretList;
	}
}
