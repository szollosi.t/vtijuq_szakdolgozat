package vtijuq_szakdoli.popup.creator.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.formcreator.creator.kovetelmeny.SzakKovetelmenyCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;

public class SzakKovetelmenyCreatorPopup extends AbstractCreatorPopup<SzakKovetelmenyDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakKovetelmenyCreatorPopup.class);
	private static final String NAME = "SzakKovetelmenyFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.KOVETELMENY_SZERKESZTO;

	@Inject
	private KovetelmenyService service;

	private List<SzakDTO> szakList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected SzakKovetelmenyCreator createCreator(Consumer<SzakKovetelmenyDTO> createdDTOHandler) {
		return new SzakKovetelmenyCreator(createdDTOHandler, getSzakList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.szakKovetelmeny.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}

	public List<SzakDTO> getSzakList() {
		if (szakList == null) {
			szakList = service.listAllSzak();
		}
		return szakList;
	}
}
