package vtijuq_szakdoli.popup.creator.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.formcreator.creator.kovetelmeny.SzakmaiJellemzoCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.service.kovetelmeny.IsmeretService;

public class SzakmaiJellemzoCreatorPopup extends AbstractCreatorPopup<SzakmaiJellemzoDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakmaiJellemzoCreatorPopup.class);
	private static final String NAME = "SzakmaiJellemzoFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.KOVETELMENY_SZERKESZTO;

	@Inject
	private IsmeretService ismeretService;

	private List<IsmeretDTO> ismeretList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected SzakmaiJellemzoCreator createCreator(Consumer<SzakmaiJellemzoDTO> createdDTOHandler) {
		return new SzakmaiJellemzoCreator(createdDTOHandler, getIsmeretList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.szakmaiJellemzo.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}

	private List<IsmeretDTO> getIsmeretList() {
		if (ismeretList == null ) {
			ismeretList = ismeretService.listAllIsmeret();
		}
		return ismeretList;
	}
}
