package vtijuq_szakdoli.popup.creator.kovetelmeny;

import java.util.function.Consumer;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.formcreator.creator.kovetelmeny.IsmeretCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;

public class IsmeretCreatorPopup extends AbstractCreatorPopup<IsmeretDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(IsmeretCreatorPopup.class);
	private static final String NAME = "IsmeretFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.ISMERET_KARBANTARTO;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected IsmeretCreator createCreator(Consumer<IsmeretDTO> createdDTOHandler) {
		return new IsmeretCreator(createdDTOHandler);
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.ismeret.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}
}
