package vtijuq_szakdoli.popup.creator.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.KepzesiTeruletDTO;
import vtijuq_szakdoli.formcreator.creator.kovetelmeny.SzakCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;

public class SzakCreatorPopup extends AbstractCreatorPopup<SzakDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakCreatorPopup.class);
	private static final String NAME = "SzakKovetelmenyFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.KOVETELMENY_SZERKESZTO;

	@Inject
	private KovetelmenyService service;

	private List<KepzesiTeruletDTO> kepzesiTeruletList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected SzakCreator createCreator(Consumer<SzakDTO> createdDTOHandler) {
		return new SzakCreator(createdDTOHandler, getKepzesiTeruletList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.szak.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}

	public List<KepzesiTeruletDTO> getKepzesiTeruletList() {
		if (kepzesiTeruletList == null) {
			kepzesiTeruletList = service.listAllKepzesiTerulet();
		}
		return kepzesiTeruletList;
	}
}
