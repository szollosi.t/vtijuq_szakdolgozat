package vtijuq_szakdoli.popup.creator.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.formcreator.creator.kovetelmeny.SzakiranyKovetelmenyCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;

public class SzakiranyKovetelmenyCreatorPopup extends AbstractCreatorPopup<SzakiranyKovetelmenyDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakiranyKovetelmenyCreatorPopup.class);
	private static final String NAME = "SzakiranyKovetelmenyFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.KOVETELMENY_SZERKESZTO;

	@Inject
	private KovetelmenyService service;

	private List<SzakKovetelmenyDTO> szakKovetelmenyList;
	private List<SzakiranyDTO> szakiranyList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected SzakiranyKovetelmenyCreator createCreator(Consumer<SzakiranyKovetelmenyDTO> createdDTOHandler) {
		return new SzakiranyKovetelmenyCreator(createdDTOHandler, getSzakKovetelmenyList(), getSzakiranyList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.szakiranyKovetelmeny.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}

	public List<SzakKovetelmenyDTO> getSzakKovetelmenyList() {
		if (szakKovetelmenyList == null) {
			szakKovetelmenyList = service.listAllSzakKovetelmeny();
		}
		return szakKovetelmenyList;
	}

	public List<SzakiranyDTO> getSzakiranyList() {
		if (szakiranyList == null) {
			szakiranyList = service.listAllSzakirany();
		}
		return szakiranyList;
	}
}
