package vtijuq_szakdoli.popup.creator.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.formcreator.creator.kovetelmeny.SzakiranyCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;

public class SzakiranyCreatorPopup extends AbstractCreatorPopup<SzakiranyDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakiranyCreatorPopup.class);
	private static final String NAME = "SzakiranyFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.KOVETELMENY_SZERKESZTO;

	@Inject
	private KovetelmenyService service;

	private List<SzakDTO> szakList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected SzakiranyCreator createCreator(Consumer<SzakiranyDTO> createdDTOHandler) {
		return new SzakiranyCreator(createdDTOHandler, getSzakList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.szakirany.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}

	public List<SzakDTO> getSzakList() {
		if (szakList == null) {
			szakList = service.listAllSzak();
		}
		return szakList;
	}
}
