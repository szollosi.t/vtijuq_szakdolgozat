package vtijuq_szakdoli.popup.creator;

import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;
import vtijuq_szakdoli.formcreator.creator.CreatorFormCreator;
import vtijuq_szakdoli.popup.AbstractManipulatorPopup;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.captions.Caption;

public abstract class AbstractCreatorPopup<T extends EditableDTO> extends AbstractManipulatorPopup<T> implements CreatorPopup<T> {

	@Inject @Caption
	@ConfigurationValue(value = "button.cancel")
	private String cancelCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.validate")
	private String validateCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.create")
	private String createCaption;

	protected CreatorFormCreator<T> creator;

	@Override
	public DTOManipulator<T> getDTOManipulator() {
		return creator;
	}

	public void show(Consumer<T> createdDTOHandler) {
		creator = createCreator(createdDTOHandler);
		show();
	}

	protected abstract CreatorFormCreator<T> createCreator(Consumer<T> createdDTOHandler);

	@Override
	public final boolean isAccessibleByLoggedInUser() {
		return isEditableByLoggedInUser();
	}

	@Override
	protected HorizontalLayout createBottomButtons() {
		final HorizontalLayout buttons = new HorizontalLayout();
		addButton(buttons, Alignment.BOTTOM_LEFT, cancelCaption, e -> hide());
		if(renderValidateButton()) {
			addButton(buttons, Alignment.BOTTOM_CENTER, validateCaption, e -> validate());
		}
		addButton(buttons, Alignment.BOTTOM_RIGHT, createCaption, e -> create());
		return buttons;
	}

	private void create() {
		final boolean valid = validate();
		if (valid) {
			creator.create();
			hide();
		}
	}

	@Override
	protected boolean validate() {
		final boolean formValid = creator.validateForm();
		final boolean dtoValid = super.validate();
		return formValid && dtoValid;
	}

	protected Layout createForm() {
		return creator.createForm(false);
	}

}
