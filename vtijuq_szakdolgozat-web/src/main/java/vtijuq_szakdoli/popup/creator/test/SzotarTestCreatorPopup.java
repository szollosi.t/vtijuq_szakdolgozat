package vtijuq_szakdoli.popup.creator.test;

import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.formcreator.creator.test.SzotarTestCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;

import java.util.function.Consumer;

public class SzotarTestCreatorPopup extends AbstractCreatorPopup<SzotarTestDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzotarTestCreatorPopup.class);
	private static final String NAME = "SzotarTestFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.TEST;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected SzotarTestCreator createCreator(Consumer<SzotarTestDTO> createdDTOHandler) {
		return new SzotarTestCreator(createdDTOHandler);
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.test.creator.popup";
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();
		content.addComponent(createForm());
		return content;
	}
}
