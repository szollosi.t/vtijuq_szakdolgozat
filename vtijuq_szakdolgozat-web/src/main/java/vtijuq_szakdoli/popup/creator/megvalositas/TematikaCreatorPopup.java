package vtijuq_szakdoli.popup.creator.megvalositas;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.IsmeretSzintDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.formcreator.creator.megvalositas.TematikaCreator;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.popup.editor.megvalositas.TemaEditorPopup;
import vtijuq_szakdoli.service.megvalositas.TantargyService;

public class TematikaCreatorPopup extends AbstractCreatorPopup<TematikaDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(TematikaCreatorPopup.class);
	private static final String NAME = "TematikaFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.TANTARGY_SZERKESZTO;

	@Inject
	private TantargyService service;

	private List<TemaDTO> temaList;
	private List<IsmeretSzintDTO> leadottSzintList;
	private List<IsmeretSzintDTO> szamonkertSzintList;

	@Inject
	private TemaEditorPopup temaEditorPopup;
	@Inject
	private TemaCreatorPopup temaCreatorPopup;

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(temaEditorPopup, temaCreatorPopup).collect(Collectors.toSet());
	}

	@Override
	public TematikaCreator getDTOManipulator() {
		return (TematikaCreator) creator;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.tematika.creator.popup";
	}

	@Override
	protected TematikaCreator createCreator(Consumer<TematikaDTO> createdDTOHandler) {
		return new TematikaCreator(createdDTOHandler, getTemaList(), getLeadottSzintList(), getSzamonkertSzintList());
	}

	@Override
	protected boolean validate() {
		//FIXME a form invalid, de a felületen jól jelenik meg és a dto-ban sincs hiány
		final boolean formValid = true || getDTOManipulator().validateForm();
		final boolean dtoValid = validate(this::resolveMessage);
		return formValid && dtoValid;
	}

	private List<TemaDTO> getTemaList() {
		if (temaList == null ) {
			temaList = service.listAllTema();
		}
		return temaList;
	}

	private List<IsmeretSzintDTO> getLeadottSzintList() {
		if (leadottSzintList == null ) {
			leadottSzintList = service.listAllLeadottSzint();
		}
		return leadottSzintList;
	}

	private List<IsmeretSzintDTO> getSzamonkertSzintList() {
		if (szamonkertSzintList == null ) {
			szamonkertSzintList = service.listAllSzamonkertSzint();
		}
		return szamonkertSzintList;
	}

	@Override
	protected Layout createContent() {
		final Layout layout = new VerticalLayout();
		layout.addComponent(creator.createForm(false));
		layout.addComponent(createSajatTemaButtonsLayout());
		return layout;
	}

	private HorizontalLayout createSajatTemaButtonsLayout() {
		final HorizontalLayout buttons = new HorizontalLayout();
		addCreateSajatTemaButton(buttons);
		if (service.hasSajatTema(getDTOManipulator().getDTO()) ) {
			addEditSajatTemaButton(buttons);
		}
		return buttons;
	}

	private void addCreateSajatTemaButton(HorizontalLayout buttons) {
		final Button createSajatTemaButton = new Button();
		createSajatTemaButton.setCaption(resolveCaption("button.create.sajatTema"));
		createSajatTemaButton.addClickListener(event ->
				temaCreatorPopup.show(sajatTema -> {
					getDTOManipulator().getTemaList().add(sajatTema);
					getDTOManipulator().getDTO().setTema(sajatTema);
				}));
		buttons.addComponent(createSajatTemaButton);
	}

	private void addEditSajatTemaButton(HorizontalLayout buttons) {
		final Button editSajatTemaButton = new Button();
		editSajatTemaButton.setCaption(resolveCaption("button.edit.sajatTema"));
		editSajatTemaButton.addClickListener(event -> {
			final TemaDTO temaDTO = getDTOManipulator().getDTO().getTema();
			temaEditorPopup.showForEdit(
					temaDTO instanceof EditableDTO
							? (vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO)temaDTO
							: new vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO(temaDTO),
					sajatTema -> {
						if (!(getDTOManipulator().getDTO().getTema() instanceof EditableDTO)) {
							getDTOManipulator().getTemaList().remove(getDTOManipulator().getDTO().getTema());
							getDTOManipulator().getTemaList().add(sajatTema);
							getDTOManipulator().getDTO().setTema(sajatTema);
						}
					});
		});
		buttons.addComponent(editSajatTemaButton);
	}
}
