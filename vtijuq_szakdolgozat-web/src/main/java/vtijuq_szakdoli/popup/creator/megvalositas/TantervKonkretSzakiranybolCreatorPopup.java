package vtijuq_szakdoli.popup.creator.megvalositas;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.ErtekelesTipusDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.exception.ConsistencyException;
import vtijuq_szakdoli.formcreator.creator.megvalositas.TantervKonkretSzakiranybolCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.service.megvalositas.KonkretService;
import vtijuq_szakdoli.service.megvalositas.TantargyService;


public class TantervKonkretSzakiranybolCreatorPopup extends AbstractCreatorPopup<TantervDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(TantervKonkretSzakiranybolCreatorPopup.class);
	private static final String NAME = "TantervKonkretSzakiranybolFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.KONKRET_SZERKESZTO;

	@Inject
	private KonkretService konkretService;

	@Inject
	private TantargyService tantargyService;

	private KonkretSzakiranyDTO konkretSzakirany;
	private List<TantargyDTO> tantargyList;
	private List<ErtekelesTipusDTO> ertekelesTipusList;

	public void show(KonkretSzakiranyDTO konkretSzakirany, Consumer<TantervDTO> createdDTOHandler) {
		this.konkretSzakirany = konkretSzakirany;
		creator = createCreator(createdDTOHandler);
		creator.getDTO().setKonkretSzakirany(konkretSzakirany);
		show();
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected TantervKonkretSzakiranybolCreator createCreator(Consumer<TantervDTO> createdDTOHandler) {
		return new TantervKonkretSzakiranybolCreator(createdDTOHandler, getKonkretSzakiranyList(), getTantargyList(), getErtekelesTipusList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.tanterv.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}

	public List<KonkretSzakiranyDTO> getKonkretSzakiranyList() {
		if (konkretSzakirany == null ) {
			throw new ConsistencyException("error.consistency.tanterv.creator.konkretSzakirany.null");
		}
		return Collections.singletonList(konkretSzakirany);
	}

	private List<TantargyDTO> getTantargyList() {
		if (tantargyList == null ) {
			tantargyList = tantargyService.listAllKonkretTantargy();
		}
		return tantargyList;
	}

	private List<ErtekelesTipusDTO> getErtekelesTipusList() {
		if (ertekelesTipusList == null ) {
			ertekelesTipusList = konkretService.listAllErtekelesTipus();
		}
		return ertekelesTipusList;
	}
}
