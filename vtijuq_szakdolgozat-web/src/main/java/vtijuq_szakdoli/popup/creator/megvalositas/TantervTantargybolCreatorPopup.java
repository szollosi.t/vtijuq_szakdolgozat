package vtijuq_szakdoli.popup.creator.megvalositas;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.ErtekelesTipusDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.exception.ConsistencyException;
import vtijuq_szakdoli.formcreator.creator.megvalositas.TantervTantargybolCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.service.megvalositas.KonkretService;

public class TantervTantargybolCreatorPopup extends AbstractCreatorPopup<TantervDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(TantervTantargybolCreatorPopup.class);
	private static final String NAME = "TantargyFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.TANTARGY_SZERKESZTO;

	@Inject
	private KonkretService konkretService;

	private List<KonkretSzakiranyDTO> konkretSzakiranyList;
	private TantargyDTO tantargy;
	private List<ErtekelesTipusDTO> ertekelesTipusList;

	public void show(TantargyDTO tantargy, Consumer<TantervDTO> createdDTOHandler) {
		this.tantargy = tantargy;
		creator = createCreator(createdDTOHandler);
		creator.getDTO().setTantargy(tantargy);
		show();
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected TantervTantargybolCreator createCreator(Consumer<TantervDTO> createdDTOHandler) {
		return new TantervTantargybolCreator(createdDTOHandler, getKonkretSzakiranyList(), getTantargyList(), getErtekelesTipusList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.tanterv.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}

	public List<KonkretSzakiranyDTO> getKonkretSzakiranyList() {
		if (konkretSzakiranyList == null ) {
			konkretSzakiranyList = konkretService.listAllKonkretSzakirany();
		}
		return konkretSzakiranyList;
	}

	public List<TantargyDTO> getTantargyList() {
		if (tantargy == null ) {
			throw new ConsistencyException("error.consistency.tanterv.creator.tantargy.null");
		}
		return Collections.singletonList(tantargy);
	}

	public List<ErtekelesTipusDTO> getErtekelesTipusList() {
		if (ertekelesTipusList == null ) {
			ertekelesTipusList = konkretService.listAllErtekelesTipus();
		}
		return ertekelesTipusList;
	}
}
