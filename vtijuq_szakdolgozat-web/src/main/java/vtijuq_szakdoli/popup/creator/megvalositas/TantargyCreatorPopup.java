package vtijuq_szakdoli.popup.creator.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.formcreator.creator.megvalositas.TantargyCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;

public class TantargyCreatorPopup extends AbstractCreatorPopup<TantargyDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(TantargyCreatorPopup.class);
	private static final String NAME = "TantargyFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.TANTARGY_KARBANTARTO;

	@Inject
	private AdminisztracioService adminisztracioService;

	private List<SzervezetDTO> szervezetList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected TantargyCreator createCreator(Consumer<TantargyDTO> createdDTOHandler) {
		return new TantargyCreator(createdDTOHandler, getSzervezetList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.tantargy.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}

	public List<SzervezetDTO> getSzervezetList() {
		if (szervezetList == null ) {
			szervezetList = adminisztracioService.listAllSzervezet();
		}
		return szervezetList;
	}
}
