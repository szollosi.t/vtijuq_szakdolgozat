package vtijuq_szakdoli.popup.creator.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.formcreator.creator.megvalositas.KonkretSzakCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.service.adminisztracio.AdminisztracioService;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;

public class KonkretSzakCreatorPopup extends AbstractCreatorPopup<KonkretSzakDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(KonkretSzakCreatorPopup.class);
	private static final String NAME = "KonkretSzakFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.KONKRET_SZERKESZTO;

	@Inject
	private AdminisztracioService adminisztracioService;

	@Inject
	private KovetelmenyService kovetelmenyService;

	private List<SzakKovetelmenyDTO> szakKovetelmenyList;
	private List<SzervezetDTO> szervezetList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected KonkretSzakCreator createCreator(Consumer<KonkretSzakDTO> createdDTOHandler) {
		return new KonkretSzakCreator(createdDTOHandler, getSzakKovetelmenyList(), getSzervezetList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.konkretSzak.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}

	private List<SzakKovetelmenyDTO> getSzakKovetelmenyList() {
		if (szakKovetelmenyList == null) {
			szakKovetelmenyList = kovetelmenyService.listAllSzakKovetelmeny();
		}
		return szakKovetelmenyList;
	}

	private List<SzervezetDTO> getSzervezetList() {
		if (szervezetList == null) {
			szervezetList = adminisztracioService.listAllSzervezet();
		}
		return szervezetList;
	}
}
