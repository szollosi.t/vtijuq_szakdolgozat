package vtijuq_szakdoli.popup.creator.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.formcreator.creator.megvalositas.TemaCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.service.kovetelmeny.IsmeretService;

public class TemaCreatorPopup extends AbstractCreatorPopup<TemaDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(TemaCreatorPopup.class);
	private static final String NAME = "TemaFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.TANTARGY_SZERKESZTO;

	@Inject
	private IsmeretService ismeretService;

	private List<IsmeretDTO> ismeretList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected TemaCreator createCreator(Consumer<TemaDTO> createdDTOHandler) {
		return new TemaCreator(createdDTOHandler, getIsmeretList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.tema.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}

	private List<IsmeretDTO> getIsmeretList() {
		if (ismeretList == null ) {
			ismeretList = ismeretService.listAllIsmeret();
		}
		return ismeretList;
	}
}
