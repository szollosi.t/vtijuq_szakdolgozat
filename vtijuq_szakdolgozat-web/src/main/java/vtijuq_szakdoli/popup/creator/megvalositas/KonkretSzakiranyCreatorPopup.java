package vtijuq_szakdoli.popup.creator.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.formcreator.creator.megvalositas.KonkretSzakiranyCreator;
import vtijuq_szakdoli.popup.creator.AbstractCreatorPopup;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;
import vtijuq_szakdoli.service.megvalositas.KonkretService;

public class KonkretSzakiranyCreatorPopup extends AbstractCreatorPopup<KonkretSzakiranyDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(KonkretSzakiranyCreatorPopup.class);
	private static final String NAME = "KonkretSzakiranyFelvitel";
	private static final String FUNKCIO_NEV = FunctionNames.KONKRET_SZERKESZTO;

	@Inject
	private KonkretService konkretService;

	@Inject
	private KovetelmenyService kovetelmenyService;

	private List<KonkretSzakDTO> konkretSzakList;
	private List<SzakiranyKovetelmenyDTO> szakiranyKovetelmenyList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected KonkretSzakiranyCreator createCreator(Consumer<KonkretSzakiranyDTO> createdDTOHandler) {
		return new KonkretSzakiranyCreator(createdDTOHandler, getKonkretSzakList(), getSzakiranyKovetelmenyList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.konkretSzakirany.creator.popup";
	}

	@Override
	protected Layout createContent() {
		return creator.createForm(false);
	}

	public List<KonkretSzakDTO> getKonkretSzakList() {
		if (konkretSzakList == null) {
			konkretSzakList = konkretService.listAllKonkretSzak();
		}
		return konkretSzakList;
	}

	public List<SzakiranyKovetelmenyDTO> getSzakiranyKovetelmenyList() {
		if (szakiranyKovetelmenyList == null) {
			szakiranyKovetelmenyList = kovetelmenyService.listAllSzakiranyKovetelmeny();
		}
		return szakiranyKovetelmenyList;
	}
}
