package vtijuq_szakdoli.popup;

public interface Popup {

	void show();

	void hide();

}
