package vtijuq_szakdoli.popup;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.PopupView.PopupVisibilityEvent;
import com.vaadin.ui.PopupView.PopupVisibilityListener;

import vtijuq_szakdoli.layout.AbstractRestrictableLayout;

public abstract class AbstractPopup extends AbstractRestrictableLayout implements Popup, PopupVisibilityListener {

	private PopupView popupView;
	private Runnable atHideAction;

	public AbstractPopup(){
		super();
		popupView = new PopupView(null, this);
		popupView.setHideOnMouseOut(false);
		popupView.addPopupVisibilityListener(this);
	}

	public final void addPopupView(final AbstractRestrictableLayout layout){
		layout.addComponent(popupView);
		atHideAction = layout::refresh;
	}

	@Override
	public void show() {
		popupView.setPopupVisible(true);
	}

	@Override
	public void hide() {
		popupView.setPopupVisible(false);
	}

	@Override
	public void popupVisibilityChange(PopupVisibilityEvent event) {
		if (event.isPopupVisible()) {
			refresh();
		} else if (atHideAction != null) {
			atHideAction.run();
		}
	}

	@Override
	protected HorizontalLayout createTopButtons() {
		return null;
	}
}
