package vtijuq_szakdoli.popup.editor.test;

import java.util.function.Consumer;

import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.formcreator.editor.test.SzotarTestEditor;
import vtijuq_szakdoli.popup.editor.AbstractEditorPopup;

public class SzotarTestEditorPopup extends AbstractEditorPopup<SzotarTestDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzotarTestEditorPopup.class);
	private static final String NAME = "SzotarTestSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.TEST;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected SzotarTestEditor createEditor(SzotarTestDTO dto, Consumer<SzotarTestDTO> editedDTOHandler) {
		return new SzotarTestEditor(dto, editedDTOHandler);
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.test.editor.popup";
	}

	@Override
	protected Layout createContent() {
		VerticalLayout content = new VerticalLayout();
		content.addComponent(createForm());
		return content;
	}
}
