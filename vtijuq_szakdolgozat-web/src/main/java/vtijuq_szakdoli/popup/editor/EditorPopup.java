package vtijuq_szakdoli.popup.editor;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.layout.Editor;
import vtijuq_szakdoli.popup.ManipulatorPopup;

public interface EditorPopup<T extends EditableDTO> extends Editor<T>, ManipulatorPopup<T> {

	void show(T dto, Consumer<T> editedDTOHandler);

}
