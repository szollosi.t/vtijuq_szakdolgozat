package vtijuq_szakdoli.popup.editor.kovetelmeny;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.formcreator.editor.kovetelmeny.SzakmaiJellemzoEditor;
import vtijuq_szakdoli.popup.editor.AbstractEditorPopup;
import vtijuq_szakdoli.service.kovetelmeny.IsmeretService;
import vtijuq_szakdoli.service.kovetelmeny.KovetelmenyService;

public class SzakmaiJellemzoEditorPopup extends AbstractEditorPopup<SzakmaiJellemzoDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SzakmaiJellemzoEditorPopup.class);
	private static final String NAME = "SzakmaiJellemzoSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.KOVETELMENY_SZERKESZTO;

	@Inject
	private KovetelmenyService service;

	@Inject
	private IsmeretService ismeretService;

	private List<IsmeretDTO> ismeretList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected SzakmaiJellemzoEditor createEditor(SzakmaiJellemzoDTO dto, Consumer<SzakmaiJellemzoDTO> editedDTOHandler) {
		return new SzakmaiJellemzoEditor(dto, editedDTOHandler, getIsmeretList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.szakmaiJellemzo.editor.popup";
	}

	@Override
	protected Layout createContent() {
		final Layout layout = editor.createForm(!isEditable());
		//TODO
		return layout;
	}

	public List<IsmeretDTO> getIsmeretList() {
		if (ismeretList == null ) {
			ismeretList = ismeretService.listAllIsmeret();
		}
		return ismeretList;
	}
}
