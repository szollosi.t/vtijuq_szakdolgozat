package vtijuq_szakdoli.popup.editor.kovetelmeny;

import java.util.function.Consumer;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.formcreator.editor.kovetelmeny.IsmeretEditor;
import vtijuq_szakdoli.popup.editor.AbstractEditorPopup;

public class IsmeretEditorPopup extends AbstractEditorPopup<IsmeretDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(IsmeretEditorPopup.class);
	private static final String NAME = "IsmeretSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.ISMERET_SZERKESZTO;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected IsmeretEditor createEditor(IsmeretDTO dto, Consumer<IsmeretDTO> editedDTOHandler) {
		return new IsmeretEditor(dto, editedDTOHandler);
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.ismeret.editor.popup";
	}

	@Override
	protected Layout createContent() {
		final Layout layout = editor.createForm(!isEditable());
		//TODO
		return layout;
	}
}
