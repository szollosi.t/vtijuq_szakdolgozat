package vtijuq_szakdoli.popup.editor.megvalositas;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.ErtekelesTipusDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.exception.ConsistencyException;
import vtijuq_szakdoli.formcreator.editor.megvalositas.TantervEditor;
import vtijuq_szakdoli.popup.editor.AbstractEditorPopup;
import vtijuq_szakdoli.service.megvalositas.KonkretService;
import vtijuq_szakdoli.service.megvalositas.TantargyService;

public class TantervKonkretSzakiranybolEditorPopup extends AbstractEditorPopup<TantervDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(TantervKonkretSzakiranybolEditorPopup.class);
	private static final String NAME = "TantervSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.KONKRET_SZERKESZTO;

	@Inject
	private KonkretService konkretService;

	@Inject
	private TantargyService tantargyService;

	private KonkretSzakiranyDTO konkretSzakirany;
	private List<TantargyDTO> tantargyList;
	private List<ErtekelesTipusDTO> ertekelesTipusList;

	@Override
	public void show(TantervDTO dto, Consumer<TantervDTO> editedDTOHandler) {
		this.konkretSzakirany = dto.getKonkretSzakirany();
		super.show(dto, editedDTOHandler);
	}

	@Override
	public void showForEdit(TantervDTO dto, Consumer<TantervDTO> editedDTOHandler) {
		this.konkretSzakirany = dto.getKonkretSzakirany();
		super.showForEdit(dto, editedDTOHandler);
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected TantervEditor createEditor(TantervDTO dto, Consumer<TantervDTO> editedDTOHandler) {
		return new TantervEditor(dto, editedDTOHandler, getKonkretSzakiranyList(), getTantargyList(), getErtekelesTipusList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.tanterv.editor.popup";
	}

    @Override
    public boolean isFinal(Long id) {
        return getValidatorService().isTantervFinal(id);
    }

	@Override
	protected Layout createContent() {
		final Layout layout = editor.createForm(!isEditable());
		//TODO
		return layout;
	}

	public List<KonkretSzakiranyDTO> getKonkretSzakiranyList() {
		if (konkretSzakirany == null ) {
			throw new ConsistencyException("error.consistency.tanterv.editor.konkretSzakirany.null");
		}
		return Collections.singletonList(konkretSzakirany);
	}

    private List<TantargyDTO> getTantargyList() {
		if (tantargyList == null ) {
			tantargyList = tantargyService.listAllKonkretTantargy();
		}
		return tantargyList;
	}

    private List<ErtekelesTipusDTO> getErtekelesTipusList() {
		if (ertekelesTipusList == null ) {
			ertekelesTipusList = konkretService.listAllErtekelesTipus();
		}
		return ertekelesTipusList;
	}
}
