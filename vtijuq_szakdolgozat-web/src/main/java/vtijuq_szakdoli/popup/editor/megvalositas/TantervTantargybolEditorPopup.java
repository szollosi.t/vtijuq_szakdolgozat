package vtijuq_szakdoli.popup.editor.megvalositas;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.ErtekelesTipusDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.exception.ConsistencyException;
import vtijuq_szakdoli.formcreator.editor.megvalositas.TantervEditor;
import vtijuq_szakdoli.popup.editor.AbstractEditorPopup;
import vtijuq_szakdoli.service.megvalositas.KonkretService;

public class TantervTantargybolEditorPopup extends AbstractEditorPopup<TantervDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(TantervTantargybolEditorPopup.class);
	private static final String NAME = "TantervSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.TANTARGY_SZERKESZTO;

	@Inject
	private KonkretService konkretService;

	private List<KonkretSzakiranyDTO> konkretSzakiranyList;
	private TantargyDTO tantargy;
	private List<ErtekelesTipusDTO> ertekelesTipusList;

	@Override
	public void show(TantervDTO dto, Consumer<TantervDTO> editedDTOHandler) {
		this.tantargy = dto.getTantargy();
		super.show(dto, editedDTOHandler);
	}

	@Override
	public void showForEdit(TantervDTO dto, Consumer<TantervDTO> editedDTOHandler) {
		this.tantargy = dto.getTantargy();
		super.showForEdit(dto, editedDTOHandler);
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected TantervEditor createEditor(TantervDTO dto, Consumer<TantervDTO> editedDTOHandler) {
		return new TantervEditor(dto, editedDTOHandler, getKonkretSzakiranyList(), getTantargyList(), getErtekelesTipusList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.tanterv.editor.popup";
	}

	@Override
    public boolean isFinal(Long id) {
        return getValidatorService().isTantervFinal(id);
	}

	@Override
	protected Layout createContent() {
		final Layout layout = editor.createForm(!isEditable());
		//TODO
		return layout;
	}

    private List<KonkretSzakiranyDTO> getKonkretSzakiranyList() {
		if (konkretSzakiranyList == null ) {
			konkretSzakiranyList = konkretService.listAllKonkretSzakirany();
		}
		return konkretSzakiranyList;
	}

    private List<TantargyDTO> getTantargyList() {
		if (tantargy == null ) {
			throw new ConsistencyException("error.consistency.tanterv.editor.tantargy.null");
		}
		return Collections.singletonList(tantargy);
	}

    private List<ErtekelesTipusDTO> getErtekelesTipusList() {
		if (ertekelesTipusList == null ) {
			ertekelesTipusList = konkretService.listAllErtekelesTipus();
		}
		return ertekelesTipusList;
	}
}
