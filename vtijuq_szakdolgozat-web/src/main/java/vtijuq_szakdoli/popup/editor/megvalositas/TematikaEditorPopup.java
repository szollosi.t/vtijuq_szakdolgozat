package vtijuq_szakdoli.popup.editor.megvalositas;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.IsmeretSzintDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.formcreator.editor.megvalositas.TematikaEditor;
import vtijuq_szakdoli.popup.AbstractPopup;
import vtijuq_szakdoli.popup.creator.megvalositas.TemaCreatorPopup;
import vtijuq_szakdoli.popup.editor.AbstractEditorPopup;
import vtijuq_szakdoli.service.megvalositas.TantargyService;

public class TematikaEditorPopup extends AbstractEditorPopup<TematikaDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(TematikaEditorPopup.class);
	private static final String NAME = "TematikaSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.TANTARGY_SZERKESZTO;

	@Inject
	private TantargyService service;

	private List<IsmeretSzintDTO> szamonkertSzintList;
	private List<IsmeretSzintDTO> leadottSzintList;
	private List<TemaDTO> temaList;

	@Inject
	private TemaEditorPopup temaEditorPopup;
	@Inject
	private TemaCreatorPopup temaCreatorPopup;

	@Override
	protected Set<AbstractPopup> getPopups() {
		return Stream.of(temaEditorPopup, temaCreatorPopup).collect(Collectors.toSet());
	}

	@Override
	public TematikaEditor getDTOManipulator() {
		return (TematikaEditor) editor;
	}

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected TematikaEditor createEditor(TematikaDTO dto, Consumer<TematikaDTO> editedDTOHandler) {
		return new TematikaEditor(dto, editedDTOHandler, getSzamonkertSzintList(), getLeadottSzintList(), getTemaList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.tematika.editor.popup";
	}

	@Override
	public boolean isFinal(Long id) {
		return getValidatorService().isTematikaFinal(id);
	}

	@Override
	protected Layout createContent() {
		final Layout layout = new VerticalLayout();
		layout.addComponent(editor.createForm(!isEditable()));
		if (isEditable()) {
			layout.addComponent(createSajatTemaButtonsLayout());
		}
		return layout;
	}

	private HorizontalLayout createSajatTemaButtonsLayout() {
		final HorizontalLayout buttons = new HorizontalLayout();
		addCreateSajatTemaButton(buttons);
		if (service.hasSajatTema(getDTOManipulator().getDTO()) ) {
			addEditSajatTemaButton(buttons);
		}
		return buttons;
	}

	private void addCreateSajatTemaButton(HorizontalLayout buttons) {
		final Button createSajatTemaButton = new Button();
		createSajatTemaButton.setCaption(resolveCaption("button.create.sajatTema"));
		createSajatTemaButton.addClickListener(event ->
				temaCreatorPopup.show(sajatTema -> {
					getDTOManipulator().getTemaList().add(sajatTema);
					getDTOManipulator().getDTO().setTema(sajatTema);
				}));
		buttons.addComponent(createSajatTemaButton);
	}

	private void addEditSajatTemaButton(HorizontalLayout buttons) {
		final Button editSajatTemaButton = new Button();
		editSajatTemaButton.setCaption(resolveCaption("button.edit.sajatTema"));
		editSajatTemaButton.addClickListener(event -> {
			final TemaDTO temaDTO = getDTOManipulator().getDTO().getTema();
			temaEditorPopup.showForEdit(
					temaDTO instanceof EditableDTO
							? (vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO)temaDTO
							: new vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO(temaDTO),
					sajatTema -> {
						if (!(getDTOManipulator().getDTO().getTema() instanceof EditableDTO)) {
							getDTOManipulator().getTemaList().remove(getDTOManipulator().getDTO().getTema());
							getDTOManipulator().getTemaList().add(sajatTema);
							getDTOManipulator().getDTO().setTema(sajatTema);
						}
					});
		});
		buttons.addComponent(editSajatTemaButton);
	}

	private List<IsmeretSzintDTO> getSzamonkertSzintList() {
		if (szamonkertSzintList == null) {
			szamonkertSzintList = service.listAllSzamonkertSzint();
		}
		return szamonkertSzintList;
	}

	private List<IsmeretSzintDTO> getLeadottSzintList() {
		if (leadottSzintList == null) {
			leadottSzintList = service.listAllLeadottSzint();
		}
		return leadottSzintList;
	}

	private List<TemaDTO> getTemaList() {
		if (temaList == null) {
			temaList = service.listAllTema();
		}
		return temaList;
	}
}
