package vtijuq_szakdoli.popup.editor.megvalositas;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.constants.FunctionNames;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.formcreator.editor.megvalositas.TemaEditor;
import vtijuq_szakdoli.popup.editor.AbstractEditorPopup;
import vtijuq_szakdoli.service.kovetelmeny.IsmeretService;
import vtijuq_szakdoli.service.megvalositas.TantargyService;

public class TemaEditorPopup extends AbstractEditorPopup<TemaDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(TemaEditorPopup.class);
	private static final String NAME = "TemaSzerkeszto";
	private static final String FUNKCIO_NEV = FunctionNames.TANTARGY_SZERKESZTO;

	@Inject
	private TantargyService service;

	@Inject
	private IsmeretService ismeretService;

	private List<IsmeretDTO> ismeretList;

	@Override
	public String getFunkcioNev() {
		return FUNKCIO_NEV;
	}

	@Override
	protected TemaEditor createEditor(TemaDTO dto, Consumer<TemaDTO> editedDTOHandler) {
		return new TemaEditor(dto, editedDTOHandler, getIsmeretList());
	}

	@Override
	public String getTitleCaptionKey() {
		return "title.tema.editor.popup";
	}

    @Override
    public boolean isFinal(Long id) {
        return getValidatorService().isTemaFinal(id);
    }

    @Override
	protected Layout createContent() {
		final Layout layout = editor.createForm(!isEditable());
		//TODO
		return layout;
	}

	private List<IsmeretDTO> getIsmeretList() {
		if (ismeretList == null) {
			ismeretList = ismeretService.listAllIsmeret();
		}
		return ismeretList;
	}
}
