package vtijuq_szakdoli.popup.editor;

import java.util.function.Consumer;

import javax.inject.Inject;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import lombok.AccessLevel;
import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;
import vtijuq_szakdoli.formcreator.editor.EditorFormCreator;
import vtijuq_szakdoli.popup.AbstractManipulatorPopup;
import vtijuq_szakdoli.service.ValidatorService;
import vtijuq_szakdoli.util.cdiconfig.ConfigurationValue;
import vtijuq_szakdoli.util.cdiconfig.captions.Caption;

public abstract class AbstractEditorPopup<T extends EditableDTO> extends AbstractManipulatorPopup<T> implements EditorPopup<T> {

	@Inject @Caption
	@ConfigurationValue(value = "button.back")
	private String backCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.edit")
	private String editCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.cancel")
	private String cancelCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.validate")
	private String validateCaption;

	@Inject @Caption
	@ConfigurationValue(value = "button.save")
	private String saveCaption;

	@Inject @Getter(AccessLevel.PROTECTED)
	private ValidatorService validatorService;

	protected EditorFormCreator<T> editor;

	protected boolean alwaysReadOnly;
	protected boolean editable;

	public void init(boolean alwaysReadOnly) {
		this.alwaysReadOnly = alwaysReadOnly;
		editable = false;
	}

	protected boolean isFinal(Long id){
		return false;
	}

	@Override
	public DTOManipulator<T> getDTOManipulator() {
		return editor;
	}

	private void changeToEditable() {
		editable = true;
		refresh();
	}

	protected void changeToReadOnly() {
		editable = false;
		refresh();
	}

	@Override
	protected HorizontalLayout createBottomButtons() {
		final HorizontalLayout buttons = new HorizontalLayout();
		if (isEditable()) {
			addEditableButtons(buttons);
		} else {
			addReadOnlyButtons(buttons);
		}
		return buttons;
	}

	protected boolean isEditable() {
		return !alwaysReadOnly && isEditableByLoggedInUser() && editable;
	}

	private void addReadOnlyButtons(final HorizontalLayout buttons) {
		addButton(buttons, Alignment.BOTTOM_LEFT, backCaption, e -> hide());
		if (!alwaysReadOnly && isEditableByLoggedInUser()) {
			addButton(buttons, Alignment.BOTTOM_RIGHT, editCaption, e -> changeToEditable());
		}
	}

	private void addEditableButtons(final HorizontalLayout buttons) {
		addButton(buttons, Alignment.BOTTOM_LEFT, cancelCaption, e -> hide());
		if(renderValidateButton()) {
			addButton(buttons, Alignment.BOTTOM_CENTER, validateCaption, e -> validate());
		}
		addButton(buttons, Alignment.BOTTOM_RIGHT, saveCaption, e -> edit());
	}

	public void showForEdit(T dto, Consumer<T> editedDTOHandler){
		if (!isFinal(dto.getId())) {
			editable = true;
		}
		show(dto, editedDTOHandler);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void show(T dto, Consumer<T> editedDTOHandler) {
		if (isFinal(dto.getId())) {
			alwaysReadOnly = true;
		}
		editor = createEditor(dto, editedDTOHandler);
		show();
	}

	protected abstract EditorFormCreator<T> createEditor(T dto, Consumer<T> editedDTOHandler);

	@Override
	public void hide() {
		editor = null;
		editable = false;
		super.hide();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void edit() {
		final boolean valid = validate();
		if (valid) {
			editor.edit();
			//changeToReadOnly(); TODO végiggondolni miért volt így
			hide();
		}
	}

	protected Layout createForm() {
		return editor.createForm(!isEditable());
	}

	@Override
	protected boolean validate() {
		return editor.validateForm() && super.validate();
	}
}
