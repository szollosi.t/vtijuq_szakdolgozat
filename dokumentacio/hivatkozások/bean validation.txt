Bean Validation - Home
http://beanvalidation.org/

wildfly-quickstart/pom.xml at master · darranl/wildfly-quickstart · GitHub
https://github.com/darranl/wildfly-quickstart/blob/master/bean-validation/pom.xml

Getting started with Hibernate Validator - Hibernate Validator
http://hibernate.org/validator/documentation/getting-started/

Bean Validation in Vaadin 7 - DZone Java
https://dzone.com/articles/bean-validation-vaadin-7

vaadin7 - Vaadin Bean validation for table - Stack Overflow
https://stackoverflow.com/questions/17919636/vaadin-bean-validation-for-table

Vaadin 7 Validation (BeanValidator and setrequired) - Stack Overflow
https://stackoverflow.com/questions/16169286/vaadin-7-validation-beanvalidator-and-setrequired

exception - Vaadin 7 - Bean Validation - Stack Overflow
https://stackoverflow.com/questions/16163162/vaadin-7-bean-validation

Using bean validation
https://www.packtpub.com/mapt/book/web-development/9781849518802/7/ch07lvl1sec67/using-bean-validation

wildfly-bean-validation : 10.1.0.Final - WildFly: Parent Aggregator
https://www.versioneye.com/java/org.wildfly:wildfly-bean-validation/10.1.0.Final

Java EE 7 Implementations in WildFly (Tech Tip #3)
http://blog.arungupta.me/java-ee-7-implementations-in-wildfly-tech-tip-3/

Chapter 6. Creating custom constraints
https://docs.jboss.org/hibernate/validator/5.0/reference/en-US/html/validator-customconstraints.html

Chapter 2. Validation step by step
https://docs.jboss.org/hibernate/validator/4.1/reference/en-US/html/validator-usingvalidator.html

Bean Validation in Java EE 7 - Creating Custom Constraints and Validations
http://javabeat.net/bean-validation-java-ee-creating-custom-constraints-validations/
