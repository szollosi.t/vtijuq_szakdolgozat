Named bean not scanned in a Maven multi module ... | JBoss Developer
https://developer.jboss.org/thread/178733

Getting Started Guide - WildFly 10 - Project Documentation Editor
https://docs.jboss.org/author/display/WFLY10/Getting+Started+Guide?_sscc=t

JBoss Wildfly Application Server Tutorial | Examples Java Code Geeks - 2016
https://examples.javacodegeeks.com/enterprise-java/jboss-wildfly/jboss-wildfly-application-server-tutorial/

Tutorials » Installing and Configuring WildFly and JBoss Tools
https://wwu-pi.github.io/tutorials/lectures/eai/010_tutorial_jboss_setup.html

Maven configuration for Java EE 7 projects on WildFly
http://www.mastertheboss.com/jboss-server/wildfly-8/maven-configuration-for-java-ee-7-projects-on-wildfly

Introducing the Java EE Web Profile - JAXenter
https://jaxenter.com/introducing-the-java-ee-web-profile-103275.html

Java EE7 and Maven project for newbies - part 1 - a simple maven project structure - the parent pom
https://www.javacodegeeks.com/2014/04/java-ee7-and-maven-project-for-newbies-part-1-a-simple-maven-project-structure-the-parent-pom.html

Java EE7 and Maven project for newbies - part 2 - defining a simple war for our application
https://www.javacodegeeks.com/2014/05/java-ee7-and-maven-project-for-newbies-part-2-defining-a-simple-war-for-our-application.html

Papo's log: Java EE7 and Maven project for newbies - part 3 - defining the ejb services and jpa entities modules #javaee #java #maven #juniordevs
http://javapapo.blogspot.hu/2014/05/java-ee7-and-maven-project-for-newbies.html

Papo's log: Java EE7 and Maven project for newbies - part 4 - defining the ear module #javaee #java #maven #juniordevs
http://javapapo.blogspot.hu/2014/05/java-ee7-and-maven-project-for-newbies_30.html

Papo's log: Java EE7 and Maven project for newbies - part 5 - Unit testing using Arquillian / Wildfly 8 #maven #jboss #wildfly #arquillian
http://javapapo.blogspot.hu/2014/06/java-ee7-and-maven-project-for-newbies.html

Papo's log: Java EE7 and Maven project for newbies - part 6 - #jpa2 and Unit testing using #arquillian / #WildflyAs 8.1 #maven #jboss
http://javapapo.blogspot.hu/2014/06/java-ee7-and-maven-project-for-newbies_23.html

Papo's log: Java EE7 and Maven project for newbies - part 7 - #jpa2 and Unit testing using #arquillian / #WildflyAs 8.1 #maven #jboss - using a real Datasource (#postgresql)
http://javapapo.blogspot.hu/2014/07/java-ee7-and-maven-project-for-newbies.html

Java EE7 and Maven project for newbies - part 8 | Java Code Geeks - 2016
https://www.javacodegeeks.com/2015/03/java-ee7-and-maven-project-for-newbies-part-8.html

Antal Gábor weboldala
http://www.inf.u-szeged.hu/~antalg/prf.html

Class Loading in WildFly - WildFly 10 - Project Documentation Editor
https://docs.jboss.org/author/display/WFLY10/Class+Loading+in+WildFly

Oracle Database Express Edition 11g Release 2
http://www.oracle.com/technetwork/database/database-technologies/express-edition/overview/index.html

Re: java.lang.ClassNotFoundException: com.mysema.query.types.EntityPath - Google Groups
https://groups.google.com/forum/#!topic/querydsl/C9P7JwSZRIo

How to fetch entities multiple levels deep with Hibernate | Vlad Mihalcea's Blog
https://vladmihalcea.com/2013/10/22/hibernate-facts-multi-level-fetching/

Maven Repository: org.hibernate » hibernate-annotations
https://mvnrepository.com/artifact/org.hibernate/hibernate-annotations

Maven Repository: org.wildfly.bom » wildfly-javaee7-with-tools » 10.1.0.Final
https://mvnrepository.com/artifact/org.wildfly.bom/wildfly-javaee7-with-tools/10.1.0.Final

java - org.hibernate.loader.MultipleBagFetchException: cannot simultaneously fetch multiple bags - Stack Overflow
https://stackoverflow.com/questions/24675340/org-hibernate-loader-multiplebagfetchexception-cannot-simultaneously-fetch-mult

ejb 3.0 - Where to use EJB 3.1 and CDI? - Stack Overflow
https://stackoverflow.com/questions/13487987/where-to-use-ejb-3-1-and-cdi

java ee - @ApplicationScoped CDI bean and @PersistenceContext - is this safe? - Stack Overflow
https://stackoverflow.com/questions/13885918/applicationscoped-cdi-bean-and-persistencecontext-is-this-safe

Installing Oracle JDBC-Driver On Wildfly / JBoss : Adam Bien's Weblog
http://www.adam-bien.com/roller/abien/entry/installing_oracle_jdbc_driver_on
