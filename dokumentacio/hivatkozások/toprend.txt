Fix Oracle mutating trigger table errors
http://www.dba-oracle.com/t_avoiding_mutating_table_error.htm

ORACLE-BASE - Oracle9i Mutating Table Exceptions
https://oracle-base.com/articles/9i/mutating-table-exceptions

graph theory - DAG proof by numbering nodes - Mathematics Stack Exchange
https://math.stackexchange.com/questions/1186230/dag-proof-by-numbering-nodes

A Dynamic Topological Sort Algorithm for Directed Acyclic Graphs
ACMJ212-07.tex - DynamicTopoSortAlg-JEA-07.pdf
https://www.doc.ic.ac.uk/~phjk/Publications/DynamicTopoSortAlg-JEA-07.pdf

ORACLE-BASE - Trigger Enhancements in Oracle Database 11g Release 1
https://oracle-base.com/articles/11g/trigger-enhancements-11gr1

ORACLE-BASE - Database Triggers Overview
https://oracle-base.com/articles/misc/database-triggers-overview