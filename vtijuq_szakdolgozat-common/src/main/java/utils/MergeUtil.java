package utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import java.util.Collection;
//import vtijuq_szakdoli.common.dto.collectionDTO.CollectionDTO;

//OLD_FIXME nem oldható meg jól az Entitas-DTO, DTO-DTO merge, mert nem tudom kideríteni az osztályokat a collection-ből
//azaz nem tudom, milyen DTO-t kell példányosítani
//úgyhogy úgy tűnik, máshol kell megoldanom a mapping-t
public class MergeUtil {

	private static final Logger LOG = LoggerFactory.getLogger(MergeUtil.class);

	private static final Set<String> excludedFields = Stream.of("class", "entitasClass").collect(Collectors.toSet());

	public static <T, S> S merge(T source, S target) {
		List<String> commonPropertyNames = ListUtils.intersection(
				Arrays.stream(PropertyUtils.getPropertyDescriptors(source)).map(PropertyDescriptor::getName).collect(Collectors.toList()),
				Arrays.stream(PropertyUtils.getPropertyDescriptors(target)).map(PropertyDescriptor::getName).collect(Collectors.toList()));
		List<String> mergablePropertyNames = commonPropertyNames.stream()
				.filter(name -> //not excluded explicitly
				        !excludedFields.contains(name))
				.filter(name -> //mergable by other means
				        PropertyUtils.isReadable(source, name) && PropertyUtils.isWriteable(target, name))
				.collect(Collectors.toList());
		mergablePropertyNames.forEach(name -> {
						try {//copy only if the classes are compatible
							if (PropertyUtils.getPropertyType(target, name).isAssignableFrom(PropertyUtils.getPropertyType(source, name))) {
								final Object value = PropertyUtils.getSimpleProperty(source, name);
								BeanUtils.copyProperty(target, name, value);
							}
						} catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
							// Should not happen
							LOG.error(e.getMessage(), e);
						}
		});
		return target;
	}

//	@SuppressWarnings("unchecked")
//	public static <S extends PersistentEntitas> S mergeDTOtoEntity(
//			EntitasDTO source, EntitasByDTOProviderFactory entitasByDTOProviderFactory) {
//		S target = (S) entitasByDTOProviderFactory.getEntitasByDTOProvider(source.getEntitasClass()).provide(source);
//		for (PropertyDescriptor pd : PropertyUtils.getPropertyDescriptors(source.getEntitasClass())) {
//			final String name = pd.getName();
//			if ("class".equals(name)) {
//				continue; // No point in trying to set an object's class
//			}
//			if (PropertyUtils.isReadable(source, name) && PropertyUtils.isWriteable(target, name)) {
//				try {
//					final Object value = PropertyUtils.getProperty(source, name);
//					if (value instanceof EntitasDTO) {
//						final Class targetType = PropertyUtils.getPropertyType(target, name);
//						if (!((EntitasDTO) value).getEntitasClass().isAssignableFrom(targetType)) {
//							throw new ConsistencyException("Nem egyező típusú DTO-entitás mapping");
//						}
//						PropertyUtils.setProperty(target, name,
//									mergeDTOtoEntity((EntitasDTO) value, entitasByDTOProviderFactory));
//					} else if (value instanceof CollectionDTO) {
//						for(EntitasDTO dto : ((CollectionDTO<? extends EntitasDTO>)PropertyUtils.getProperty(source, name))) {
//							PersistentEntitas entitas = mergeDTOtoEntity(dto, entitasByDTOProviderFactory);
//							((Collection) PropertyUtils.getProperty(target, name)).add(entitas);
//						}
//					} else {
//						PropertyUtils.setProperty(target, name, value);
//					}
//				} catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
//					// Should not happen
//					LOG.error(e.getMessage(), e);
//				}
//			}
//		}
//		return target;
//	}
//
//	@SuppressWarnings("unchecked")
	//FIXME review
//	public static EntitasDTO mergeEntityToDTO(PersistentEntitas source, EntitasDTO target) {
//		if (source.getEntitasClass() != target.getEntitasClass()) {
//			throw new ConsistencyException("Nem egyező típusú entitás-DTO mapping");
//		}
//		for (PropertyDescriptor pd : PropertyUtils.getPropertyDescriptors(source.getEntitasClass())) {
//			final String name = pd.getName();
//			if ("class".equals(name)) {
//				continue; // No point in trying to set an object's class
//			}
//			if (PropertyUtils.isReadable(source, name) && PropertyUtils.isWriteable(target, name)) {
//				try {
//					final Object value = PropertyUtils.getProperty(source, name);
//					if (value instanceof PersistentEntitas) {
//						final Class targetType = PropertyUtils.getPropertyType(target, name);
//						if (!((PersistentEntitas) value).getEntitasClass().isAssignableFrom(targetType)) {
//							throw new ConsistencyException("Nem egyező típusú DTO-entitás mapping");
//						}
//						PropertyUtils.setProperty(target, name, mergeEntityToDTO(
//								(PersistentEntitas) value, (EntitasDTO) targetType.newInstance()));
//					} else if (value instanceof Collection) {
//						for(PersistentEntitas entitas : ((Collection<? extends PersistentEntitas>)PropertyUtils.getProperty(source, name))) {
//							EntitasDTO dto = mergeEntityToDTO(entitas, );
//							((CollectionDTO) PropertyUtils.getProperty(target, name)).add(dto);
//						}
//					} else {
//						PropertyUtils.setProperty(target, name, value);
//					}
//				} catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
//					// Should not happen
//					LOG.error(e.getMessage(), e);
//				}
//			}
//		}
//		return target;
//	}
//
//	@SuppressWarnings("unchecked")
	//FIXME review
//	public static <T extends Editable, S extends EntitasDTO<T>> S mergeDTO(S source, S target) {
//		for (PropertyDescriptor pd : PropertyUtils.getPropertyDescriptors(source.getEntitasClass())) {
//			final String name = pd.getName();
//			if ("class".equals(name)) {
//				continue; // No point in trying to set an object's class
//			}
//			if (PropertyUtils.isReadable(source, name) && PropertyUtils.isWriteable(target, name)) {
//				try {
//					final Object value = PropertyUtils.getProperty(source, name);
//					final Class<?> targetPropertyType = PropertyUtils.getPropertyType(target, name);
//					if (value instanceof EditableDTO
//							&& EditableDTO.class.isAssignableFrom(targetPropertyType)) {
//						final EditableDTO dto = (EditableDTO) value;
//						if (Objects.equals(source.getEntitasClass(), target.getEntitasClass())) {
//							EditableDTO targetProperty = (EditableDTO) PropertyUtils.getProperty(target, name);
//							if (targetProperty == null) {
//								targetProperty = (EditableDTO) targetPropertyType.newInstance();
//							}
							//TODO???
//							dto.getEditor().edit(targetProperty);
//						}
					//FIXME containsById!!!, getById!!! a CollectionDTO-ba, stb.
//					} else if (value instanceof CollectionDTO) {
//						CollectionDTO tagretCollection = ((CollectionDTO) PropertyUtils.getProperty(target, name));
//						for(EntitasDTO dto : ((CollectionDTO<? extends EntitasDTO>)PropertyUtils.getProperty(source, name))) {
//							//igazából itt probléma, hogy nem tudjuk a pontos osztályokat,
//							// és nem lenne szép, ha kölcsönösen hivatkoznának az osztályok egymásra,
//							// vagy minden DTO-nak lenne külön CollectionDTO osztálya
//							// így a mapping-et nem lehet egyetlen univerzális osztályban végezni
//							//EntitasDTO entitas = mergeDTO(dto, dto);
//							//tagretCollection.add(entitas);
//						}
//					} else {
//						PropertyUtils.setProperty(target, name, value);
//					}
//				} catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException
//						| InstantiationException e) {
//					// Should not happen
//					LOG.error(e.getMessage(), e);
//				}
//			}
//		}
//		return target;
//	}
}
