package vtijuq_szakdoli.common.constants;

//hogy a kódban ne String-literálként legyenek a funkciónevek
//a UI-részen általában: a CreatorView-k a karbantartó vagy szerkesztő funkciókra hivatkoznak
// a karbantartó konvenció szerint megegyező nevő a funkcióval
public class FunctionNames {
    public static final String KOVETELMENY_SZERKESZTO 	= "KovetelmenySzerkeszto";
	public static final String ISMERET_KARBANTARTO 		= "IsmeretKarbantarto";
	public static final String ISMERET_SZERKESZTO 		= "IsmeretSzerkeszto";
	public static final String TANTARGY_KARBANTARTO 	= "TantargyKarbantarto";
	public static final String TANTARGY_SZERKESZTO 		= "TantargySzerkeszto";
	public static final String KONKRET_SZERKESZTO 		= "KonkretSzerkeszto";
	public static final String AKKREDITACIO_KARBANTARTO = "AkkreditacioKarbantarto";
	public static final String AKKREDITACIO_SZERKESZTO 	= "AkkreditacioSzerkeszto";
	public static final String SZERVEZET_KARBANTARTO 	= "SzervezetKarbantarto";
	public static final String SZERVEZET_SZERKESZTO 	= "SzervezetSzerkeszto";
	public static final String MUNKATARS_KARBANTARTO 	= "MunkatarsKarbantarto";
	public static final String MUNKATARS_SZERKESZTO 	= "MunkatarsSzerkeszto";
	public static final String TEST 					= "SzotarTest";
}
