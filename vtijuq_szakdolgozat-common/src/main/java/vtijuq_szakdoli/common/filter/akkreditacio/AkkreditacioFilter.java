package vtijuq_szakdoli.common.filter.akkreditacio;

import java.util.Date;
import java.util.Objects;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.akkreditacio.Akkreditacio;

public interface AkkreditacioFilter extends Filter<Akkreditacio> {

	Date getErvenyessegKezdet();
	void setErvenyessegKezdet(Date ervkezdet);

	Date getErvenyessegVeg();
	void setErvenyessegVeg(Date ervveg);

	Boolean getValid();
	void setValid(Boolean valid);

	Boolean getVegleges();
	void setVegleges(Boolean vegleges);

	Long getIdKonkretSzakirany();
	void setIdKonkretSzakirany(Long idKonkretSzakirany);

	@Override
	default boolean hasConditions() {
		return hasConditionErvenyessegKezdet()
			|| hasConditionErvenyessegVeg()
			|| hasConditionValid()
			|| hasConditionVegleges()
			|| hasConditionIdKonkretSzakirany();
	}

	@Override
	default boolean match(Akkreditacio entitas) {
		return matchErvenyessegKezdet(entitas)
			&& matchErvenyessegVeg(entitas)
			&& matchValid(entitas)
			&& matchVegleges(entitas)
			&& matchIdKonkretSzakirany(entitas);
	}

	default boolean hasConditionErvenyessegKezdet() {
		return null != getErvenyessegKezdet();
	}
	default boolean matchErvenyessegKezdet(Akkreditacio entitas) {
		return !hasConditionErvenyessegKezdet() || !getErvenyessegKezdet().after(entitas.getErvenyessegKezdet());
	}

	default boolean hasConditionErvenyessegVeg() {
		return null != getErvenyessegVeg();
	}
	default boolean matchErvenyessegVeg(Akkreditacio entitas) {
		return !hasConditionErvenyessegVeg() || !getErvenyessegVeg().before(entitas.getErvenyessegVeg());
	}

	default boolean hasConditionValid() {
		return null != getValid();
	}
	default boolean matchValid(Akkreditacio entitas) {
		return !hasConditionValid() || getValid() == entitas.isValid();
	}

	default boolean hasConditionVegleges() {
		return null != getVegleges();
	}
	default boolean matchVegleges(Akkreditacio entitas) {
		return !hasConditionVegleges() || getVegleges() == entitas.isVegleges();
	}

	default boolean hasConditionIdKonkretSzakirany() {
		return null != getIdKonkretSzakirany();
	}
	default boolean matchIdKonkretSzakirany(Akkreditacio entitas) {
		return !(hasConditionIdKonkretSzakirany() && entitas.getKonkretSzakirany() != null)
				|| Objects.equals(entitas.getKonkretSzakirany().getId(), getIdKonkretSzakirany());
	}

	@Override
	default void clear() {
		setErvenyessegKezdet(null);
		setErvenyessegVeg(null);
		setValid(null);
		setVegleges(null);
		setIdKonkretSzakirany(null);
	}
}
