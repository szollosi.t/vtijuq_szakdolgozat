package vtijuq_szakdoli.common.filter.adminisztracio;

import java.util.Objects;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Munkatars;

public interface MunkatarsFilter extends Filter<Munkatars> {

	String getFelhasznalonev();
	void setFelhasznalonev(String felhasznalonev);

	String getNev();
	void setNev(String nev);

	Boolean getAktiv();
	void setAktiv(Boolean aktiv);

	Long getIdSzervezet();
	void setIdSzervezet(Long idSzervezet);

	Long getIdSzerepkor();
	void setIdSzerepkor(Long idSzerepkor);

	@Override
	default boolean hasConditions() {
		return hasConditionFelhasznalonev()
			|| hasConditionNev()
			|| hasConditionAktiv()
			|| hasConditionIdSzervezet()
			|| hasConditionIdSzerepkor();
	}

	@Override
	default boolean match(Munkatars entitas) {
		return matchFelhasznalonev(entitas)
			&& matchNev(entitas)
			&& matchAktiv(entitas)
			&& matchIdSzervezet(entitas)
			&& matchIdSzerepkor(entitas);
	}

	default boolean hasConditionFelhasznalonev() {
		return StringUtils.isNotBlank(getFelhasznalonev());
	}
	default boolean matchFelhasznalonev(Munkatars entitas) {
		return !hasConditionFelhasznalonev() || StringUtils.containsIgnoreCase(entitas.getFelhasznalonev(), getFelhasznalonev());
	}

	default boolean hasConditionNev() {
		return StringUtils.isNotBlank(getNev());
	}
	default boolean matchNev(Munkatars entitas) {
		return !hasConditionNev() || StringUtils.containsIgnoreCase(entitas.getNev(), getNev());
	}

	default boolean hasConditionAktiv() {
		return null != getAktiv();
	}
	default boolean matchAktiv(Munkatars entitas) {
		return !hasConditionAktiv() || getAktiv() == BooleanUtils.isTrue(entitas.isAktiv());
	}

	default boolean hasConditionIdSzervezet() {
		return null != getIdSzervezet();
	}
	default boolean matchIdSzervezet(Munkatars entitas) {
		return !(hasConditionIdSzervezet() && entitas.getSzervezet() != null)
				|| Objects.equals(entitas.getSzervezet().getId(), getIdSzervezet());
	}

	default boolean hasConditionIdSzerepkor() {
		return null != getIdSzerepkor();
	}
	default boolean matchIdSzerepkor(Munkatars entitas) {
		return !(hasConditionIdSzerepkor() && entitas.getSzerepkor() != null)
				|| Objects.equals(entitas.getSzerepkor().getId(), getIdSzerepkor());
	}

	@Override
	default void clear() {
		setFelhasznalonev(null);
		setNev(null);
		setAktiv(null);
		setIdSzervezet(null);
		setIdSzerepkor(null);
	}
}
