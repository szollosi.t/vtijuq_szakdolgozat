package vtijuq_szakdoli.common.filter.adminisztracio;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet;

public interface SzervezetFilter extends Filter<Szervezet> {

	String getNev();
	void setNev(String nev);

	String getRovidites();
	void setRovidites(String rovidites);

	Long getIdSzervezetTipus();
	void setIdSzervezetTipus(Long idSzervezetTipus);

	@Override
	default boolean hasConditions() {
		return hasConditionNev()
			|| hasConditionRovidites()
			|| hasConditionIdSzervezetTipus();
	}

	@Override
	default boolean match(Szervezet entitas) {
		return matchNev(entitas)
			&& matchRovidites(entitas)
			&& matchIdSzervezetTipus(entitas);
	}

	default boolean hasConditionNev() {
		return StringUtils.isNotBlank(getNev());
	}
	default boolean matchNev(Szervezet entitas) {
		return !hasConditionNev() || StringUtils.containsIgnoreCase(entitas.getNev(), getNev());
	}

	default boolean hasConditionRovidites() {
		return StringUtils.isNotBlank(getRovidites());
	}
	default boolean matchRovidites(Szervezet entitas) {
		return !hasConditionRovidites() || StringUtils.containsIgnoreCase(entitas.getRovidites(), getRovidites());
	}

	default boolean hasConditionIdSzervezetTipus() {
		return null != getIdSzervezetTipus();
	}
	default boolean matchIdSzervezetTipus(Szervezet entitas) {
		return !(hasConditionIdSzervezetTipus() && entitas.getSzervezetTipus() != null)
				|| Objects.equals(entitas.getSzervezetTipus().getId(), getIdSzervezetTipus());
	}

	@Override
	default void clear() {
		setNev(null);
		setRovidites(null);
		setIdSzervezetTipus(null);
	}
}
