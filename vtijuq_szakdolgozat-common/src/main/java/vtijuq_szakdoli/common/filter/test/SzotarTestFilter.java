package vtijuq_szakdoli.common.filter.test;

import java.util.Objects;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.test.SzotarTest;

public interface SzotarTestFilter extends Filter<SzotarTest> {

	String getKod();
	void setKod(String kod);

	String getNev();
	void setNev(String nev);

	String getLeiras();
	void setLeiras(String leiras);

	Long getIdTipus();
	void setIdTipus(Long idTipus);

	Boolean getTechnikai();
	void setTechnikai(Boolean technikai);

	@Override
	default boolean hasConditions() {
		return hasConditionKod()
			|| hasConditionNev()
			|| hasConditionLeiras()
			|| hasConditionIdTipus()
			|| hasConditionTechnikai();
	}

	@Override
	default boolean match(SzotarTest entitas) {
		return matchKod(entitas)
			&& matchNev(entitas)
			&& matchLeiras(entitas)
			&& matchIdTipus(entitas)
			&& matchTechnikai(entitas);
	}

	default boolean hasConditionKod() {
		return StringUtils.isNotBlank(getKod());
	}
	default boolean matchKod(SzotarTest entitas) {
		return !hasConditionKod() || StringUtils.containsIgnoreCase(entitas.getKod(), getKod());
	}

	default boolean hasConditionNev() {
		return StringUtils.isNotBlank(getNev());
	}
	default boolean matchNev(SzotarTest entitas) {
		return !hasConditionNev() || StringUtils.containsIgnoreCase(entitas.getNev(), getNev());
	}

	default boolean hasConditionLeiras() {
		return StringUtils.isNotBlank(getLeiras());
	}
	default boolean matchLeiras(SzotarTest entitas) {
		return !hasConditionLeiras() || StringUtils.containsIgnoreCase(entitas.getLeiras(), getLeiras());
	}

	default boolean hasConditionIdTipus() {
		return null != getIdTipus();
	}
	default boolean matchIdTipus(SzotarTest entitas) {
		return !(hasConditionIdTipus() && entitas.getIdTipus() != null)
				|| Objects.equals(entitas.getIdTipus(), getIdTipus());
	}

	default boolean hasConditionTechnikai() {
		return null != getTechnikai();
	}
	default boolean matchTechnikai(SzotarTest entitas) {
		return !hasConditionTechnikai() || getTechnikai() == BooleanUtils.isTrue(entitas.getTechnikai());
	}

	@Override
	default void clear() {
		setKod(null);
		setNev(null);
		setLeiras(null);
		setIdTipus(null);
		setTechnikai(null);
	}
}
