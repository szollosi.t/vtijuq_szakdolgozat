package vtijuq_szakdoli.common.filter.megvalositas;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany;

public interface KonkretSzakiranyFilter extends Filter<KonkretSzakirany> {

	String getNev();
	void setNev(String nev);

	String getKod();
	void setKod(String kod);

	Long getIdKonkretSzak();
	void setIdKonkretSzak(Long idKonkretSzak);

	Long getIdSzakiranyKovetelmeny();
	void setIdSzakiranyKovetelmeny(Long idSzakiranyKovetelmeny);

	@Override
	default boolean hasConditions() {
		return hasConditionNev()
			|| hasConditionKod()
			|| hasConditionIdKonkretSzak()
			|| hasConditionIdSzakiranyKovetelmeny();
	}

	@Override
	default boolean match(KonkretSzakirany entitas) {
		return matchNev(entitas)
			&& matchKod(entitas)
			&& matchIdKonkretSzak(entitas)
			&& matchIdSzakiranyKovetelmeny(entitas);
	}

	default boolean hasConditionNev() {
		return StringUtils.isNotBlank(getNev());
	}
	default boolean matchNev(KonkretSzakirany entitas) {
		return !hasConditionNev() || StringUtils.containsIgnoreCase(entitas.getNev(), getNev());
	}

	default boolean hasConditionKod() {
		return StringUtils.isNotBlank(getKod());
	}
	default boolean matchKod(KonkretSzakirany entitas) {
		return !hasConditionKod() || StringUtils.containsIgnoreCase(entitas.getKod(), getKod());
	}

	default boolean hasConditionIdKonkretSzak() {
		return null != getIdKonkretSzak();
	}
	default boolean matchIdKonkretSzak(KonkretSzakirany entitas) {
		return !(hasConditionIdKonkretSzak() && entitas.getKonkretSzak() != null)
				|| Objects.equals(entitas.getKonkretSzak().getId(), getIdKonkretSzak());
	}

	default boolean hasConditionIdSzakiranyKovetelmeny() {
		return null != getIdSzakiranyKovetelmeny();
	}
	default boolean matchIdSzakiranyKovetelmeny(KonkretSzakirany entitas) {
		return !(hasConditionIdSzakiranyKovetelmeny() && entitas.getSzakiranyKovetelmeny() != null)
				|| Objects.equals(entitas.getSzakiranyKovetelmeny().getId(), getIdSzakiranyKovetelmeny());
	}

	@Override
	default void clear() {
		setNev(null);
		setKod(null);
		setIdKonkretSzak(null);
		setIdSzakiranyKovetelmeny(null);
	}
}
