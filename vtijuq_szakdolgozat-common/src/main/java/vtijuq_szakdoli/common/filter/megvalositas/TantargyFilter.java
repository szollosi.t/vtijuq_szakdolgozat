package vtijuq_szakdoli.common.filter.megvalositas;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tantargy;

public interface TantargyFilter extends Filter<Tantargy> {

	String getNev();
	void setNev(String nev);

	String getKod();
	void setKod(String kod);

	Long getIdSzervezet();
	void setIdSzervezet(Long idSzervezet);

	@Override
	default boolean hasConditions() {
		return hasConditionNev()
			|| hasConditionKod()
			|| hasConditionIdSzervezet();
	}

	@Override
	default boolean match(Tantargy entitas) {
		return matchNev(entitas)
			&& matchKod(entitas)
			&& matchIdSzervezet(entitas);
	}

	default boolean hasConditionNev() {
		return StringUtils.isNotBlank(getNev());
	}
	default boolean matchNev(Tantargy entitas) {
		return !hasConditionNev() || StringUtils.containsIgnoreCase(entitas.getNev(), getNev());
	}

	default boolean hasConditionKod() {
		return StringUtils.isNotBlank(getKod());
	}
	default boolean matchKod(Tantargy entitas) {
		return !hasConditionKod() || StringUtils.containsIgnoreCase(entitas.getKod(), getKod());
	}

	default boolean hasConditionIdSzervezet() {
		return null != getIdSzervezet();
	}
	default boolean matchIdSzervezet(Tantargy entitas) {
		return !(hasConditionIdSzervezet() && entitas.getSzervezet() != null)
				|| Objects.equals(entitas.getSzervezet().getId(), getIdSzervezet());
	}

	@Override
	default void clear() {
		setNev(null);
		setKod(null);
		setIdSzervezet(null);
	}
}
