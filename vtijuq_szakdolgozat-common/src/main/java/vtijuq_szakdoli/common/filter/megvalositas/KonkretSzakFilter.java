package vtijuq_szakdoli.common.filter.megvalositas;

import java.util.Objects;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzak;

public interface KonkretSzakFilter extends Filter<KonkretSzak> {

	Long getIdSzakKovetelmeny();
	void setIdSzakKovetelmeny(Long idSzakKovetelmeny);

	Long getIdSzervezet();
	void setIdSzervezet(Long idSzervezet);

	@Override
	default boolean hasConditions() {
		return hasConditionIdSzakKovetelmeny()
			|| hasConditionIdSzervezet();
	}

	@Override
	default boolean match(KonkretSzak entitas) {
		return matchIdSzakKovetelmeny(entitas)
			&& matchIdSzervezet(entitas);
	}

	default boolean hasConditionIdSzakKovetelmeny() {
		return null != getIdSzakKovetelmeny();
	}
	default boolean matchIdSzakKovetelmeny(KonkretSzak entitas) {
		return !(hasConditionIdSzakKovetelmeny() && entitas.getSzakKovetelmeny() != null)
				|| Objects.equals(entitas.getSzakKovetelmeny().getId(), getIdSzakKovetelmeny());
	}

	default boolean hasConditionIdSzervezet() {
		return null != getIdSzervezet();
	}
	default boolean matchIdSzervezet(KonkretSzak entitas) {
		return !(hasConditionIdSzervezet() && entitas.getSzervezet() != null)
				|| Objects.equals(entitas.getSzervezet().getId(), getIdSzervezet());
	}

	@Override
	default void clear() {
		setIdSzakKovetelmeny(null);
		setIdSzervezet(null);
	}
}
