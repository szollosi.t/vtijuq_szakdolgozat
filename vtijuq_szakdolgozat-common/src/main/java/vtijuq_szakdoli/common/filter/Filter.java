package vtijuq_szakdoli.common.filter;

import vtijuq_szakdoli.common.interfaces.Entitas;

public interface Filter<T extends Entitas> {

	boolean hasConditions();

	boolean match(T entitas);

	void clear();
}
