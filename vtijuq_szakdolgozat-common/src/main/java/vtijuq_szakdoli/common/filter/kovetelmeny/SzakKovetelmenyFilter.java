package vtijuq_szakdoli.common.filter.kovetelmeny;

import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakKovetelmeny;

public interface SzakKovetelmenyFilter extends Filter<SzakKovetelmeny> {

	Long getIdSzak();
	void setIdSzak(Long idSzak);

	Date getErvenyessegKezdet();
	void setErvenyessegKezdet(Date ervenyessegKezdet);

	Date getErvenyessegVeg();
	void setErvenyessegVeg(Date ervenyessegVeg);

	Boolean getValid();
	void setValid(Boolean valid);

	Boolean getVegleges();
	void setVegleges(Boolean vegleges);

	String getOklevelMegjeloles();
	void setOklevelMegjeloles(String oklevelMegjeloles);

	@Override
	default boolean hasConditions() {
		return hasConditionIdSzak()
			|| hasConditionErvenyessegKezdet()
			|| hasConditionErvenyessegVeg()
			|| hasConditionValid()
			|| hasConditionVegleges()
			|| hasConditionOklevelMegjeloles();
	}

	@Override
	default boolean match(SzakKovetelmeny entitas) {
		return matchIdSzak(entitas)
			&& matchErvenyessegKezdet(entitas)
			&& matchErvenyessegVeg(entitas)
			&& matchValid(entitas)
			&& matchVegleges(entitas)
			&& matchOklevelMegjeloles(entitas);
	}

	default boolean hasConditionIdSzak() {
		return null != getIdSzak();
	}
	default boolean matchIdSzak(SzakKovetelmeny entitas) {
		return !(hasConditionIdSzak() && entitas.getSzak() != null)
				|| Objects.equals(entitas.getSzak().getId(), getIdSzak());
	}

	default boolean hasConditionErvenyessegKezdet() {
		return null != getErvenyessegKezdet();
	}
	default boolean matchErvenyessegKezdet(SzakKovetelmeny entitas) {
		return !hasConditionErvenyessegKezdet() || !getErvenyessegKezdet().after(entitas.getErvenyessegKezdet());
	}

	default boolean hasConditionErvenyessegVeg() {
		return null != getErvenyessegVeg();
	}
	default boolean matchErvenyessegVeg(SzakKovetelmeny entitas) {
		return !hasConditionErvenyessegVeg() || !getErvenyessegVeg().before(entitas.getErvenyessegVeg());
	}

	default boolean hasConditionValid() {
		return null != getValid();
	}
	default boolean matchValid(SzakKovetelmeny entitas) {
		return !hasConditionValid() || getValid() == entitas.isValid();
	}

	default boolean hasConditionVegleges() {
		return null != getVegleges();
	}
	default boolean matchVegleges(SzakKovetelmeny entitas) {
		return !hasConditionVegleges() || getVegleges() == entitas.isVegleges();
	}

	default boolean hasConditionOklevelMegjeloles() {
		return StringUtils.isNotBlank(getOklevelMegjeloles());
	}
	default boolean matchOklevelMegjeloles(SzakKovetelmeny entitas) {
		return !hasConditionOklevelMegjeloles() || StringUtils.containsIgnoreCase(entitas.getOklevelMegjeloles(), getOklevelMegjeloles());
	}

	@Override
	default void clear() {
		setIdSzak(null);
		setErvenyessegKezdet(null);
		setErvenyessegVeg(null);
		setValid(null);
		setVegleges(null);
		setOklevelMegjeloles(null);
	}
}
