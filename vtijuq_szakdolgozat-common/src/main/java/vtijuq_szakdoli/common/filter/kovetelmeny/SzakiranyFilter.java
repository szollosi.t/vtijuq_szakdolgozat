package vtijuq_szakdoli.common.filter.kovetelmeny;

import java.util.Objects;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szakirany;

public interface SzakiranyFilter extends Filter<Szakirany> {

	Long getIdSzak();
	void setIdSzak(Long idSzak);

	@Override
	default boolean hasConditions() {
		return hasConditionIdSzak();
	}

	@Override
	default boolean match(Szakirany entitas) {
		return matchIdSzak(entitas);
	}

	default boolean hasConditionIdSzak() {
		return null != getIdSzak();
	}
	default boolean matchIdSzak(Szakirany entitas) {
		return !(hasConditionIdSzak() && entitas.getSzak() != null)
				|| Objects.equals(entitas.getSzak().getId(), getIdSzak());
	}

	@Override
	default void clear() {
		setIdSzak(null);
	}
}
