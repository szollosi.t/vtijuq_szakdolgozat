package vtijuq_szakdoli.common.filter.kovetelmeny;

import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ismeret;

public interface IsmeretFilter extends Filter<Ismeret> {

	String getNev();
	void setNev(String nev);

	String getLeiras();
	void setLeiras(String leiras);

	String getKod();
	void setKod(String kod);

	@Override
	default boolean hasConditions() {
		return hasConditionNev()
			|| hasConditionLeiras()
			|| hasConditionKod();
	}

	@Override
	default boolean match(Ismeret entitas) {
		return matchNev(entitas)
			&& matchLeiras(entitas)
			&& matchKod(entitas);
	}

	default boolean hasConditionNev() {
		return StringUtils.isNotBlank(getNev());
	}
	default boolean matchNev(Ismeret entitas) {
		return !hasConditionNev() || StringUtils.containsIgnoreCase(entitas.getNev(), getNev());
	}

	default boolean hasConditionLeiras() {
		return StringUtils.isNotBlank(getLeiras());
	}
	default boolean matchLeiras(Ismeret entitas) {
		return !hasConditionLeiras() || StringUtils.containsIgnoreCase(entitas.getLeiras(), getLeiras());
	}

	default boolean hasConditionKod() {
		return StringUtils.isNotBlank(getKod());
	}
	default boolean matchKod(Ismeret entitas) {
		return !hasConditionKod() || StringUtils.containsIgnoreCase(entitas.getKod(), getKod());
	}

	@Override
	default void clear() {
		setNev(null);
		setLeiras(null);
		setKod(null);
	}
}
