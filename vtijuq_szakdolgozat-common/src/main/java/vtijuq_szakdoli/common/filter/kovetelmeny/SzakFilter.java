package vtijuq_szakdoli.common.filter.kovetelmeny;

import java.util.Objects;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ciklus;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szak;

public interface SzakFilter extends Filter<Szak> {

	Long getIdKepzesiTerulet();
	void setIdKepzesiTerulet(Long idKepzesiTerulet);

	Ciklus getCiklus();
	void setCiklus(Ciklus ciklus);

	@Override
	default boolean hasConditions() {
		return hasConditionCiklus()
			|| hasConditionKepzesiTerulet();
	}

	@Override
	default boolean match(Szak entitas) {
		return matchCiklus(entitas)
			&& matchKepzesiTerulet(entitas);
	}

	default boolean hasConditionCiklus() {
		return null != getCiklus();
	}
	default boolean matchCiklus(Szak entitas) {
		return !hasConditionCiklus()
				|| Objects.equals(entitas.getCiklus(), getCiklus());
	}

	default boolean hasConditionKepzesiTerulet() {
		return null != getIdKepzesiTerulet();
	}
	default boolean matchKepzesiTerulet(Szak entitas) {
		return !hasConditionKepzesiTerulet()
				|| Objects.equals(entitas.getKepzesiTerulet().getId(), getIdKepzesiTerulet());
	}

	@Override
	default void clear() {
		setCiklus(null);
		setIdKepzesiTerulet(null);
	}
}
