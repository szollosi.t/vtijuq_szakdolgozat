package vtijuq_szakdoli.common.filter.kovetelmeny;

import java.util.Objects;

import org.apache.commons.lang3.BooleanUtils;

import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakiranyKovetelmeny;

public interface SzakiranyKovetelmenyFilter extends Filter<SzakiranyKovetelmeny> {

	Boolean getCsakkozos();
	void setCsakkozos(Boolean csakkozos);

	Long getIdSzakKovetelmeny();
	void setIdSzakKovetelmeny(Long idSzakKovetelmeny);

	Long getIdSzakirany();
	void setIdSzakirany(Long idSzakirany);

	@Override
	default boolean hasConditions() {
		return hasConditionCsakkozos()
			|| hasConditionIdSzakKovetelmeny()
			|| hasConditionIdSzakirany();
	}

	@Override
	default boolean match(SzakiranyKovetelmeny entitas) {
		return matchCsakkozos(entitas)
			&& matchIdSzakKovetelmeny(entitas)
			&& matchIdSzakirany(entitas);
	}

	default boolean hasConditionCsakkozos() {
		return null != getCsakkozos();
	}
	default boolean matchCsakkozos(SzakiranyKovetelmeny entitas) {
		return !hasConditionCsakkozos() || getCsakkozos() == BooleanUtils.isTrue(entitas.getCsakkozos());
	}

	default boolean hasConditionIdSzakKovetelmeny() {
		return null != getIdSzakKovetelmeny();
	}
	default boolean matchIdSzakKovetelmeny(SzakiranyKovetelmeny entitas) {
		return !(hasConditionIdSzakKovetelmeny() && entitas.getSzakKovetelmeny() != null)
				|| Objects.equals(entitas.getSzakKovetelmeny().getId(), getIdSzakKovetelmeny());
	}

	default boolean hasConditionIdSzakirany() {
		return null != getIdSzakirany();
	}
	default boolean matchIdSzakirany(SzakiranyKovetelmeny entitas) {
		return !(hasConditionIdSzakirany() && entitas.getSzakirany() != null)
				|| Objects.equals(entitas.getSzakirany().getId(), getIdSzakirany());
	}

	@Override
	default void clear() {
		setCsakkozos(null);
		setIdSzakKovetelmeny(null);
		setIdSzakirany(null);
	}
}
