package vtijuq_szakdoli.common.util.graph;

import java.util.Collections;
import java.util.Set;
import java.util.function.Function;

import lombok.Getter;

import vtijuq_szakdoli.common.exception.ConsistencyException;

public class Tree<T extends Hierarchical<? super T>> extends Hierarchy<T> {

	@Getter
	private T root;

	public Tree(final T root) {
		super();
		this.root = root;
		addNode(root);
	}

	public Tree(final T root, final SubordinatesProvider<T> subordinatesProvider) {
		this(root);
		addSubHierarchy(root, subordinatesProvider);
	}

	@Override
	public Set<T> getSovereigns() {
		return Collections.singleton(root);
	}

	@Override
	public void addNode(T newNode) throws ConsistencyException {
		if (graph.vertexSet().isEmpty()) {
			super.addNode(newNode);
		} else if (!graph.containsVertex(newNode)) {
			super.addNode(root, newNode);
		}
	}

	@Override
	public void removeSubHierarchy(T principal) {
		if (root.equals(principal)) {
			throw new ConsistencyException("consistency.error.remove.root");
		}
		super.removeSubHierarchy(principal);
	}

	@Override
	public void removeNode(T node) {
		if (root.equals(node)) {
			throw new ConsistencyException("consistency.error.remove.root");
		}
		super.removeNode(node);
	}

	@Override
	public <S extends Hierarchical<? super S>> Tree<S> map(Function<T, S> mapping) {
		final S mappedRoot = mapping.apply(root);
		final Tree<S> mappedTree = new Tree<>(mappedRoot);
		mapSubordinates(mappedTree, root, mappedRoot, mapping);
		return mappedTree;
	}

}
