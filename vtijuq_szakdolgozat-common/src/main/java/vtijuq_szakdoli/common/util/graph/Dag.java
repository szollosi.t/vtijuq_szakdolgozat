package vtijuq_szakdoli.common.util.graph;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import vtijuq_szakdoli.common.exception.ConsistencyException;

public class Dag<T extends Hierarchical<? super T>> extends Hierarchy<T> {

	private Set<T> sovereigns;

	public Dag(Set<T> sovereigns) {
		super();
		this.sovereigns = new HashSet<>(sovereigns);
		sovereigns.forEach(this::addNode);
	}

	public Dag(Hierarchy<T> hierarchy){
		this(hierarchy.getSovereigns());
		hierarchy.getSovereigns().forEach( r -> addSubHierarchy(r, hierarchy::getDirectSubordinates));
	}

	public Dag(Collection<T> sovereigns, SubordinatesProvider<T> subordinatesProvider){
		super();
		this.sovereigns = new HashSet<>();
		addIsolatedSubgraphs(sovereigns, subordinatesProvider);
	}

	public void addIsolatedSubgraph(Hierarchy<T> hierarchy) {
		hierarchy.getSovereigns().forEach(this::addNode);
		hierarchy.getSovereigns().forEach( r -> addSubHierarchy(r, hierarchy::getDirectSubordinates));
	}

	public void addIsolatedSubgraphs(Collection<T> newSovereigns, SubordinatesProvider<T> subordinatesProvider) {
		newSovereigns.forEach(sovereign -> {
			if (!hasSovereign(sovereign)) {
				addNode(sovereign);
				addSubHierarchy(sovereign, subordinatesProvider);
			}
		});
	}

	private boolean hasSovereign(T sovereign) {
		return sovereigns.contains(sovereign);
	}

	@Override
	public Set<T> getSovereigns() {
		return Collections.unmodifiableSet(sovereigns);
	}

	@Override
	public void addNode(T newNode) throws ConsistencyException {
		sovereigns.add(newNode);
		super.addNode(newNode);
	}

	@Override
	public void addSubordination(T principal, T subordinate) throws ConsistencyException {
		sovereigns.remove(subordinate);
		super.addSubordination(principal, subordinate);
	}

	@Override
	public void removeSubHierarchy(T principal) {
		super.removeSubHierarchy(principal);
		removeIfSovereign(principal);
	}

	@Override
	public void removeNode(T node) {
		super.removeNode(node);
		removeIfSovereign(node);
	}

	private void removeIfSovereign(T principal) {
		if (sovereigns.contains(principal)) {
			sovereigns.remove(principal);
		}
	}

	@Override
	public <S extends Hierarchical<? super S>> Dag<S> map(Function<T, S> mapping) {
		final Map<T, S> sovereignsMapping = sovereigns.stream().collect(Collectors.toMap(Function.identity(), mapping));
		final Dag<S> mappedDag = new Dag<>(new HashSet<>(sovereignsMapping.values()));
		sovereignsMapping.forEach((sovereign, mappedSovereign) ->
				mapSubordinates(mappedDag, sovereign, mappedSovereign, mapping));
		return mappedDag;
	}
}
