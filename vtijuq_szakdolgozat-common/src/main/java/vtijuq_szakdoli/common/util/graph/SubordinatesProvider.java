package vtijuq_szakdoli.common.util.graph;

import java.util.Set;

@FunctionalInterface
public interface SubordinatesProvider<T extends Hierarchical<? super T>> {
	Set<? extends T> findSubordinates(final T principal);
}
