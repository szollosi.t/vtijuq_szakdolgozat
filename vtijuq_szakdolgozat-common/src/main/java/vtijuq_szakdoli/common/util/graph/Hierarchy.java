package vtijuq_szakdoli.common.util.graph;

import static java.util.Collections.singletonList;

import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.jgrapht.alg.util.Pair;
import org.jgrapht.experimental.dag.DirectedAcyclicGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vtijuq_szakdoli.common.exception.ConsistencyException;

//hierarchy represented by a directed acyclic graph, edges go upwards
public abstract class Hierarchy<T extends Hierarchical<? super T>> {
//TODO interfészt csinálni belőle, amiből a HierarchicalDao is származik

	private static final Logger LOG = LoggerFactory.getLogger(Hierarchy.class);

	protected DirectedAcyclicGraph<T, Pair<T,T>> graph;

	protected Hierarchy() {
		graph = new DirectedAcyclicGraph<>(Pair::new);
	}

	public abstract Set<T> getSovereigns();

	public Set<T> getSubordinates(T node){
		if (!graph.containsVertex(node)) return null;
		return graph.getDescendants(graph, node);
	}

	public Set<T> getDirectSubordinates(T node){
		if (!graph.containsVertex(node)) return null;
		return graph.incomingEdgesOf(node).stream()
				.map(graph::getEdgeSource)
				.collect(Collectors.toSet());
	}

	public Set<T> getPrincipals(T node){
		if (!graph.containsVertex(node)) return null;
		return graph.getAncestors(graph, node);
	}

	public Set<T> getDirectPrincipals(T node){
		if (!graph.containsVertex(node)) return null;
		return graph.outgoingEdgesOf(node).stream()
				.map(graph::getEdgeTarget)
				.collect(Collectors.toSet());
	}

	public boolean hasNode(T filterByExample) {
		return graph.vertexSet().stream().anyMatch(filterByExample::equals);
	}

	/**
	 * Megkeresi a paraméterrel egyenlőnek tekintett node-t a hierarchiában.
	 * @param filterByExample példa a kereséshez.
	 * @return egyenlőnek tekintett node, vagy null, ha nincs ilyen.
	 */
	public T getEqualNode(T filterByExample) {
		return graph.vertexSet().stream().filter(filterByExample::equals).findFirst().orElse(null);
	}

	protected void addSubHierarchy(T root, SubordinatesProvider<T> subordinatesProvider) {
		addNode(root);
		subordinatesProvider.findSubordinates(root).forEach(node ->
				addSubHierarchy(root, node, subordinatesProvider));
	}

	private void addSubHierarchy(T attachPointNode, T newNode, SubordinatesProvider<T> subordinatesProvider){
		addNode(attachPointNode, newNode);
		subordinatesProvider.findSubordinates(newNode).forEach(node ->
				addSubHierarchy(newNode, node, subordinatesProvider)
		);
	}

	public void addNode(T attachPointNode, T newNode) throws ConsistencyException {
		graph.addVertex(newNode);
		this.addSubordination(attachPointNode, newNode);
	}

	public void addNode(T newNode) throws ConsistencyException {
		graph.addVertex(newNode);
	}

	public void addSubordination(T principal, T subordinate) throws ConsistencyException {
		try {
			graph.addDagEdge(subordinate, principal);
		} catch (IllegalArgumentException e) {
			LOG.info(e.getMessage(), e);
			throw new ConsistencyException(singletonList(String.format("%s (%s, %s)", e.getMessage(), principal, subordinate)));
		} catch (DirectedAcyclicGraph.CycleFoundException e) {
			LOG.info(e.getMessage(), e);
			throw new ConsistencyException(singletonList(String.format("Kör jönne létre a beszúrással (%s, %s)", principal, subordinate)));
		}
	}

	public boolean isSubordination(T principal, T subordinate) {
		return graph.containsEdge(subordinate, principal);
	}

	/**
	 * Rekurzív törlés az alárendeltekkel együtt.
	 * @param principal törlendő subHierarchia csúcsa
	 */
	public void removeSubHierarchy(T principal){
		getDirectSubordinates(principal).forEach(this::removeSubHierarchy);
		graph.removeVertex(principal);
	}

	/**
	 * Node törlése az alá és fölá rendeltek összekötésével
	 * @param node törlendő node
	 */
	public void removeNode(T node){
		final Set<T> directSubordintes = getDirectSubordinates(node);
		final Set<T> directPrincipals = getDirectPrincipals(node);
		directSubordintes.forEach(s -> directPrincipals.forEach(p -> {
			if (!isSubordination(p, s)) {
				addSubordination(p, s);
			}
		}));
		graph.removeVertex(node);
	}

	public abstract <S extends Hierarchical<? super S>> Hierarchy<S> map(Function<T,S> mapping);

	protected <S extends Hierarchical<? super S>> void mapSubordinates(Hierarchy<S> mappedHierarchy, T sourceNode, S targetNode, Function<T, S> mapping) {
		getDirectSubordinates(sourceNode).forEach(subordinate -> {
			final S mappedSubordinate = mapping.apply(subordinate);
			mappedHierarchy.addNode(targetNode, mappedSubordinate);
			mapSubordinates(mappedHierarchy, subordinate, mappedSubordinate, mapping);
		});
	}
}
