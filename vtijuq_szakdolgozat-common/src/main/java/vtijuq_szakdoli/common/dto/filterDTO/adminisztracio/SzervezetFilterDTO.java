package vtijuq_szakdoli.common.dto.filterDTO.adminisztracio;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.adminisztracio.SzervezetFilter;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet;

public class SzervezetFilterDTO implements SzervezetFilter, FilterDTO<Szervezet> {

	@Getter @Setter
	private String nev;
	@Getter @Setter
	private String rovidites;
	@Getter @Setter
	private Long idSzervezetTipus;

	@Override
	public <F extends Filter<Szervezet>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
