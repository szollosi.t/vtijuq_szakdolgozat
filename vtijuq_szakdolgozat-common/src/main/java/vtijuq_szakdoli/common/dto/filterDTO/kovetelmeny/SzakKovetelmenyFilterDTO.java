package vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.kovetelmeny.SzakKovetelmenyFilter;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakKovetelmeny;

import java.util.Date;

public class SzakKovetelmenyFilterDTO implements SzakKovetelmenyFilter, FilterDTO<SzakKovetelmeny> {

	@Getter @Setter
	private Long idSzak;
	@Getter @Setter
	private Date ervenyessegKezdet;
	@Getter @Setter
	private Date ervenyessegVeg;
	@Getter @Setter
	private Boolean valid;
	@Getter @Setter
	private Boolean vegleges;
	@Getter @Setter
	private String oklevelMegjeloles;

	@Override
	public <F extends Filter<SzakKovetelmeny>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
