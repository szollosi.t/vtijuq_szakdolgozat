package vtijuq_szakdoli.common.dto.filterDTO.adminisztracio;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.adminisztracio.MunkatarsFilter;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Munkatars;

public class MunkatarsFilterDTO implements MunkatarsFilter, FilterDTO<Munkatars> {

	@Getter @Setter
	private String felhasznalonev;
	@Getter @Setter
	private String nev;
	@Getter @Setter
	private Boolean aktiv;
	@Getter @Setter
	private Long idSzervezet;
	@Getter @Setter
	private Long idSzerepkor;

	@Override
	public <F extends Filter<Munkatars>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
