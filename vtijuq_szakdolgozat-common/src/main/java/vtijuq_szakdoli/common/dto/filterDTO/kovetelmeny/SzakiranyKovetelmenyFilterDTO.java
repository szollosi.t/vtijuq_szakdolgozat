package vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.kovetelmeny.SzakiranyKovetelmenyFilter;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakiranyKovetelmeny;

public class SzakiranyKovetelmenyFilterDTO implements SzakiranyKovetelmenyFilter, FilterDTO<SzakiranyKovetelmeny> {

	@Getter @Setter
	private Boolean csakkozos;
	@Getter @Setter
	private Long idSzakKovetelmeny;
	@Getter @Setter
	private Long idSzakirany;

	@Override
	public <F extends Filter<SzakiranyKovetelmeny>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
