package vtijuq_szakdoli.common.dto.filterDTO.megvalositas;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.megvalositas.KonkretSzakiranyFilter;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany;

public class KonkretSzakiranyFilterDTO implements KonkretSzakiranyFilter, FilterDTO<KonkretSzakirany> {

	@Getter @Setter
	private String nev;
	@Getter @Setter
	private String kod;
	@Getter @Setter
	private Long idKonkretSzak;
	@Getter @Setter
	private Long idSzakiranyKovetelmeny;

	@Override
	public <F extends Filter<KonkretSzakirany>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
