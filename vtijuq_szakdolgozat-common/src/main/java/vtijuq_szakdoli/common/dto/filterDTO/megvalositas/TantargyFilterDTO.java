package vtijuq_szakdoli.common.dto.filterDTO.megvalositas;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.megvalositas.TantargyFilter;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tantargy;

public class TantargyFilterDTO implements TantargyFilter, FilterDTO<Tantargy> {

	@Getter @Setter
	private String nev;
	@Getter @Setter
	private String kod;
	@Getter @Setter
	private Long idSzervezet;

	@Override
	public <F extends Filter<Tantargy>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
