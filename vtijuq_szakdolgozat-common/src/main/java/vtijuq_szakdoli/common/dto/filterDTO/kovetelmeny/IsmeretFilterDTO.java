package vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.kovetelmeny.IsmeretFilter;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ismeret;

public class IsmeretFilterDTO implements IsmeretFilter, FilterDTO<Ismeret> {

	@Getter @Setter
	private String nev;
	@Getter @Setter
	private String leiras;
	@Getter @Setter
	private String kod;

	@Override
	public <F extends Filter<Ismeret>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
