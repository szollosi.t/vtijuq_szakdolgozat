package vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.kovetelmeny.SzakiranyFilter;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szakirany;

public class SzakiranyFilterDTO implements SzakiranyFilter, FilterDTO<Szakirany> {

	@Getter @Setter
	private Long idSzak;

	@Override
	public <F extends Filter<Szakirany>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
