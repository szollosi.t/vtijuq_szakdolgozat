package vtijuq_szakdoli.common.dto.filterDTO.kovetelmeny;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;

import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.kovetelmeny.SzakFilter;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ciklus;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szak;

public class SzakFilterDTO implements SzakFilter, FilterDTO<Szak> {

	@Getter @Setter
	private Ciklus ciklus;
	@Getter @Setter
	private Long idKepzesiTerulet;

	@Override
	public <F extends Filter<Szak>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
