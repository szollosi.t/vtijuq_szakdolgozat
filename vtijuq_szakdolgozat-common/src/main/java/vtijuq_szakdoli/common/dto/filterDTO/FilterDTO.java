package vtijuq_szakdoli.common.dto.filterDTO;

import vtijuq_szakdoli.common.dto.DTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.interfaces.Entitas;

public interface FilterDTO<T extends Entitas> extends Filter<T>, DTO {

	<F extends Filter<T>> F merge(F filter);
}
