package vtijuq_szakdoli.common.dto.filterDTO.test;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.test.SzotarTestFilter;
import vtijuq_szakdoli.common.interfaces.entitas.test.SzotarTest;

public class SzotarTestFilterDTO implements SzotarTestFilter, FilterDTO<SzotarTest> {

	@Getter @Setter
	private String kod;
	@Getter @Setter
	private String nev;
	@Getter @Setter
	private String leiras;
	@Getter @Setter
	private Long idTipus;
	@Getter @Setter
	private Boolean technikai;

	@Override
	public <F extends Filter<SzotarTest>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
