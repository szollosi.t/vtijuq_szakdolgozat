package vtijuq_szakdoli.common.dto.filterDTO.megvalositas;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.megvalositas.KonkretSzakFilter;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzak;

public class KonkretSzakFilterDTO implements KonkretSzakFilter, FilterDTO<KonkretSzak> {

	@Getter @Setter
	private Long idSzakKovetelmeny;
	@Getter @Setter
	private Long idSzervezet;

	@Override
	public <F extends Filter<KonkretSzak>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
