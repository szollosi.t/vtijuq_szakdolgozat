package vtijuq_szakdoli.common.dto.filterDTO.akkreditacio;

import lombok.Getter;
import lombok.Setter;
import utils.MergeUtil;
import vtijuq_szakdoli.common.dto.filterDTO.FilterDTO;
import vtijuq_szakdoli.common.filter.Filter;
import vtijuq_szakdoli.common.filter.akkreditacio.AkkreditacioFilter;
import vtijuq_szakdoli.common.interfaces.entitas.akkreditacio.Akkreditacio;

import java.util.Date;

public class AkkreditacioFilterDTO implements AkkreditacioFilter, FilterDTO<Akkreditacio> {

	@Getter @Setter
	private Date ervenyessegKezdet;
	@Getter @Setter
	private Date ervenyessegVeg;
	@Getter @Setter
	private Boolean valid;
	@Getter @Setter
	private Boolean vegleges;
	@Getter @Setter
	private Long idKonkretSzakirany;

	@Override
	public <F extends Filter<Akkreditacio>> F merge(F filter) {
		return MergeUtil.merge(this, filter);
	}
}
