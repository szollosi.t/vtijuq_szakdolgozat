package vtijuq_szakdoli.common.dto.editableDTO.akkreditacio;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.akkreditacio.Akkreditacio;
import vtijuq_szakdoli.common.interfaces.editable.megvalositas.KonkretSzakirany;

public class AkkreditacioDTO extends vtijuq_szakdoli.common.dto.entitasDTO.akkreditacio.AkkreditacioDTO
		implements EditableDTO<Akkreditacio, vtijuq_szakdoli.common.interfaces.entitas.akkreditacio.Akkreditacio>, Akkreditacio {

	public AkkreditacioDTO() {
	}

	public AkkreditacioDTO(Entitas other) {
		id = other.getId();
	}

	public AkkreditacioDTO(vtijuq_szakdoli.common.dto.entitasDTO.akkreditacio.AkkreditacioDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (konkretSzakirany == null) {
			errors.add("error.required.konkretSzakirany");
		}
		return errors;
	}

	public void setErvenyessegKezdet(Date ervenyessegKezdet) {
		this.ervenyessegKezdet = ervenyessegKezdet;
	}

	public void setErvenyessegVeg(Date ervenyessegVeg) {
		this.ervenyessegVeg = ervenyessegVeg;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void setVegleges(boolean vegleges) {
		this.vegleges = vegleges;
	}

	public void setKonkretSzakirany(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO konkretSzakirany) {
		this.konkretSzakirany = konkretSzakirany;
	}

	@Override
	public void setKonkretSzakirany(KonkretSzakirany konkretSzakirany) {
		this.konkretSzakirany = (KonkretSzakiranyDTO)konkretSzakirany;
	}
}
