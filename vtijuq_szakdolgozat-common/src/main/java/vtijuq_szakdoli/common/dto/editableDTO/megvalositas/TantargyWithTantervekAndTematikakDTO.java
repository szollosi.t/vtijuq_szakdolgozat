package vtijuq_szakdoli.common.dto.editableDTO.megvalositas;

import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.editable.megvalositas.Tantargy;

public class TantargyWithTantervekAndTematikakDTO extends TantargyDTO
		implements EditableDTO<Tantargy, vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tantargy>, Tantargy {

	@Getter
	private List<TantervDTO> tantervek;

	@Getter
	private List<TematikaDTO> tematikak;

	public TantargyWithTantervekAndTematikakDTO() {
	}

	public TantargyWithTantervekAndTematikakDTO(TantargyDTO eredeti, List<TantervDTO> tantervek, List<TematikaDTO> tematikak) {
		super(eredeti);
		this.tematikak = tematikak;
		this.tantervek = tantervek;
		getTematikak().forEach(tt -> tt.setTantargy(this));
		getTantervek().forEach(tt -> tt.setTantargy(this));
	}

	public TantargyWithTantervekAndTematikakDTO(TantargyWithTantervekAndTematikakDTO eredeti) {
		super(eredeti);
		this.tantervek = eredeti.getTantervek().stream().map(TantervDTO::new).collect(Collectors.toList());
		this.tematikak = eredeti.getTematikak().stream().map(TematikaDTO::new).collect(Collectors.toList());
		getTematikak().forEach(tt -> tt.setTantargy(this));
		getTantervek().forEach(tt -> tt.setTantargy(this));
	}
}
