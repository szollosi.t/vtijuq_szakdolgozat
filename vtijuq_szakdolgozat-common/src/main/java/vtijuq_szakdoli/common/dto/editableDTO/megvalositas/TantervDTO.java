package vtijuq_szakdoli.common.dto.editableDTO.megvalositas;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.BooleanUtils;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.ErtekelesTipusDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.megvalositas.Tanterv;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.ErtekelesTipus;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tantargy;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.TantervStatusz;

public class TantervDTO extends vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantervDTO
		implements EditableDTO<Tanterv, vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tanterv>, Tanterv {

	public TantervDTO() {
	}

	public TantervDTO(Entitas other) {
		id = other.getId();
	}

	public TantervDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantervDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (tantargy == null) {
			errors.add("error.required.tantargy");
		}
		if (konkretSzakirany == null) {
			errors.add("error.required.konkretSzakirany");
		}
		if (ervenyessegKezdet != null && ervenyessegVeg != null && ervenyessegKezdet.after(ervenyessegVeg)) {
			errors.add("error.validation.valInterval");
		}
		if (BooleanUtils.isTrue(vegleges) && BooleanUtils.isNotTrue(valid)) {
			errors.add("error.validation.finalise.invalid");
		}
		return errors;
	}

	public void setErvenyessegKezdet(Date ervenyessegKezdet) {
		this.ervenyessegKezdet = ervenyessegKezdet;
	}

	public void setErvenyessegVeg(Date ervenyessegVeg) {
		this.ervenyessegVeg = ervenyessegVeg;
	}

	public void setStatusz(TantervStatusz statusz) {
		this.statusz = statusz;
	}

	public void setFelev(Integer felev) {
		this.felev = felev;
	}

	public void setKredit(Integer kredit) {
		this.kredit = kredit;
	}

	public void setEaOra(Integer eaOra) {
		this.eaOra = eaOra;
	}

	public void setGyakOra(Integer gyakOra) {
		this.gyakOra = gyakOra;
	}

	public void setLabOra(Integer labOra) {
		this.labOra = labOra;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void setVegleges(boolean vegleges) {
		this.vegleges = vegleges;
	}

	public void setKonkretSzakirany(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO konkretSzakirany) {
		this.konkretSzakirany = konkretSzakirany;
	}

	public void setTantargy(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO tantargy) {
		this.tantargy = tantargy;
	}

	public void setErtekelesTipus(ErtekelesTipusDTO ertekelesTipus) {
		this.ertekelesTipus = ertekelesTipus;
	}

	@Override
	public void setKonkretSzakirany(KonkretSzakirany konkretSzakirany) {
		this.konkretSzakirany = (vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO) konkretSzakirany;
	}

	@Override
	public void setTantargy(Tantargy tantargy) {
		this.tantargy = (vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO) tantargy;
	}

	@Override
	public void setErtekelesTipus(ErtekelesTipus ertekelesTipus) {
		this.ertekelesTipus = (ErtekelesTipusDTO) ertekelesTipus;
	}
}
