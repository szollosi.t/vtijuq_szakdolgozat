package vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.BooleanUtils;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.Szak;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.SzakKovetelmeny;

public class SzakKovetelmenyDTO extends vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO
		implements EditableDTO<SzakKovetelmeny, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakKovetelmeny>, SzakKovetelmeny {

	public SzakKovetelmenyDTO() {
	}

	public SzakKovetelmenyDTO(Entitas other) {
		id = other.getId();
	}

	public SzakKovetelmenyDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (szak == null) {
			errors.add("error.required.szak");
		}
		if (szakiranyMinKredit != null && szakiranyMaxKredit != null && szakiranyMinKredit.compareTo(szakiranyMaxKredit) > 0) {
			errors.add("error.validation.kreditInterval");
		}
		if (ervenyessegKezdet != null && ervenyessegVeg != null && ervenyessegKezdet.after(ervenyessegVeg)) {
			errors.add("error.validation.valInterval");
		}
		if (BooleanUtils.isTrue(vegleges) && BooleanUtils.isNotTrue(valid)) {
			errors.add("error.validation.finalise.invalid");
		}
		return errors;
	}

	public void setSzak(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO szak) {
		this.szak = szak;
	}

	public void setErvenyessegKezdet(Date ervenyessegKezdet) {
		this.ervenyessegKezdet = ervenyessegKezdet;
	}

	public void setErvenyessegVeg(Date ervenyessegVeg) {
		this.ervenyessegVeg = ervenyessegVeg;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void setVegleges(boolean vegleges) {
		this.vegleges = vegleges;
	}

	public void setOklevelMegjeloles(String oklevelMegjeloles) {
		this.oklevelMegjeloles = oklevelMegjeloles;
	}

	public void setFelevek(Integer felevek) {
		this.felevek = felevek;
	}

	public void setOsszKredit(Integer osszKredit) {
		this.osszKredit = osszKredit;
	}

	public void setSzakdolgozatKredit(Integer szakdolgozatKredit) {
		this.szakdolgozatKredit = szakdolgozatKredit;
	}

	public void setSzakmaigyakMinKredit(Integer szakmaigyakMinKredit) {
		this.szakmaigyakMinKredit = szakmaigyakMinKredit;
	}

	public void setSzabvalMinKredit(Integer szabvalMinKredit) {
		this.szabvalMinKredit = szabvalMinKredit;
	}

	public void setSzakiranyMinKredit(Integer szakiranyMinKredit) {
		this.szakiranyMinKredit = szakiranyMinKredit;
	}

	public void setSzakmaigyakEloirasok(String szakmaigyakEloirasok) {
		this.szakmaigyakEloirasok = szakmaigyakEloirasok;
	}

	public void setSzakiranyMaxKredit(Integer szakiranyMaxKredit) {
		this.szakiranyMaxKredit = szakiranyMaxKredit;
	}

	@Override
	public void setSzak(Szak szak) {
		this.szak = (vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO)szak;
	}
}
