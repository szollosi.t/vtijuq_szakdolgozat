package vtijuq_szakdoli.common.dto.editableDTO.megvalositas;

import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.editable.megvalositas.KonkretSzakirany;

public class KonkretSzakiranyWithTantervekDTO extends KonkretSzakiranyDTO
		implements EditableDTO<KonkretSzakirany, vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany>, KonkretSzakirany {

	@Getter
	protected List<TantervDTO> tantervek;

	public KonkretSzakiranyWithTantervekDTO(KonkretSzakiranyDTO eredeti, List<TantervDTO> tantervek) {
		super(eredeti);
		this.tantervek = tantervek;
		getTantervek().forEach(tt -> tt.setKonkretSzakirany(this));
	}

	public KonkretSzakiranyWithTantervekDTO(KonkretSzakiranyWithTantervekDTO eredeti) {
		super(eredeti);
		this.tantervek = eredeti.tantervek.stream().map(TantervDTO::new).collect(Collectors.toList());
		getTantervek().forEach(tt -> tt.setKonkretSzakirany(this));
	}
}
