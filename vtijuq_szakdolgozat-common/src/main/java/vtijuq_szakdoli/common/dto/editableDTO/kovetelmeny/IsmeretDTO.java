package vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny;

import org.apache.commons.lang3.StringUtils;
import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.Ismeret;

import java.util.HashSet;
import java.util.Set;

public class IsmeretDTO extends vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO
		implements EditableDTO<Ismeret, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ismeret>, Ismeret {

	public IsmeretDTO() {
	}

	public IsmeretDTO(Entitas other) {
		id = other.getId();
	}

	public IsmeretDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (StringUtils.isBlank(nev)) {
			errors.add("error.required.name");
		}
		if (StringUtils.isBlank(kod)) {
			errors.add("error.required.code");
		}
		return errors;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public void setLeiras(String leiras) {
		this.leiras = leiras;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}
}
