package vtijuq_szakdoli.common.dto.editableDTO.adminisztracio;

import org.apache.commons.lang3.StringUtils;
import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetTipusDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.adminisztracio.Szervezet;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.SzervezetTipus;

import java.util.HashSet;
import java.util.Set;

public class SzervezetDTO extends vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO
		implements EditableDTO<Szervezet, vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet>, Szervezet {

	public SzervezetDTO() {
	}

	public SzervezetDTO(Entitas other) {
		id = other.getId();
	}

	public SzervezetDTO(vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (StringUtils.isBlank(nev)) {
			errors.add("error.required.name");
		}
		if (szervezetTipus == null) {
			errors.add("error.required.organizationType");
		}
		return errors;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public void setRovidites(String rovidites) {
		this.rovidites = rovidites;
	}

	public void setCim(String cim) {
		this.cim = cim;
	}

	public void setSzervezetTipus(SzervezetTipusDTO szervezetTipus) {
		this.szervezetTipus = szervezetTipus;
	}

	@Override
	public void setSzervezetTipus(SzervezetTipus szervezetTipus) {
		this.szervezetTipus = (SzervezetTipusDTO)szervezetTipus;
	}
}
