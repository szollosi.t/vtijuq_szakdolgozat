package vtijuq_szakdoli.common.dto.editableDTO.megvalositas;

import org.apache.commons.lang3.StringUtils;
import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.adminisztracio.Szervezet;
import vtijuq_szakdoli.common.interfaces.editable.megvalositas.Tantargy;

import java.util.HashSet;
import java.util.Set;

public class TantargyDTO extends vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO
		implements EditableDTO<Tantargy, vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tantargy>, Tantargy {

	public TantargyDTO() {
	}

	public TantargyDTO(Entitas other) {
		id = other.getId();
	}

	public TantargyDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (StringUtils.isBlank(kod)) {
			errors.add("error.required.code");
		}
		if (StringUtils.isBlank(nev)) {
			errors.add("error.required.name");
		}
		return errors;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	public void setSzervezet(vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO szervezet) {
		this.szervezet = szervezet;
	}

	@Override
	public void setSzervezet(Szervezet szervezet) {
		this.szervezet = (vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO) szervezet;
	}
}
