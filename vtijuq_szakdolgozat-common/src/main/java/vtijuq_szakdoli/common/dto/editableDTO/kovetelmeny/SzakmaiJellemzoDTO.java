package vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny;

import java.util.HashSet;
import java.util.Set;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.Ismeret;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.SzakmaiJellemzo;

public class SzakmaiJellemzoDTO extends vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO
		implements EditableDTO<SzakmaiJellemzo, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakmaiJellemzo>, SzakmaiJellemzo {

	public SzakmaiJellemzoDTO() {
	}

	public SzakmaiJellemzoDTO(Entitas other) {
		id = other.getId();
	}

	public SzakmaiJellemzoDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakmaiJellemzoDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (ismeret == null) {
			errors.add("error.required.ismeret");
		}
		if (minKredit != null && maxKredit != null && minKredit.compareTo(maxKredit) > 0) {
			errors.add("error.validation.kreditInterval");
		}
		return errors;
	}

	public void setIdSzakiranyKovetelmeny(Long idSzakiranyKovetelmeny){
		this.idSzakiranyKovetelmeny = idSzakiranyKovetelmeny;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public void setIsmeret(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO ismeret) {
		this.ismeret = ismeret;
	}

	public void setMinKredit(Integer minKredit) {
		this.minKredit = minKredit;
	}

	public void setMaxKredit(Integer maxKredit) {
		this.maxKredit = maxKredit;
	}

	public void setMinValaszt(Integer minValaszt) {
		this.minValaszt = minValaszt;
	}

	@Override
	public void setIsmeret(Ismeret ismeret) {
		this.ismeret = (vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO)ismeret;
	}
}
