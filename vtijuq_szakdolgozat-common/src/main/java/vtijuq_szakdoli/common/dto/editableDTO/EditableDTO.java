package vtijuq_szakdoli.common.dto.editableDTO;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.Entitas;

public interface EditableDTO<T extends Editable, S extends Entitas> extends EntitasDTO<S>, Editable {}
