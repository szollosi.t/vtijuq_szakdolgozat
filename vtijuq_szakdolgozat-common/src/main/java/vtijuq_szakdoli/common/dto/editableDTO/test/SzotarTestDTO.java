package vtijuq_szakdoli.common.dto.editableDTO.test;

import org.apache.commons.lang3.StringUtils;
import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.test.SzotarTest;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class SzotarTestDTO extends vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO
		implements EditableDTO<SzotarTest, vtijuq_szakdoli.common.interfaces.entitas.test.SzotarTest>, SzotarTest {

	public SzotarTestDTO() {
	}

	public SzotarTestDTO(Entitas other) {
		id = other.getId();
	}

	public SzotarTestDTO(vtijuq_szakdoli.common.dto.entitasDTO.test.SzotarTestDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (StringUtils.isBlank(kod)) {
			errors.add("error.required.code");
		}
		if (StringUtils.isBlank(nev)) {
			errors.add("error.required.name");
		}
		if (technikai == null) {
			errors.add("error.required.technical");
		}
		return errors;
	}

	@Override
	public void setIdTipus(Long idTipus) {
		this.idTipus = idTipus;
	}

	@Override
	public void setKod(String kod) {
		this.kod = kod;
	}

	@Override
	public void setNev(String nev) {
		this.nev = nev;
	}

	@Override
	public void setLeiras(String leiras) {
		this.leiras = leiras;
	}

	@Override
	public void setErvKezdet(Date ervKezdet) {
		this.ervKezdet = ervKezdet;
	}

	@Override
	public void setErvVeg(Date ervVeg) {
		this.ervVeg = ervVeg;
	}

	@Override
	public void setTechnikai(Boolean technikai) {
		this.technikai = technikai;
	}
}
