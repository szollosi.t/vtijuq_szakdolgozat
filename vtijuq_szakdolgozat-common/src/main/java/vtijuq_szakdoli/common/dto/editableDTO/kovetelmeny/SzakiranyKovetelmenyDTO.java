package vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.BooleanUtils;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.SzakKovetelmeny;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.Szakirany;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.SzakiranyKovetelmeny;

public class SzakiranyKovetelmenyDTO extends vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO
		implements EditableDTO<SzakiranyKovetelmeny, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakiranyKovetelmeny>, SzakiranyKovetelmeny {

	public SzakiranyKovetelmenyDTO() {
	}

	public SzakiranyKovetelmenyDTO(Entitas other) {
		id = other.getId();
	}

	public SzakiranyKovetelmenyDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (szakKovetelmeny == null) {
			errors.add("error.required.szakKovetelmeny");
		}
		if (BooleanUtils.isTrue(csakkozos) && szakirany != null) {
			errors.add("error.validation.csakkKozosWithSzakirany");
		}
		return errors;
	}

	public void setCsakkozos(Boolean csakkozos) {
		this.csakkozos = csakkozos;
	}

	public void setSzakKovetelmeny(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO szakKovetelmeny) {
		this.szakKovetelmeny = szakKovetelmeny;
	}

	public void setSzakirany(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO szakirany) {
		this.szakirany = szakirany;
	}

	@Override
	public void setSzakKovetelmeny(SzakKovetelmeny szakKovetelmeny) {
		this.szakKovetelmeny = (SzakKovetelmenyDTO)szakKovetelmeny;
	}

	@Override
	public void setSzakirany(Szakirany szakirany) {
		this.szakirany = (vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO)szakirany;
	}
}
