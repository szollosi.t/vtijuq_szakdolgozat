package vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny;

import org.apache.commons.lang3.StringUtils;
import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.Szak;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.Szakirany;

import java.util.HashSet;
import java.util.Set;

public class SzakiranyDTO extends vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO
		implements EditableDTO<Szakirany, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szakirany>, Szakirany {

	public SzakiranyDTO() {
	}

	public SzakiranyDTO(Entitas other) {
		id = other.getId();
	}

	public SzakiranyDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (StringUtils.isBlank(nev)) {
			errors.add("error.required.name");
		}
		if (szak == null) {
			errors.add("error.required.szak");
		}
		return errors;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public void setLeiras(String leiras) {
		this.leiras = leiras;
	}

	public void setSzak(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO szak) {
		this.szak = szak;
	}

	@Override
	public void setSzak(Szak szak) {
		this.szak = (vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO)szak;
	}
}
