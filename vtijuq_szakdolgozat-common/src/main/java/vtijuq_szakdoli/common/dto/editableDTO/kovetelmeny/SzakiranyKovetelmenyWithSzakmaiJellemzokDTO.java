package vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.SzakiranyKovetelmeny;
import vtijuq_szakdoli.common.util.graph.Dag;

public class SzakiranyKovetelmenyWithSzakmaiJellemzokDTO extends SzakiranyKovetelmenyDTO
		implements EditableDTO<SzakiranyKovetelmeny, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakiranyKovetelmeny>, SzakiranyKovetelmeny {

	@Getter
	private Dag<SzakmaiJellemzoDTO> szakmaiJellemzok;

	public SzakiranyKovetelmenyWithSzakmaiJellemzokDTO(SzakiranyKovetelmenyDTO eredeti, Dag<SzakmaiJellemzoDTO> szakmaiJellemzok) {
		super(eredeti);
		csakkozos = eredeti.getCsakkozos();
		szakKovetelmeny = new SzakKovetelmenyDTO(eredeti.getSzakKovetelmeny());
		if (eredeti.getSzakirany() != null) {
			szakirany = new SzakiranyDTO(eredeti.getSzakirany());
		}
		this.szakmaiJellemzok = szakmaiJellemzok;
	}

	public SzakiranyKovetelmenyWithSzakmaiJellemzokDTO(SzakiranyKovetelmenyWithSzakmaiJellemzokDTO eredeti) {
		this(eredeti, eredeti.getSzakmaiJellemzok().map(SzakmaiJellemzoDTO::new));
	}
}
