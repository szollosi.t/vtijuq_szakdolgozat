package vtijuq_szakdoli.common.dto.editableDTO.adminisztracio;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzerepkorDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.adminisztracio.Munkatars;
import vtijuq_szakdoli.common.interfaces.editable.adminisztracio.Szervezet;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szerepkor;

public class MunkatarsDTO extends vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.MunkatarsDTO
		implements EditableDTO<Munkatars, vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Munkatars>, Munkatars {

	public MunkatarsDTO() {
		jelszovaltoztatas = true;
	}

	public MunkatarsDTO(Entitas other) {
		id = other.getId();
	}

	public MunkatarsDTO(vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.MunkatarsDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (StringUtils.isBlank(felhasznalonev)) {
			errors.add("error.required.username");
		}
		if (StringUtils.isBlank(nev)) {
			errors.add("error.required.name");
		}
		return errors;
	}

	public void setFelhasznalonev(String felhasznalonev) {
		this.felhasznalonev = felhasznalonev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public void setAktiv(boolean aktiv) {
		this.aktiv = aktiv;
	}

	public void setJelszovaltoztatas(boolean jelszovaltoztatas) {
		this.jelszovaltoztatas = jelszovaltoztatas;
	}

	public void setSzervezet(vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO szervezet) {
		this.szervezet = szervezet;
	}

	public void setSzerepkor(SzerepkorDTO szerepkor) {
		this.szerepkor = szerepkor;
	}

	@Override
	public void setSzervezet(Szervezet szervezet) {
		this.szervezet = (SzervezetDTO)szervezet;
	}

	@Override
	public void setSzerepkor(Szerepkor szerepkor) {
		this.szerepkor = (SzerepkorDTO)szerepkor;
	}
}
