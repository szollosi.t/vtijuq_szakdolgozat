package vtijuq_szakdoli.common.dto.editableDTO.megvalositas;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.SzakiranyKovetelmeny;
import vtijuq_szakdoli.common.interfaces.editable.megvalositas.KonkretSzak;
import vtijuq_szakdoli.common.interfaces.editable.megvalositas.KonkretSzakirany;

import java.util.HashSet;
import java.util.Set;

public class KonkretSzakiranyDTO extends vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO
		implements EditableDTO<KonkretSzakirany, vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany>, KonkretSzakirany {

	public KonkretSzakiranyDTO() {
	}

	public KonkretSzakiranyDTO(Entitas other) {
		id = other.getId();
	}

	public KonkretSzakiranyDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (szakiranyKovetelmeny == null) {
			errors.add("error.required.szakiranyKovetelmeny");
		}
		if (konkretSzak == null) {
			errors.add("error.required.konkretSzak");
		}
		return errors;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	public void setKonkretSzak(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO konkretSzak) {
		this.konkretSzak = konkretSzak;
	}

	public void setSzakiranyKovetelmeny(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO szakiranyKovetelmeny) {
		this.szakiranyKovetelmeny = szakiranyKovetelmeny;
	}

	@Override
	public void setKonkretSzak(KonkretSzak konkretSzak) {
		this.konkretSzak = (vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO)konkretSzak;
	}

	@Override
	public void setSzakiranyKovetelmeny(SzakiranyKovetelmeny szakiranyKovetelmeny) {
		this.szakiranyKovetelmeny = (SzakiranyKovetelmenyDTO) szakiranyKovetelmeny;
	}
}
