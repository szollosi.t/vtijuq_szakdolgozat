package vtijuq_szakdoli.common.dto.editableDTO.megvalositas;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.BooleanUtils;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.IsmeretSzintDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.megvalositas.Tematika;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.IsmeretSzint;

public class TematikaDTO extends vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TematikaDTO
		implements EditableDTO<Tematika, vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tematika>, Tematika {

	public TematikaDTO() {
	}

	public TematikaDTO(Entitas other) {
		id = other.getId();
	}

	public TematikaDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TematikaDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (tema == null) {
			errors.add("error.required.tema");
		}
		if (ervenyessegKezdet != null && ervenyessegVeg != null && ervenyessegKezdet.after(ervenyessegVeg)) {
			errors.add("error.validation.valInterval");
		}
		if (BooleanUtils.isTrue(vegleges) && BooleanUtils.isNotTrue(valid)) {
			errors.add("error.validation.finalise.invalid");
		}
		return errors;
	}

	public void setErvenyessegKezdet(Date ervenyessegKezdet) {
		this.ervenyessegKezdet = ervenyessegKezdet;
	}

	public void setErvenyessegVeg(Date ervenyessegVeg) {
		this.ervenyessegVeg = ervenyessegVeg;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void setVegleges(boolean vegleges) {
		this.vegleges = vegleges;
	}

	public void setTantargy(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TantargyDTO tantargy) {
		this.tantargy = tantargy;
	}

	public void setTema(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO tema) {
		this.tema = tema;
	}

	public void setLeadottSzint(IsmeretSzintDTO leadottSzint) {
		this.leadottSzint = leadottSzint;
	}

	public void setSzamonkertSzint(IsmeretSzintDTO szamonkertSzint) {
		this.szamonkertSzint = szamonkertSzint;
	}

	@Override
	public void setLeadottSzint(IsmeretSzint leadottSzint) {
		this.leadottSzint = (IsmeretSzintDTO) leadottSzint;
	}

	@Override
	public void setSzamonkertSzint(IsmeretSzint szamonkertSzint) {
		this.szamonkertSzint = (IsmeretSzintDTO) szamonkertSzint;
	}
}
