package vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.SzakKovetelmeny;

public class SzakKovetelmenyWithSzakiranyokDTO extends SzakKovetelmenyDTO
		implements EditableDTO<SzakKovetelmeny, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakKovetelmeny>, SzakKovetelmeny {

	@Getter
	private SzakiranyKovetelmenyWithSzakmaiJellemzokDTO alapSzakiranyKovetelmeny;
	@Getter
	private List<SzakiranyKovetelmenyWithSzakmaiJellemzokDTO> szakiranyKovetelmenyek;

	public SzakKovetelmenyWithSzakiranyokDTO() {
	}

	public SzakKovetelmenyWithSzakiranyokDTO(SzakKovetelmenyDTO eredeti,
			SzakiranyKovetelmenyWithSzakmaiJellemzokDTO alapSzakiranyKovetelmeny,
			List<SzakiranyKovetelmenyWithSzakmaiJellemzokDTO> szakiranyKovetelmenyek) {
		super(eredeti);
		this.alapSzakiranyKovetelmeny = alapSzakiranyKovetelmeny;
		this.szakiranyKovetelmenyek = szakiranyKovetelmenyek;
	}

	public SzakKovetelmenyWithSzakiranyokDTO(SzakKovetelmenyWithSzakiranyokDTO eredeti) {
		super(eredeti);
		this.alapSzakiranyKovetelmeny = new SzakiranyKovetelmenyWithSzakmaiJellemzokDTO(eredeti.getAlapSzakiranyKovetelmeny());
		this.szakiranyKovetelmenyek = eredeti.getSzakiranyKovetelmenyek().stream()
				.map(SzakiranyKovetelmenyWithSzakmaiJellemzokDTO::new)
				.collect(Collectors.toList());
	}

	@Override
	public Set<String> getErrors() {
		final Set<String> errors = super.getErrors();
		errors.addAll(alapSzakiranyKovetelmeny.getErrors());
		return errors;
	}
}
