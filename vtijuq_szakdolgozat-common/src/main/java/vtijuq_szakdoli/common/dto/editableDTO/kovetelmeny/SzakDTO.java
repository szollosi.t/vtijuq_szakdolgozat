package vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny;

import org.apache.commons.lang3.StringUtils;
import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.KepzesiTeruletDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.Szak;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ciklus;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.KepzesiTerulet;

import java.util.HashSet;
import java.util.Set;

public class SzakDTO extends vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO
		implements EditableDTO<Szak, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szak>, Szak {

	public SzakDTO() {
	}

	public SzakDTO(Entitas other) {
		id = other.getId();
	}

	public SzakDTO(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (StringUtils.isBlank(nev)) {
			errors.add("error.required.name");
		}
		if (kepzesiTerulet == null) {
			errors.add("error.required.kepzesiTerulet");
		}
		if (ciklus == null) {
			errors.add("error.required.ciklus");
		}
		return errors;
	}

	public void setKepzesiTerulet(KepzesiTeruletDTO kepzesiTerulet) {
		this.kepzesiTerulet = kepzesiTerulet;
	}

	public void setCiklus(Ciklus ciklus) {
		this.ciklus = ciklus;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	@Override
	public void setKepzesiTerulet(KepzesiTerulet kepzesiTerulet) {
		this.kepzesiTerulet = (KepzesiTeruletDTO)kepzesiTerulet;
	}
}
