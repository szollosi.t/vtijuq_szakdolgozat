package vtijuq_szakdoli.common.dto.editableDTO.megvalositas;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.adminisztracio.Szervezet;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.SzakKovetelmeny;
import vtijuq_szakdoli.common.interfaces.editable.megvalositas.KonkretSzak;

import java.util.HashSet;
import java.util.Set;

public class KonkretSzakDTO extends vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO
		implements EditableDTO<KonkretSzak, vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzak>, KonkretSzak {

	public KonkretSzakDTO() {
	}

	public KonkretSzakDTO(Entitas other) {
		id = other.getId();
	}

	public KonkretSzakDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (szakKovetelmeny == null) {
			errors.add("error.required.szakKovetelmeny");
		}
		if (szervezet == null) {
			errors.add("error.required.organization");
		}
		return errors;
	}

	public void setSzakmaigyakKredit(Integer szakmaigyakKredit) {
		this.szakmaigyakKredit = szakmaigyakKredit;
	}

	public void setSzabvalKredit(Integer szabvalKredit) {
		this.szabvalKredit = szabvalKredit;
	}

	public void setSzakiranyKredit(Integer szakiranyKredit) {
		this.szakiranyKredit = szakiranyKredit;
	}

	public void setSzakKovetelmeny(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO szakKovetelmeny) {
		this.szakKovetelmeny = szakKovetelmeny;
	}

	public void setSzervezet(vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO szervezet) {
		this.szervezet = szervezet;
	}

	@Override
	public void setSzakKovetelmeny(SzakKovetelmeny szakKovetelmeny) {
		this.szakKovetelmeny = (SzakKovetelmenyDTO)szakKovetelmeny;
	}

	@Override
	public void setSzervezet(Szervezet szervezet) {
		this.szervezet = (vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO)szervezet;
	}
}
