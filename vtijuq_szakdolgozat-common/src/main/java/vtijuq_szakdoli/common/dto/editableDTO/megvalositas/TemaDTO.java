package vtijuq_szakdoli.common.dto.editableDTO.megvalositas;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.Ismeret;
import vtijuq_szakdoli.common.interfaces.editable.megvalositas.Tema;

public class TemaDTO extends vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO
		implements EditableDTO<Tema, vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tema>, Tema {

	public TemaDTO() {
	}

	public TemaDTO(Entitas other) {
		id = other.getId();
	}

	public TemaDTO(vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.TemaDTO eredeti) {
		super(eredeti);
	}

	@Override
	public Set<String> getErrors() {
		final HashSet<String> errors = new HashSet<>();
		if (StringUtils.isBlank(nev) && ismeret == null) {
			errors.add("error.required.ismeretOrNev");
		}
		return errors;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public void setLeiras(String leiras) {
		this.leiras = leiras;
	}

	public void setIsmeret(vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO ismeret) {
		this.ismeret = ismeret;
	}

	@Override
	public void setIsmeret(Ismeret ismeret) {
		this.ismeret = (vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO) ismeret;
	}
}
