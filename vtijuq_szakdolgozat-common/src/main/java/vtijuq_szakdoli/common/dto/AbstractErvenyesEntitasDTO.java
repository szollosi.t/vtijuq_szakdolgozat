package vtijuq_szakdoli.common.dto;

import java.util.Date;

import lombok.Getter;

import vtijuq_szakdoli.common.interfaces.Entitas;

public abstract class AbstractErvenyesEntitasDTO extends AbstractEntitasDTO {

	@Getter
	protected Date ervenyessegKezdet;
	@Getter
	protected Date ervenyessegVeg;
	@Getter
	protected boolean valid;
	@Getter
	protected boolean vegleges;

	public AbstractErvenyesEntitasDTO() {}

	public AbstractErvenyesEntitasDTO(Entitas other) {
		id = other.getId();
	}
}
