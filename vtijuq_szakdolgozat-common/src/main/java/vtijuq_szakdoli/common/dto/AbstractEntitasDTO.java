package vtijuq_szakdoli.common.dto;

import lombok.Getter;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import vtijuq_szakdoli.common.interfaces.Entitas;

public abstract class AbstractEntitasDTO implements Entitas {
	@Getter
	protected Long id;

	public AbstractEntitasDTO() {}

	public AbstractEntitasDTO(Entitas other) {
		id = other.getId();
	}

	public int compareTo(AbstractEntitasDTO other){
		return Long.compare(id, other.id);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		AbstractEntitasDTO that = (AbstractEntitasDTO) o;
		return id != null && id.equals(that.id);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
