package vtijuq_szakdoli.common.dto;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Szotar;

public abstract class SzotarDTO extends ErtekkeszletDTO implements Szotar {
	protected String leiras;

	public SzotarDTO(){}

	public SzotarDTO(Entitas other) {
		id = other.getId();
	}

	public SzotarDTO(Szotar eredeti) {
		super(eredeti);
		leiras = eredeti.getLeiras();
	}

	@Override
	public String getLeiras() {
		return leiras == null ? getNev() : leiras;
	}
}
