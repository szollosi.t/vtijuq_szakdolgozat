package vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.AbstractErvenyesEntitasDTO;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakKovetelmeny;

public class SzakKovetelmenyDTO extends AbstractErvenyesEntitasDTO implements EntitasDTO<SzakKovetelmeny>, SzakKovetelmeny {

	@Getter
	protected SzakDTO szak;
	@Getter
	protected String oklevelMegjeloles;
	@Getter
	protected Integer felevek;
	@Getter
	protected Integer osszKredit;
	@Getter
	protected Integer szakdolgozatKredit;
	@Getter
	protected Integer szakmaigyakMinKredit;
	@Getter
	protected Integer szabvalMinKredit;
	@Getter
	protected Integer szakiranyMinKredit;
	@Getter
	protected String szakmaigyakEloirasok;
	@Getter
	protected Integer szakiranyMaxKredit;

	public SzakKovetelmenyDTO() {
	}

	public SzakKovetelmenyDTO(Entitas other) {
		id = other.getId();
	}

	public SzakKovetelmenyDTO(SzakKovetelmenyDTO eredeti) {
		super(eredeti);
		szak = new SzakDTO(eredeti.getSzak());
		ervenyessegKezdet = eredeti.getErvenyessegKezdet();
		ervenyessegVeg = eredeti.getErvenyessegVeg();
		valid = eredeti.isValid();
		vegleges = eredeti.isVegleges();
		oklevelMegjeloles = eredeti.getOklevelMegjeloles();
		felevek = eredeti.getFelevek();
		osszKredit = eredeti.getOsszKredit();
		szakdolgozatKredit = eredeti.getSzakdolgozatKredit();
		szakmaigyakMinKredit = eredeti.getSzakmaigyakMinKredit();
		szabvalMinKredit = eredeti.getSzabvalMinKredit();
		szakiranyMinKredit = eredeti.getSzakiranyMinKredit();
		szakmaigyakEloirasok = eredeti.getSzakmaigyakEloirasok();
		szakiranyMaxKredit = eredeti.getSzakiranyMaxKredit();
	}

	@Override
	public String toString() {
		return getNev();
	}
}
