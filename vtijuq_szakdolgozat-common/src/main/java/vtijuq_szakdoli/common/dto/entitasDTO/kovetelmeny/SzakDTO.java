package vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny;

import lombok.Getter;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.ErtekkeszletDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ciklus;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szak;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SzakDTO extends ErtekkeszletDTO implements EntitasDTO<Szak>, Szak {

	@Getter
	protected KepzesiTeruletDTO kepzesiTerulet;
	@Getter
	protected Ciklus ciklus;
	protected List<SzakiranyDTO> szakiranyok;

	public SzakDTO() {
	}

	public SzakDTO(Entitas other) {
		id = other.getId();
	}

	public SzakDTO(SzakDTO eredeti) {
		super(eredeti);
		kepzesiTerulet = new KepzesiTeruletDTO(eredeti.getKepzesiTerulet());
		ciklus = eredeti.getCiklus();
		szakiranyok = eredeti.getSzakiranyok().stream().map(SzakiranyDTO::new).collect(Collectors.toList());
	}

	public List<SzakiranyDTO> getSzakiranyok() {
		if(szakiranyok == null){
			szakiranyok = new ArrayList<>();
		}
		return szakiranyok;
	}
}
