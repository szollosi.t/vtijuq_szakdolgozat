package vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.ErtekkeszletDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Munkatars;

public class MunkatarsDTO extends ErtekkeszletDTO implements EntitasDTO<Munkatars>, Munkatars {

	@Getter
	protected String felhasznalonev;
	@Getter
	protected boolean aktiv;
	@Getter
	protected boolean jelszovaltoztatas;
	@Getter
	protected SzervezetDTO szervezet;
	@Getter
	protected SzerepkorDTO szerepkor;

	public MunkatarsDTO() {
	}

	public MunkatarsDTO(Entitas other) {
		id = other.getId();
	}

	public MunkatarsDTO(MunkatarsDTO eredeti) {
		super(eredeti);
		felhasznalonev = eredeti.getFelhasznalonev();
		aktiv = eredeti.isAktiv();
		jelszovaltoztatas = eredeti.isJelszovaltoztatas();
		if (eredeti.getSzervezet() != null) {
			szervezet = new SzervezetDTO(eredeti.getSzervezet());
		}
		if (eredeti.getSzerepkor() != null) {
			szerepkor = new SzerepkorDTO(eredeti.getSzerepkor());
		}

	}
}
