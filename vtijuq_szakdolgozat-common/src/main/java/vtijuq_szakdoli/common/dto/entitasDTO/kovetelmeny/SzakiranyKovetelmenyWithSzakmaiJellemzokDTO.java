package vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakiranyKovetelmeny;
import vtijuq_szakdoli.common.util.graph.Dag;

public class SzakiranyKovetelmenyWithSzakmaiJellemzokDTO extends SzakiranyKovetelmenyDTO implements EntitasDTO<SzakiranyKovetelmeny>, SzakiranyKovetelmeny {

	@Getter
	private Dag<SzakmaiJellemzoDTO> szakmaiJellemzok;

	public SzakiranyKovetelmenyWithSzakmaiJellemzokDTO(SzakiranyKovetelmenyDTO eredeti, Dag<SzakmaiJellemzoDTO> szakmaiJellemzok) {
		super(eredeti);
		csakkozos = eredeti.getCsakkozos();
		if (eredeti.getSzakKovetelmeny() != null) {
			szakKovetelmeny = new SzakKovetelmenyDTO(eredeti.getSzakKovetelmeny());
		}
		if (eredeti.getSzakirany() != null) {
			szakirany = new SzakiranyDTO(eredeti.getSzakirany());
		}
		this.szakmaiJellemzok = szakmaiJellemzok;
	}

	public SzakiranyKovetelmenyWithSzakmaiJellemzokDTO(SzakiranyKovetelmenyWithSzakmaiJellemzokDTO eredeti) {
		this(eredeti, eredeti.getSzakmaiJellemzok().map(SzakmaiJellemzoDTO::new));
	}
}
