package vtijuq_szakdoli.common.dto.entitasDTO.megvalositas;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.SzotarDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tema;

public class TemaDTO extends SzotarDTO implements EntitasDTO<Tema>, Tema {

	@Getter
	protected IsmeretDTO ismeret;

	public TemaDTO() {
	}

	public TemaDTO(Entitas other) {
		id = other.getId();
	}

	public TemaDTO(TemaDTO eredeti) {
		super(eredeti);
		ismeret = new IsmeretDTO(eredeti.getIsmeret());
	}

	@Override
	public String getKod() {
		return getIsmeret() != null ? getIsmeret().getKod() : StringUtils.EMPTY;
	}

	@Override
	public String getNev() {
		return isNotBlank(nev) ? nev : getIsmeret() != null ? getIsmeret().getNev() : StringUtils.EMPTY;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	@Override
	public String getLeiras() {
		return isNotBlank(leiras) ? leiras : getIsmeret() != null ? getIsmeret().getLeiras() : StringUtils.EMPTY;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (!(o instanceof TemaDTO)) return false;

		TemaDTO temaDTO = (TemaDTO) o;
		return new EqualsBuilder()
				.append(getIsmeret(), temaDTO.getIsmeret())
				.append(getNev(), temaDTO.getNev())
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(getIsmeret())
				.append(getNev())
				.toHashCode();
	}
}
