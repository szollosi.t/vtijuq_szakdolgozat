package vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio;

import lombok.Getter;
import vtijuq_szakdoli.common.dto.AbstractEntitasDTO;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Jogosultsag;

public class JogosultsagDTO extends AbstractEntitasDTO implements EntitasDTO<Jogosultsag>, Jogosultsag {

	@Getter
	protected JogosultsagSzint szint;
	@Getter
	protected FunkcioDTO funkcio;

	public JogosultsagDTO() {}

	public JogosultsagDTO(Jogosultsag eredeti, FunkcioDTO funkcio) {
		this(eredeti);
		this.funkcio = funkcio;
	}

	public JogosultsagDTO(Jogosultsag eredeti) {
		super(eredeti);
		this.szint = eredeti.getSzint();
		this.funkcio = new FunkcioDTO(eredeti.getFunkcio());
	}

	@Override
	public String toString() {
		return String.format("%s - %s", getFunkcio().getNev(), getSzint());
	}
}
