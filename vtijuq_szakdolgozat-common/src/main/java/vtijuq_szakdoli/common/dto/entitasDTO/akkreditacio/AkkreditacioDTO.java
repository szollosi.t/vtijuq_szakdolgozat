package vtijuq_szakdoli.common.dto.entitasDTO.akkreditacio;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.AbstractErvenyesEntitasDTO;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.akkreditacio.Akkreditacio;

public class AkkreditacioDTO extends AbstractErvenyesEntitasDTO implements EntitasDTO<Akkreditacio>, Akkreditacio {

	@Getter
	protected KonkretSzakiranyDTO konkretSzakirany;

	public AkkreditacioDTO() {
	}

	public AkkreditacioDTO(Entitas other) {
		id = other.getId();
	}

	public AkkreditacioDTO(AkkreditacioDTO eredeti) {
		super(eredeti);
		ervenyessegKezdet = eredeti.getErvenyessegKezdet();
		ervenyessegVeg = eredeti.getErvenyessegVeg();
		valid = eredeti.isValid();
		vegleges = eredeti.isVegleges();
		if (eredeti.getKonkretSzakirany() != null) {
			konkretSzakirany = new KonkretSzakiranyDTO(eredeti.getKonkretSzakirany());
		}
	}

	@Override
	public String toString() {
		return String.format("%s : %s - %s",
				getKonkretSzakirany().getKonkretSzak().getSzervezet().getRovidites(),
				getKonkretSzakirany().getKonkretSzak().getSzakKovetelmeny(),
				getKonkretSzakirany().getSzakiranyKovetelmeny());
	}
}
