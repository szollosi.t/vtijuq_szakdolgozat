package vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.SzotarDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ismeret;
import vtijuq_szakdoli.common.util.graph.Hierarchical;

public class IsmeretDTO extends SzotarDTO implements EntitasDTO<Ismeret>, Ismeret, Hierarchical<IsmeretDTO> {

	public IsmeretDTO() {
	}

	public IsmeretDTO(Entitas other) {
		id = other.getId();
	}

	public IsmeretDTO(IsmeretDTO eredeti) {
		super(eredeti);
	}
}
