package vtijuq_szakdoli.common.dto.entitasDTO.megvalositas;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.SzotarDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.IsmeretSzint;

public class IsmeretSzintDTO extends SzotarDTO implements EntitasDTO<IsmeretSzint>, IsmeretSzint {

	public IsmeretSzintDTO() {}

	public IsmeretSzintDTO(Entitas other) {
		id = other.getId();
	}

	public IsmeretSzintDTO(IsmeretSzint eredeti) {
		super(eredeti);
	}
}
