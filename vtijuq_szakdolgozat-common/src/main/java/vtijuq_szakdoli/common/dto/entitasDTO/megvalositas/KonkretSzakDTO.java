package vtijuq_szakdoli.common.dto.entitasDTO.megvalositas;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.AbstractEntitasDTO;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzak;

public class KonkretSzakDTO extends AbstractEntitasDTO implements EntitasDTO<KonkretSzak>, KonkretSzak {

	@Getter
	protected Integer szakmaigyakKredit;
	@Getter
	protected Integer szabvalKredit;
	@Getter
	protected Integer szakiranyKredit;
	@Getter
	protected SzakKovetelmenyDTO szakKovetelmeny;
	@Getter
	protected SzervezetDTO szervezet;

	public KonkretSzakDTO() {
	}

	public KonkretSzakDTO(Entitas other) {
		id = other.getId();
	}

	public KonkretSzakDTO(KonkretSzakDTO eredeti) {
		super(eredeti);
		szakmaigyakKredit = eredeti.getSzakmaigyakKredit();
		szabvalKredit = eredeti.getSzabvalKredit();
		szakiranyKredit = eredeti.getSzakiranyKredit();
		if (eredeti.getSzakKovetelmeny() != null) {
			szakKovetelmeny = new SzakKovetelmenyDTO(eredeti.getSzakKovetelmeny());
		}
		if (eredeti.getSzervezet() != null) {
			szervezet = new SzervezetDTO(eredeti.getSzervezet());
		}
	}

	@Override
	public String toString() {
		return getNev();
	}
}
