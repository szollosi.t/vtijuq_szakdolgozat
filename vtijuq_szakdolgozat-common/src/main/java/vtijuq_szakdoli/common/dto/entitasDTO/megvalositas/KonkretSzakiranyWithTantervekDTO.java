package vtijuq_szakdoli.common.dto.entitasDTO.megvalositas;

import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany;

public class KonkretSzakiranyWithTantervekDTO extends KonkretSzakiranyDTO implements EntitasDTO<KonkretSzakirany>, KonkretSzakirany {

	@Getter
	protected List<TantervDTO> tantervek;

	public KonkretSzakiranyWithTantervekDTO(KonkretSzakiranyDTO eredeti, List<TantervDTO> tantervek) {
		super(eredeti);
		this.tantervek = tantervek;
	}

	public KonkretSzakiranyWithTantervekDTO(KonkretSzakiranyWithTantervekDTO eredeti) {
		super(eredeti);
		this.tantervek = eredeti.getTantervek().stream().map(TantervDTO::new).collect(Collectors.toList());
	}
}
