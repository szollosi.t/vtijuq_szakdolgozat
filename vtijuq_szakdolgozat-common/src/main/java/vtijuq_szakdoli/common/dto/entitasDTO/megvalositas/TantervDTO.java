package vtijuq_szakdoli.common.dto.entitasDTO.megvalositas;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.AbstractErvenyesEntitasDTO;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tanterv;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.TantervStatusz;

public class TantervDTO extends AbstractErvenyesEntitasDTO implements EntitasDTO<Tanterv>, Tanterv {

	@Getter
	protected TantervStatusz statusz;
	@Getter
	protected Integer felev;
	@Getter
	protected Integer kredit;
	@Getter
	protected Integer eaOra;
	@Getter
	protected Integer gyakOra;
	@Getter
	protected Integer labOra;
	@Getter
	protected KonkretSzakiranyDTO konkretSzakirany;
	@Getter
	protected TantargyDTO tantargy;
	@Getter
	protected ErtekelesTipusDTO ertekelesTipus;

	public TantervDTO() {
	}

	public TantervDTO(Entitas other) {
		id = other.getId();
	}

	public TantervDTO(TantervDTO eredeti) {
		super(eredeti);
		ervenyessegKezdet = eredeti.getErvenyessegKezdet();
		ervenyessegVeg = eredeti.getErvenyessegVeg();
		statusz = eredeti.getStatusz();
		felev = eredeti.getFelev();
		kredit = eredeti.getKredit();
		eaOra = eredeti.getEaOra();
		gyakOra = eredeti.getGyakOra();
		labOra = eredeti.getLabOra();
		valid = eredeti.isValid();
		vegleges = eredeti.isVegleges();
		if (eredeti.getKonkretSzakirany() != null) {
			konkretSzakirany = new KonkretSzakiranyDTO(eredeti.getKonkretSzakirany());
		}
		if (eredeti.getTantargy() != null) {
			tantargy = new TantargyDTO(eredeti.getTantargy());
		}
		if (eredeti.getErtekelesTipus() != null) {
			ertekelesTipus = new ErtekelesTipusDTO(eredeti.getErtekelesTipus());
		}
	}

	@Override
	public String toString() {
		return String.format("%s - %s, %s", getKonkretSzakirany(), getTantargy(), getStatusz());
	}
}
