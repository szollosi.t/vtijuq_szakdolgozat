package vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio;

import lombok.Getter;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.SzotarDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.common.util.graph.Hierarchical;

public class SzervezetDTO extends SzotarDTO implements EntitasDTO<Szervezet>, Szervezet, Hierarchical<SzervezetDTO> {

	@Getter
	protected String rovidites;
	@Getter
	protected String cim;
	@Getter
	protected SzervezetTipusDTO szervezetTipus;

	public SzervezetDTO() {
	}

	public SzervezetDTO(Entitas other) {
		id = other.getId();
	}

	public SzervezetDTO(SzervezetDTO eredeti) {
		super(eredeti);
		rovidites = eredeti.getRovidites();
		cim = eredeti.getCim();
		szervezetTipus = new SzervezetTipusDTO(eredeti.getSzervezetTipus());
	}
}
