package vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.SzotarDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakmaiJellemzo;
import vtijuq_szakdoli.common.util.graph.Hierarchical;

public class SzakmaiJellemzoDTO extends SzotarDTO implements EntitasDTO<SzakmaiJellemzo>, SzakmaiJellemzo, Hierarchical<SzakmaiJellemzoDTO> {

	@Getter
	protected Long idSzakiranyKovetelmeny;
	@Getter
	protected IsmeretDTO ismeret;
	@Getter
	protected Integer minKredit;
	@Getter
	protected Integer maxKredit;
	@Getter
	protected Integer minValaszt;

	public SzakmaiJellemzoDTO() {
	}

	public SzakmaiJellemzoDTO(Entitas other) {
		id = other.getId();
	}

	public SzakmaiJellemzoDTO(SzakmaiJellemzoDTO eredeti) {
		super(eredeti);
		idSzakiranyKovetelmeny = eredeti.getIdSzakiranyKovetelmeny();
		ismeret = new IsmeretDTO(eredeti.getIsmeret());
		minKredit = eredeti.getMinKredit();
		maxKredit = eredeti.getMaxKredit();
		minValaszt = eredeti.getMinValaszt();
	}
}
