package vtijuq_szakdoli.common.dto.entitasDTO.megvalositas;

import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzak;

public class KonkretSzakWithSzakiranyokDTO extends KonkretSzakDTO implements EntitasDTO<KonkretSzak>, KonkretSzak {

	@Getter
	private KonkretSzakiranyWithTantervekDTO alapKonkretSzakirany;

	@Getter
	private List<KonkretSzakiranyWithTantervekDTO> konkretSzakiranyok;

	public KonkretSzakWithSzakiranyokDTO() {
	}

	public KonkretSzakWithSzakiranyokDTO(KonkretSzakDTO eredeti,
			KonkretSzakiranyWithTantervekDTO alapKonkretSzakirany,
			List<KonkretSzakiranyWithTantervekDTO> konkretSzakiranyok) {
		super(eredeti);
		this.alapKonkretSzakirany = alapKonkretSzakirany;
		this.konkretSzakiranyok = konkretSzakiranyok;
	}

	public KonkretSzakWithSzakiranyokDTO(KonkretSzakWithSzakiranyokDTO eredeti) {
		super(eredeti);
		this.alapKonkretSzakirany = new KonkretSzakiranyWithTantervekDTO(eredeti.getAlapKonkretSzakirany());
		this.konkretSzakiranyok = eredeti.getKonkretSzakiranyok().stream()
				.map(KonkretSzakiranyWithTantervekDTO::new)
				.collect(Collectors.toList());
	}
}
