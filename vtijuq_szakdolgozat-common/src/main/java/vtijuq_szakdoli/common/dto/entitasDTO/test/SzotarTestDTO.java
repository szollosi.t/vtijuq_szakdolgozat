package vtijuq_szakdoli.common.dto.entitasDTO.test;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.SzotarDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.test.SzotarTest;
import vtijuq_szakdoli.common.util.graph.Hierarchical;

import java.util.Date;

public class SzotarTestDTO extends SzotarDTO implements EntitasDTO<SzotarTest>, SzotarTest, Hierarchical<SzotarTestDTO> {

	@Getter
	protected Long idTipus;
	@Getter
	protected Date ervKezdet;
	@Getter
	protected Date ervVeg;
	@Getter
	protected Boolean technikai;

	public SzotarTestDTO() {}

	public SzotarTestDTO(Entitas other) {
		id = other.getId();
	}

	public SzotarTestDTO(SzotarTestDTO eredeti) {
		super(eredeti);
		idTipus = eredeti.getIdTipus();
		ervKezdet = eredeti.getErvKezdet();
		ervVeg = eredeti.getErvVeg();
		technikai = eredeti.getTechnikai();
	}

	@Override
	public String getKod() {
		return kod;
	}

	@Override
	public String getLeiras() {
		return leiras;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (!(o instanceof SzotarTestDTO)) return false;

		SzotarTestDTO that = (SzotarTestDTO) o;

		return new EqualsBuilder()
				.append(getId(), that.getId())
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(getId())
				.toHashCode();
	}
}
