package vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.ErtekkeszletDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.KepzesiTerulet;

public class KepzesiTeruletDTO extends ErtekkeszletDTO implements EntitasDTO<KepzesiTerulet>, KepzesiTerulet {

	public KepzesiTeruletDTO() {}

	public KepzesiTeruletDTO(Entitas other) {
		id = other.getId();
	}

	public KepzesiTeruletDTO(KepzesiTerulet eredeti) {
		super(eredeti);
	}
}
