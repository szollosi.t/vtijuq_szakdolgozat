package vtijuq_szakdoli.common.dto.entitasDTO.megvalositas;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.ErtekkeszletDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tantargy;

public class TantargyDTO extends ErtekkeszletDTO implements EntitasDTO<Tantargy>, Tantargy {

	@Getter
	protected SzervezetDTO szervezet;

	public TantargyDTO() {
	}

	public TantargyDTO(Entitas other) {
		id = other.getId();
	}

	public TantargyDTO(TantargyDTO eredeti) {
		super(eredeti);
		if (eredeti.getSzervezet() != null) {
			szervezet = new SzervezetDTO(eredeti.getSzervezet());
		}
	}
}
