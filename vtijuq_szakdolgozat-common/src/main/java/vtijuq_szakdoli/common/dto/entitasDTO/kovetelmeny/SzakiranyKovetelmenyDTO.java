package vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.AbstractEntitasDTO;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakiranyKovetelmeny;

public class SzakiranyKovetelmenyDTO extends AbstractEntitasDTO implements EntitasDTO<SzakiranyKovetelmeny>, SzakiranyKovetelmeny {

	@Getter
	protected Boolean csakkozos;
	@Getter
	protected SzakKovetelmenyDTO szakKovetelmeny;
	@Getter
	protected SzakiranyDTO szakirany;

	public SzakiranyKovetelmenyDTO() {
	}

	public SzakiranyKovetelmenyDTO(Entitas other) {
		id = other.getId();
	}

	public SzakiranyKovetelmenyDTO(SzakiranyKovetelmenyDTO eredeti) {
		super(eredeti);
		csakkozos = eredeti.getCsakkozos();
		szakKovetelmeny = new SzakKovetelmenyDTO(eredeti.getSzakKovetelmeny());
		if (eredeti.getSzakirany() != null) {
			szakirany = new SzakiranyDTO(eredeti.getSzakirany());
		}
	}

	public String toString (){
		return szakirany != null ? szakirany.toString() : "alap";
	}
}
