package vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny;

import lombok.Getter;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.SzotarDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szakirany;

public class SzakiranyDTO extends SzotarDTO implements EntitasDTO<Szakirany>, Szakirany {

	@Getter
	protected SzakDTO szak;

	public SzakiranyDTO() {
	}

	public SzakiranyDTO(Entitas other) {
		id = other.getId();
	}

	public SzakiranyDTO(SzakiranyDTO eredeti) {
		super(eredeti);
		szak = new SzakDTO(eredeti.getSzak());
	}
}
