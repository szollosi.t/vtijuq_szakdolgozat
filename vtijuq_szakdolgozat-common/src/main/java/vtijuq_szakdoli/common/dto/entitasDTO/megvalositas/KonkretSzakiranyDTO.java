package vtijuq_szakdoli.common.dto.entitasDTO.megvalositas;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.ErtekkeszletDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany;

public class KonkretSzakiranyDTO extends ErtekkeszletDTO implements EntitasDTO<KonkretSzakirany>, KonkretSzakirany {

	@Getter
	protected KonkretSzakDTO konkretSzak;
	@Getter
	protected SzakiranyKovetelmenyDTO szakiranyKovetelmeny;

	public KonkretSzakiranyDTO() {
	}

	public KonkretSzakiranyDTO(Entitas other) {
		id = other.getId();
	}

	public KonkretSzakiranyDTO(KonkretSzakiranyDTO eredeti) {
		super(eredeti);
		if (eredeti.getKonkretSzak() != null) {
			konkretSzak = new KonkretSzakDTO(eredeti.getKonkretSzak());
		}
		if (eredeti.getSzakiranyKovetelmeny() != null) {
			szakiranyKovetelmeny = new SzakiranyKovetelmenyDTO(eredeti.getSzakiranyKovetelmeny());
		}
	}
}
