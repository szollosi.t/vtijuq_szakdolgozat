package vtijuq_szakdoli.common.dto.entitasDTO.kovetelmeny;

import java.util.List;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.entitasDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakKovetelmeny;

public class SzakKovetelmenyWithSzakiranyokDTO extends SzakKovetelmenyDTO implements EntitasDTO<SzakKovetelmeny>, SzakKovetelmeny {

	@Getter
	private SzakiranyKovetelmenyWithSzakmaiJellemzokDTO alapSzakiranyKovetelmeny;
	@Getter
	private List<SzakiranyKovetelmenyWithSzakmaiJellemzokDTO> szakiranyKovetelmenyek;

	public SzakKovetelmenyWithSzakiranyokDTO() {
	}

	public SzakKovetelmenyWithSzakiranyokDTO(KonkretSzakDTO eredeti,
			SzakiranyKovetelmenyWithSzakmaiJellemzokDTO alapSzakiranyKovetelmeny, List<SzakiranyKovetelmenyWithSzakmaiJellemzokDTO> szakiranyKovetelmenyek) {
		super(eredeti);
		this.alapSzakiranyKovetelmeny = alapSzakiranyKovetelmeny;
		this.szakiranyKovetelmenyek = szakiranyKovetelmenyek;
	}
}
