package vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.ErtekkeszletDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Funkcio;

public class FunkcioDTO extends ErtekkeszletDTO implements EntitasDTO<Funkcio>, Funkcio {

	public FunkcioDTO() {}

	public FunkcioDTO(Entitas other) {
		id = other.getId();
	}

	public FunkcioDTO(Funkcio eredeti) {
		super(eredeti);
	}
}
