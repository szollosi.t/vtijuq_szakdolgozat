package vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio;

import lombok.Getter;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet;

public class SzervezetHierarchiaDTO extends SzervezetDTO implements EntitasDTO<Szervezet>, Szervezet {

	@Getter
	private Szervezet szervezet;
	@Getter
	private String rovPath;
	@Getter
	private Boolean intezmenyi;

	public SzervezetHierarchiaDTO() {
	}

	public SzervezetHierarchiaDTO(SzervezetDTO eredeti, String rovPath, Boolean intezmenyi) {
		super(eredeti);
		szervezet = eredeti;
		this.rovPath = rovPath;
		this.intezmenyi = intezmenyi;
	}

	public String getKod() {
		return getSzervezet().getKod();
	}

	public String getCim() {
		return getSzervezet().getCim();
	}
}
