package vtijuq_szakdoli.common.dto.entitasDTO.megvalositas;

import lombok.Getter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import vtijuq_szakdoli.common.dto.AbstractErvenyesEntitasDTO;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tematika;

public class TematikaDTO extends AbstractErvenyesEntitasDTO implements EntitasDTO<Tematika>, Tematika {

	@Getter
	protected TantargyDTO tantargy;
	@Getter
	protected TemaDTO tema;
	@Getter
	protected IsmeretSzintDTO leadottSzint;
	@Getter
	protected IsmeretSzintDTO szamonkertSzint;

	public TematikaDTO() {
	}

	public TematikaDTO(Entitas other) {
		id = other.getId();
	}

	public TematikaDTO(TematikaDTO eredeti) {
		super(eredeti);
		ervenyessegKezdet = eredeti.getErvenyessegKezdet();
		ervenyessegVeg = eredeti.getErvenyessegVeg();
		valid = eredeti.isValid();
		vegleges = eredeti.isVegleges();
		if (eredeti.getTantargy() != null) {
			tantargy = new TantargyDTO(eredeti.getTantargy());
		}
		if (eredeti.getTema() != null) {
			tema = new TemaDTO(eredeti.getTema());
		}
		if (eredeti.getLeadottSzint() != null) {
			leadottSzint = new IsmeretSzintDTO(eredeti.getLeadottSzint());
		}
		if (eredeti.getSzamonkertSzint() != null) {
			szamonkertSzint = new IsmeretSzintDTO(eredeti.getSzamonkertSzint());
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (!(o instanceof TematikaDTO)) return false;

		TematikaDTO that = (TematikaDTO) o;

		return getId() != null && getId().equals(that.getId())
				|| new EqualsBuilder()
				.append(getTantargy(), that.getTantargy())
				.append(getTema(), that.getTema())
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(getTantargy())
				.append(getTema())
				.toHashCode();
	}

	@Override
	public String toString() {
		return String.format("%s (%s - %s)", getTema(), getErvenyessegKezdet(), getErvenyessegVeg());
	}
}
