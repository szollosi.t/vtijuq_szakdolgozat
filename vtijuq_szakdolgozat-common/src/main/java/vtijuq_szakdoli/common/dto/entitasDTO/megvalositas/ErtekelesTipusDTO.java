package vtijuq_szakdoli.common.dto.entitasDTO.megvalositas;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.SzotarDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.ErtekelesTipus;

public class ErtekelesTipusDTO extends SzotarDTO implements EntitasDTO<ErtekelesTipus>, ErtekelesTipus {

	public ErtekelesTipusDTO() {}

	public ErtekelesTipusDTO(Entitas other) {
		id = other.getId();
	}

	public ErtekelesTipusDTO(ErtekelesTipus eredeti) {
		super(eredeti);
	}
}
