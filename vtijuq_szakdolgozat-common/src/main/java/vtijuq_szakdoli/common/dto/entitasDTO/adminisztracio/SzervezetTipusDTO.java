package vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.ErtekkeszletDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.SzervezetTipus;

public class SzervezetTipusDTO extends ErtekkeszletDTO implements EntitasDTO<SzervezetTipus>, SzervezetTipus {

	public SzervezetTipusDTO() {}

	public SzervezetTipusDTO(Entitas other) {
		id = other.getId();
	}

	public SzervezetTipusDTO(SzervezetTipus eredeti) {
		super(eredeti);
	}
}
