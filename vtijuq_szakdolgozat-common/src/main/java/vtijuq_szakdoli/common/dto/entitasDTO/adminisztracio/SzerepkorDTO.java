package vtijuq_szakdoli.common.dto.entitasDTO.adminisztracio;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.ErtekkeszletDTO;
import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szerepkor;

public class SzerepkorDTO extends ErtekkeszletDTO implements EntitasDTO<Szerepkor>, Szerepkor {

	protected Set<JogosultsagDTO> jogosultsagok;
	@Getter
	protected SzervezetTipusDTO szervezetTipus;

	public SzerepkorDTO() {}

	public SzerepkorDTO(Entitas other) {
		id = other.getId();
	}

	public SzerepkorDTO(Szerepkor eredeti, SzervezetTipusDTO szervezetTipus) {
		this(eredeti);
		this.szervezetTipus = szervezetTipus;
	}

	public SzerepkorDTO(Szerepkor eredeti) {
		super(eredeti);
		this.jogosultsagok = eredeti.getJogosultsagok().stream().map(JogosultsagDTO::new).collect(Collectors.toSet());
		if (eredeti.getSzervezetTipus() != null) {
			this.szervezetTipus = new SzervezetTipusDTO(eredeti.getSzervezetTipus());
		}
	}

	public Set<JogosultsagDTO> getJogosultsagok() {
		if(jogosultsagok == null){
			jogosultsagok = new HashSet<>();
		}
		return new HashSet<>(jogosultsagok);
	}
}
