package vtijuq_szakdoli.common.dto;

import vtijuq_szakdoli.common.interfaces.Entitas;

public abstract class EntitasDTOAbstractImpl implements Entitas, DTO {
	protected Long id;

	public EntitasDTOAbstractImpl() {}

	public Long getId(){
		return -1L;
	}
}
