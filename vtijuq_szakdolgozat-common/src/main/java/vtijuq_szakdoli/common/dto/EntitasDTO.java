package vtijuq_szakdoli.common.dto;

import vtijuq_szakdoli.common.interfaces.Entitas;

public interface EntitasDTO<T extends Entitas> extends Entitas, DTO {}
