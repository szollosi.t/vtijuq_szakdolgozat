package vtijuq_szakdoli.common.dto.subordinationDTO;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import vtijuq_szakdoli.common.dto.DTO;
import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.interfaces.subordination.Subordination;
import vtijuq_szakdoli.common.util.graph.Hierarchical;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class SubordinationDTO<T extends EntitasDTO & Hierarchical<T>> implements Subordination<T>, DTO {

    private T principalDTO;
    private T subordinateDTO;

    @Override
    public Long getPrincipalId() {
        return principalDTO != null ? principalDTO.getId() : null;
    }

    @Override
    public Long getSubordinateId() {
        return subordinateDTO != null ? subordinateDTO.getId() : null;
    }

    public Set<String> getErrors() {
        final HashSet<String> errors = new HashSet<>();
        if (Objects.equals(getPrincipalId(), getSubordinateId())) {
            errors.add("error.validation.subordination.same.entities");
        }
        if (principalDTO == null) {
            errors.add("error.required.subordination.principal");
        }
        if (subordinateDTO == null) {
            errors.add("error.required.subordination.subordinate");
        }
        return errors;
    }

}
