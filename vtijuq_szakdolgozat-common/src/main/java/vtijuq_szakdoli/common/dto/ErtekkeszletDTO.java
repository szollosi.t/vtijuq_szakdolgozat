package vtijuq_szakdoli.common.dto;

import java.util.Objects;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;

public abstract class ErtekkeszletDTO extends AbstractEntitasDTO implements Ertekkeszlet {
	protected String kod;
	@Getter
	protected String nev;

	public ErtekkeszletDTO(){}

	public ErtekkeszletDTO(Entitas other) {
		id = other.getId();
	}

	public ErtekkeszletDTO(Ertekkeszlet eredeti) {
		super(eredeti);
		kod = eredeti.getKod();
		nev = eredeti.getNev();
	}

	public ErtekkeszletDTO(String nev) {
		this.nev = nev;
		kod = nev.toUpperCase();
	}

	@Override
	public String getKod() {
		return StringUtils.defaultIfBlank(kod, Ertekkeszlet.super.getKod());
	}

	@Override
	public String toString() {
		return (nev == null || nev.trim().isEmpty()) ? kod : nev;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 23 * hash + Objects.hashCode(getKod());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Ertekkeszlet
				&& Objects.equals(getKod(), ((Ertekkeszlet) obj).getKod());
	}
}
