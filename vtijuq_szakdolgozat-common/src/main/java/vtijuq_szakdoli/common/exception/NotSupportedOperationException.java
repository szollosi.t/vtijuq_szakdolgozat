package vtijuq_szakdoli.common.exception;

public class NotSupportedOperationException extends RuntimeException {

	public NotSupportedOperationException() {
		super("Not Supported Operation");
	}
}
