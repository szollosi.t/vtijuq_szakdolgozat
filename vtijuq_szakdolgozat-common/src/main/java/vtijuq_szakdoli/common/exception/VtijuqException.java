package vtijuq_szakdoli.common.exception;

public class VtijuqException extends RuntimeException {

	public VtijuqException(String message, Throwable cause) {
		super(message, cause);
	}
}
