package vtijuq_szakdoli.common.exception;

import java.util.ArrayList;
import java.util.List;

public class ConsistencyException extends ValidationException {

	public ConsistencyException(List<String> hibaUzenetek) {
		super(hibaUzenetek);
		getHibaUzenetek().add(0, "Konszisztencia hiba");
	}

	public ConsistencyException(String message) {
		this(new ArrayList<String>(){{add(message);}});
	}
}
