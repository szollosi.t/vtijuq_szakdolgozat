package vtijuq_szakdoli.common.exception;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ValidationException extends RuntimeException {

	private List<String> hibaUzenetek = new ArrayList<>();

	public ValidationException(Collection<String> hibaUzenetek) {
		this.hibaUzenetek.addAll(hibaUzenetek);
	}

	protected List<String> getHibaUzenetek() {
		if(hibaUzenetek == null){
			return new ArrayList<>();
		}
		return hibaUzenetek;
	}

	@Override
	public String getMessage() {
		return hibaUzenetek.stream().collect(Collectors.joining("\n"));
	}
}
