package vtijuq_szakdoli.common.interfaces;

import vtijuq_szakdoli.common.dto.EntitasDTO;

@SuppressWarnings("serial")
public interface MappedByDTO<T extends EntitasDTO> extends Entitas {}
