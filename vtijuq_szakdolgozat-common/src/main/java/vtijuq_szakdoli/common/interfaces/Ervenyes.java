package vtijuq_szakdoli.common.interfaces;

import java.util.Date;

public interface Ervenyes {
	Date getErvenyessegKezdet();

	Date getErvenyessegVeg();

	boolean isValid();

	boolean isVegleges();
}
