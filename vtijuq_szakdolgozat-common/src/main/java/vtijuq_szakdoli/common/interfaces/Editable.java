package vtijuq_szakdoli.common.interfaces;

import java.util.Set;

public interface Editable extends Entitas {

	Set<String> getErrors();

}
