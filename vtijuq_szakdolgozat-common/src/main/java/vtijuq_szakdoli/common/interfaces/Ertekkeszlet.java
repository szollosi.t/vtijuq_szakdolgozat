package vtijuq_szakdoli.common.interfaces;

import org.apache.commons.lang3.StringUtils;

public interface Ertekkeszlet extends Entitas, Comparable<Ertekkeszlet> {

	default String getKod(){
		return StringUtils.upperCase(getNev());
	}

	String getNev();

	default int compareTo(Ertekkeszlet t) {
		return getKod().compareTo(t.getKod());
	}
}
