package vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Szotar;

public interface Ismeret extends Entitas, Szotar {

	default Class<Ismeret> getEntitasClass() {
		return Ismeret.class;
	}

	@Override
	default String getType() {
		return getEntitasClass().getSimpleName().toUpperCase();
	}

}
