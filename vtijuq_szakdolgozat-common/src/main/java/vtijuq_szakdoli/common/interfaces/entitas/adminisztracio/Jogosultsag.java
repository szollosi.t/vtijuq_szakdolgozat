package vtijuq_szakdoli.common.interfaces.entitas.adminisztracio;

import vtijuq_szakdoli.common.interfaces.Entitas;

public interface Jogosultsag extends Entitas {

	default Class<Jogosultsag> getEntitasClass() {
		return Jogosultsag.class;
	}

	JogosultsagSzint getSzint();

	Funkcio getFunkcio();

	enum JogosultsagSzint {
		LATHATO,
		IRHATO
	}
}
