package vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;
import vtijuq_szakdoli.common.interfaces.Ervenyes;

public interface SzakKovetelmeny extends Entitas, Ertekkeszlet, Ervenyes {

	default Class<SzakKovetelmeny> getEntitasClass() {
		return SzakKovetelmeny.class;
	}

	default String getNev() {
		return getOklevelMegjeloles();
	}

	Szak getSzak();

	String getOklevelMegjeloles();

	Integer getFelevek();

	Integer getOsszKredit();

	Integer getSzakdolgozatKredit();

	Integer getSzakmaigyakMinKredit();

	Integer getSzabvalMinKredit();

	Integer getSzakiranyMinKredit();

	String getSzakmaigyakEloirasok();

	Integer getSzakiranyMaxKredit();

//	List<? extends SzakiranyKovetelmeny> getSzakiranyKovetelmenyek();
}
