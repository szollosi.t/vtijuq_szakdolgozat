package vtijuq_szakdoli.common.interfaces.entitas.adminisztracio;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;

public interface Funkcio extends Entitas, Ertekkeszlet {

	default Class<Funkcio> getEntitasClass() {
		return Funkcio.class;
	}
}
