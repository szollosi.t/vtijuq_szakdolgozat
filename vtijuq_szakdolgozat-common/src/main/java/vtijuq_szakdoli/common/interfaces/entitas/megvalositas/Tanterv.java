package vtijuq_szakdoli.common.interfaces.entitas.megvalositas;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ervenyes;

public interface Tanterv extends Entitas, Ervenyes {

	default Class<Tanterv> getEntitasClass() {
		return Tanterv.class;
	}

	TantervStatusz getStatusz();

	Integer getFelev();

	Integer getKredit();

	Integer getEaOra();

	Integer getGyakOra();

	Integer getLabOra();

	KonkretSzakirany getKonkretSzakirany();

	Tantargy getTantargy();

	ErtekelesTipus getErtekelesTipus();

}
