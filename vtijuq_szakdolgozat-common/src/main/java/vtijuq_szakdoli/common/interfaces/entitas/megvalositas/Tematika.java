package vtijuq_szakdoli.common.interfaces.entitas.megvalositas;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ervenyes;

public interface Tematika extends Entitas, Ervenyes {

	default Class<Tematika> getEntitasClass() {
		return Tematika.class;
	}

//	Tantargy getTantargy();

	Tema getTema();

	IsmeretSzint getLeadottSzint();

	IsmeretSzint getSzamonkertSzint();
}
