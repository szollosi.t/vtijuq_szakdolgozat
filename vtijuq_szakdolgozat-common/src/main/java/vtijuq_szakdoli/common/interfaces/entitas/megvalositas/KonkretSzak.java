package vtijuq_szakdoli.common.interfaces.entitas.megvalositas;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakKovetelmeny;

public interface KonkretSzak extends Entitas, Ertekkeszlet {

	default Class<KonkretSzak> getEntitasClass() {
		return KonkretSzak.class;
	}

	default String getNev() {
		return getSzakKovetelmeny().getNev();
	}

	Integer getSzakmaigyakKredit();

	Integer getSzabvalKredit();

	Integer getSzakiranyKredit();

	SzakKovetelmeny getSzakKovetelmeny();

	Szervezet getSzervezet();
}
