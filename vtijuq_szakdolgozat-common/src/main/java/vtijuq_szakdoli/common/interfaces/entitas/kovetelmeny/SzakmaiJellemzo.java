package vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Szotar;

public interface SzakmaiJellemzo extends Entitas, Szotar {

	default Class<SzakmaiJellemzo> getEntitasClass() {
		return SzakmaiJellemzo.class;
	}

	@Override
	default String getType() {
		return getEntitasClass().getSimpleName().toUpperCase();
	}

	Ismeret getIsmeret();

	default String getKod() {
		return getIsmeret().getKod();
	}

	default String getLeiras() {
		return getIsmeret().getLeiras();
	}

	Integer getMinKredit();

	Integer getMaxKredit();

	Integer getMinValaszt();
}
