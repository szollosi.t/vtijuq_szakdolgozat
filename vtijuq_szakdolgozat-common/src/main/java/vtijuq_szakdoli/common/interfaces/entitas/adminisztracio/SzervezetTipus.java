package vtijuq_szakdoli.common.interfaces.entitas.adminisztracio;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;

public interface SzervezetTipus extends Entitas, Ertekkeszlet {

	default Class<SzervezetTipus> getEntitasClass() {
		return SzervezetTipus.class;
	}

}
