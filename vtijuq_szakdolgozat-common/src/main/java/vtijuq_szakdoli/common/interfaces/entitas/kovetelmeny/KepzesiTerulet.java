package vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;

public interface KepzesiTerulet extends Entitas, Ertekkeszlet {

	default Class<KepzesiTerulet> getEntitasClass() {
		return KepzesiTerulet.class;
	}

}
