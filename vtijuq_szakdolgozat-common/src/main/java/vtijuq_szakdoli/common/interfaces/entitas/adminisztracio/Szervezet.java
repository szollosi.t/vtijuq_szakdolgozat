package vtijuq_szakdoli.common.interfaces.entitas.adminisztracio;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Szotar;

public interface Szervezet extends Entitas, Szotar {

	default Class<Szervezet> getEntitasClass() {
		return Szervezet.class;
	}

	@Override
	default String getType() {
		return getEntitasClass().getSimpleName().toUpperCase();
	}

	default String getKod(){
		return getRovidites();
	}

	default String getLeiras() {
		return String.format("%s: %s", getNev(), getCim());
	}

	String getRovidites();

	String getCim();

	SzervezetTipus getSzervezetTipus();
}
