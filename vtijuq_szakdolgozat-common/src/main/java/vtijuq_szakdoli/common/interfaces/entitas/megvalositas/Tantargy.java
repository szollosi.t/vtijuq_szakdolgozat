package vtijuq_szakdoli.common.interfaces.entitas.megvalositas;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet;

public interface Tantargy extends Entitas, Ertekkeszlet {

	default Class<Tantargy> getEntitasClass() {
		return Tantargy.class;
	}

	Szervezet getSzervezet();
}
