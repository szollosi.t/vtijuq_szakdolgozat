package vtijuq_szakdoli.common.interfaces.entitas.megvalositas;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakiranyKovetelmeny;

public interface KonkretSzakirany extends Entitas, Ertekkeszlet {

	default Class<KonkretSzakirany> getEntitasClass() {
		return KonkretSzakirany.class;
	}

	KonkretSzak getKonkretSzak();

	SzakiranyKovetelmeny getSzakiranyKovetelmeny();
}
