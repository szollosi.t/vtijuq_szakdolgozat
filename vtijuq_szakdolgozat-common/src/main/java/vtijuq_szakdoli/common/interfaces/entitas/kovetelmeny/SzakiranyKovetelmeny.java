package vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;

public interface SzakiranyKovetelmeny extends Entitas, Ertekkeszlet {

	default Class<SzakiranyKovetelmeny> getEntitasClass() {
		return SzakiranyKovetelmeny.class;
	}

	default String getNev() {
		return getSzakirany() != null ? getSzakirany().getNev() : getSzakKovetelmeny().getNev();
	}

	Boolean getCsakkozos();

	SzakKovetelmeny getSzakKovetelmeny();

	Szakirany getSzakirany();
}
