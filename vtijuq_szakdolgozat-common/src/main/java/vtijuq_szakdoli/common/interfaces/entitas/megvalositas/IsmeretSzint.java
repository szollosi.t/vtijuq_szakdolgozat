package vtijuq_szakdoli.common.interfaces.entitas.megvalositas;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Szotar;

public interface IsmeretSzint extends Entitas, Szotar {

	default Class<IsmeretSzint> getEntitasClass() {
		return IsmeretSzint.class;
	}

	@Override
	default String getType() {
		return getEntitasClass().getSimpleName().toUpperCase();
	}

}
