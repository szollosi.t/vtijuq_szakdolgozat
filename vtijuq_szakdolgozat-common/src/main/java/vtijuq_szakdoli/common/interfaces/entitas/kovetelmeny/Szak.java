package vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;

public interface Szak extends Entitas, Ertekkeszlet {

	default Class<Szak> getEntitasClass() {
		return Szak.class;
	}

	KepzesiTerulet getKepzesiTerulet();

	Ciklus getCiklus();

//	List<? extends Szakirany> getSzakiranyok();

}
