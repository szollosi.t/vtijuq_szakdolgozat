package vtijuq_szakdoli.common.interfaces.entitas.megvalositas;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Szotar;

public interface ErtekelesTipus extends Entitas, Szotar {

	default Class<ErtekelesTipus> getEntitasClass() {
		return ErtekelesTipus.class;
	}

	@Override
	default String getType() {
		return getEntitasClass().getSimpleName().toUpperCase();
	}

}
