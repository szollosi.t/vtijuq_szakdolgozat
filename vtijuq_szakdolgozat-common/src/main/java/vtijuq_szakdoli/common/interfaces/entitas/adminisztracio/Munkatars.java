package vtijuq_szakdoli.common.interfaces.entitas.adminisztracio;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;

public interface Munkatars extends Entitas, Ertekkeszlet {

	default Class<Munkatars> getEntitasClass() {
		return Munkatars.class;
	}

	String getFelhasznalonev();

	default String getKod() {
		return getFelhasznalonev();
	}

	boolean isAktiv();

	boolean isJelszovaltoztatas();

	Szervezet getSzervezet();

	Szerepkor getSzerepkor();
}
