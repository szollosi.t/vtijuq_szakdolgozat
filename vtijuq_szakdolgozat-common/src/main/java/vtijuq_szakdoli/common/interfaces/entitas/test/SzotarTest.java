package vtijuq_szakdoli.common.interfaces.entitas.test;

import java.util.Date;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Szotar;

public interface SzotarTest extends Entitas, Szotar {

	default Class<SzotarTest> getEntitasClass() {
		return SzotarTest.class;
	}

	@Override
	default String getType() {
		return getEntitasClass().getSimpleName().toUpperCase();
	}

	Long getIdTipus();

	Date getErvKezdet();

	Date getErvVeg();

	Boolean getTechnikai();
}
