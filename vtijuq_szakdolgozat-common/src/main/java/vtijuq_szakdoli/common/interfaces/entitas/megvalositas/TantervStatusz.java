package vtijuq_szakdoli.common.interfaces.entitas.megvalositas;

public enum TantervStatusz {
    KOTELEZO,
    KOTVAL,
    SZABVAL
}
