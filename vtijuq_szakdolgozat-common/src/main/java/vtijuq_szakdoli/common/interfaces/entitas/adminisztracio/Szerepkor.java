package vtijuq_szakdoli.common.interfaces.entitas.adminisztracio;

import java.util.Set;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ertekkeszlet;

public interface Szerepkor extends Entitas, Ertekkeszlet {

	default Class<Szerepkor> getEntitasClass() {
		return Szerepkor.class;
	}

	Set<? extends Jogosultsag> getJogosultsagok();

	SzervezetTipus getSzervezetTipus();
}
