package vtijuq_szakdoli.common.interfaces.entitas.akkreditacio;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Ervenyes;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany;

public interface Akkreditacio extends Entitas, Ervenyes {

	default Class<Akkreditacio> getEntitasClass() {
		return Akkreditacio.class;
	}

	KonkretSzakirany getKonkretSzakirany();
}
