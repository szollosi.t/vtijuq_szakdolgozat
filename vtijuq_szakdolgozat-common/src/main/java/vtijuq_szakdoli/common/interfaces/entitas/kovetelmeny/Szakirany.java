package vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Szotar;

public interface Szakirany extends Entitas, Szotar {

	default Class<Szakirany> getEntitasClass() {
		return Szakirany.class;
	}

	@Override
	default String getType() {
		return getEntitasClass().getSimpleName().toUpperCase();
	}

	Szak getSzak();
}
