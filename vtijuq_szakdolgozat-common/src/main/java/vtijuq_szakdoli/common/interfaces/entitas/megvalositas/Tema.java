package vtijuq_szakdoli.common.interfaces.entitas.megvalositas;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.interfaces.Szotar;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ismeret;

public interface Tema extends Entitas, Szotar {

	default Class<Tema> getEntitasClass() {
		return Tema.class;
	}

	@Override
	default String getType() {
		return getEntitasClass().getSimpleName().toUpperCase();
	}

	Ismeret getIsmeret();
}
