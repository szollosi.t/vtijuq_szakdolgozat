package vtijuq_szakdoli.common.interfaces;

import java.io.Serializable;

@SuppressWarnings("serial")
public interface Entitas extends Serializable {

	Class<? extends Entitas> getEntitasClass();

	Long getId();

	boolean equals(Object o);

	int hashCode();
}
