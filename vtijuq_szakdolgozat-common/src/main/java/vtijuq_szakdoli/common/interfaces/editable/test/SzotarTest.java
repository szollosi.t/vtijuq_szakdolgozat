package vtijuq_szakdoli.common.interfaces.editable.test;

import java.util.Date;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Ertekkeszlet;

public interface SzotarTest extends Ertekkeszlet, vtijuq_szakdoli.common.interfaces.entitas.test.SzotarTest, Editable {

	void setIdTipus(Long idTipus);

	void setKod(String kod);

	void setLeiras(String leiras);

	void setErvKezdet(Date ervKezdet);

	void setErvVeg(Date ervVeg);

	void setTechnikai(Boolean technikai);
}
