package vtijuq_szakdoli.common.interfaces.editable.megvalositas;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Ertekkeszlet;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.SzakiranyKovetelmeny;

public interface KonkretSzakirany extends Ertekkeszlet, vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany, Editable {

	void setKod(String kod);

	void setKonkretSzak(KonkretSzak konkretSzak);

	void setSzakiranyKovetelmeny(SzakiranyKovetelmeny szakiranyKovetelmeny);
}
