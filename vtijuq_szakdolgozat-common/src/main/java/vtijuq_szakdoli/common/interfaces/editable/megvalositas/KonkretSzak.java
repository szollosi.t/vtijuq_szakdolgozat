package vtijuq_szakdoli.common.interfaces.editable.megvalositas;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.adminisztracio.Szervezet;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.SzakKovetelmeny;

public interface KonkretSzak extends vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzak, Editable {

	void setSzakmaigyakKredit(Integer szakmaigyakKredit);

	void setSzabvalKredit(Integer szabvalKredit);

	void setSzakiranyKredit(Integer szakiranyKredit);

	void setSzakKovetelmeny(SzakKovetelmeny szakKovetelmeny);

	void setSzervezet(Szervezet szervezet);
}
