package vtijuq_szakdoli.common.interfaces.editable.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Szotar;

public interface SzakmaiJellemzo extends Szotar, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakmaiJellemzo, Editable {

	void setIsmeret(Ismeret ismeret);

	void setMinKredit(Integer minKredit);

	void setMaxKredit(Integer maxKredit);

	void setMinValaszt(Integer minValaszt);
}
