package vtijuq_szakdoli.common.interfaces.editable.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Ertekkeszlet;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ciklus;
import vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.KepzesiTerulet;

public interface Szak extends Ertekkeszlet, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szak, Editable {

	void setKepzesiTerulet(KepzesiTerulet kepzesiTerulet);

	void setCiklus(Ciklus ciklus);
}
