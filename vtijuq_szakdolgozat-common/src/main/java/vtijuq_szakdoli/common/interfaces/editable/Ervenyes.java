package vtijuq_szakdoli.common.interfaces.editable;

import java.util.Date;

public interface Ervenyes extends vtijuq_szakdoli.common.interfaces.Ervenyes {

	void setErvenyessegKezdet(Date ervenyessegKezdet);

	void setErvenyessegVeg(Date ervenyessegVeg);

	void setValid(boolean valid);

	void setVegleges(boolean vegleges);
}
