package vtijuq_szakdoli.common.interfaces.editable.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Ervenyes;

public interface SzakKovetelmeny extends vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakKovetelmeny, Editable, Ervenyes {

	void setSzak(Szak szak);

	void setOklevelMegjeloles(String oklevelMegjeloles);

	void setFelevek(Integer felevek);

	void setOsszKredit(Integer osszKredit);

	void setSzakdolgozatKredit(Integer szakdolgozatKredit);

	void setSzakmaigyakMinKredit(Integer szakmaigyakMinKredit);

	void setSzabvalMinKredit(Integer szabvalMinKredit);

	void setSzakiranyMinKredit(Integer szakiranyMinKredit);

	void setSzakmaigyakEloirasok(String szakmaigyakEloirasok);

	void setSzakiranyMaxKredit(Integer szakiranyMaxKredit);
}
