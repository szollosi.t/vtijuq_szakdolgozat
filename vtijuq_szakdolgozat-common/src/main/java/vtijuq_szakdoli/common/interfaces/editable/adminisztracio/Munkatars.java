package vtijuq_szakdoli.common.interfaces.editable.adminisztracio;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Ertekkeszlet;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szerepkor;

public interface Munkatars extends Ertekkeszlet, vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Munkatars, Editable {

	void setFelhasznalonev(String felhasznalonev);

	void setAktiv(boolean aktiv);

	void setJelszovaltoztatas(boolean jelszovaltoztatas);

	void setSzervezet(Szervezet szervezet);

	void setSzerepkor(Szerepkor szerepkor);
}
