package vtijuq_szakdoli.common.interfaces.editable.megvalositas;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Ertekkeszlet;
import vtijuq_szakdoli.common.interfaces.editable.adminisztracio.Szervezet;

public interface Tantargy extends Ertekkeszlet, vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tantargy, Editable {

	void setKod(String kod);

	void setSzervezet(Szervezet szervezet);
}
