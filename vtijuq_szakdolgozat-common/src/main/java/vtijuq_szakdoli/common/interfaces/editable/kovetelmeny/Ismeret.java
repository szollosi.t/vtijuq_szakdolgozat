package vtijuq_szakdoli.common.interfaces.editable.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Szotar;

public interface Ismeret extends Szotar, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Ismeret, Editable {

	void setLeiras(String leiras);

	void setKod(String kod);
}
