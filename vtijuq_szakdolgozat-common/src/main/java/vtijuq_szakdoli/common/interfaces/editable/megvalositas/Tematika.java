package vtijuq_szakdoli.common.interfaces.editable.megvalositas;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Ervenyes;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.IsmeretSzint;

public interface Tematika extends vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tematika, Editable, Ervenyes {

	void setLeadottSzint(IsmeretSzint leadottSzint);

	void setSzamonkertSzint(IsmeretSzint szamonkertSzint);
}
