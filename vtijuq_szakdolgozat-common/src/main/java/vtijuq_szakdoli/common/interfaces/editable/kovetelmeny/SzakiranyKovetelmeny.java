package vtijuq_szakdoli.common.interfaces.editable.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Editable;

public interface SzakiranyKovetelmeny extends vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.SzakiranyKovetelmeny, Editable {

	void setCsakkozos(Boolean csakkozos);

	void setSzakKovetelmeny(SzakKovetelmeny szakKovetelmeny);

	void setSzakirany(Szakirany szakirany);
}
