package vtijuq_szakdoli.common.interfaces.editable.akkreditacio;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Ervenyes;
import vtijuq_szakdoli.common.interfaces.editable.megvalositas.KonkretSzakirany;

public interface Akkreditacio extends vtijuq_szakdoli.common.interfaces.entitas.akkreditacio.Akkreditacio, Editable, Ervenyes {

	void setKonkretSzakirany(KonkretSzakirany konkretSzakirany);
}
