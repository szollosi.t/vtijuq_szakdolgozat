package vtijuq_szakdoli.common.interfaces.editable.kovetelmeny;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Szotar;

public interface Szakirany extends Szotar, vtijuq_szakdoli.common.interfaces.entitas.kovetelmeny.Szakirany, Editable {

	@Override
	default String getType() {
		return getEntitasClass().getSimpleName().toUpperCase();
	}

	void setLeiras(String leiras);

	void setSzak(Szak szak);
}
