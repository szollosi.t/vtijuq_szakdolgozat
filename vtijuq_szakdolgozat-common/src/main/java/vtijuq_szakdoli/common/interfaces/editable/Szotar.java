package vtijuq_szakdoli.common.interfaces.editable;

import java.io.Serializable;

/**
 * @author Szőllősi Tibor Béla, Vígh László
 */
public interface Szotar extends Serializable, Ertekkeszlet {}
