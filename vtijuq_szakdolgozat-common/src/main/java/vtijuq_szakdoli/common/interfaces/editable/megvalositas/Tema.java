package vtijuq_szakdoli.common.interfaces.editable.megvalositas;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Szotar;
import vtijuq_szakdoli.common.interfaces.editable.kovetelmeny.Ismeret;

public interface Tema extends Szotar, vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tema, Editable {

	void setLeiras(String leiras);

	void setIsmeret(Ismeret ismeret);
}
