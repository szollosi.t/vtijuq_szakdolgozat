package vtijuq_szakdoli.common.interfaces.editable.megvalositas;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Ervenyes;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.ErtekelesTipus;
import vtijuq_szakdoli.common.interfaces.entitas.megvalositas.TantervStatusz;

public interface Tanterv extends vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tanterv, Editable, Ervenyes {

	void setStatusz(TantervStatusz statusz);

	void setFelev(Integer felev);

	void setKredit(Integer kredit);

	void setEaOra(Integer eaOra);

	void setGyakOra(Integer gyakOra);

	void setLabOra(Integer labOra);

	void setKonkretSzakirany(vtijuq_szakdoli.common.interfaces.entitas.megvalositas.KonkretSzakirany konkretSzakirany);

	void setTantargy(vtijuq_szakdoli.common.interfaces.entitas.megvalositas.Tantargy tantargy);

	void setErtekelesTipus(ErtekelesTipus ertekelesTipus);
}
