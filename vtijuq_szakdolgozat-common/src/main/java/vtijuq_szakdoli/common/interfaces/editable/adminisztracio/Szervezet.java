package vtijuq_szakdoli.common.interfaces.editable.adminisztracio;

import vtijuq_szakdoli.common.interfaces.Editable;
import vtijuq_szakdoli.common.interfaces.editable.Ertekkeszlet;
import vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.SzervezetTipus;

public interface Szervezet extends Ertekkeszlet, vtijuq_szakdoli.common.interfaces.entitas.adminisztracio.Szervezet, Editable {

	void setRovidites(String rovidites);

	void setCim(String cim);

	void setSzervezetTipus(SzervezetTipus szervezetTipus);
}
