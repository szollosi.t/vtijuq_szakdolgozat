package vtijuq_szakdoli.common.interfaces.subordination;

import java.io.Serializable;

import vtijuq_szakdoli.common.interfaces.Entitas;
import vtijuq_szakdoli.common.util.graph.Hierarchical;

@SuppressWarnings("serial")
public interface Subordination<T extends Entitas & Hierarchical<T>> extends Serializable {

	Long getPrincipalId();

	Long getSubordinateId();
}
