package vtijuq_szakdoli.common.interfaces;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;

@SuppressWarnings("serial")
public interface EditableByDTO<T extends EditableDTO> {}
