package vtijuq_szakdoli.common.interfaces;

import java.io.Serializable;

/**
 * @author Szőllősi Tibor Béla, Vígh László
 */
public interface Szotar extends Serializable, Ertekkeszlet {

	String getType();

	String getLeiras();

	String toString();

	int hashCode();

	boolean equals(Object obj);
}
