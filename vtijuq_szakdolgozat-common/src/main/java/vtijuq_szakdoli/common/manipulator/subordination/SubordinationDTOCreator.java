package vtijuq_szakdoli.common.manipulator.subordination;

import java.util.Set;
import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.EntitasDTO;
import vtijuq_szakdoli.common.dto.subordinationDTO.SubordinationDTO;
import vtijuq_szakdoli.common.util.graph.Hierarchical;

public class SubordinationDTOCreator<T extends EntitasDTO & Hierarchical<T>> {

	private Consumer<SubordinationDTO<T>> dtoHandler;
	private SubordinationDTO<T> dto;

	public SubordinationDTOCreator(Consumer<SubordinationDTO<T>> createdDTOHandler) {
		dtoHandler = createdDTOHandler;
		dto = new SubordinationDTO<>();
	}

	public void create() {
		dtoHandler.accept(dto);
	}

	public SubordinationDTO<T> getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}

}
