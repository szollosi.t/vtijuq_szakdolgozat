package vtijuq_szakdoli.common.manipulator;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;

public interface DTOManipulator<T extends EditableDTO> {

	Consumer<T> getDTOHandler();

	T getDTO();

}
