package vtijuq_szakdoli.common.manipulator.creator;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public interface DTOCreator<T extends EditableDTO> extends DTOManipulator<T> {

	default void create() {
		getDTOHandler().accept(getDTO());
	}

}
