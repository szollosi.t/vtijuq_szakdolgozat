package vtijuq_szakdoli.common.manipulator.creator.adminisztracio;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.adminisztracio.AbstractMunkatarsDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class MunkatarsDTOCreator extends AbstractMunkatarsDTOManipulator implements DTOCreator<MunkatarsDTO> {

	public MunkatarsDTOCreator(Consumer<MunkatarsDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new MunkatarsDTO();
	}
}
