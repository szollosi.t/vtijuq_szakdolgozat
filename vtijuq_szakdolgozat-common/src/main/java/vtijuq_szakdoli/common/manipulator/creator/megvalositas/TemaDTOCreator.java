package vtijuq_szakdoli.common.manipulator.creator.megvalositas;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractTemaDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class TemaDTOCreator extends AbstractTemaDTOManipulator implements DTOCreator<TemaDTO> {

	public TemaDTOCreator(Consumer<TemaDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new TemaDTO();
	}
}
