package vtijuq_szakdoli.common.manipulator.creator.kovetelmeny;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractSzakiranyDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class SzakiranyDTOCreator extends AbstractSzakiranyDTOManipulator implements DTOCreator<SzakiranyDTO> {

	public SzakiranyDTOCreator(Consumer<SzakiranyDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new SzakiranyDTO();
	}
}
