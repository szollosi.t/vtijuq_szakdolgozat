package vtijuq_szakdoli.common.manipulator.creator.kovetelmeny;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractSzakDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class SzakDTOCreator extends AbstractSzakDTOManipulator implements DTOCreator<SzakDTO> {

	public SzakDTOCreator(Consumer<SzakDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new SzakDTO();
	}
}
