package vtijuq_szakdoli.common.manipulator.creator.megvalositas;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractTematikaDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class TematikaDTOCreator extends AbstractTematikaDTOManipulator implements DTOCreator<TematikaDTO> {

	public TematikaDTOCreator(Consumer<TematikaDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new TematikaDTO();
	}
}
