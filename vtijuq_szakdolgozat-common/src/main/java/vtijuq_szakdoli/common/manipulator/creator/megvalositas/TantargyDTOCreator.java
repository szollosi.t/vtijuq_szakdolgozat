package vtijuq_szakdoli.common.manipulator.creator.megvalositas;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractTantargyDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class TantargyDTOCreator extends AbstractTantargyDTOManipulator implements DTOCreator<TantargyDTO> {

	public TantargyDTOCreator(Consumer<TantargyDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new TantargyDTO();
	}
}
