package vtijuq_szakdoli.common.manipulator.creator.kovetelmeny;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractIsmeretDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class IsmeretDTOCreator extends AbstractIsmeretDTOManipulator implements DTOCreator<IsmeretDTO> {

	public IsmeretDTOCreator(Consumer<IsmeretDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new IsmeretDTO();
	}
}
