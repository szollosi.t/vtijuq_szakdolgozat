package vtijuq_szakdoli.common.manipulator.creator.adminisztracio;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.adminisztracio.AbstractSzervezetDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class SzervezetDTOCreator extends AbstractSzervezetDTOManipulator implements DTOCreator<SzervezetDTO> {

	public SzervezetDTOCreator(Consumer<SzervezetDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new SzervezetDTO();
	}
}
