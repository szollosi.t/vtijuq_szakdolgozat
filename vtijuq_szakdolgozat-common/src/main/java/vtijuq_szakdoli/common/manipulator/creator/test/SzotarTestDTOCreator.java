package vtijuq_szakdoli.common.manipulator.creator.test;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.test.AbstractSzotarTestDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class SzotarTestDTOCreator extends AbstractSzotarTestDTOManipulator implements DTOCreator<SzotarTestDTO> {

	public SzotarTestDTOCreator(Consumer<SzotarTestDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new SzotarTestDTO();
	}
}
