package vtijuq_szakdoli.common.manipulator.creator.megvalositas;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractKonkretSzakiranyDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class KonkretSzakiranyDTOCreator extends AbstractKonkretSzakiranyDTOManipulator implements DTOCreator<KonkretSzakiranyDTO> {

	public KonkretSzakiranyDTOCreator(Consumer<KonkretSzakiranyDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new KonkretSzakiranyDTO();
	}
}
