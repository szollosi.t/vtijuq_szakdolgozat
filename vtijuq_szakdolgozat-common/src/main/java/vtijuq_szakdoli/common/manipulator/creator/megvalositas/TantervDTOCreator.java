package vtijuq_szakdoli.common.manipulator.creator.megvalositas;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractTantervDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class TantervDTOCreator extends AbstractTantervDTOManipulator implements DTOCreator<TantervDTO> {

	public TantervDTOCreator(Consumer<TantervDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new TantervDTO();
	}
}
