package vtijuq_szakdoli.common.manipulator.creator.megvalositas;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractKonkretSzakDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class KonkretSzakDTOCreator extends AbstractKonkretSzakDTOManipulator implements DTOCreator<KonkretSzakDTO> {

	public KonkretSzakDTOCreator(Consumer<KonkretSzakDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new KonkretSzakDTO();
	}
}
