package vtijuq_szakdoli.common.manipulator.creator.kovetelmeny;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractSzakKovetelmenyDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class SzakKovetelmenyDTOCreator extends AbstractSzakKovetelmenyDTOManipulator implements DTOCreator<SzakKovetelmenyDTO> {

	public SzakKovetelmenyDTOCreator(Consumer<SzakKovetelmenyDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new SzakKovetelmenyDTO();
	}
}
