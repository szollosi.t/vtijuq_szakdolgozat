package vtijuq_szakdoli.common.manipulator.creator.kovetelmeny;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractSzakmaiJellemzoDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class SzakmaiJellemzoDTOCreator extends AbstractSzakmaiJellemzoDTOManipulator implements DTOCreator<SzakmaiJellemzoDTO> {

	public SzakmaiJellemzoDTOCreator(Consumer<SzakmaiJellemzoDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new SzakmaiJellemzoDTO();
	}
}
