package vtijuq_szakdoli.common.manipulator.creator.akkreditacio;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.akkreditacio.AbstractAkkreditacioDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class AkkreditacioDTOCreator extends AbstractAkkreditacioDTOManipulator implements DTOCreator<AkkreditacioDTO> {

	public AkkreditacioDTOCreator(Consumer<AkkreditacioDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new AkkreditacioDTO();
	}
}
