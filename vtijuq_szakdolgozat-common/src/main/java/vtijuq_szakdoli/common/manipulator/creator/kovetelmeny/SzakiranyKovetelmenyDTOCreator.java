package vtijuq_szakdoli.common.manipulator.creator.kovetelmeny;

import java.util.function.Consumer;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractSzakiranyKovetelmenyDTOManipulator;
import vtijuq_szakdoli.common.manipulator.creator.DTOCreator;

public class SzakiranyKovetelmenyDTOCreator extends AbstractSzakiranyKovetelmenyDTOManipulator implements DTOCreator<SzakiranyKovetelmenyDTO> {

	public SzakiranyKovetelmenyDTOCreator(Consumer<SzakiranyKovetelmenyDTO> createdDTOHandler) {
		super(createdDTOHandler);
		dto = new SzakiranyKovetelmenyDTO();
	}
}
