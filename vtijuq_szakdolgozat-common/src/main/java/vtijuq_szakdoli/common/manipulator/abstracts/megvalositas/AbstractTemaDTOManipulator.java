package vtijuq_szakdoli.common.manipulator.abstracts.megvalositas;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractTemaDTOManipulator implements DTOManipulator<TemaDTO> {

	protected TemaDTO dto;
	@Getter
	protected Consumer<TemaDTO> DTOHandler;

	public AbstractTemaDTOManipulator(Consumer<TemaDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public TemaDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
