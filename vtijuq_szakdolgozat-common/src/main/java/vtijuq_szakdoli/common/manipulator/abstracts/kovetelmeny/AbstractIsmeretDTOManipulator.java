package vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractIsmeretDTOManipulator implements DTOManipulator<IsmeretDTO> {

	protected IsmeretDTO dto;
	@Getter
	protected Consumer<IsmeretDTO> DTOHandler;

	public AbstractIsmeretDTOManipulator(Consumer<IsmeretDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public IsmeretDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
