package vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractSzakDTOManipulator implements DTOManipulator<SzakDTO> {

	protected SzakDTO dto;
	@Getter
	protected Consumer<SzakDTO> DTOHandler;

	public AbstractSzakDTOManipulator(Consumer<SzakDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public SzakDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
