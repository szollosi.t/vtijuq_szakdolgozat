package vtijuq_szakdoli.common.manipulator.abstracts.akkreditacio;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractAkkreditacioDTOManipulator implements DTOManipulator<AkkreditacioDTO> {

	protected AkkreditacioDTO dto;
	@Getter
	protected Consumer<AkkreditacioDTO> DTOHandler;

	public AbstractAkkreditacioDTOManipulator(Consumer<AkkreditacioDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public AkkreditacioDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
