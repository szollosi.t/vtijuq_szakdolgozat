package vtijuq_szakdoli.common.manipulator.abstracts.megvalositas;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractTematikaDTOManipulator implements DTOManipulator<TematikaDTO> {

	protected TematikaDTO dto;
	@Getter
	protected Consumer<TematikaDTO> DTOHandler;

	public AbstractTematikaDTOManipulator(Consumer<TematikaDTO> DTOHandler) {
		this.DTOHandler = DTOHandler;
	}

	public TematikaDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
