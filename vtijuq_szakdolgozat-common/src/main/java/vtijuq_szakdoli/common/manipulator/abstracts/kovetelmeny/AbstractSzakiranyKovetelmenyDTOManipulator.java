package vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractSzakiranyKovetelmenyDTOManipulator implements DTOManipulator<SzakiranyKovetelmenyDTO> {

	protected SzakiranyKovetelmenyDTO dto;
	@Getter
	protected Consumer<SzakiranyKovetelmenyDTO> DTOHandler;

	public AbstractSzakiranyKovetelmenyDTOManipulator(Consumer<SzakiranyKovetelmenyDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public SzakiranyKovetelmenyDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
