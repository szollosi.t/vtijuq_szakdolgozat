package vtijuq_szakdoli.common.manipulator.abstracts.adminisztracio;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractSzervezetDTOManipulator implements DTOManipulator<SzervezetDTO> {

	protected SzervezetDTO dto;
	@Getter
	protected Consumer<SzervezetDTO> DTOHandler;

	public AbstractSzervezetDTOManipulator(Consumer<SzervezetDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public SzervezetDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
