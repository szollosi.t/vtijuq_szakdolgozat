package vtijuq_szakdoli.common.manipulator.abstracts.test;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractSzotarTestDTOManipulator implements DTOManipulator<SzotarTestDTO> {

	protected SzotarTestDTO dto;
	@Getter
	protected Consumer<SzotarTestDTO> DTOHandler;

	public AbstractSzotarTestDTOManipulator(Consumer<SzotarTestDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	@Override
	public SzotarTestDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
