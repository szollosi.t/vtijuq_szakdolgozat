package vtijuq_szakdoli.common.manipulator.abstracts.megvalositas;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractTantargyDTOManipulator implements DTOManipulator<TantargyDTO> {

	protected TantargyDTO dto;
	@Getter
	protected Consumer<TantargyDTO> DTOHandler;

	public AbstractTantargyDTOManipulator(Consumer<TantargyDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public TantargyDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
