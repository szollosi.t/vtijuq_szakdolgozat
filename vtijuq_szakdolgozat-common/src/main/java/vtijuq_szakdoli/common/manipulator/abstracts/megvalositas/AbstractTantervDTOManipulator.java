package vtijuq_szakdoli.common.manipulator.abstracts.megvalositas;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractTantervDTOManipulator implements DTOManipulator<TantervDTO> {

	protected TantervDTO dto;
	@Getter
	protected Consumer<TantervDTO> DTOHandler;

	public AbstractTantervDTOManipulator(Consumer<TantervDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public TantervDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
