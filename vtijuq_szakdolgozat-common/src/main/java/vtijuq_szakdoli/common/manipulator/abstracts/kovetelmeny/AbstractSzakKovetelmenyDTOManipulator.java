package vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractSzakKovetelmenyDTOManipulator implements DTOManipulator<SzakKovetelmenyDTO> {

	protected SzakKovetelmenyDTO dto;
	@Getter
	protected Consumer<SzakKovetelmenyDTO> DTOHandler;

	public AbstractSzakKovetelmenyDTOManipulator(Consumer<SzakKovetelmenyDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public SzakKovetelmenyDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
