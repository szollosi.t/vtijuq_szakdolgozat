package vtijuq_szakdoli.common.manipulator.abstracts.megvalositas;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractKonkretSzakiranyDTOManipulator implements DTOManipulator<KonkretSzakiranyDTO> {

	protected KonkretSzakiranyDTO dto;
	@Getter
	protected Consumer<KonkretSzakiranyDTO> DTOHandler;

	public AbstractKonkretSzakiranyDTOManipulator(Consumer<KonkretSzakiranyDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public KonkretSzakiranyDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
