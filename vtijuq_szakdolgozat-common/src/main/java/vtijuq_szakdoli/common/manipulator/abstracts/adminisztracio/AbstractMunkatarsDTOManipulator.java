package vtijuq_szakdoli.common.manipulator.abstracts.adminisztracio;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractMunkatarsDTOManipulator implements DTOManipulator<MunkatarsDTO> {

	protected MunkatarsDTO dto;
	@Getter
	protected Consumer<MunkatarsDTO> DTOHandler;

	public AbstractMunkatarsDTOManipulator(Consumer<MunkatarsDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public MunkatarsDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
