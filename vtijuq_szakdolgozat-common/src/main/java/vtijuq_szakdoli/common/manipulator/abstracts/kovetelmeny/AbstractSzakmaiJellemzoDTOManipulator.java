package vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractSzakmaiJellemzoDTOManipulator implements DTOManipulator<SzakmaiJellemzoDTO> {

	protected SzakmaiJellemzoDTO dto;
	@Getter
	protected Consumer<SzakmaiJellemzoDTO> DTOHandler;

	public AbstractSzakmaiJellemzoDTOManipulator(Consumer<SzakmaiJellemzoDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public SzakmaiJellemzoDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
