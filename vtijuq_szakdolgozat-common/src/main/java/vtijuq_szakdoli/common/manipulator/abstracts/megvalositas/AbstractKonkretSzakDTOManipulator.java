package vtijuq_szakdoli.common.manipulator.abstracts.megvalositas;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractKonkretSzakDTOManipulator implements DTOManipulator<KonkretSzakDTO> {

	protected KonkretSzakDTO dto;
	@Getter
	protected Consumer<KonkretSzakDTO> DTOHandler;

	public AbstractKonkretSzakDTOManipulator(Consumer<KonkretSzakDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public KonkretSzakDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
