package vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny;

import java.util.Set;
import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

public abstract class AbstractSzakiranyDTOManipulator implements DTOManipulator<SzakiranyDTO> {

	protected SzakiranyDTO dto;
	@Getter
	protected Consumer<SzakiranyDTO> DTOHandler;

	public AbstractSzakiranyDTOManipulator(Consumer<SzakiranyDTO> editedDTOHandler) {
		this.DTOHandler = editedDTOHandler;
	}

	public SzakiranyDTO getDTO() {
		return dto;
	}

	public Set<String> getErrors() {
		return dto.getErrors();
	}
}
