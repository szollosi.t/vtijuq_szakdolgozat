package vtijuq_szakdoli.common.manipulator.editor.kovetelmeny;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.IsmeretDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractIsmeretDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class IsmeretDTOEditor extends AbstractIsmeretDTOManipulator implements DTOEditor<IsmeretDTO> {

	@Getter
	private IsmeretDTO eredeti;

	public IsmeretDTOEditor(IsmeretDTO dto, Consumer<IsmeretDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new IsmeretDTO(dto);
	}

}
