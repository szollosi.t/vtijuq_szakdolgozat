package vtijuq_szakdoli.common.manipulator.editor.test;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.test.SzotarTestDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.test.AbstractSzotarTestDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class SzotarTestDTOEditor extends AbstractSzotarTestDTOManipulator implements DTOEditor<SzotarTestDTO> {

	@Getter
	public SzotarTestDTO eredeti;

	public SzotarTestDTOEditor(SzotarTestDTO dto, Consumer<SzotarTestDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new SzotarTestDTO(dto);
	}
}
