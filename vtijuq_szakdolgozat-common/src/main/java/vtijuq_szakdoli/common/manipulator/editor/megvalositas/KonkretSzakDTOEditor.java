package vtijuq_szakdoli.common.manipulator.editor.megvalositas;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractKonkretSzakDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class KonkretSzakDTOEditor extends AbstractKonkretSzakDTOManipulator implements DTOEditor<KonkretSzakDTO> {

	@Getter
	private KonkretSzakDTO eredeti;

	public KonkretSzakDTOEditor(KonkretSzakDTO eredeti, Consumer<KonkretSzakDTO> editedDTOHandler){
		super(editedDTOHandler);
		this.eredeti = eredeti;
		this.dto = new KonkretSzakDTO(eredeti);
	}

	public KonkretSzakDTOEditor(KonkretSzakDTO eredeti, KonkretSzakDTO dto, Consumer<KonkretSzakDTO> editedDTOHandler){
		super(editedDTOHandler);
		this.eredeti = eredeti;
		this.dto = dto;
	}

}
