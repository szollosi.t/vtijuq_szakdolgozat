package vtijuq_szakdoli.common.manipulator.editor.kovetelmeny;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyKovetelmenyDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractSzakiranyKovetelmenyDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class SzakiranyKovetelmenyDTOEditor extends AbstractSzakiranyKovetelmenyDTOManipulator implements DTOEditor<SzakiranyKovetelmenyDTO> {

	@Getter
	private SzakiranyKovetelmenyDTO eredeti;

	public SzakiranyKovetelmenyDTOEditor(SzakiranyKovetelmenyDTO dto, Consumer<SzakiranyKovetelmenyDTO> editedDTOHandler){
		this(dto, new SzakiranyKovetelmenyDTO(dto), editedDTOHandler);
	}

	public SzakiranyKovetelmenyDTOEditor(SzakiranyKovetelmenyDTO eredeti, SzakiranyKovetelmenyDTO dto, Consumer<SzakiranyKovetelmenyDTO> editedDTOHandler){
		super(editedDTOHandler);
		this.eredeti = eredeti;
		this.dto = dto;
	}

}
