package vtijuq_szakdoli.common.manipulator.editor.megvalositas;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantervDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractTantervDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class TantervDTOEditor extends AbstractTantervDTOManipulator implements DTOEditor<TantervDTO> {

	@Getter
	private TantervDTO eredeti;

	public TantervDTOEditor(TantervDTO dto, Consumer<TantervDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new TantervDTO(dto);
	}

}
