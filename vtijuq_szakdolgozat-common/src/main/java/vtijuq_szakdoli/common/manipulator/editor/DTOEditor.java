package vtijuq_szakdoli.common.manipulator.editor;

import vtijuq_szakdoli.common.dto.editableDTO.EditableDTO;
import vtijuq_szakdoli.common.manipulator.DTOManipulator;

/**
 * Generic DTO editor class.
 * @param <T> the type of entity, this editor can edit. By cconvention, subclasses should implement it also.
 */
public interface DTOEditor<T extends EditableDTO> extends DTOManipulator<T> {

	default void edit() {
		if (getDTOHandler() != null) {
			getDTOHandler().accept(getDTO());
		}
	}

	T getEredeti();
}
