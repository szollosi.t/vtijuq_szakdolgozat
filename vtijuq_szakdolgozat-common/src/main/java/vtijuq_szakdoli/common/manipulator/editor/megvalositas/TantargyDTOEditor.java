package vtijuq_szakdoli.common.manipulator.editor.megvalositas;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TantargyDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractTantargyDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class TantargyDTOEditor extends AbstractTantargyDTOManipulator implements DTOEditor<TantargyDTO> {

	@Getter
	private TantargyDTO eredeti;

	public TantargyDTOEditor(TantargyDTO dto, Consumer<TantargyDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new TantargyDTO(dto);
	}

	protected TantargyDTOEditor(TantargyDTO eredeti, TantargyDTO dto, Consumer<TantargyDTO> editedDTOHandler){
		super(editedDTOHandler);
		this.eredeti = eredeti;
		this.dto = dto;
	}

}
