package vtijuq_szakdoli.common.manipulator.editor.megvalositas;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.KonkretSzakiranyDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractKonkretSzakiranyDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class KonkretSzakiranyDTOEditor extends AbstractKonkretSzakiranyDTOManipulator implements DTOEditor<KonkretSzakiranyDTO> {

	@Getter
	private KonkretSzakiranyDTO eredeti;

	public KonkretSzakiranyDTOEditor(KonkretSzakiranyDTO eredeti, Consumer<KonkretSzakiranyDTO> editedDTOHandler){
		super(editedDTOHandler);
		this.eredeti = eredeti;
		this.dto = new KonkretSzakiranyDTO(eredeti);
	}

	protected KonkretSzakiranyDTOEditor(KonkretSzakiranyDTO eredeti, KonkretSzakiranyDTO dto, Consumer<KonkretSzakiranyDTO> editedDTOHandler){
		super(editedDTOHandler);
		this.eredeti = eredeti;
		this.dto = dto;
	}

}
