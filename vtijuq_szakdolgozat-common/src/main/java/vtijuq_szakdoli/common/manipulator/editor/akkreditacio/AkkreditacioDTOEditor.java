package vtijuq_szakdoli.common.manipulator.editor.akkreditacio;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.akkreditacio.AkkreditacioDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.akkreditacio.AbstractAkkreditacioDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class AkkreditacioDTOEditor extends AbstractAkkreditacioDTOManipulator implements DTOEditor<AkkreditacioDTO> {

	@Getter
	private AkkreditacioDTO eredeti;

	public AkkreditacioDTOEditor(AkkreditacioDTO dto, Consumer<AkkreditacioDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new AkkreditacioDTO(dto);
	}

}
