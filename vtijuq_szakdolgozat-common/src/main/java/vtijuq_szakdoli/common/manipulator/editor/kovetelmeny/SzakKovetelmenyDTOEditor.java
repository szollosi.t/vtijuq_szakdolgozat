package vtijuq_szakdoli.common.manipulator.editor.kovetelmeny;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakKovetelmenyDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractSzakKovetelmenyDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class SzakKovetelmenyDTOEditor extends AbstractSzakKovetelmenyDTOManipulator implements DTOEditor<SzakKovetelmenyDTO> {

	@Getter
	private SzakKovetelmenyDTO eredeti;

	public SzakKovetelmenyDTOEditor(SzakKovetelmenyDTO dto, Consumer<SzakKovetelmenyDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new SzakKovetelmenyDTO(dto);
	}

	public SzakKovetelmenyDTOEditor(SzakKovetelmenyDTO eredeti, SzakKovetelmenyDTO dto, Consumer<SzakKovetelmenyDTO> editedDTOHandler){
		super(editedDTOHandler);
		this.eredeti = eredeti;
		this.dto = dto;
	}

}
