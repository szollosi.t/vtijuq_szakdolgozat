package vtijuq_szakdoli.common.manipulator.editor.kovetelmeny;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractSzakDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class SzakDTOEditor extends AbstractSzakDTOManipulator implements DTOEditor<SzakDTO> {

	@Getter
	private SzakDTO eredeti;

	public SzakDTOEditor(SzakDTO dto, Consumer<SzakDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new SzakDTO(dto);
	}

}
