package vtijuq_szakdoli.common.manipulator.editor.kovetelmeny;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakiranyDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractSzakiranyDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class SzakiranyDTOEditor extends AbstractSzakiranyDTOManipulator implements DTOEditor<SzakiranyDTO> {

	@Getter
	private SzakiranyDTO eredeti;

	public SzakiranyDTOEditor(SzakiranyDTO dto, Consumer<SzakiranyDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new SzakiranyDTO(dto);
	}

}
