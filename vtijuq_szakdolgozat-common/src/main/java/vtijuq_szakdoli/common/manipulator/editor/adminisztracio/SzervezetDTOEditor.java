package vtijuq_szakdoli.common.manipulator.editor.adminisztracio;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.SzervezetDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.adminisztracio.AbstractSzervezetDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class SzervezetDTOEditor extends AbstractSzervezetDTOManipulator implements DTOEditor<SzervezetDTO> {

	@Getter
	private SzervezetDTO eredeti;

	public SzervezetDTOEditor(SzervezetDTO dto, Consumer<SzervezetDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new SzervezetDTO(dto);
	}

}
