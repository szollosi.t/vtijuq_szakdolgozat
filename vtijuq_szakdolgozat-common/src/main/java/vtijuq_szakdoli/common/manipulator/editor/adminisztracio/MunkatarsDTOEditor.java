package vtijuq_szakdoli.common.manipulator.editor.adminisztracio;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.adminisztracio.MunkatarsDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.adminisztracio.AbstractMunkatarsDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class MunkatarsDTOEditor extends AbstractMunkatarsDTOManipulator implements DTOEditor<MunkatarsDTO> {

	@Getter
	private MunkatarsDTO eredeti;

	public MunkatarsDTOEditor(MunkatarsDTO dto, Consumer<MunkatarsDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new MunkatarsDTO(dto);
	}

}
