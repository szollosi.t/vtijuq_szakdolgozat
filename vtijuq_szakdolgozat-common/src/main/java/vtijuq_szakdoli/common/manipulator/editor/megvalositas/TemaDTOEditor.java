package vtijuq_szakdoli.common.manipulator.editor.megvalositas;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TemaDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractTemaDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class TemaDTOEditor extends AbstractTemaDTOManipulator implements DTOEditor<TemaDTO> {

	@Getter
	private TemaDTO eredeti;

	public TemaDTOEditor(TemaDTO dto, Consumer<TemaDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new TemaDTO(dto);
	}

}
