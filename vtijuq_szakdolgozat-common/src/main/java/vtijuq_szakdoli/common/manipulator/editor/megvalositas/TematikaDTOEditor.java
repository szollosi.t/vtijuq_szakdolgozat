package vtijuq_szakdoli.common.manipulator.editor.megvalositas;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.megvalositas.TematikaDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.megvalositas.AbstractTematikaDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class TematikaDTOEditor extends AbstractTematikaDTOManipulator implements DTOEditor<TematikaDTO> {

	@Getter
	private TematikaDTO eredeti;

	public TematikaDTOEditor(TematikaDTO dto, Consumer<TematikaDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new TematikaDTO(dto);
	}
}
