package vtijuq_szakdoli.common.manipulator.editor.kovetelmeny;

import java.util.function.Consumer;

import lombok.Getter;

import vtijuq_szakdoli.common.dto.editableDTO.kovetelmeny.SzakmaiJellemzoDTO;
import vtijuq_szakdoli.common.manipulator.abstracts.kovetelmeny.AbstractSzakmaiJellemzoDTOManipulator;
import vtijuq_szakdoli.common.manipulator.editor.DTOEditor;

public class SzakmaiJellemzoDTOEditor extends AbstractSzakmaiJellemzoDTOManipulator implements DTOEditor<SzakmaiJellemzoDTO> {

	@Getter
	private SzakmaiJellemzoDTO eredeti;

	public SzakmaiJellemzoDTOEditor(SzakmaiJellemzoDTO dto, Consumer<SzakmaiJellemzoDTO> editedDTOHandler){
		super(editedDTOHandler);
		eredeti = dto;
		this.dto = new SzakmaiJellemzoDTO(dto);
	}

}
